. $HOME/.solusconfig

##### BEFORE ###############
####  Pick and Place CDM_ENGAGEMEMT.sql , CDM_Stg_ENGAGEMENT.sql from Tables in Cart_Solus_User
####  Pick and Place CDM_ETL_ENGAGEMEMT.sql


mysql --host=$HOST --user=${USER_NAME} --password=${PASSWORD}  --database=$DB_NAME   --verbose << EOF
 TRUNCATE CDM_Points_Earned;
 CALL CDM_ETL_Point_Earned_Generator_Run();
EOF
