. $HOME/.solusconfig
##
## CREATE CSV TABLE
##
SOURCEFILE=sample.csv
CSV_TABLE=csv_sample
dos2unix  ${SOURCEFILE} 
sed 's/\\N//Ig' ${SOURCEFILE} | sed 's/"//g' | sed 's/|/"|"/Ig' | sed 's/^/"/Ig' | sed 's/$/"/Ig'  | sed 's/\\r\\n/\\n/g' > ${SOURCEFILE}.processing 
export SOURCEFILE=${SOURCEFILE}.processing 

echo "Creating Table ${CSV_TABLE} from file :  $SOURCEFILE"
echo "No of Records                         :  "`wc -l $SOURCEFILE`

rm -f list_of_columns
sqlcreate=""
head -1 ${SOURCEFILE} | sed 's/"//g' | cut -f1- -d\| --output-delimiter=$'\n' > list_of_columns
cp list_of_columns list_of_columns_all
sqlcreate=`head -1 list_of_columns`
sqlcreate="$sqlcreate varchar(512) "
sed -i '1d' list_of_columns

for columns in `cat list_of_columns`
do
sqlcreate="$sqlcreate"" , $columns varchar(512) "
done
#sqlcreate="create table $CSV_TABLE ( Id bigint(20) NOT NULL AUTO_INCREMENT KEY,   Created_Date timestamp NOT NULL DEFAULT current_timestamp(),""$sqlcreate""); "
sqlcreate="create table $CSV_TABLE ( ""$sqlcreate""); "

mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
drop table if exists ${CSV_TABLE};
${sqlcreate};
-- alter table ${CSV_TABLE} add column created_date timestamp DEFAULT current_timestamp();
EOF


##
## LOADING CSV TABLE
##
#echo "Loading data to Table ${CSV_TABLE} from file  ${SOURCEFILE} "
wc -l ${SOURCEFILE};
mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
LOAD DATA LOCAL  INFILE "${SOURCEFILE}" INTO TABLE ${CSV_TABLE}
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n'
IGNORE 1 lines``;
select count(1) from ${CSV_TABLE} ;
EOF
