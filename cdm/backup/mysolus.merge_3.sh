. $HOME/.solusconfig
CDM_ENTITY=$1
MAPPING_FILE=$2
UPDATE_COLUMN=$3
CDM_CORE=CDM_${CDM_ENTITY}
CSV_TABLE=csv_${CDM_ENTITY}
SOURCE_DELIM="|"
MAPPING_DELIM="="
DEBUG=1

echo $1 $2 

if [[ "$1" == "--help" || "$1" == "-h" || -z "$1" || -z "$2" ]]
then
echo "-- Gettig Started ----"
echo "Usage: $0 <CDM_ENTITY> <MAPPING_FILE> <MODE>"
echo "Valid Values CDM_ENTITY : Product_Master, Customer_Master, Store_Master, Bill_Details, Inventory_Master "
echo "Mapping_File must follow the mapping template provided at $HOME/client/config/mapping " 
echo "Data File location should be provided in Mapping File. Please check Mapping Templates" 
echo "Data File DELIMITER is |. You must preprocess the delimiter accordingly"  
echo "---------------------"
exit 1
fi
#####################################################################################################################
########### Recieve Mode
#####################################################################################################################



echo "" 
echo "MAPPING FILE     :"$MAPPING_FILE
if [ ! -f ${MAPPING_FILE} ] 
then
exit "ERROR-mysolus-sdl001 : Mapping File ${MAPPING_FILE} doesnt exists "
fi




export SOURCEFILE=`grep ^SOURCEFILE ${MAPPING_FILE} | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
echo "SOURCE FILE        :"$SOURCEFILE
if [ ! -f ${SOURCEFILE} ] 
then
echo "ERROR-mysolus-sdl002 : Source File ${SOURCEFILE} doesnt exist as mentioned in mapping file :${MAPPING_FILE}"
exit 1
fi

###################
# Mapping Validation
###################

for i in `cat ${MAPPING_FILE} | grep -v ^#  | grep -v ^SOURCEFILE `  
do 
echo $i
sdl_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $1}' | sed 's/ *$//g' | sed 's/^ *//g'`
csv_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
echo $sdl_colname
echo $csv_colname


if [[ ${i:0:1} == "#" ]]
then
 echo $sdl_colname
 echo $csv_colname
fi

done

if [[ $CDM_ENTITY == 'Customer_Master'  ]]
then  
 Primary_Key='Customer_Key'
 Primary_Id='Customer_Id'
 Update_Id=$UPDATE_COLUMN
 Lookup_Table='CDM_Customer_Key_Lookup'
elif [[ $CDM_ENTITY == 'Store_Master'  ]]
then 
 Primary_Key='Store_Key'
 Primary_Id='Store_Id'
 Update_Id=$UPDATE_COLUMN
 Lookup_Table='CDM_Store_Key_Lookup'
elif [[ $CDM_ENTITY == 'Product_Master'  ]]
then
 Primary_Key='Product_Key'
 Primary_Id='Product_Id'
 Update_Id=$UPDATE_COLUMN
 Lookup_Table='CDM_Product_Key_Lookup'
elif [[ $CDM_ENTITY == 'Bill_Header'  ]]
then
 Primary_Key='Bill_Header_Key'
 Primary_Id='Bill_Header_Id'
 Update_Id=$UPDATE_COLUMN
 Lookup_Table='CDM_Bill_Header_Key_Lookup'
elif [[ $CDM_ENTITY == 'Bill_Details'  ]]
then
 Primary_Key='Bill_Details_Key'
 Primary_Id='Bill_Details_Id'
 Update_Id=$UPDATE_COLUMN
 Lookup_Table='CDM_Bill_Details_Key_Lookup'
else 
 echo 'Incorrect CDM Table'
 exit 1
fi

echo $Primary_Key
echo $Update_Id


Update_Key=$Update_Id
echo 'Update_Key' $Update_Key
 if [[ $Update_Id == *'_LOV_Id' ]]
 then 
  Update_Key=${Update_Key//'_LOV_Id'/''}
  echo 'Update_Key' $Update_Key
  sqlcreate="create table $CSV_TABLE ( $Primary_Key varchar(512) default null , $Update_Key varchar(512) default null ,$Primary_Id bigint(20) default null  ,  $Update_Id bigint(20) default null )  Engine=InnoDb Default charset=Latin1  "
  echo $sqlcreate
  mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
  set sql_safe_updates=0;
  drop table if exists ${CSV_TABLE};
  ${sqlcreate};
  -- alter table ${CSV_TABLE} add column created_date timestamp DEFAULT current_timestamp();
  set sql_safe_updates=0;
  LOAD DATA LOCAL  INFILE "${SOURCEFILE}" INTO TABLE 
  ${CSV_TABLE} 
  FIELDS TERMINATED BY '|' ENCLOSED BY '"' LINES TERMINATED BY '\n'
  IGNORE 1 lines``
  ( $Primary_Key, 
  $Update_Key );
  alter table ${CSV_TABLE}  add index  ( ${Primary_Key}) ;
  alter table ${CSV_TABLE}  add index ( ${Primary_Id} );
  alter table ${CSV_TABLE}  add index  ( ${Update_Key}) ;
  alter table ${CSV_TABLE}  add index ( ${Update_Id} ); 
EOF
## Insert into LOV_MAster 
## Add Index
## Update LOV_Id
## Update Primary_Id from Respective Lookup Table
  mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        '${Update_Key^^}', 
        ${Update_Key}, 
        ${Update_Key}, 
        ${Update_Key} 
      FROM  ${CSV_TABLE}  
      WHERE  
        ${Update_Key} <> ''  
      GROUP BY ${Update_Key} ; 

    UPDATE IGNORE  ${CSV_TABLE} AS A 
    SET ${Update_Key}_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.${Update_Key} = B.LOV_Key 
        AND B.LOV_Attribute = '${Update_Key^^}'  
    ); 

        INSERT IGNORE INTO ${Lookup_Table} ( 
      ${Primary_Key}
    )     
      SELECT  
        ${Primary_Key} 
      FROM ${CSV_TABLE} 
      WHERE  
        ${Primary_Key}  <> '';

    UPDATE IGNORE  ${CSV_TABLE} 
    SET  ${Primary_Id}  = ( 
      SELECT   ${Primary_Id}
      FROM  ${Lookup_Table} 
      WHERE ${CSV_TABLE}.${Primary_Key} = ${Lookup_Table}.${Primary_Key} 
    ); 

    UPDATE IGNORE  ${CDM_CORE}
    SET  ${Update_Id}  = (
      SELECT   ${Update_Id}
      FROM  ${CSV_TABLE} 
      WHERE ${CSV_TABLE}.${Primary_Id} = ${CDM_CORE}.${Primary_Id}
    );
EOF
fi

  if [[ $Update_Id != *'_LOV_Id' &&  $Update_Id != *'_Id'  ]]
  then
   sqlcreate="create table $CSV_TABLE ( $Primary_Key varchar(512) default null , $Update_Key varchar(512) default null ,$Primary_Id bigint(    20) default null  )  Engine=InnoDb Default charset=Latin1  "

mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
  set sql_safe_updates=0;
  drop table if exists ${CSV_TABLE};
  ${sqlcreate};
  -- alter table ${CSV_TABLE} add column created_date timestamp DEFAULT current_timestamp();

  set sql_safe_updates=0;
  LOAD DATA LOCAL  INFILE "${SOURCEFILE}" INTO TABLE
  ${CSV_TABLE}
  FIELDS TERMINATED BY '|' ENCLOSED BY '"' LINES TERMINATED BY '\n'
  IGNORE 1 lines``
  ( $Primary_Key,
  $Update_Key )
  ;
  alter table ${CSV_TABLE}  add index  ( ${Primary_Key}) ;
  alter table ${CSV_TABLE}  add index ( ${Primary_Id} );
  alter table ${CSV_TABLE}  add index  ( ${Update_Key}) ;


        INSERT IGNORE INTO ${Lookup_Table} (
      ${Primary_Key}
    )
      SELECT
        ${Primary_Key}
      FROM ${CSV_TABLE}
      WHERE
        ${Primary_Key}  <> '';

    UPDATE IGNORE  ${CSV_TABLE}
    SET  ${Primary_Id}  = (
      SELECT   ${Primary_Id}
      FROM  ${Lookup_Table}
      WHERE ${CSV_TABLE}.${Primary_Key} = ${Lookup_Table}.${Primary_Key}
    );

    UPDATE IGNORE  ${CDM_CORE}
    SET  ${Update_Key}  = (
      SELECT   ${Update_Key}
      FROM  ${CSV_TABLE}
      WHERE ${CSV_TABLE}.${Primary_Id} = ${CDM_CORE}.${Primary_Id}
    );


EOF
  

fi


#Case 1
#primary key= primary key from Customer_Master|Store_Master|Bill_Details|Bill_Header|Product_Master
#update key= primary key from Customer_Master|Store_Master|Bill_Details|Bill_Header|Product_Master

  if [[ $Update_Id != *'_LOV_Id' ]]   &&   [[ "$Update_Id" =~ *(Customer_Id|Store_Id|Bill_Details_Id|Bill_Header_Id|Product_Id)* ]]
  then
  echo "Case 1"
  sqlcreate="create table $CSV_TABLE ( $Primary_Key varchar(512) default null , $Update_Key varchar(512) default null ,$Primary_Id bigint(20) default null  ,  $Update_Id bigint(20) default null )  Engine=InnoDb Default charset=Latin1  " 
  if [[ $Update_Id == *'Customer_Id'  ]]
   then
     Secondary_Lookup_Table='CDM_Customer_Key_Lookup'
  elif [[ $Update_Id == *'Store_Id'  ]]
   then
     Secondary_Lookup_Table='CDM_Store_Key_Lookup'
  elif [[ $Update_Id == *'Product_Id'  ]]
   then
     Secondary_Lookup_Table='CDM_Product_Key_Lookup'
  elif [[ $Update_Id == *'Bill_Details_Id'  ]]
   then
     Secondary_Lookup_Table='CDM_Bill_Details_Key_Lookup'
  elif [[ $Update_Id == *'Bill_Header_Id'  ]]
   then
     Secondary_Lookup_Table='CDM_Bill_Header_Key_Lookup'
  else 
     echo 'Error:Wrong Update Column'
     exit 1
   fi


mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
  set sql_safe_updates=0;
  drop table if exists ${CSV_TABLE};
  ${sqlcreate};
  -- alter table ${CSV_TABLE} add column created_date timestamp DEFAULT current_timestamp();

  set sql_safe_updates=0;
  LOAD DATA LOCAL  INFILE "${SOURCEFILE}" INTO TABLE
  ${CSV_TABLE}
  FIELDS TERMINATED BY '|' ENCLOSED BY '"' LINES TERMINATED BY '\n'
  IGNORE 1 lines``
  ( $Primary_Key,
  $Update_Key )
  ;
  alter table ${CSV_TABLE}  add index  ( ${Primary_Key}) ;
  alter table ${CSV_TABLE}  add index ( ${Primary_Id} );
  alter table ${CSV_TABLE}  add index  ( ${Update_Key}) ;
  alter table ${CSV_TABLE}  add index ( ${Update_Id} );

        INSERT IGNORE INTO ${Lookup_Table} (
      ${Primary_Key}
    )
      SELECT
        ${Primary_Key}
      FROM ${CSV_TABLE}
      WHERE
        ${Primary_Key}  <> '';

    UPDATE IGNORE  ${CSV_TABLE}
    SET  ${Primary_Id}  = (
      SELECT   ${Primary_Id}
      FROM  ${Lookup_Table}
      WHERE ${CSV_TABLE}.${Primary_Key} = ${Lookup_Table}.${Primary_Key}
    );

    UPDATE IGNORE  ${CDM_CORE}
    SET  ${Update_Key}  = (
      SELECT   ${Update_Key}
      FROM  ${CSV_TABLE}
      WHERE ${CSV_TABLE}.${Primary_Id} = ${CDM_CORE}.${Primary_Id}
    );

        INSERT IGNORE INTO ${Secondary_Lookup_Table} (
      ${Update_Key}
    )
      SELECT
        ${Update_Key}
      FROM ${CSV_TABLE}
      WHERE
        ${Update_Key}  <> '';

    UPDATE IGNORE  ${CSV_TABLE}
    SET  ${Update_Id}  = (
      SELECT   ${Update_Id}
      FROM  ${Secondary_Lookup_Table}
      WHERE ${CSV_TABLE}.${Update_Key} = ${Secondary_Lookup_Table}.${Update_Key}
    );

    UPDATE IGNORE  ${CDM_CORE}
    SET  ${Update_Id}  = (
      SELECT   ${Update_Id}
      FROM  ${CSV_TABLE}
      WHERE ${CSV_TABLE}.${Update_Id} = ${CDM_CORE}.${Update_Id}
    );
EOF
fi



#Case 1
#primary key= primary key from Customer_Master|Store_Master|Bill_Details|Bill_Header|Product_Master
#update key= primary key from Customer_Master|Store_Master|Bill_Details|Bill_Header|Product_Master
#Case 2
#primary key= primary key from Customer_Master|Store_Master|Bill_Details|Bill_Header|Product_Master
#update key= KEY has LOV_Id
#Case 3
#primary key= primary key from Customer_Master|Store_Master|Bill_Details|Bill_Header|Product_Master
#update key= KEY has no LOV Id







