. $HOME/.solusconfig
CDM_ENTITY=$1
MAPPING_FILE=$2
CDM_TABLE=CDM_Stg_${CDM_ENTITY}
CDM_CORE=CDM_${CDM_ENTITY}
CSV_TABLE=csv_${CDM_ENTITY}
SOURCE_DELIM="|"
MAPPING_DELIM="="
DEBUG=1

line=`sed "3q;d" ${MAPPING_FILE} `  ####
echo ${line}   #### 
sed -i "7s/.*/${line}/" $HOME/mysolus/cdm/mysolus.cdm.sh  ###
x=`sed "3q;d" ${MAPPING_FILE} | awk  -F${MAPPING_DELIM} '{print $2}'`
echo "x="$x
echo $0 $1 $2 

if [[ "$1" == "--help" || "$1" == "-h" || -z "$1" || -z "$2" ]]
then
echo "-- Gettig Started ----"
echo "Usage: $0 <CDM_ENTITY> <MAPPING_FILE> <MODE>"
echo "Valid Values CDM_ENTITY : Product_Master, Customer_Master, Store_Master, Bill_Details, Inventory_Master "
echo "Mapping_File must follow the mapping template provided at $HOME/client/config/mapping " 
echo "Data File location and delimiter used should be provided in Mapping File. Please check Mapping Templates" 
echo "If Data File dosent have a delimiter then '|' is taken as a default delimiter ."  
echo "---------------------"
exit 1
fi


if [[ "$3" == "historical"  ]]
then
MODE=0
elif [[  "$3" == "incremental" ]]
then
MODE=1
else 
echo "Invalid Data Load Mode "
exit 1 
fi
echo "" 
echo "MAPPING FILE     :"$MAPPING_FILE
if [[ ! -f ${MAPPING_FILE} ]] 
then
exit "ERROR-mysolus-sdl001 : Mapping File ${MAPPING_FILE} doesnt exists "
fi
dos2unix $MAPPING_FILE


export SOURCEFILE=`grep ^SOURCEFILE ${MAPPING_FILE} | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
echo "SOURCE FILE        :"$SOURCEFILE
if [[ ! -f ${SOURCEFILE} ]] 
then
echo "ERROR-mysolus-sdl002 : Source File ${SOURCEFILE} doesnt exist as mentioned in mapping file :${MAPPING_FILE}"
exit 1
fi
dos2unix $SOURCEFILE

#echo "CDM Table         :"$CDM_ENTITY
#echo ""
#status=`mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -s -e "select 1 from ${CDM_TABLE} limit 1" `
#if [ $status -ne 1 ]
#then
#exit "ERROR-mysolus-sdl003 : Source Table ${CDM_TABLE} doesnt exists "
#fi
###################
# Mapping Validation
###################
echo "Mapping Validation starts ...."
for i in `cat ${MAPPING_FILE} | grep -v ^#  | grep -v ^SOURCEFILE | grep -v ^SOURCE_DELIM `  
do 
echo $i
sdl_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $1}' | sed 's/ *$//g' | sed 's/^ *//g'`
csv_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
if [[ ${i:0:1} == "#" ]]
then
 continue
fi
if [[ ${i:0:1} == "*" ]]
then 
  if [[ -z "${csv_colname}" ]]
  then 
     echo "ERROR-mysolus-sdl008 : Mapping is Incorrect. Mandatory attribute $sdl_colname is not mapped to any attribute"
  fi
  sdl_colname=`echo $sdl_colname | awk -F"*" '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
fi

if [[ ! -z "${sdl_colname}" ]]
then 
  sqlstmt="select 1 from Information_Schema.columns where table_schema='$DB_NAME' and table_name='${CDM_TABLE}' and column_name='${sdl_colname}' limit 1;"
  status=`mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -s -e "${sqlstmt}" `
  if [[ $status -ne 1 ]]
  then
     exit "ERROR-mysolus-sdl0011 : Mapping is Incorrect. Column Name ${sdl_colname} doesnt exist in CDM TABLE ${CDM_TABLE} "
  fi
fi
done
echo "MAPPING VALIDATION ....OK !!!!"


##
## CREATE CSV TABLE
##
echo "x="${x/\"/}
y=${x/\"/}
y=${y/\"/}

dos2unix  ${SOURCEFILE}
sed 's/\\N//Ig' ${SOURCEFILE} | sed 's/"//g' | sed 's/|/"|"/Ig'  | sed 's/${y}/"${y}"/Ig'  | sed 's/^/"/Ig' | sed 's/$/"/Ig'|sed 's/"//g' | sed 's/\\r\\n/\\n/g' > ${SOURCEFILE}.processing 
export SOURCEFILE=${SOURCEFILE}.processing 

echo "Creating Table ${CSV_TABLE} from file :  $SOURCEFILE"
echo "No of Records                         :  "`wc -l $SOURCEFILE`


rm -f list_of_columns
#echo "x="${x/\"/}
#y=${x/\"/}
#y=${y/\"/}
sqlcreate=""
head -1 ${SOURCEFILE} | sed 's/"//g' | cut -f1- -d ${y} --output-delimiter=$'\n' > list_of_columns
cp list_of_columns list_of_columns_all
sqlcreate=`head -1 list_of_columns`
sqlcreate="$sqlcreate varchar(1024) "
sed -i '1d' list_of_columns


for columns in `cat list_of_columns`
do
sqlcreate="$sqlcreate"" , $columns varchar(1024) "
done

head -1 ${SOURCEFILE} | sed 's/"//g' | cut -f1- -d ${y} --output-delimiter=$'\n' 
echo ${sqlcreate}
#sqlcreate="create table $CSV_TABLE ( Id bigint(20) NOT NULL AUTO_INCREMENT KEY,   Created_Date timestamp NOT NULL DEFAULT current_timestamp(),""$sqlcreate""); "
sqlcreate="create table $CSV_TABLE ( ""$sqlcreate""); "

mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
drop table if exists ${CSV_TABLE};
${sqlcreate};
-- alter table ${CSV_TABLE} add column created_date timestamp DEFAULT current_timestamp();
EOF

##
## LOADING CSV TABLE
##
#echo "Loading data to Table ${CSV_TABLE} from file  ${SOURCEFILE} "
wc -l ${SOURCEFILE};
mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
LOAD DATA LOCAL  INFILE "${SOURCEFILE}" INTO TABLE ${CSV_TABLE}
FIELDS TERMINATED BY '${y}'  OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n'
IGNORE 1 lines``;
select count(1) from ${CSV_TABLE} ;
EOF

##
## Generating SQL to insert data into CDM Staging Table
##
insertstmt="insert into ${CDM_TABLE} ( " ;
selectstmt=" select  " ;

ifirst=0

for i in `cat ${MAPPING_FILE} | grep -v "^#"  | grep -v ^SOURCEFILE | grep -v SOURCE_DELIM`; 
do 
sdl_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $1}' | sed 's/ *$//g' | sed 's/^ *//g'`
csv_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
if [[ ${i:0:1} == "#" ]]
then
 continue
fi

if [[ ${sdl_colname:0:1} == "*" ]]
then 
  sdl_colname=`echo $sdl_colname | awk -F"*" '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
fi

if [ ! -z "${csv_colname}" ] 
then
if [ $ifirst == 0 ]
then
insertstmt="$insertstmt""${sdl_colname}"
selectstmt="$selectstmt""${csv_colname}"
ifirst=1
else
insertstmt="$insertstmt"",${sdl_colname}"
selectstmt="$selectstmt"",${csv_colname}"
fi
fi
done
sqlstatement="${insertstmt} ) "" ${selectstmt} "" FROM ${CSV_TABLE}"

##
## LOADING TO CDM STAGING
##
mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
truncate ${CDM_TABLE};
-- call solus_loop(${sqlstatement},${CSV_TABLE}),100000;
${sqlstatement};
EOF


##
## ADDING INDEX TO ALL COLUMNS
##
##echo "Adding Index to ${CSV_TABLE}"
##for columns in `cat list_of_columns_all`
##do
##sqlindex="alter table $CSV_TABLE add index $columns ($columns)";
##mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -e "$sqlindex" 
##done


###
### ADD INDEX TO CSV TABLE
###
#for i in `cat ${MAPPING_FILE} | grep -v "^#"  | grep -v ^SOURCEFILE`; 
#do 
#   sdl_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $1}' | sed 's/ *$//g' | sed 's/^ *//g'`
#   csv_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
#   if [[ ${i:0:1} == "#" ]]
#   then
#      continue
#   fi
#   if [ ! -z "${csv_colname}" ] 
#   then
#      if [[ ${sdl_colname:0:1} == "*" ]]
#      then 
#        sdl_colname=`echo $sdl_colname | awk -F"*" '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
#      fi
#      sqlstmt="select 1 from Information_Schema.columns where table_schema='$DB_NAME' and table_name='${CDM_TABLE}' and column_name='${csv_colname}' and Column_Key <> '' ;"
#      status=`mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -s -e "${sqlstmt}" `
#      if [[ $status -eq "1" ]]
#      then
#         sqlindex="alter table $CSV_TABLE add index ${csv_colname} (${csv_colname})";
#         echo $sqlindex
#         mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -e "$sqlindex" 
#      fi
#   fi
#done

##
##  DATA QUALITY CHECK
##
mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
CALL CDM_DQC_${CDM_ENTITY} (@msg); 
select @msg;
EOF
status=`mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -s -e "select 1 from CDM_DQC_ERRORS where entity='${CDM_ENTITY}' and error_msg <> '' limit 1" ;`
if [[ $status -eq "1" ]]
then
exit "ERROR-mysolus-sdl010 : Data Quality Check FAILED. Please check the details at CDM_DQC_ERRORS!! "
fi


###
### LOAD CDM 

if [[ "${CDM_ENTITY}" == "Customer_Master" || "${CDM_ENTITY}" == "Bill_Details" || "${CDM_ENTITY}" == "Feedback"  || "${CDM_ENTITY}" == "Inventory_Master" || "${CDM_ENTITY}" == "Bill_Header" ]] 
then
   ETL_PROC="CDM_ETL_"${CDM_ENTITY}"(100000,$MODE)"
elif [[ "${CDM_ENTITY}" == "Product_Master" ]] 
then
   ETL_PROC="CDM_ETL_"${CDM_ENTITY}"(1)"
elif [[ "${CDM_ENTITY}" == "ENGAGEMENT" ]]
then
   ETL_PROC="CDM_ETL_ENGAGEMENT(100000,$MODE)"
elif [[ "${CDM_ENTITY}" == "Points_Earned" || "${CDM_ENTITY}" == "Points_Expiry" || "${CDM_ENTITY}" == "Points_Redemption" || "${CDM_ENTITY}" == "Offers" ||  "${CDM_ENTITY}" == "Voucher_Master" ||  "${CDM_ENTITY}" == "Voucher_Redemption"  ]]
then
   ETL_PROC="CDM_ETL_"${CDM_ENTITY}"(100000)"
elif [[ "${CDM_ENTITY}" == "SMS_Del_Rep" || "${CDM_ENTITY}" == "PN_Rep"  || "${CDM_ENTITY}" == "Email_Rep" ]]
then
   ETL_PROC="S_CLM_Event_Attribution(100000)"
else
   ETL_PROC="CDM_ETL_"${CDM_ENTITY}"()"
fi


if [[ "${CDM_ENTITY}" == "Product_Master" ]]
then
	CDM_CORE="CDM_Product_Master_Original"
elif [[ "${CDM_ENTITY}" == "SMS_Del_Rep" ]]
then
	CDM_CORE="CDM_Stg_SMS_Del_Rep"
elif [[ "${CDM_ENTITY}" == "PN_Rep" ]]
then
        CDM_CORE="CDM_Stg_PN_Rep"
elif [[ "${CDM_ENTITY}" == "Email_Rep" ]]
then
        CDM_CORE="CDM_Stg_Email_Rep"
elif [[ "${CDM_ENTITY}" == "Voucher_Master" ]]
then
        CDM_CORE="CDM_Vouchers"
fi

if [[ "${CDM_ENTITY}" == "Bill_Details_Derived_Attrib" ]]
then 
ETL_PROC="S_Bill_Detail_Derived_Attrib(100000,$MODE)"
fi

mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
call ${ETL_PROC};
select count(1) from ${CDM_CORE};
EOF

rm -f list_of_columns_all 
rm -f list_of_columns

#### Restorng the DELIMITER 
sed -i "7s/.*/SOURCE_DELIM=\"|\"/" $HOME/mysolus/cdm/mysolus.cdm.sh
 
