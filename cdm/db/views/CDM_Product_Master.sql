DELIMITER mysolus$$

CREATE  OR REPLACE  VIEW  CDM_Product_Master  AS 
select
a.*,b.*
from CART_SOLUS_PROD.CDM_Product_Master_Original a 
LEFT JOIN CART_SOLUS_PROD.CDM_Product_Key_Lookup c
ON c.Product_Id=a.Product_Id
LEFT JOIN CART_SOLUS_CUSTOM.Curated_Product_Master b
ON b.Product_Key=c.Product_Key


mysolus$$
