DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_ETL_Product_Pricefeed (IN vBatchSize BIGINT,IN vTreat_As_Incr TINYINT)
BEGIN 
/* Version 1.0 20221202 */

	  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
	  DECLARE vEnd_Cnt BIGINT; 
     
	  SET vEnd_Cnt = vBatchSize; 
      
    /* Getting the Min Rec_Id*/
    
	  SET @Rec_Cnt = 0; 
	  SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Product_Pricefeed ORDER BY Rec_Id ASC LIMIT 1; 
	  SET vStart_Cnt = @Rec_Cnt; 
	  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
   
   /* Getting the Max Rec_Id */
   
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Product_Pricefeed ORDER BY Rec_Id DESC LIMIT 1; 
    
    IF @Rec_Cnt IS NULL THEN  
		SET @Rec_Cnt = 0; 
    END IF; 
    
    
   
    
  /* Creating Temp Table For Each Batch */  
  
	CREATE OR REPLACE TABLE Temp_CDM_Product_Pricefeed(
    Rec_Id bigint(20) NOT NULL AUTO_INCREMENT,
    Product_Pricefeed_Key varchar(128) NOT NULL DEFAULT '-1',
    Product_Pricefeed_Id bigint(20) NOT NULL DEFAULT -1,
    Data_Source varchar(20) NOT NULL,
	Product_Key varchar(128) NOT NULL,
	Product_Id bigint(20) DEFAULT -1,
	Record_Date varchar(128) DEFAULT '0000-00-00',
	Open decimal(15,2) DEFAULT -1.0,
	Close decimal(15,2) DEFAULT -1.0,
	High decimal(15,2) DEFAULT -1.0,
	Low decimal(15,2) DEFAULT -1.0,
	Volume decimal(15,2) DEFAULT -1.0,
	No_Of_Trades bigint(20) DEFAULT -1,
	Net_Turnover decimal(15,2) DEFAULT -1.0,
	D_Change decimal(15,2) DEFAULT -1.0,
	Custom_Tag1 varchar(128) DEFAULT NULL,
	Custom_Tag2 varchar(128) DEFAULT NULL,
	Custom_Tag3 varchar(128) DEFAULT NULL,
	PRIMARY KEY (Rec_Id),
    KEY Data_Source (Data_Source),
	KEY Product_Key (Product_Key),
	KEY Record_Date (Record_Date),
	KEY Product_Id (Product_Id));
   
  CALL CDM_Update_Process_Log('CDM_ETL_Product_Pricefeed', 1, 'CDM_ETL_Product_Pricefeed', 1); 
     
  PROCESS_PP_HIST: LOOP 
     
    TRUNCATE TABLE Temp_CDM_Product_Pricefeed; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_Product_Pricefeed', CONCAT(vStart_Cnt,'_0'), 'CDM_ETL_Product_Pricefeed', 1); 
    
    /* Insert Into Temp Table */
    
	   INSERT INTO  Temp_CDM_Product_Pricefeed 
	  ( Rec_Id ,
	  Product_Pricefeed_Key ,
     Data_Source ,
     Product_Key ,
     Product_Id ,
     Record_Date ,
     Open ,
     Close ,
     High ,
     Low ,
     Volume ,
     No_Of_Trades ,
     Net_Turnover ,
     D_Change ,
     Custom_Tag1 ,
     Custom_Tag2 ,
     Custom_Tag3 
		) 
		  
	SELECT 
     Rec_Id ,
	CONCAT(Data_Source,'_',Product_Key,'_',DATE_FORMAT(Record_Date,'%Y%m%d')),
     Data_Source ,
     Product_Key ,
     Product_Id ,
    DATE_FORMAT(Record_Date,'%Y-%m-%d'),
     Open ,
     Close ,
     High ,
     Low ,
     Volume ,
     No_Of_Trades ,
     Net_Turnover ,
     D_Change ,
     Custom_Tag1 ,
     Custom_Tag2 ,
     Custom_Tag3 
		
	FROM  CDM_Stg_Product_Pricefeed 
	WHERE 
		Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt
    AND Product_Key is not null
    AND Record_Date is not null; 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Product_Pricefeed', CONCAT(vStart_Cnt,'_1'), 'CDM_ETL_Product_Pricefeed', 1); 
 
     
         
    CALL CDM_Update_Process_Log('CDM_ETL_Product_Pricefeed', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Product_Pricefeed', 1); 
    
    /* Insert Into Product Pricefeed Key Lookup Table */
     
    INSERT IGNORE INTO CDM_Product_Pricefeed_Key_Lookup ( 
      Product_Pricefeed_Key 
    ) 
      SELECT  
        Product_Pricefeed_Key 
      FROM  
        Temp_CDM_Product_Pricefeed TCP
      WHERE 
        Product_Pricefeed_Key <> '';
        
            
    
    /* Insert Into Product Key Lookup Table */
    
    INSERT IGNORE INTO CDM_Product_Key_Lookup(Product_Key)
    
    SELECT Product_Key 
	FROM Temp_CDM_Product_Pricefeed
    WHERE Product_Key <> '';
        
	
    CALL CDM_Update_Process_Log('CDM_ETL_Product_Pricefeed', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Product_Pricefeed', 1); 
 
 
    /* Update the Product Pricefeed Id in Temp Table */ 
    UPDATE IGNORE Temp_CDM_Product_Pricefeed TGT, CDM_Product_Pricefeed_Key_Lookup SRC
    SET TGT.Product_Pricefeed_Id = SRC.Product_Pricefeed_Id
    WHERE TGT.Product_Pricefeed_Key = SRC.Product_Pricefeed_Key;

    
    /* Update the Product Id in Temp Table */
    UPDATE IGNORE Temp_CDM_Product_Pricefeed TGT, CDM_Product_Key_Lookup SRC
    SET TGT.Product_Id = SRC.Product_Id
    WHERE TGT.Product_Key = SRC.Product_Key
    AND TGT.Product_Key <> '';

    CALL CDM_Update_Process_Log('CDM_ETL_Product_Pricefeed', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Product_Pricefeed', 1); 

     IF vTreat_As_Incr = 1 -- Incremental Mode 
    THEN 
       
       UPDATE CDM_Product_Pricefeed TGT, Temp_CDM_Product_Pricefeed SRC
       SET TGT.Open = SRC.OPen,
           TGT.Close = SRC.Close,
           TGT.High = SRC.High,
           TGT.Low = SRC.Low,
           TGT.Volume = SRC.Volume,
           TGT.No_Of_Trades = SRC.No_of_Trades,
           TGT.Net_Turnover = SRC.Net_Turnover,
           TGT.D_Change = SRC.D_Change,
           TGT.Custom_Tag1 = SRC.Custom_Tag1,
           TGT.Custom_Tag2 = SRC.Custom_Tag2,
           TGT.Custom_Tag3 = SRC.Custom_Tag3
       WHERE TGT.Product_Pricefeed_Id = SRC.Product_Pricefeed_Id;  
    
     END IF; 
    
   
         
    CALL CDM_Update_Process_Log('CDM_ETL_Product_Pricefeed', CONCAT(vStart_Cnt,'_5'), 'CDM_ETL_Product_Pricefeed', 1); 
 
 /* Insert into CDM_Product_Pricefeed */
 
    INSERT IGNORE INTO  CDM_Product_Pricefeed 
 ( Product_Pricefeed_Id ,
     Data_Source ,
     Product_Id ,
     Record_Date ,
     Open ,
     Close ,
     High ,
     Low ,
     Volume ,
     No_Of_Trades ,
     Net_Turnover ,
     D_Change ,
     Custom_Tag1 ,
     Custom_Tag2 ,
     Custom_Tag3 
    ) 
      SELECT
      
         Product_Pricefeed_Id ,
		 Data_Source ,
		 Product_Id ,
		DATE( Record_Date ),
		 Open ,
		 Close ,
		 High ,
		 Low ,
		 Volume ,
		 No_Of_Trades ,
		 Net_Turnover ,
		 D_Change ,
		 Custom_Tag1 ,
		 Custom_Tag2 ,
		 Custom_Tag3 
      
      FROM Temp_CDM_Product_Pricefeed; 
 

        SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_PP_HIST; 
        END IF; 
         
  END LOOP PROCESS_PP_HIST; 
 
  DROP TABLE Temp_CDM_Product_Pricefeed; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Product_Pricefeed', 6, 'CDM_ETL_Product_Pricefeed', 1); 
END$$
DELIMITER ;
