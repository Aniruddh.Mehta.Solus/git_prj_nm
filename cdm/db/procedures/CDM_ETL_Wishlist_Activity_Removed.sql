DELIMITER $$
CREATE or Replace PROCEDURE  CDM_ETL_Wishlist_Activity_Removed (IN vBatchSize BIGINT,IN vTreat_As_Incr TINYINT)
BEGIN 
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
     
  SET vEnd_Cnt = vBatchSize; 
 
  SET @Rec_Cnt = 0; 
  SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Wishlist_Activity_Removed ORDER BY Rec_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
   
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Wishlist_Activity_Removed ORDER BY Rec_Id DESC LIMIT 1; 
    IF @Rec_Cnt IS NULL THEN  
    SET @Rec_Cnt = 0; 
  END IF; 
  DROP TABLE IF EXISTS Temp_CDM_Wishlist_Removed_Details; 
  CREATE TABLE IF NOT EXISTS Temp_CDM_Wishlist_Removed_Details ( 
    Rec_Id  bigint(20) NOT NULL AUTO_INCREMENT,
   Rec_Is_Processed  tinyint(4) NOT NULL DEFAULT -1,
   Wishlist_Activity_Key  varchar(128) NOT NULL,
   Wishlist_Activity_Id  bigint(20) NOT NULL DEFAULT -1,
   Line_No  smallint(6) NOT NULL DEFAULT -1,
   Store_Key  varchar(128) NOT NULL,
   Store_Id  bigint(20) NOT NULL DEFAULT -1,
   Store_LOV_Id  bigint(20) NOT NULL DEFAULT -1,
   Product_Key  varchar(128) NOT NULL,
   Broker_Key  varchar(128) DEFAULT NULL,
   Broker_Id  bigint(20) NOT NULL DEFAULT -1,
   Product_Id  bigint(20) NOT NULL DEFAULT -1,
   Product_LOV_Id  bigint(20) NOT NULL DEFAULT -1,
   Customer_Key  varchar(128) NOT NULL DEFAULT '',
   Customer_Id  bigint(20) NOT NULL DEFAULT -1,
   Activity_Date  date DEFAULT NULL,
   Activity_Time  time DEFAULT NULL,
   Cust_Txn_Number  smallint(6) NOT NULL DEFAULT -1,
   Qty  decimal(15,2) NOT NULL DEFAULT 0.00,
   Gross_Val  decimal(15,2) NOT NULL DEFAULT 0.00,
   Disc_Val  decimal(15,2) NOT NULL DEFAULT 0.00,
   Tax_Val  decimal(15,2) NOT NULL DEFAULT 0.00,
   Net_Val  decimal(15,2) NOT NULL DEFAULT 0.00,
   Mrp  decimal(15,2) NOT NULL DEFAULT -1.00,
   Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),
   Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
   Trans_Type  varchar(128) DEFAULT NULL,
   Trans_Sub_Type  varchar(128) DEFAULT NULL,
   Source_Program  varchar(128) DEFAULT NULL,
  PRIMARY KEY ( Rec_Id ),
  KEY  Broker_Key  ( Broker_Key ),
  KEY  Broker_Id  ( Broker_Id ),
  KEY  Wishlist_Activity_Key  ( Wishlist_Activity_Key ),
  KEY  Wishlist_Activity_Id  ( Wishlist_Activity_Id ),
  KEY  Store_Key  ( Store_Key ),
  KEY  Customer_Key  ( Customer_Key ),
  KEY  Product_Key  ( Product_Key ),
  KEY  Rec_Is_Processed  ( Rec_Is_Processed ),
  KEY  Activity_Date  ( Activity_Date ),
  KEY  Modified_Date  ( Modified_Date )
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1; 
 
  ALTER TABLE Temp_CDM_Wishlist_Removed_Details
    ADD INDEX (Wishlist_Activity_Key),     
    ADD INDEX (Wishlist_Activity_Id),     
    ADD INDEX (Store_Id), 
    ADD INDEX (Store_Key), 
    ADD INDEX (Customer_Key), 
    ADD INDEX (Customer_Id), 
    ADD INDEX (Product_Key), 
    ADD INDEX (Product_Id), 
    ADD INDEX (Broker_Key), 
    ADD INDEX (Broker_Id), 
    ADD INDEX (Rec_Is_Processed), 
    ADD INDEX (Activity_Date); 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', 1, 'CDM_ETL_Wishlist_Activity_Removed', 1); 
     
  PROCESS_BD_HIST: LOOP 
     
    TRUNCATE TABLE Temp_CDM_Wishlist_Removed_Details; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', CONCAT(vStart_Cnt,'_0'), 'CDM_ETL_Wishlist_Activity_Removed', 1); 
   INSERT INTO  Temp_CDM_Wishlist_Removed_Details 
( Rec_Id ,
 Rec_Is_Processed ,
 Wishlist_Activity_Key ,
 Wishlist_Activity_Id ,
 Line_No ,
 Store_Key ,
 Store_Id ,
 Store_LOV_Id ,
 Product_Key ,
 Broker_Key ,
 Broker_Id ,
 Product_Id ,
 Product_LOV_Id ,
 Customer_Key ,
 Customer_Id ,
 Activity_Date ,
 Activity_Time ,
 Cust_Txn_Number ,
 Qty ,
 Gross_Val ,
 Disc_Val ,
 Tax_Val ,
 Net_Val ,
 Mrp ,
 Created_Date ,
 Modified_Date ,
 Trans_Type ,
 Trans_Sub_Type ,
 Source_Program 
    ) 
      
select 
 Rec_Id ,
 Rec_Is_Processed ,
 Wishlist_Activity_Key ,
 Wishlist_Activity_Id ,
 Line_No ,
 Store_Key ,
 Store_Id ,
 Store_LOV_Id ,
 Product_Key ,
 Broker_Key ,
 Broker_Id ,
 Product_Id ,
 Product_LOV_Id ,
 Customer_Key ,
 Customer_Id ,
 Activity_Date ,
 Activity_Time ,
 Cust_Txn_Number ,
 Qty ,
 Gross_Val ,
 Disc_Val ,
 Tax_Val ,
 Net_Val ,
 Mrp ,
 Created_Date ,
 Modified_Date ,
 Trans_Type ,
 Trans_Sub_Type ,
 Source_Program 
FROM  CDM_Stg_Wishlist_Activity_Removed 
      WHERE 
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', CONCAT(vStart_Cnt,'_1'), 'CDM_ETL_Wishlist_Activity_Removed', 1); 
 
     
         
    CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Wishlist_Activity_Removed', 1); 
     
    INSERT IGNORE INTO CDM_Wishlist_Activity_Key_Lookup ( 
      Wishlist_Activity_Key 
    ) 
      SELECT  
        Wishlist_Activity_Key 
      FROM  
        Temp_CDM_Wishlist_Removed_Details TCBD
      WHERE 
        Wishlist_Activity_Key <> '';
     
    CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Wishlist_Activity_Removed', 1); 
 
    UPDATE IGNORE Temp_CDM_Wishlist_Removed_Details 
        SET Wishlist_Activity_Id = ( 
      SELECT Wishlist_Activity_Id  
            FROM CDM_Wishlist_Activity_Key_Lookup 
            WHERE 
        Temp_CDM_Wishlist_Removed_Details.Wishlist_Activity_Key = CDM_Wishlist_Activity_Key_Lookup.Wishlist_Activity_Key 
    ) 
    WHERE  
            Wishlist_Activity_Id <= 0; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Wishlist_Activity_Removed', 1); 
     
         UPDATE IGNORE Temp_CDM_Wishlist_Removed_Details 
    SET Store_Id = ( 
        SELECT Store_Id 
        FROM CDM_Store_Key_Lookup 
        WHERE CDM_Store_Key_Lookup.Store_Key = Temp_CDM_Wishlist_Removed_Details.Store_Key 
      ) 
    WHERE 
            Store_Key  <> ''; 
       
    CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', CONCAT(vStart_Cnt,'_5'), 'CDM_ETL_Wishlist_Activity_Removed', 1); 
 
    UPDATE IGNORE Temp_CDM_Wishlist_Removed_Details 
    SET Broker_Id = ( 
        SELECT Broker_Id 
        FROM CDM_Broker_Key_Lookup 
        WHERE CDM_Broker_Key_Lookup.Broker_Key = Temp_CDM_Wishlist_Removed_Details.Broker_Key 
      ) 
    WHERE 
            Broker_Key  <> ''; 
     
     
    CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', CONCAT(vStart_Cnt,'_6'), 'CDM_ETL_Wishlist_Activity_Removed', 1); 
 
    UPDATE IGNORE Temp_CDM_Wishlist_Removed_Details 
    SET Customer_Id = ( 
        SELECT Customer_Id 
        FROM CDM_Customer_Key_Lookup 
        WHERE  
          CDM_Customer_Key_Lookup.Customer_Key = Temp_CDM_Wishlist_Removed_Details.Customer_Key 
      ) 
    WHERE  
            Customer_Key <> ''; 
 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_Wishlist_Activity_Removed', 1); 
 
    UPDATE IGNORE Temp_CDM_Wishlist_Removed_Details 
    SET  
      Product_Id = ( 
        SELECT Product_Id 
        FROM CDM_Product_Key_Lookup 
        WHERE  
          Temp_CDM_Wishlist_Removed_Details.Product_Key = CDM_Product_Key_Lookup.Product_Key 
      ) 
    WHERE  
            Product_Key  <> ''; 
     
    IF vTreat_As_Incr = 1 THEN 
       
       
      UPDATE CDM_Wishlist_Activity_Removed AS TGT, Temp_CDM_Wishlist_Removed_Details AS SRC, CDM_Product_Master_Original AS SRC2 
      SET 
        TGT.Customer_Id = IFNULL(SRC.Customer_Id,-1), 
        TGT.Line_No = SRC.Line_No, 
        TGT.Store_Id = IFNULL(SRC.Store_Id,-1), 
        TGT.Product_Id = IFNULL(SRC.Product_Id,-1), 
        TGT.Broker_Id = IFNULL(SRC.Broker_Id,-1), 
        TGT.Qty = SRC.Qty, 
        TGT.Gross_Val= SRC.Gross_Val, 
        TGT.Disc_Val = SRC.Disc_Val, 
        TGT.Tax_Val= SRC.Tax_Val, 
        TGT.Net_Val = SRC.Net_Val, 
        TGT.Activity_Date = SRC.Activity_Date, 
        TGT.Activity_Time = SRC.Activity_Time, 
                TGT.Trans_Type = SRC.Trans_Type, 
                TGT.Trans_Sub_Type = SRC.Trans_Sub_Type, 
                TGT.Source_Program = SRC.Source_Program
      WHERE  
        SRC.Wishlist_Activity_Id = TGT.Wishlist_Activity_Id AND 
        SRC.Product_Id = SRC2.Product_ID AND 
        SRC.Wishlist_Activity_Key <> '' AND 
        SRC.Customer_Id > 0 ;
     
 
    END IF; 
   
         
    CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', CONCAT(vStart_Cnt,'_29'), 'CDM_ETL_Wishlist_Activity_Removed', 1); 
 
    INSERT IGNORE INTO  CDM_Wishlist_Activity_Removed 
( Wishlist_Activity_Id ,
 Customer_Id ,
 Line_No ,
 Store_Id ,
 Product_Id ,
 Broker_Id ,
 Qty ,
 Gross_Val ,
 Disc_Val ,
 Tax_Val ,
 Net_Val ,
 Activity_Date ,
 Activity_Time ,
 Mrp ,
 Trans_Type ,
 Trans_Sub_Type ,
 Source_Program 
    ) 
      SELECT  
        Wishlist_Activity_Id, 
        IFNULL(Customer_Id,-1), 
        Line_No, 
        IFNULL(Store_Id,-1), 
        IFNULL(Product_Id,-1), 
        IFNULL(Broker_Id,-1), 
        Qty, 
        Gross_Val, 
        Disc_Val, 
        Tax_Val, 
        Net_Val, 
        Activity_Date, 
        Activity_Time, 
        Mrp, 
        Trans_Type, 
        Trans_Sub_Type, 
        Source_Program
      FROM Temp_CDM_Wishlist_Removed_Details; 
 

      SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_BD_HIST; 
        END IF; 
         
  END LOOP PROCESS_BD_HIST; 
 
  DROP TABLE Temp_CDM_Wishlist_Removed_Details; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Wishlist_Activity_Removed', 9, 'CDM_ETL_Wishlist_Activity_Removed', 1); 
END$$
DELIMITER ;

