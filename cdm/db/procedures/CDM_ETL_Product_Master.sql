DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_ETL_Product_Master (IN vTreat_As_Incr TINYINT)
BEGIN
	
	SET SQL_SAFE_UPDATES=0;
	IF vTreat_As_Incr = 1 THEN
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 1, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Brand_Key_Lookup (
			Brand_Key
		)
			SELECT 
				Brand_Key
			FROM CDM_Stg_Product_Master
			WHERE Brand_Key <> '' AND Rec_Is_Processed = -1
			GROUP BY Brand_Key;
	 
	  
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 2, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_BU_Key_Lookup (
			BU_Key
		)
			SELECT 
				BU_Key
			FROM CDM_Stg_Product_Master
			WHERE BU_Key <> '' AND Rec_Is_Processed = -1
			GROUP BY BU_Key;
	  
	  
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 3, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Cat_Key_Lookup (
			Cat_Key
		)
			SELECT 
				Cat_Key
			FROM CDM_Stg_Product_Master
			WHERE Cat_Key <> '' AND Rec_Is_Processed = -1
			GROUP BY Cat_Key;
	  
	  
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 4, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Dep_Key_Lookup (
			Dep_Key
		)
			SELECT 
				Dep_Key
			FROM CDM_Stg_Product_Master
			WHERE Dep_Key <> '' AND Rec_Is_Processed = -1
			GROUP BY Dep_Key;
	 
	  
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 5, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Div_Key_Lookup (
			Div_Key
		)
			SELECT 
				Div_Key
			FROM CDM_Stg_Product_Master
			WHERE Div_Key <> '' AND Rec_Is_Processed = -1
			GROUP BY Div_Key;
	 
	  
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 6, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Season_Key_Lookup (
			Season_Key
		)
			SELECT 
				Season_Key
			FROM CDM_Stg_Product_Master
			WHERE Season_Key <> '' AND Rec_Is_Processed = -1
			GROUP BY Season_Key;
	  
	  
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 7, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Section_Key_Lookup (
			Section_Key
		)
			SELECT 
				Section_Key
			FROM CDM_Stg_Product_Master
			WHERE Section_Key <> '' AND Rec_Is_Processed = -1
			GROUP BY Section_Key;
	  
	  
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 8, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Manuf_Key_Lookup (
			Manuf_Key
		)
			SELECT 
				Manuf_Key
			FROM CDM_Stg_Product_Master
			WHERE Manuf_Key <> '' AND Rec_Is_Processed = -1
			GROUP BY Manuf_Key;

	END IF;
       
	   
	IF vTreat_As_Incr = 1 THEN
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 17, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Brand_Master (
			Brand_Id,
			Brand_Name,
			LOV_Id
		)
			SELECT 
				Brand_Id,
				Brand_Name,
				Brand_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Brand_Id,Brand_Name,Brand_LOV_Id;

		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 18, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_BU_Master (
			BU_Id,
			BU_Name,
			LOV_Id
		)
			SELECT 
				BU_Id,
				BU_Name,
				BU_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY BU_Id,BU_Name,BU_LOV_Id;
    
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 19, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Cat_Master (
			Cat_Id,
			Cat_Name,
			LOV_Id
		)
			SELECT 
				Cat_Id,
				Cat_Name,
				Cat_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Cat_Id, Cat_Name,Cat_LOV_Id;

		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 20, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Div_Master (
			Div_Id,
			Div_Name,
			LOV_Id
		)
			SELECT 
				Div_Id,
				Div_Name,
				Div_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Div_Id,Div_Name,Div_LOV_Id;

		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 21, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Dep_Master (
			Dep_Id,
			Dep_Name,
			LOV_Id
		)
			SELECT 
				Dep_Id,
				Dep_Name,
				Dep_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Dep_Id, Dep_Name,Dep_LOV_Id;
        
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 22, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Season_Master (
			Season_Id,
			Season_Name,
			LOV_Id
		)
			SELECT 
				Season_Id,
				Season_Name,
				Season_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Season_Id, Season_Name, Season_LOV_Id;

		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 23, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Section_Master (
			Section_Id,
			Section_Name,
			LOV_Id
		)
			SELECT 
				Section_Id,
				Section_Name,
				Section_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Section_Id, Section_Name, Section_LOV_Id;
        
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 24, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Manuf_Master (
			Manuf_Id,
			Manuf_Name,
			LOV_Id
		)
			SELECT 
				Manuf_Id,
				Manuf_Name,
				Manuf_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Manuf_Id, Manuf_Name, Manuf_LOV_Id;
	END IF;
	   
	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 9, 'CDM_ETL_Product_Master', 1);
	
	UPDATE IGNORE  CDM_Stg_Product_Master AS A
    SET Brand_Id = (
		SELECT Brand_Id
        FROM CDM_Brand_Key_Lookup AS B
        WHERE A.Brand_Key = B.Brand_Key
    )
    WHERE Brand_Id <= 0;

	UPDATE IGNORE  CDM_Brand_Key_Lookup AS A
		SET A.LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Brand_Key = B.LOV_Key
				AND B.LOV_Attribute = 'BRAND' 
		)
	WHERE LOV_Id <= 0;
	
	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 10, 'CDM_ETL_Product_Master', 1);

	UPDATE IGNORE CDM_Stg_Product_Master AS A
    SET BU_Id = (
		SELECT BU_Id
        FROM CDM_BU_Key_Lookup AS B
        WHERE A.BU_Key = B.BU_Key
    )
    WHERE BU_Id <= 0;

	UPDATE IGNORE  CDM_BU_Key_Lookup AS A
		SET A.LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.BU_Key = B.LOV_Key
				AND B.LOV_Attribute = 'BU' 
		)
	WHERE LOV_Id <= 0;
	
	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 11, 'CDM_ETL_Product_Master', 1);

	UPDATE IGNORE  CDM_Stg_Product_Master AS A
    SET Cat_Id = (
		SELECT Cat_Id
        FROM CDM_Cat_Key_Lookup AS B
        WHERE A.Cat_Key = B.Cat_Key
    )
    WHERE Cat_Id <= 0;

	UPDATE IGNORE  CDM_Cat_Key_Lookup AS A
		SET A.LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Cat_Key = B.LOV_Key
				AND B.LOV_Attribute = 'CAT' 
		)
	WHERE LOV_Id <= 0;
	
	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 12, 'CDM_ETL_Product_Master', 1);
	
	UPDATE IGNORE  CDM_Stg_Product_Master AS A
    SET Dep_Id = (
		SELECT Dep_Id
        FROM CDM_Dep_Key_Lookup AS B
        WHERE A.Dep_Key = B.Dep_Key
    )
    WHERE Dep_Id <= 0;

	UPDATE IGNORE  CDM_Dep_Key_Lookup AS A
		SET A.LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Dep_Key = B.LOV_Key
				AND B.LOV_Attribute = 'DEP' 
		)
	WHERE LOV_Id <= 0;

	
	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 13, 'CDM_ETL_Product_Master', 1);

	UPDATE IGNORE  CDM_Stg_Product_Master AS A
    SET Div_Id = (
		SELECT Div_Id
        FROM CDM_Div_Key_Lookup AS B
        WHERE A.Div_Key = B.Div_Key
    )
	WHERE Div_Id <= 0;

	UPDATE IGNORE  CDM_Div_Key_Lookup AS A
		SET A.LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Div_Key = B.LOV_Key
				AND B.LOV_Attribute = 'DIV' 
		)
	WHERE LOV_Id <= 0;
	
	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 14, 'CDM_ETL_Product_Master', 1);

	UPDATE IGNORE  CDM_Stg_Product_Master AS A
    SET Season_Id = (
		SELECT Season_Id
        FROM CDM_Season_Key_Lookup AS B
        WHERE A.Season_Key = B.Season_Key
		)
	WHERE Season_Id <= 0;

	UPDATE IGNORE  CDM_Season_Key_Lookup AS A
		SET A.LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Season_Key = B.LOV_Key
				AND B.LOV_Attribute = 'SEASON' 
		)
	WHERE LOV_Id <= 0;
	
	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 15, 'CDM_ETL_Product_Master', 1);

	UPDATE IGNORE  CDM_Stg_Product_Master AS A
    SET Section_Id = (
		SELECT Section_Id
        FROM CDM_Section_Key_Lookup AS B
        WHERE A.Section_Key = B.Section_Key
		)
	WHERE Section_Id <= 0;

	UPDATE IGNORE  CDM_Section_Key_Lookup AS A
		SET A.LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Section_Key = B.LOV_Key
				AND B.LOV_Attribute = 'SECTION' 
		)
	WHERE LOV_Id <= 0;
	
	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 16, 'CDM_ETL_Product_Master', 1);

	UPDATE IGNORE  CDM_Stg_Product_Master AS A
    SET Manuf_Id = (
		SELECT Manuf_Id
        FROM CDM_Manuf_Key_Lookup AS B
        WHERE A.Manuf_Key = B.Manuf_Key
		)
	WHERE Manuf_Id <= 0;
	
	UPDATE IGNORE  CDM_Manuf_Key_Lookup AS A
		SET A.LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Manuf_Key = B.LOV_Key
				AND B.LOV_Attribute = 'MANUF' 
		)
	WHERE LOV_Id <= 0;

	
	IF vTreat_As_Incr = 1 THEN
  	   
	   
		INSERT IGNORE INTO CDM_LOV_Master
		(
			LOV_Attribute,
			LOV_Key,
			LOV_Value,
			LOV_Comm_Value
		)
			SELECT 
				'BRAND',
				Brand_Key,
				Brand_Key,
				Brand_Name
			FROM CDM_Stg_Product_Master 
			WHERE Brand_Key <> '' AND Rec_Is_Processed = -1;

 	 	   
   
		INSERT IGNORE INTO CDM_LOV_Master
		(
			LOV_Attribute,
			LOV_Key,
			LOV_Value,
			LOV_Comm_Value
		)
			SELECT 
				'BU',
				BU_Key,
				BU_Key,
				BU_Name
			FROM CDM_Stg_Product_Master 
			WHERE BU_Key <> '' AND Rec_Is_Processed = -1;

 	   
 	   
		INSERT IGNORE INTO CDM_LOV_Master
		(
			LOV_Attribute,
			LOV_Key,
			LOV_Value,
			LOV_Comm_Value
		)
			SELECT 
				'CAT',
				Cat_Key,
				Cat_Key,
				Cat_Name
			FROM CDM_Stg_Product_Master 
			WHERE Cat_Key <> '' AND Rec_Is_Processed = -1;

  	   
	   
		INSERT IGNORE INTO CDM_LOV_Master
		(
			LOV_Attribute,
			LOV_Key,
			LOV_Value,
			LOV_Comm_Value
		)
			SELECT 
				'DEP',
				Dep_Key,
				Dep_Key,
				Dep_Name
			FROM CDM_Stg_Product_Master 
			WHERE Dep_Key <> '' AND Rec_Is_Processed = -1;

  	   
	   
		INSERT IGNORE INTO CDM_LOV_Master
		(
			LOV_Attribute,
			LOV_Key,
			LOV_Value,
			LOV_Comm_Value
		)
			SELECT 
				'DIV',
				Div_Key,
				Div_Key,
				Div_Name
			FROM CDM_Stg_Product_Master 
			WHERE Div_Key <> '' AND Rec_Is_Processed = -1;

  	   
	   
		INSERT IGNORE INTO CDM_LOV_Master
		(
			LOV_Attribute,
			LOV_Key,
			LOV_Value,
			LOV_Comm_Value
		)
			SELECT 
				'SEASON',
				Season_Key,
				Season_Key,
				Season_Name
			FROM CDM_Stg_Product_Master 
			WHERE Season_Key <> '' AND Rec_Is_Processed = -1;

   
		INSERT IGNORE INTO CDM_LOV_Master
		(
			LOV_Attribute,
			LOV_Key,
			LOV_Value,
			LOV_Comm_Value
		)
			SELECT 
				'SECTION',
				Section_Key,
				Section_Key,
				Section_Name
			FROM CDM_Stg_Product_Master 
			WHERE Section_Key <> '' AND Rec_Is_Processed = -1;

	 	   
   
		INSERT IGNORE INTO CDM_LOV_Master
		(
			LOV_Attribute,
			LOV_Key,
			LOV_Value,
			LOV_Comm_Value
		)
			SELECT 
				'MANUF',
				Manuf_Key,
				Manuf_Key,
				Manuf_Name
			FROM CDM_Stg_Product_Master 
			WHERE Manuf_Key <> '' AND Rec_Is_Processed = -1;

		INSERT IGNORE INTO CDM_LOV_Master
		(
			LOV_Attribute,
			LOV_Key,
			LOV_Value,
			LOV_Comm_Value
		)
			SELECT 
				'PRODUCT',
				Product_Key,
				Product_Key,
				Product_Comm_Name
			FROM CDM_Stg_Product_Master 
			WHERE Product_Key <> '' AND Rec_Is_Processed = -1;

 	   
			
		INSERT IGNORE INTO CDM_LOV_Master
		(
			LOV_Attribute,
			LOV_Key,
			LOV_Value,
			LOV_Comm_Value
		)
			SELECT 
				'PRODUCT_GENOME',
				Product_Genome,
				Product_Genome,
				Product_Genome
			FROM CDM_Stg_Product_Master 
			WHERE Product_Genome <> '' AND Rec_Is_Processed = -1;
			


		UPDATE IGNORE  CDM_LOV_Master AS A
    			SET LOV_Comm_Value = (
			SELECT Product_Comm_Name
		    	FROM CDM_Stg_Product_Master AS B
		    	WHERE A.LOV_Key = B.Product_Key
			AND A.LOV_Attribute='PRODUCT'
            		AND  B.Product_Key<>''
        	    	AND Product_Name<>''
  	          	GROUP BY Product_Name
			) where A.LOV_Attribute='PRODUCT';




		UPDATE  IGNORE CDM_LOV_Master AS A
    		SET LOV_Comm_Value = (
			SELECT Cat_Name
		    FROM CDM_Stg_Product_Master AS B
		    WHERE A.LOV_Key = B.Cat_Key
			AND A.LOV_Attribute='CAT'
            AND Cat_Name<>''
            GROUP BY Cat_Name
			) where  A.LOV_Attribute='CAT';

		UPDATE IGNORE  CDM_LOV_Master AS A
    		SET LOV_Comm_Value = (
			SELECT Div_Name
		    FROM CDM_Stg_Product_Master AS B
		    WHERE A.LOV_Key = B.Div_Key
			AND A.LOV_Attribute='DIV'
            AND Div_Name <>''
            GROUP BY Div_Name
			)where A.LOV_Attribute='DIV';

		UPDATE IGNORE  CDM_LOV_Master AS A
    		SET LOV_Comm_Value = (
			SELECT Dep_Name
		    FROM CDM_Stg_Product_Master AS B
		    WHERE A.LOV_Key = B.Dep_Key
			AND A.LOV_Attribute='DEP'
            AND  Dep_Name <>''
            GROUP BY Dep_Name
			) where  A.LOV_Attribute='DEP';

		UPDATE IGNORE  CDM_LOV_Master AS A
    		SET LOV_Comm_Value = (
			SELECT Section_Name
		    FROM CDM_Stg_Product_Master AS B
		    WHERE A.LOV_Key = B.Section_Key
			AND A.LOV_Attribute='SECTION'
            AND  B.Section_Key<>''
            AND Section_Name<>''
            GROUP BY Section_Name
			) where A.LOV_Attribute='SECTION';
		
		UPDATE IGNORE  CDM_LOV_Master AS A
    		SET LOV_Comm_Value = (
			SELECT Season_Name
		    FROM CDM_Stg_Product_Master AS B
		    WHERE A.LOV_Key = B.Season_Key
			AND A.LOV_Attribute='SEASON'
            AND  B.Season_Key<>''
            AND Season_Name<>''
            GROUP BY Season_Name
			) where A.LOV_Attribute='SEASON';

		UPDATE IGNORE  CDM_LOV_Master AS A
    		SET LOV_Comm_Value = (
			SELECT Brand_Name
		    FROM CDM_Stg_Product_Master AS B
		    WHERE A.LOV_Key = B.Brand_Key
			AND A.LOV_Attribute='BRAND'
            AND B.Brand_Key<>''
            AND Brand_Name<>''
            GROUP BY Brand_Name
			) where  A.LOV_Attribute='BRAND';

		UPDATE IGNORE  CDM_LOV_Master AS A
    		SET LOV_Comm_Value = (
			SELECT BU_Name
		    FROM CDM_Stg_Product_Master AS B
		    WHERE A.LOV_Key = B.BU_Key
			AND A.LOV_Attribute='BU'
            AND B.BU_Key<>''
            GROUP BY BU_Name
			) where  A.LOV_Attribute='BU';

		UPDATE IGNORE CDM_LOV_Master AS A
    		SET LOV_Comm_Value = (
			SELECT Manuf_Name
		    FROM CDM_Stg_Product_Master AS B
		    WHERE A.LOV_Key = B.Manuf_Key
			AND A.LOV_Attribute='MANUF'
            AND B.Manuf_Name<>''
            AND Manuf_Name<>''
            GROUP BY Manuf_Name
			) where  A.LOV_Attribute='MANUF';


			
	END IF;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Brand_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Brand_Key = B.LOV_Key
			AND B.LOV_Attribute = 'BRAND' 
	)
	WHERE Brand_LOV_Id <= 0;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
		SET BU_LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.BU_Key = B.LOV_Key
				AND B.LOV_Attribute = 'BU' 
		)
	WHERE BU_LOV_Id <= 0;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
		SET Cat_LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Cat_Key = B.LOV_Key
				AND B.LOV_Attribute = 'CAT' 
		)
	WHERE Cat_LOV_Id <= 0;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
		SET Dep_LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Dep_Key = B.LOV_Key
				AND B.LOV_Attribute = 'DEP' 
		)
	WHERE Dep_LOV_Id <= 0;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
		SET Div_LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Div_Key = B.LOV_Key
				AND B.LOV_Attribute = 'DIV' 
		)
	WHERE Div_LOV_Id <= 0;
	

	UPDATE IGNORE CDM_Stg_Product_Master AS A
		SET Season_LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Season_Key = B.LOV_Key
				AND B.LOV_Attribute = 'SEASON' 
		)
	WHERE Season_LOV_Id <= 0;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
		SET Section_LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Section_Key = B.LOV_Key
				AND B.LOV_Attribute = 'SECTION' 
		)
	WHERE Section_LOV_Id <= 0;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
		SET Manuf_LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Manuf_Key = B.LOV_Key
				AND B.LOV_Attribute = 'MANUF' 
		)
	WHERE Manuf_LOV_Id <= 0;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
		SET Product_Genome_LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Product_Genome = B.LOV_Key
				AND B.LOV_Attribute = 'PRODUCT_GENOME' 
		)
	WHERE Product_Genome_LOV_Id <= 0;


	IF vTreat_As_Incr = 1 THEN
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 17, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Brand_Master (
			Brand_Id,
			Brand_Name,
			LOV_Id
		)
			SELECT 
				Brand_Id,
				Brand_Name,
				Brand_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Brand_Id,Brand_Name,Brand_LOV_Id;

		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 18, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_BU_Master (
			BU_Id,
			BU_Name,
			LOV_Id
		)
			SELECT 
				BU_Id,
				BU_Name,
				BU_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY BU_Id,BU_Name,BU_LOV_Id;
    
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 19, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Cat_Master (
			Cat_Id,
			Cat_Name,
			LOV_Id
		)
			SELECT 
				Cat_Id,
				Cat_Name,
				Cat_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Cat_Id, Cat_Name,Cat_LOV_Id;

		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 20, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Div_Master (
			Div_Id,
			Div_Name,
			LOV_Id
		)
			SELECT 
				Div_Id,
				Div_Name,
				Div_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Div_Id,Div_Name,Div_LOV_Id;

		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 21, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Dep_Master (
			Dep_Id,
			Dep_Name,
			LOV_Id
		)
			SELECT 
				Dep_Id,
				Dep_Name,
				Dep_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Dep_Id, Dep_Name,Dep_LOV_Id;
        
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 22, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Season_Master (
			Season_Id,
			Season_Name,
			LOV_Id
		)
			SELECT 
				Season_Id,
				Season_Name,
				Season_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Season_Id, Season_Name, Season_LOV_Id;

		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 23, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Section_Master (
			Section_Id,
			Section_Name,
			LOV_Id
		)
			SELECT 
				Section_Id,
				Section_Name,
				Section_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Section_Id, Section_Name, Section_LOV_Id;
        
		CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 24, 'CDM_ETL_Product_Master', 1);

		INSERT IGNORE INTO CDM_Manuf_Master (
			Manuf_Id,
			Manuf_Name,
			LOV_Id
		)
			SELECT 
				Manuf_Id,
				Manuf_Name,
				Manuf_LOV_Id
			FROM CDM_Stg_Product_Master
			WHERE Rec_Is_Processed = -1
			GROUP BY Manuf_Id, Manuf_Name, Manuf_LOV_Id;
	END IF;

	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 25, 'CDM_ETL_Product_Master', 1);

	INSERT IGNORE INTO CDM_Product_Key_Lookup (
		Product_Key
	)
		SELECT 
			Product_Key
		FROM CDM_Stg_Product_Master
        WHERE Product_Key <> '' AND Rec_Is_Processed = -1;

	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 26, 'CDM_ETL_Product_Master', 1);

	UPDATE IGNORE CDM_Stg_Product_Master AS A
    SET Product_Id = (
		SELECT Product_Id 
        FROM CDM_Product_Key_Lookup AS B
        WHERE B.Product_Key = A.Product_Key
    );

   
	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT',
			Product_Key,
			Product_Key,
			Product_Comm_Name
		FROM CDM_Stg_Product_Master 
		WHERE Product_Key <> '' AND Rec_Is_Processed = -1;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
		SET LOV_Id = (
			SELECT LOV_Id  
			FROM CDM_LOV_Master AS B
			WHERE 
				A.Product_Key = B.LOV_Key
				AND B.LOV_Attribute = 'PRODUCT' 
		)
	WHERE LOV_Id <= 0;

	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG1',
			Custom_Tag1,
			Custom_Tag1,
			Custom_Tag1
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag1 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag1;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag1_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag1 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG1' 
	);

	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG2',
			Custom_Tag2,
			Custom_Tag2,
			Custom_Tag2
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag2 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag2;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag2_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag2 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG2' 
	);
	
	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG3',
			Custom_Tag3,
			Custom_Tag3,
			Custom_Tag3
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag3 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag3;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag3_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag3 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG3' 
	);
		
	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG4',
			Custom_Tag4,
			Custom_Tag4,
			Custom_Tag4
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag4 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag4;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag4_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag4 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG4' 
	);
		
	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG5',
			Custom_Tag5,
			Custom_Tag5,
			Custom_Tag5
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag5 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag5;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag5_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag5 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG5' 
	);

	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG6',
			Custom_Tag6,
			Custom_Tag6,
			Custom_Tag6
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag6 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag6;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag6_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag6 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG6' 
	);

	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG7',
			Custom_Tag7,
			Custom_Tag7,
			Custom_Tag7
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag7 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag7;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag7_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag7 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG7' 
	);
    
    	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG8',
			Custom_Tag8,
			Custom_Tag8,
			Custom_Tag8
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag8 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag8;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag8_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag8 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG8' 
	);
		
        	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG9',
			Custom_Tag9,
			Custom_Tag9,
			Custom_Tag9
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag9 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag9;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag9_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag9 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG9' 
	);
    
    	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG10',
			Custom_Tag10,
			Custom_Tag10,
			Custom_Tag10
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag10 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag10;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag10_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag10 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG10' 
	);
    
    	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG11',
			Custom_Tag11,
			Custom_Tag11,
			Custom_Tag11
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag11 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag11;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag11_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag11 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG11' 
	);
    
    	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG12',
			Custom_Tag12,
			Custom_Tag12,
			Custom_Tag12
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag12 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag12;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag12_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag12 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG12' 
	);
    
    	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG13',
			Custom_Tag13,
			Custom_Tag13,
			Custom_Tag13
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag13 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag13;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag13_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag13 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG13' 
	);
    
   	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG14',
			Custom_Tag14,
			Custom_Tag14,
			Custom_Tag14
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag14 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag14;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag14_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag14 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG14' 
	);
    
    	INSERT IGNORE INTO CDM_LOV_Master
	(
		LOV_Attribute,
		LOV_Key,
		LOV_Value,
		LOV_Comm_Value
	)
		SELECT 
			'PRODUCT_CUSTOM_TAG15',
			Custom_Tag15,
			Custom_Tag15,
			Custom_Tag15
		FROM CDM_Stg_Product_Master 
		WHERE Custom_Tag15 <> '' AND Rec_Is_Processed = -1
		GROUP BY Custom_Tag15;

	UPDATE IGNORE CDM_Stg_Product_Master AS A
	SET Custom_Tag15_LOV_Id = (
		SELECT LOV_Id  
		FROM CDM_LOV_Master AS B
		WHERE 
			A.Custom_Tag15 = B.LOV_Key
			AND B.LOV_Attribute = 'PRODUCT_CUSTOM_TAG15' 
	);
    
	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 27, 'CDM_ETL_Product_Master', 1);

	IF vTreat_As_Incr = 1 THEN
	
		INSERT IGNORE INTO CDM_Product_Master_Hist (
			Product_Id,
			Product_LOV_Id,
			Is_Active,
			Is_Private_Label,
			Is_Premium,
			Unit_Price,
			Brand_Id,
			Div_Id,
			Dep_Id,
			Cat_Id,
			Season_Id,
			BU_Id,
			Section_Id,
			Manuf_Id,
			Brand_LOV_Id,
			Div_LOV_Id,
			Dep_LOV_Id,
			Cat_LOV_Id,
			Season_LOV_Id,
			BU_LOV_Id,
			Section_LOV_Id,
			Manuf_LOV_Id,
			Launch_Date,
            Expiry_Date,
			Launch_Year,
			Launch_Year_Month,
			Launch_Day_Week,
			Uom,
			Tax_Category,
			Product_Name,
			Product_Desc,
			Product_Comm_Name,
			To_Be_Excluded,
			Is_Combo,
			Is_Ingredient,
			Festival_Id_During_Launch,
			Brand_Event_Id_During_Launch,
			Fabric_Base,
			Fit,
			Design,
			Colour,
			Replenish_Days,
			Custom_Attrib,
			Custom_Tag1_LOV_Id,
			Custom_Tag2_LOV_Id,
			Custom_Tag3_LOV_Id,
			Custom_Tag4_LOV_Id,
			Custom_Tag5_LOV_Id,
			Custom_Tag6_LOV_Id,
			Custom_Tag7_LOV_Id,
			Custom_Tag8_LOV_Id,
			Custom_Tag9_LOV_Id,
			Custom_Tag10_LOV_Id,
			Custom_Tag11_LOV_Id,
			Custom_Tag12_LOV_Id,
			Custom_Tag13_LOV_Id,
			Custom_Tag14_LOV_Id,
			Custom_Tag15_LOV_Id,
			Product_Genome,
			Product_Genome_LOV_Id,
			Product_URL
		)
			SELECT 
				A.Product_Id,
				A.Product_LOV_Id,
				A.Is_Active,
				A.Is_Private_Label,
				A.Is_Premium,
				A.Unit_Price,
				A.Brand_Id,
				A.Div_Id,
				A.Dep_Id,
				A.Cat_Id,
				A.Season_Id,
				A.BU_Id,
				A.Section_Id,
				A.Manuf_Id,
				A.Brand_LOV_Id,
				A.Div_LOV_Id,
				A.Dep_LOV_Id,
				A.Cat_LOV_Id,
				A.Season_LOV_Id,
				A.BU_LOV_Id,
				A.Section_LOV_Id,
				A.Manuf_LOV_Id,
				A.Launch_Date,
                A.Expiry_Date,
				A.Launch_Year,
				A.Launch_Year_Month,
				A.Launch_Day_Week,
				A.Uom,
				A.Tax_Category,
				A.Product_Name,
				A.Product_Desc,
				A.Product_Comm_Name,
				A.To_Be_Excluded,
				A.Is_Combo,
				A.Is_Ingredient,
				A.Festival_Id_During_Launch,
				A.Brand_Event_Id_During_Launch,
				A.Fabric_Base,
				A.Fit,
				A.Design,
				A.Colour,
				A.Replenish_Days,
				A.Custom_Attrib,
				A.Custom_Tag1_LOV_Id,
				A.Custom_Tag2_LOV_Id,
				A.Custom_Tag3_LOV_Id,
				A.Custom_Tag4_LOV_Id,
				A.Custom_Tag5_LOV_Id,
				A.Custom_Tag6_LOV_Id,
			    A.Custom_Tag7_LOV_Id,
			    A.Custom_Tag8_LOV_Id,
			    A.Custom_Tag9_LOV_Id,
			    A.Custom_Tag10_LOV_Id,
			    A.Custom_Tag11_LOV_Id,
			    A.Custom_Tag12_LOV_Id,
			    A.Custom_Tag13_LOV_Id,
			    A.Custom_Tag14_LOV_Id,
			    A.Custom_Tag15_LOV_Id,
				A.Product_Genome,
				A.Product_Genome_LOV_Id,
				A.Product_URL
			FROM CDM_Product_Master_Original AS A, CDM_Stg_Product_Master AS B
			WHERE 
				A.Product_Id = B.Product_Id
				AND B.Rec_Is_Processed = -1;

	
		UPDATE IGNORE CDM_Product_Master_Original AS A, CDM_Stg_Product_Master AS B
		SET
			A.Product_LOV_Id = B.LOV_Id,
			A.Is_Private_Label = B.Is_Private_Label,
			A.Is_Premium = B.Is_Premium,
			A.Unit_Price = B.Unit_Price,
			A.Brand_Id = B.Brand_Id,
			A.Div_Id = B.Div_Id,
			A.Dep_Id = B.Dep_Id,
			A.Cat_Id = B.Cat_Id,
			A.Season_Id = B.Season_Id,
			A.BU_Id = B.BU_Id,
			A.Section_Id = B.Section_Id,
			A.Manuf_Id = B.Manuf_Id,
			A.Brand_LOV_Id = B.Brand_LOV_Id,
			A.Div_LOV_Id = B.Div_LOV_Id,
			A.Dep_LOV_Id = B.Dep_LOV_Id,
			A.Cat_LOV_Id = B.Cat_LOV_Id,
			A.Season_LOV_Id = B.Season_LOV_Id,
			A.BU_LOV_Id = B.BU_LOV_Id,
			A.Section_LOV_Id = B.Section_LOV_Id,
			A.Manuf_LOV_Id = B.Manuf_LOV_Id,
			A.Launch_Date = B.Launch_Date,
			A.Expiry_Date = B.Expiry_Date,
			A.Launch_Year = IFNULL(YEAR(B.Launch_Date),''),
			A.Launch_Year_Month = IFNULL(EXTRACT(YEAR_MONTH FROM B.Launch_Date),''),
			A.Launch_Day_Week = IFNULL(WEEKDAY(B.Launch_Date),-1),
			A.Uom = B.Uom,
			A.Tax_Category = B.Tax_Category,
			A.Product_Name = B.Product_Name,
			A.Product_Desc = B.Product_Desc,
			A.Product_Comm_Name = B.Product_Comm_Name,
			A.To_Be_Excluded = B.To_Be_Excluded,
			A.Is_Combo = B.Is_Combo,
			A.Is_Ingredient = B.Is_Ingredient,
			A.Festival_Id_During_Launch = B.Festival_Id_During_Launch,
			A.Brand_Event_Id_During_Launch = B.Brand_Event_Id_During_Launch,
			A.Fabric_Base = B.Fabric_Base,
			A.Fit = B.Fit,
			A.Design = B.Design,
			A.Colour = B.Colour,
			A.Replenish_Days = B.Replenish_Days,
			A.Custom_Attrib = B.Custom_Attrib,
			A.Custom_Tag1_LOV_Id = B.Custom_Tag1_LOV_Id,
			A.Custom_Tag2_LOV_Id = B.Custom_Tag2_LOV_Id,
			A.Custom_Tag3_LOV_Id = B.Custom_Tag3_LOV_Id,
			A.Custom_Tag4_LOV_Id = B.Custom_Tag4_LOV_Id,
			A.Custom_Tag5_LOV_Id = B.Custom_Tag5_LOV_Id,
			A.Custom_Tag6_LOV_Id = B.Custom_Tag6_LOV_Id,
			A.Custom_Tag7_LOV_Id = B.Custom_Tag7_LOV_Id,
			A.Custom_Tag8_LOV_Id = B.Custom_Tag8_LOV_Id,
			A.Custom_Tag9_LOV_Id = B.Custom_Tag9_LOV_Id,
			A.Custom_Tag10_LOV_Id = B.Custom_Tag10_LOV_Id,
			A.Custom_Tag11_LOV_Id = B.Custom_Tag11_LOV_Id,
			A.Custom_Tag12_LOV_Id = B.Custom_Tag12_LOV_Id,
			A.Custom_Tag13_LOV_Id = B.Custom_Tag13_LOV_Id,
			A.Custom_Tag14_LOV_Id = B.Custom_Tag14_LOV_Id,
			A.Custom_Tag15_LOV_Id = B.Custom_Tag15_LOV_Id,
			A.Product_Genome = B.Product_Genome,
			A.Product_Genome_LOV_Id = B.Product_Genome_LOV_Id,
			A.Product_URL=B.Product_URL
		WHERE
			A.Product_Id = B.Product_Id
			AND Rec_Is_Processed = -1;

	END IF;
	
	INSERT IGNORE INTO CDM_Product_Master_Original (
		Product_Id,
		Product_LOV_Id,
		Is_Private_Label,
		Is_Premium,
		Unit_Price,
		Brand_Id,
		Div_Id,
		Dep_Id,
		Cat_Id,
		Season_Id,
		BU_Id,
		Section_Id,
		Manuf_Id,
		Brand_LOV_Id,
		Div_LOV_Id,
		Dep_LOV_Id,
		Cat_LOV_Id,
		Season_LOV_Id,
		BU_LOV_Id,
		Section_LOV_Id,
		Manuf_LOV_Id,
		Launch_Date,
        Expiry_Date,
		Launch_Year,
		Launch_Year_Month,
		Launch_Day_Week,
		Uom,
		Tax_Category,
		Product_Name,
		Product_Desc,
		Product_Comm_Name,
		To_Be_Excluded,
		Is_Combo,
		Is_Ingredient,
		Festival_Id_During_Launch,
		Brand_Event_Id_During_Launch,
		Fabric_Base,
		Fit,
		Design,
		Colour,
		Replenish_Days,
		Custom_Attrib,
		Custom_Tag1_LOV_Id,
		Custom_Tag2_LOV_Id,
		Custom_Tag3_LOV_Id,
		Custom_Tag4_LOV_Id,
		Custom_Tag5_LOV_Id,
		Custom_Tag6_LOV_Id,
		Custom_Tag7_LOV_Id,
		Custom_Tag8_LOV_Id,
		Custom_Tag9_LOV_Id,
		Custom_Tag10_LOV_Id,
		Custom_Tag11_LOV_Id,
		Custom_Tag12_LOV_Id,
		Custom_Tag13_LOV_Id,
		Custom_Tag14_LOV_Id,
		Custom_Tag15_LOV_Id,
		Product_Genome,
		Product_Genome_LOV_Id,
		Product_URL
	)
		SELECT 
			Product_Id,
			LOV_Id,
			Is_Private_Label,
			Is_Premium,
			Unit_Price,
			Brand_Id,
			Div_Id,
			Dep_Id,
			Cat_Id,
			Season_Id,
			BU_Id,
			Section_Id,
			Manuf_Id,
			Brand_LOV_Id,
			Div_LOV_Id,
			Dep_LOV_Id,
			Cat_LOV_Id,
			Season_LOV_Id,
			BU_LOV_Id,
			Section_LOV_Id,
			Manuf_LOV_Id,
			Launch_Date,
            Expiry_Date,
			IFNULL(YEAR(Launch_Date),''),
			IFNULL(EXTRACT(YEAR_MONTH FROM Launch_Date),''),
			IFNULL(WEEKDAY(Launch_Date),-1),
			Uom,
			Tax_Category,
			Product_Name,
			Product_Desc,
			Product_Comm_Name,
			To_Be_Excluded,
			Is_Combo,
			Is_Ingredient,
			Festival_Id_During_Launch,
			Brand_Event_Id_During_Launch,
			Fabric_Base,
			Fit,
			Design,
			Colour,
			Replenish_Days,
			Custom_Attrib,
			Custom_Tag1_LOV_Id,
			Custom_Tag2_LOV_Id,
			Custom_Tag3_LOV_Id,
			Custom_Tag4_LOV_Id,
			Custom_Tag5_LOV_Id,
			Custom_Tag6_LOV_Id,
			Custom_Tag7_LOV_Id,
			Custom_Tag8_LOV_Id,
			Custom_Tag9_LOV_Id,
			Custom_Tag10_LOV_Id,
			Custom_Tag11_LOV_Id,
			Custom_Tag12_LOV_Id,
			Custom_Tag13_LOV_Id,
			Custom_Tag14_LOV_Id,
			Custom_Tag15_LOV_Id,
			Product_Genome,
			Product_Genome_LOV_Id,
			Product_URL
		FROM CDM_Stg_Product_Master
        WHERE Rec_Is_Processed = -1;
	CALL CDM_Update_Process_Log('CDM_ETL_Product_Master', 28, 'CDM_ETL_Product_Master', 1);
END$$
DELIMITER ;
