DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Brand_Master () 
BEGIN 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Brand_Master', 1, 'CDM_ETL_Brand_Master', 1); 
 
  INSERT IGNORE INTO CDM_Brand_Key_Lookup ( 
    Brand_Key 
  ) 
    SELECT  
      Brand_Key 
    FROM CDM_Stg_Brand_Master 
        WHERE Brand_Key <> '' AND Rec_Is_Processed = -1; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Brand_Master', 2, 'CDM_ETL_Brand_Master', 1); 
 
  UPDATE IGNORE CDM_Stg_Brand_Master AS A 
    SET Brand_Id = ( 
    SELECT Brand_Id  
        FROM CDM_Brand_Key_Lookup AS B 
        WHERE A.Brand_Key = B.Brand_Key 
    ); 
 
     
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'BRAND', 
      Brand_Key, 
      Brand_Key, 
      Brand_Comm_Name 
    FROM CDM_Stg_Brand_Master  
    WHERE Brand_Key <> '' AND Rec_Is_Processed = -1; 
 
  UPDATE IGNORE CDM_Stg_Brand_Master AS A 
  SET  
    LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Brand_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'BRAND'  
    ) 
  WHERE LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Brand_Master AS A 
  SET  
    Comp_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'BRAND'  
    ) 
  WHERE Comp_LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Brand_Master AS A 
  SET  
    Up_Sell_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'BRAND'  
    ) 
  WHERE Up_Sell_LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Brand_Master AS A 
  SET  
    Cross_Sell_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'BRAND'  
    ) 
  WHERE Cross_Sell_LOV_Id <= 0; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Brand_Master', 3, 'CDM_ETL_Brand_Master', 1); 
     
  INSERT IGNORE INTO CDM_Brand_Master ( 
    Brand_Id, 
    LOV_Id, 
    Comp_LOV_Id, 
    Up_Sell_LOV_Id, 
    Cross_Sell_LOV_Id, 
    Brand_Name, 
    Is_Premium, 
    Brand_Type_Tag1, 
    Brand_Type_Tag2, 
    Brand_Type_Tag3, 
    Brand_Comm_Name 
  ) 
    SELECT  
      Brand_Id, 
      LOV_Id, 
      Comp_LOV_Id, 
      Up_Sell_LOV_Id, 
      Cross_Sell_LOV_Id, 
      Brand_Name, 
      Is_Premium, 
      Brand_Type_Tag1, 
      Brand_Type_Tag2, 
      Brand_Type_Tag3, 
      Brand_Comm_Name 
    FROM CDM_Stg_Brand_Master 
        WHERE Rec_Is_Processed = -1; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Brand_Master', 4, 'CDM_ETL_Brand_Master', 1); 
        
END	
mysolus$$
