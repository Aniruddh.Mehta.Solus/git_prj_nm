DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Section_Master () 
BEGIN 
  CALL CDM_Update_Process_Log('CDM_ETL_Section_Master', 1, 'CDM_ETL_Section_Master', 1); 
 
  INSERT IGNORE INTO CDM_Section_Key_Lookup ( 
    Section_Key 
  ) 
    SELECT  
      Section_Key 
    FROM CDM_Stg_Section_Master 
        WHERE Section_Key <> '' AND Rec_Is_Processed = -1; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Section_Master', 2, 'CDM_ETL_Section_Master', 1); 
 
  UPDATE IGNORE CDM_Stg_Section_Master AS A 
    SET Section_Id = ( 
    SELECT Section_Id  
        FROM CDM_Section_Key_Lookup AS B 
        WHERE A.Section_Key = B.Section_Key 
    ); 
 
     
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'SECTION', 
      Section_Key, 
      Section_Key, 
      Section_Comm_Name 
    FROM CDM_Stg_Section_Master  
    WHERE Section_Key <> '' AND Rec_Is_Processed = -1; 
 
  UPDATE IGNORE CDM_Stg_Section_Master AS A 
        SET LOV_Id = ( 
      SELECT LOV_Id   
            FROM CDM_LOV_Master AS B 
            WHERE  
        A.Section_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'SECTION'  
        ) 
  WHERE LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Section_Master AS A 
  SET  
    Comp_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'SECTION'  
    ) 
  WHERE Comp_LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Section_Master AS A 
  SET  
    Up_Sell_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'SECTION'  
    ) 
  WHERE Up_Sell_LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Section_Master AS A 
  SET  
    Cross_Sell_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'SECTION'  
    ) 
  WHERE Cross_Sell_LOV_Id <= 0; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Section_Master', 3, 'CDM_ETL_Section_Master', 1); 
     
  INSERT IGNORE INTO CDM_Section_Master ( 
    Section_Id, 
    LOV_Id, 
    Comp_LOV_Id, 
    Up_Sell_LOV_Id, 
    Cross_Sell_LOV_Id, 
    Section_Name, 
    Section_Type_Tag1, 
    Section_Type_Tag2, 
    Section_Type_Tag3, 
    Section_Comm_Name 
  ) 
    SELECT  
      Section_Id, 
      LOV_Id, 
      Comp_LOV_Id, 
      Up_Sell_LOV_Id, 
      Cross_Sell_LOV_Id, 
      Section_Name, 
      Section_Type_Tag1, 
      Section_Type_Tag2, 
      Section_Type_Tag3, 
      Section_Comm_Name 
    FROM CDM_Stg_Section_Master 
        WHERE Rec_Is_Processed = -1; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Section_Master', 4, 'CDM_ETL_Section_Master', 1); 
 
END	
mysolus$$
