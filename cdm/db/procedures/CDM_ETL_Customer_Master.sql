DELIMITER $$
CREATE or replace PROCEDURE  CDM_ETL_Customer_Master (IN vBatchSize BIGINT,IN vTreat_As_Incr TINYINT)
BEGIN 
 
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
 
  SET @Rec_Cnt = 0; 
  SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Customer_Master ORDER BY Rec_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
     
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Customer_Master ORDER BY Rec_Id DESC LIMIT 1; 
     
    IF @Rec_Cnt IS NULL THEN  
    SET @Rec_Cnt = 0; 
  END IF; 
 
  CREATE TABLE IF NOT EXISTS Temp_CDM_Customer_Master ( 
    Rec_Id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
      Rec_Is_Processed TINYINT DEFAULT - 1 NOT NULL, 
    Customer_Key VARCHAR(128) NOT NULL, 
    Customer_Id BIGINT DEFAULT -1 NOT NULL,     
    Mobile CHAR(15) DEFAULT '' NOT NULL, 
    Email VARCHAR(128) DEFAULT '' NOT NULL, 
    First_Name VARCHAR(128) DEFAULT '' NOT NULL, 
    Middle_Name VARCHAR(128) DEFAULT '' NOT NULL,     
    Last_Name VARCHAR(128) DEFAULT '' NOT NULL, 
    Full_Name VARCHAR(128) DEFAULT '' NOT NULL, 
    Address_Line1 VARCHAR(128) DEFAULT '' NOT NULL, 
    Address_Line2 VARCHAR(128) DEFAULT '' NOT NULL, 
    Latitude DECIMAL(15,2) DEFAULT -1.0 NOT NULL, 
    Longitude DECIMAL(15,2) DEFAULT -1.0 NOT NULL, 
    Comm_Name VARCHAR(128) DEFAULT '' NOT NULL, 
    City VARCHAR(128) DEFAULT '' NOT NULL, 
    Doj DATE, 
    Enrolled_Store_Key VARCHAR(128) DEFAULT '' NOT NULL, 
    Enrolled_Store_Id BIGINT DEFAULT -1 NOT NULL,     
    Enrolled_Channel VARCHAR(128) DEFAULT '' NOT NULL, 
    Enrolled_Channel_LOV_Id BIGINT DEFAULT -1 NOT NULL, 
    Gender VARCHAR(128) DEFAULT '' NOT NULL, 
    Gender_LOV_Id BIGINT DEFAULT - 1 NOT NULL, 
    Dob DATE, 
    Pincode CHAR(6) DEFAULT '' NOT NULL, 
    Marital_Status VARCHAR(128)  DEFAULT '' NOT NULL, 
    Marital_Status_LOV_Id BIGINT DEFAULT - 1 NOT NULL, 
    Members_In_Household TINYINT DEFAULT - 1 NOT NULL, 
    Email_Consent VARCHAR(128) DEFAULT '' NOT NULL, 
    Email_Consent_LOV_Id BIGINT DEFAULT -1 NOT NULL, 
    SMS_Consent VARCHAR(128) DEFAULT '' NOT NULL, 
    SMS_Consent_LOV_Id BIGINT DEFAULT -1 NOT NULL, 
    Customer_State VARCHAR(128) DEFAULT '' NOT NULL, 
    DoA DATE, 
    DND VARCHAR(128) DEFAULT '' NOT NULL, 
    Ethnicity VARCHAR(128)  DEFAULT '' NOT NULL, 
    Ethnicity_LOV_Id BIGINT DEFAULT -1 NOT NULL, 
    Tier_Name VARCHAR(128) DEFAULT '' NOT NULL, 
    Tier_Name_LOV_Id BIGINT DEFAULT -1 NOT NULL, 
    Point_Balance DECIMAL(15,2) DEFAULT -1 NOT NULL, 
    Customer_Custom_Tag1_LOV_Id BIGINT DEFAULT -1 NOT NULL, 
    Customer_Custom_Tag2_LOV_Id BIGINT DEFAULT -1 NOT NULL, 
    Customer_Custom_Tag3_LOV_Id BIGINT DEFAULT -1 NOT NULL,  
    Customer_Custom_Tag1 VARCHAR(128) NOT NULL DEFAULT '', 
    Customer_Custom_Tag2 VARCHAR(128) NOT NULL DEFAULT '', 
    Customer_Custom_Tag3 VARCHAR(128) NOT NULL DEFAULT ''  
  ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 

	/*select count(1) into @x from  INFORMATION_SCHEMA.STATISTICS where TABLE_SCHEMA='CART_SOLUS_PROD'  and TABLE_NAME = 'CDM_Customer_Master' and COLUMN_NAME = 'Customer_Id';
	IF @x>0 then 
	Select "Index Already Exists";
	ELSE 
	ALTER TABLE Temp_CDM_Customer_Master  ADD INDEX (Customer_Id)  ; 
	END IF;    
    */
  CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', 1, 'CDM_ETL_Customer_Master', 1); 
   
  PROCESS_CUST_HIST: LOOP 
 
    TRUNCATE TABLE Temp_CDM_Customer_Master; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', CONCAT(vStart_Cnt,'_0'), 'CDM_ETL_Customer_Master', 1); 
     
    INSERT INTO Temp_CDM_Customer_Master ( 
      Customer_Key, 
      Customer_Id, 
      Mobile, 
      Email, 
      First_Name, 
      Middle_Name, 
      Last_Name, 
      Full_Name, 
      Address_Line1, 
      Address_Line2, 
      Latitude, 
      Longitude, 
      Comm_Name, 
      City, 
      Doj, 
      Enrolled_Store_Key, 
      Enrolled_Store_Id, 
      Enrolled_Channel, 
      Enrolled_Channel_LOV_Id, 
      Gender, 
      Gender_LOV_Id, 
      Dob, 
      Pincode, 
      Marital_Status, 
      Marital_Status_LOV_Id, 
      Members_In_Household, 
      Email_Consent, 
      Email_Consent_LOV_Id, 
      SMS_Consent, 
      SMS_Consent_LOV_Id, 
      Customer_State, 
      DoA, 
      DND, 
      Ethnicity, 
      Ethnicity_LOV_Id, 
      Tier_Name, 
      Tier_Name_LOV_Id, 
      Point_Balance, 
      Customer_Custom_Tag1_LOV_Id, 
      Customer_Custom_Tag2_LOV_Id, 
      Customer_Custom_Tag3_LOV_Id, 
      Customer_Custom_Tag1, 
      Customer_Custom_Tag2, 
      Customer_Custom_Tag3 
    ) 
      SELECT 
        Customer_Key, 
        Customer_Id, 
        Mobile, 
        Email, 
        First_Name, 
        Middle_Name, 
        Last_Name, 
        Full_Name, 
        Address_Line1, 
        Address_Line2, 
        Latitude, 
        Longitude, 
        Comm_Name, 
        City, 
        case when Doj= '0000-00-00' THEN NULL else Doj end, 
        Enrolled_Store_Key, 
        Enrolled_Store_Id, 
        Enrolled_Channel, 
        Enrolled_Channel_LOV_Id, 
        Gender, 
        Gender_LOV_Id, 
        case when Dob= '0000-00-00' THEN NULL else Dob end,
        Pincode, 
        Marital_Status, 
        Marital_Status_LOV_Id, 
        Members_In_Household, 
        Email_Consent, 
        Email_Consent_LOV_Id, 
        SMS_Consent, 
        SMS_Consent_LOV_Id, 
        Customer_State, 
	case when DoA= '0000-00-00' THEN NULL else DoA end,
        DND, 
        Ethnicity, 
        Ethnicity_LOV_Id, 
        Tier_Name, 
        Tier_Name_LOV_Id, 
        Point_Balance, 
        Customer_Custom_Tag1_LOV_Id, 
        Customer_Custom_Tag2_LOV_Id, 
        Customer_Custom_Tag3_LOV_Id, 
        Customer_Custom_Tag1, 
        Customer_Custom_Tag2, 
        Customer_Custom_Tag3 
      FROM CDM_Stg_Customer_Master 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
 
             
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', CONCAT(vStart_Cnt,'_1'), 'CDM_ETL_Customer_Master', 1); 
 
    INSERT IGNORE INTO CDM_Customer_Key_Lookup ( 
      Customer_Key 
    )     
      SELECT  
        Customer_Key 
      FROM Temp_CDM_Customer_Master 
      WHERE  
        Customer_Key <> ''; 
 
             
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Customer_Master', 1); 
 
    UPDATE IGNORE  Temp_CDM_Customer_Master 
    SET Customer_Id = ( 
      SELECT Customer_Id 
      FROM CDM_Customer_Key_Lookup 
      WHERE Temp_CDM_Customer_Master.Customer_Key = CDM_Customer_Key_Lookup.Customer_Key 
    ); 
 
             
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Customer_Master', 1); 
     

    INSERT IGNORE INTO CDM_Store_Key_Lookup ( 
      Store_Key 
    )     
      SELECT  
        Enrolled_Store_Key 
      FROM Temp_CDM_Customer_Master 
      WHERE  
        Enrolled_Store_Key <> '';


    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'STORE', 
        Enrolled_Store_Key, 
        Enrolled_Store_Key, 
        Enrolled_Store_Key 
      FROM Temp_CDM_Customer_Master  
      WHERE  
        Enrolled_Store_Key <> ''  
      GROUP BY Enrolled_Store_Key; 



    UPDATE IGNORE  Temp_CDM_Customer_Master 
    SET Enrolled_Store_Id = ( 
      SELECT Store_Id 
      FROM CDM_Store_Key_Lookup 
      WHERE CDM_Store_Key_Lookup.Store_Key = Temp_CDM_Customer_Master.Enrolled_Store_Key 
    ); 
 
 
    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'GENDER', 
        Gender, 
        Gender, 
        Gender 
      FROM Temp_CDM_Customer_Master  
      WHERE  
        Gender <> ''  
      GROUP BY Gender; 
 
    UPDATE IGNORE Temp_CDM_Customer_Master AS A 
    SET Gender_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Gender = B.LOV_Key 
        AND B.LOV_Attribute = 'GENDER'  
    ); 
 
             
 
    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'MARITAL STATUS', 
        Marital_Status, 
        Marital_Status, 
        Marital_Status 
      FROM Temp_CDM_Customer_Master  
      WHERE  
        Marital_Status <> ''  
      GROUP BY Marital_Status; 
 
    UPDATE IGNORE Temp_CDM_Customer_Master AS A 
    SET Marital_Status_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Marital_Status = B.LOV_Key 
        AND B.LOV_Attribute = 'MARITAL STATUS'  
    ); 
       
             
    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'ENROLLED_CHANNEL', 
        Enrolled_Channel, 
        Enrolled_Channel, 
        Enrolled_Channel 
      FROM Temp_CDM_Customer_Master  
      WHERE Enrolled_Channel <> ''  
      GROUP BY Enrolled_Channel; 
 
    UPDATE IGNORE Temp_CDM_Customer_Master AS A 
    SET Enrolled_Channel_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Enrolled_Channel = B.LOV_Key 
        AND B.LOV_Attribute = 'ENROLLED_CHANNEL'  
    ); 
             
    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'TIER_NAME', 
        Tier_Name, 
        Tier_Name, 
        Tier_Name 
      FROM Temp_CDM_Customer_Master  
      WHERE  
        Tier_Name <> '' 
      GROUP BY Tier_Name; 
 
    UPDATE IGNORE Temp_CDM_Customer_Master AS A 
    SET Tier_Name_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Tier_Name = B.LOV_Key 
        AND B.LOV_Attribute = 'TIER_NAME'  
    );   
             
 
    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'ETHNICITY', 
        Ethnicity, 
        Ethnicity, 
        Ethnicity 
      FROM Temp_CDM_Customer_Master  
      WHERE  
        Ethnicity <> '' 
      GROUP BY Ethnicity; 
 
    UPDATE IGNORE Temp_CDM_Customer_Master AS A 
    SET Ethnicity_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Ethnicity = B.LOV_Key 
        AND B.LOV_Attribute = 'ETHNICITY'  
    );       
     
      
      
      
     INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'CUSTOMER_CUSTOM_TAG1', 
        Customer_Custom_Tag1, 
        Customer_Custom_Tag1, 
        Customer_Custom_Tag1 
      FROM Temp_CDM_Customer_Master  
      WHERE  
        Customer_Custom_Tag1 <> '' 
      GROUP BY Customer_Custom_Tag1; 
 
    UPDATE IGNORE Temp_CDM_Customer_Master AS A 
    SET Customer_Custom_Tag1_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Customer_Custom_Tag1 = B.LOV_Key 
        AND B.LOV_Attribute = 'CUSTOMER_CUSTOM_TAG1') ; 
         
     
     INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'CUSTOMER_CUSTOM_TAG2', 
        Customer_Custom_Tag2, 
        Customer_Custom_Tag2, 
        Customer_Custom_Tag2 
      FROM Temp_CDM_Customer_Master  
      WHERE  
        Customer_Custom_Tag2 <> '' 
      GROUP BY Customer_Custom_Tag2; 
 
    UPDATE IGNORE Temp_CDM_Customer_Master AS A 
    SET Customer_Custom_Tag2_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Customer_Custom_Tag2 = B.LOV_Key 
        AND B.LOV_Attribute = 'CUSTOMER_CUSTOM_TAG2') ; 
         
     
     INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'CUSTOMER_CUSTOM_TAG3', 
        Customer_Custom_Tag3, 
        Customer_Custom_Tag3, 
        Customer_Custom_Tag3 
      FROM Temp_CDM_Customer_Master  
      WHERE  
        Customer_Custom_Tag3 <> '' 
      GROUP BY Customer_Custom_Tag3; 
 
    UPDATE IGNORE Temp_CDM_Customer_Master AS A 
    SET Customer_Custom_Tag3_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Customer_Custom_Tag3 = B.LOV_Key 
        AND B.LOV_Attribute = 'CUSTOMER_CUSTOM_TAG3') ; 


    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'EMAIL_CONSENT', 
        Email_Consent, 
        Email_Consent, 
        Email_Consent 
      FROM Temp_CDM_Customer_Master  
      WHERE  
        Email_Consent <> ''  
      GROUP BY Email_Consent; 
 
    UPDATE IGNORE Temp_CDM_Customer_Master AS A 
    SET Email_Consent_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Email_Consent = B.LOV_Key 
        AND B.LOV_Attribute = 'EMAIL_CONSENT'  
    ); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value, 
      LOV_Comm_Value 
    ) 
      SELECT  
        'SMS_CONSENT', 
        SMS_Consent, 
        SMS_Consent, 
        SMS_Consent 
      FROM Temp_CDM_Customer_Master  
      WHERE  
        SMS_Consent <> ''  
      GROUP BY SMS_Consent; 
 
    UPDATE IGNORE Temp_CDM_Customer_Master AS A 
    SET SMS_Consent_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.SMS_Consent = B.LOV_Key 
        AND B.LOV_Attribute = 'SMS_CONSENT'  
    );  



    IF vTreat_As_Incr = 1 THEN 
               
      CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Customer_Master', 1); 
 
      INSERT IGNORE INTO CDM_Customer_PII_Master_Hist ( 
        Customer_Id, 
        Is_Processed, 
        Mobile, 
        Email, 
        First_Name, 
        Middle_Name, 
        Last_Name, 
        Full_Name, 
        Address_Line1, 
        Address_Line2, 
        Latitude, 
        Longitude 
      ) 
        SELECT 
          A.Customer_Id, 
          A.Is_Processed, 
          A.Mobile, 
          A.Email, 
          A.First_Name, 
          A.Middle_Name, 
          A.Last_Name, 
          A.Full_Name, 
          A.Address_Line1, 
          A.Address_Line2, 
          A.Latitude, 
          A.Longitude 
        FROM 
          CDM_Customer_PII_Master AS A, 
          Temp_CDM_Customer_Master AS B 
        WHERE 
          A.Customer_Id = B.Customer_Id; 
 
      CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', CONCAT(vStart_Cnt,'_5'), 'CDM_ETL_Customer_Master', 1); 
 
      UPDATE IGNORE  
          CDM_Customer_PII_Master AS A, 
          Temp_CDM_Customer_Master AS B 
      SET  
        A.Is_Processed = -1, 
        A.Mobile = B.Mobile, 
        A.Email = B.Email, 
        A.First_Name = B.First_Name, 
        A.Middle_Name = B.Middle_Name, 
        A.Last_Name = B.Last_Name, 
        A.Full_Name = B.Full_Name, 
        A.Address_Line1 = B.Address_Line1, 
        A.Address_Line2 = B.Address_Line2, 
        A.Latitude = B.Latitude, 
        A.Longitude = B.Longitude 
      WHERE 
        A.Customer_Id = B.Customer_Id; 
 
      CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', CONCAT(vStart_Cnt,'_6'), 'CDM_ETL_Customer_Master', 1); 
 
      INSERT IGNORE INTO CDM_Customer_Master_Hist ( 
        Customer_Id, 
        Is_Processed, 
        Comm_Name, 
        Is_New_Cust_Flag, 
        Email_Domain, 
        City, 
        Doj, 
        Doj_Year, 
        Doj_Year_Month, 
        Doj_Day_Week, 
        Enrolled_Store_Id, 
        Enrolled_Channel_LOV_Id, 
        Gender_LOV_Id, 
        Dob, 
        Dob_Year, 
        Dob_Year_Month, 
        Dob_Day_Week, 
        Pincode, 
        Marital_Status_LOV_Id, 
        Members_In_Household, 
        Email_Consent_LOV_Id, 
        SMS_Consent_LOV_Id, 
        Customer_State, 
        DoA, 
        DoA_Year, 
        DoA_Year_Month, 
        DoA_Day_Week, 
        DND, 
        Ethnicity_LOV_Id, 
        Tier_Name_LOV_Id, 
        Point_Balance, 
        Customer_Custom_Tag1_LOV_Id, 
        Customer_Custom_Tag2_LOV_Id, 
        Customer_Custom_Tag3_LOV_Id 
      ) 
        SELECT 
          A.Customer_Id, 
          A.Is_Processed, 
          A.Comm_Name, 
          A.Is_New_Cust_Flag, 
          A.Email_Domain, 
          A.City, 
          A.Doj, 
          A.Doj_Year, 
          A.Doj_Year_Month, 
          A.Doj_Day_Week, 
          A.Enrolled_Store_Id, 
          A.Enrolled_Channel_LOV_Id, 
          A.Gender_LOV_Id, 
          A.Dob, 
          A.Dob_Year, 
          A.Dob_Year_Month, 
          A.Dob_Day_Week, 
          A.Pincode, 
          A.Marital_Status_LOV_Id, 
          A.Members_In_Household, 
          A.Email_Consent_LOV_Id, 
          A.SMS_Consent_LOV_Id, 
          A.Customer_State, 
          A.DoA, 
          A.DoA_Year, 
          A.DoA_Year_Month, 
          A.DoA_Day_Week, 
          A.DND, 
          A.Ethnicity_LOV_Id, 
          A.Tier_Name_LOV_Id, 
          A.Point_Balance, 
          A.Customer_Custom_Tag1_LOV_Id, 
          A.Customer_Custom_Tag2_LOV_Id, 
          A.Customer_Custom_Tag3_LOV_Id 
        FROM 
          CDM_Customer_Master AS A, 
          Temp_CDM_Customer_Master AS B 
        WHERE 
          A.Customer_Id = B.Customer_Id;             
 
      CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_Customer_Master', 1); 
 
      UPDATE IGNORE  
        CDM_Customer_Master AS A, 
        Temp_CDM_Customer_Master AS B 
      SET  
        A.Is_Processed = -1, 
        A.Comm_Name =B.Comm_Name, 
        A.Is_New_Cust_Flag = 0, 
        A.Email_Domain = CDM_Get_Email_Domain(B.Email), 
        A.City = B.City, 
        A.Doj = B.Doj, 
        A.Doj_Year= IFNULL(YEAR(B.Doj),''), 
        A.Doj_Year_Month = IFNULL(EXTRACT(YEAR_MONTH FROM B.Doj),''), 
        A.Doj_Day_Week = IFNULL(WEEKDAY(B.Doj),-1), 
        A.Enrolled_Store_Id = IFNULL(B.Enrolled_Store_Id,-1), 
        A.Enrolled_Channel_LOV_Id = B.Enrolled_Channel_LOV_Id, 
        A.Gender_LOV_Id = B.Gender_LOV_Id, 
        A.Dob = B.Dob, 
        A.Dob_Year = IFNULL(YEAR(B.Dob),''), 
        A.Dob_Year_Month = IFNULL(EXTRACT(YEAR_MONTH FROM B.Dob),''), 
        A.Dob_Day_Week = IFNULL(WEEKDAY(B.Dob),-1), 
        A.Pincode = B.Pincode, 
        A.Marital_Status_LOV_Id = B.Marital_Status_LOV_Id, 
        A.Members_In_Household = B.Members_In_Household, 
        A.Email_Consent_LOV_Id = B.Email_Consent_LOV_Id, 
        A.SMS_Consent_LOV_Id = B.SMS_Consent_LOV_Id, 
        A.Customer_State = B.Customer_State, 
        A.DoA = B.DoA, 
        A.DoA_Year = IFNULL(YEAR(B.DoA),''), 
        A.DoA_Year_Month = IFNULL(EXTRACT(YEAR_MONTH FROM B.DoA),''), 
        A.DoA_Day_Week = IFNULL(WEEKDAY(B.DoA),-1), 
        A.DND = B.DND, 
        A.Ethnicity_LOV_Id = B.Ethnicity_LOV_Id, 
        A.Tier_Name_LOV_Id = B.Tier_Name_LOV_Id, 
        A.Point_Balance = B.Point_Balance, 
        A.Customer_Custom_Tag1_LOV_Id = B.Customer_Custom_Tag1_LOV_Id, 
        A.Customer_Custom_Tag2_LOV_Id = B.Customer_Custom_Tag2_LOV_Id, 
        A.Customer_Custom_Tag3_LOV_Id = B.Customer_Custom_Tag3_LOV_Id 
      WHERE 
        A.Customer_Id = B.Customer_Id; 
    END IF; 
 
             
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', CONCAT(vStart_Cnt,'_8'), 'CDM_ETL_Customer_Master', 1); 
 
    INSERT IGNORE INTO CDM_Customer_PII_Master ( 
      Customer_Id, 
      Is_Processed, 
      Mobile, 
      Email, 
      First_Name, 
      Middle_Name, 
      Last_Name, 
      Full_Name, 
      Address_Line1, 
      Address_Line2, 
      Latitude, 
      Longitude 
    )     
      SELECT  
        Customer_Id, 
        Rec_Is_Processed, 
        Mobile, 
        Email, 
        First_Name, 
        Middle_Name, 
        Last_Name, 
        Full_Name, 
        Address_Line1, 
        Address_Line2, 
        Latitude, 
        Longitude 
      FROM Temp_CDM_Customer_Master; 
 
             
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', CONCAT(vStart_Cnt,'_9'), 'CDM_ETL_Customer_Master', 1); 
     
    INSERT IGNORE INTO  CDM_Customer_Master ( 
      Customer_Id, 
      Comm_Name, 
      Is_New_Cust_Flag, 
      Email_Domain, 
      City, 
      Doj, 
      Doj_Year, 
      Doj_Year_Month, 
      Doj_Day_Week, 
      Enrolled_Store_Id, 
      Enrolled_Channel_LOV_Id, 
      Gender_LOV_Id, 
      Dob, 
      Dob_Year, 
      Dob_Year_Month, 
      Dob_Day_Week, 
      Pincode, 
      Marital_Status_LOV_Id, 
      Members_In_Household, 
      Email_Consent_LOV_Id, 
      SMS_Consent_LOV_Id, 
      Customer_State, 
      DoA, 
      DoA_Year, 
      DoA_Year_Month, 
      DoA_Day_Week, 
      DND, 
      Ethnicity_LOV_Id, 
      Tier_Name_LOV_Id, 
      Point_Balance, 
      Customer_Custom_Tag1_LOV_Id, 
      Customer_Custom_Tag2_LOV_Id, 
      Customer_Custom_Tag3_LOV_Id 
  ) 
      SELECT  
        Customer_Id, 
        Comm_Name, 
        1, 
        CDM_Get_Email_Domain(Email), 
        City, 
        Doj, 
        IFNULL(YEAR(Doj),''), 
        IFNULL(EXTRACT(YEAR_MONTH FROM Doj),''),   
        IFNULL(WEEKDAY(Doj),-1), 
        IFNULL(Enrolled_Store_Id,-1), 
        Enrolled_Channel_LOV_Id, 
        Gender_LOV_Id, 
        Dob, 
        IFNULL(YEAR(Dob),''), 
        IFNULL(EXTRACT(YEAR_MONTH FROM Dob),''),   
        IFNULL(WEEKDAY(Dob),-1), 
        Pincode, 
        Marital_Status_LOV_Id, 
        Members_In_Household, 
        Email_Consent_LOV_Id, 
        SMS_Consent_LOV_Id, 
        Customer_State, 
        DoA, 
        IFNULL(YEAR(DoA),''), 
        IFNULL(EXTRACT(YEAR_MONTH FROM DoA),''),   
        IFNULL(WEEKDAY(DoA),-1), 
        DND, 
        Ethnicity_LOV_Id, 
        Tier_Name_LOV_Id, 
        Point_Balance, 
        Customer_Custom_Tag1_LOV_Id, 
        Customer_Custom_Tag2_LOV_Id, 
        Customer_Custom_Tag3_LOV_Id 
      FROM Temp_CDM_Customer_Master; 
 
    SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_CUST_HIST; 
        END IF; 
         
  END LOOP PROCESS_CUST_HIST; 
   
   
  CALL CDM_Update_Process_Log('CDM_ETL_Customer_Master', 2, 'CDM_ETL_Customer_Master', 1); 
 
END$$
DELIMITER ;
