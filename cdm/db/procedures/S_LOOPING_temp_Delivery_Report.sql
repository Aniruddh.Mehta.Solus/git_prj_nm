DELIMITER $$
CREATE OR REPLACE  PROCEDURE S_LOOPING_temp_Delivery_Report(IN vStart_DateId bigint,IN vEnd_DateId bigint,IN vBatchSize bigint)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
select vStart_DateId into @x ;
select vEnd_DateId into @y ;
SELECT Customer_Id INTO vStart_Cnt FROM CDM_Delivery_Report where Event_Execution_Date_ID between  @x and  @y  ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt FROM CDM_Delivery_Report where Event_Execution_Date_ID between    @x and  @y  ORDER BY Customer_Id DESC LIMIT 1;

PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;
insert ignore into  temp_CDM_Delivery_Report 
select * from CDM_Delivery_Report where Customer_Id between vStart_Cnt and vEnd_Cnt and Event_Execution_Date_ID between    @x and  @y   ; 
SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP;  
END$$
DELIMITER ;
