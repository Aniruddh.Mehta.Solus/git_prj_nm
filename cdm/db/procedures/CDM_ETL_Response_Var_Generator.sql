DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_ETL_Response_Var_Generator ()
BEGIN
DECLARE done,done2 INT DEFAULT False; 
DECLARE V_ID BIGINT(20);
DECLARE V_ID_2 BIGINT(20);
DECLARE V_Sale_Qty decimal(10,2);
DECLARE V_BillHeader varchar(255);

 -- Fectch Customer Id post svoc run 



		DECLARE curCust cursor for 
		SELECT
        	Customer_Id FROM CDM_Customer_TP_Var; --  where Modified_Date>= date_sub(curdate(),interval 3 day)  ;

		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;								
		set SQL_SAFE_UPDATES=0;
      	        TRUNCATE temp_CDM_Customer_CRM_Resp_Var;
		
		 -- Start Cursor for Inserting Points for Customers 
		OPEN curCust;
		LOOP_COMMTEMPLATES: LOOP
			FETCH curCust into V_ID;
			IF done THEN
				LEAVE LOOP_COMMTEMPLATES;
			END IF;
            		delete from CDM_Customer_CRM_Resp_Var where Customer_Id=V_ID; 
            		insert INTO temp_CDM_Customer_CRM_Resp_Var 
            		(Customer_Id, Event_Execution_Date_ID, Event_ID, Communication_Template_ID, Offer_Expiry_End_Date_ID, Offer_Code, rank, No_Of_Comm_In_Last_7Days, No_Of_Comm_In_Last_30Days, Lt_No_Of_Comm, No_Of_SMS_In_Last_7Days, No_Of_SMS_In_Last_30Days, Lt_No_Of_SMS, No_Of_EMAIL_In_Last_7Days, No_Of_EMAIL_In_Last_30Days, Lt_No_Of_EMAIL, No_Of_PN_In_Last_7Days, No_Of_PN_In_Last_30Days, Lt_No_Of_PN)
            		select eeh.Customer_Id,Event_Execution_Date_ID,Event_ID,Communication_Template_ID,ct.Offer_Expiry_End_Date_ID,eeh.Offer_Code,
					 rank() OVER (partition by eeh.Customer_Id order by Event_Execution_Date_ID desc ) as rank,
					 case when Event_Execution_Date_ID between ADDDATE(CURDATE(), INTERVAL  -7 Day) and CURDATE() then 1 else 0 end as No_Of_Comm_In_Last_7Days,
					 case when Event_Execution_Date_ID between ADDDATE(CURDATE(), INTERVAL  -30 Day) and CURDATE()  then 1 else 0 end as No_Of_Comm_In_Last_30Days,
					 case when Event_Execution_Date_ID is not null  then 1 else 0 end as Lt_No_Of_Comm,
					 case when Event_Execution_Date_ID between ADDDATE(CURDATE(), INTERVAL  -7 Day) and CURDATE() and eeh.Communication_Channel='SMS'  then 1 else 0 end as No_Of_SMS_In_Last_7Days,
					 case when Event_Execution_Date_ID between ADDDATE(CURDATE(), INTERVAL  -30 Day) and CURDATE() and eeh.Communication_Channel='SMS'  then 1 else 0 end as No_Of_SMS_In_Last_30Days,
					 case when eeh.Communication_Channel='SMS' then 1 else 0 end as Lt_No_Of_SMS,
					 case when Event_Execution_Date_ID between ADDDATE(CURDATE(), INTERVAL  -7 Day) and CURDATE() and eeh.Communication_Channel='EMAIL'  then 1 else 0 end as No_Of_EMAIL_In_Last_7Days,
					 case when Event_Execution_Date_ID between ADDDATE(CURDATE(), INTERVAL  -30 Day) and CURDATE() and eeh.Communication_Channel='EMAIL'  then 1 else 0 end as No_Of_EMAIL_In_Last_30Days,
					 case when eeh.Communication_Channel='EMAIL' then 1 else 0 end as Lt_No_Of_EMAIL,
					 case when Event_Execution_Date_ID between ADDDATE(CURDATE(), INTERVAL  -7 Day) and CURDATE() and eeh.Communication_Channel='PN'  then 1 else 0 end as No_Of_PN_In_Last_7Days,
					 case when Event_Execution_Date_ID between ADDDATE(CURDATE(), INTERVAL  -30 Day) and CURDATE() and eeh.Communication_Channel='PN'  then 1 else 0 end as No_Of_PN_In_Last_30Days,
					 case when eeh.Communication_Channel='PN' then 1 else 0 end as Lt_No_Of_PN
	        		from Event_Execution_History eeh join Communication_Template ct on eeh.Communication_Template_ID=ct.id 
            		where eeh.Customer_Id=V_ID; 

        			insert ignore into CDM_Customer_CRM_Resp_Var
					(Customer_Id,Last_Comm_Date,Last_Event_Id,Last_Comm_Template_Code,Current_Voucher_Expiry_Date,
					 Offer_Code,No_Of_Comm_In_Last_7Days,No_Of_Comm_In_Last_30Days,Lt_No_Of_Comm, No_Of_SMS_In_Last_7Days,
					 No_Of_SMS_In_Last_30Days,Lt_No_Of_SMS, No_Of_EMAIL_In_Last_7Days, No_Of_EMAIL_In_Last_30Days, Lt_No_Of_EMAIL,  No_Of_PN_In_Last_7Days,
					  No_Of_PN_In_Last_30Days, Lt_No_Of_PN)  
					select Customer_Id,
					 max(case when rank =1 then Event_Execution_Date_ID end) as Last_Comm_Date, 
					 max(case when rank =1 then Event_ID end ) as Last_Event_Id ,
					 max(case when rank =1 then Communication_Template_ID end ) as Last_Comm_Template_Code,
					 max(case when rank =1 then Offer_Expiry_End_Date_ID end ) as Current_Voucher_Expiry_Date,
					 max(case when rank =1 then Offer_Code end ) as Offer_Code,
					 sum(No_Of_Comm_In_Last_7Days) as No_Of_Comm_In_Last_7Days,
					 sum(No_Of_Comm_In_Last_30Days) as No_Of_Comm_In_Last_30Days,
					 sum(Lt_No_Of_Comm) as Lt_No_Of_Comm,
					 sum(No_Of_SMS_In_Last_7Days) as No_Of_SMS_In_Last_7Days,
					 sum(No_Of_SMS_In_Last_30Days) as No_Of_SMS_In_Last_30Days,
					 sum(Lt_No_Of_SMS) as Lt_No_Of_SMS,
					 sum(No_Of_EMAIL_In_Last_7Days) as No_Of_EMAIL_In_Last_7Days,
					 sum(No_Of_EMAIL_In_Last_30Days) as No_Of_EMAIL_In_Last_30Days,
					 sum(Lt_No_Of_EMAIL) as Lt_No_Of_EMAIL,
					 sum(No_Of_PN_In_Last_7Days) as No_Of_PN_In_Last_7Days,
					 sum(No_Of_PN_In_Last_30Days) as No_Of_PN_In_Last_30Days, + sum(Lt_No_Of_PN) as Lt_No_Of_PN
        				from    temp_CDM_Customer_CRM_Resp_Var  where Customer_Id=V_ID; 

        		END LOOP LOOP_COMMTEMPLATES;
		CLOSE curCust;



END$$
DELIMITER ;
