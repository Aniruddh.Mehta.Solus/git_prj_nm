DELIMITER $$
CREATE or replace PROCEDURE  CDM_ETL_Feedback (IN vBatchSize BIGINT,IN vTreat_As_Incr TINYINT)
BEGIN 
 
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
     
  SET vEnd_Cnt = vBatchSize; 
 
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Feedback ORDER BY Rec_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
   
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Feedback ORDER BY Rec_Id DESC LIMIT 1; 
 
 
    IF @Rec_Cnt IS NULL THEN  
    SET @Rec_Cnt = 0; 
  END IF; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Feedback', 1, 'CDM_ETL_Feedback', 1); 
     
  PROCESS_BH_HIST: LOOP 
 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Feedback', CONCAT(vStart_Cnt,'_1'), 'CDM_ETL_Feedback', 1); 
 
    UPDATE IGNORE CDM_Stg_Feedback 
    SET Fdbk_Store_Id = ( 
        SELECT Store_Id 
        FROM CDM_Store_Key_Lookup 
        WHERE CDM_Store_Key_Lookup.Store_Key = CDM_Stg_Feedback.Fdbk_Store_Key 
      ) 
    WHERE 
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Fdbk_Store_Key  <> '' AND 
      Rec_Is_Processed = -1; 
 
    UPDATE IGNORE CDM_Stg_Feedback 
    SET Fdbk_Store_LOV_Id = ( 
        SELECT CDM_Store_Master.LOV_Id 
        FROM CDM_Store_Key_Lookup,CDM_Store_Master 
        WHERE 
          CDM_Store_Key_Lookup.Store_Id =CDM_Store_Master.Store_Id         
          AND CDM_Store_Key_Lookup.Store_Key = CDM_Stg_Feedback.Fdbk_Store_Key 
      ) 
    WHERE 
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Fdbk_Store_Key  <> '' AND 
      Rec_Is_Processed = -1; 
 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Feedback', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Feedback', 1); 
 
    UPDATE IGNORE CDM_Stg_Feedback 
    SET Fdbk_Customer_Id = ( 
        SELECT Customer_Id 
        FROM CDM_Customer_Key_Lookup 
        WHERE  
          CDM_Customer_Key_Lookup.Customer_Key = CDM_Stg_Feedback.Fdbk_Customer_Key 
      ) 
    WHERE  
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Fdbk_Customer_Key <> '' AND 
      Rec_Is_Processed = -1; 
 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Feedback', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Feedback', 1); 
 
    UPDATE IGNORE CDM_Stg_Feedback 
    SET Fdbk_Brand_Id = ( 
        SELECT Brand_Id 
        FROM CDM_Brand_Key_Lookup 
        WHERE  
          CDM_Brand_Key_Lookup.Brand_Key = CDM_Stg_Feedback.Fdbk_Brand_Key 
      ) 
    WHERE  
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Fdbk_Brand_Key <> '' AND 
      Rec_Is_Processed = -1; 
 
    UPDATE IGNORE CDM_Stg_Feedback 
    SET Fdbk_Brand_LOV_Id = ( 
        SELECT CDM_Brand_Master.LOV_Id 
        FROM CDM_Brand_Key_Lookup, CDM_Brand_Master 
        WHERE  
          CDM_Brand_Key_Lookup.Brand_Id = CDM_Brand_Master.Brand_Id 
          AND CDM_Brand_Key_Lookup.Brand_Key = CDM_Stg_Feedback.Fdbk_Brand_Key 
      ) 
    WHERE  
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Fdbk_Brand_Key <> '' AND 
      Rec_Is_Processed = -1; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_Feedback', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Feedback', 1); 
 
     
    INSERT IGNORE INTO CDM_Feedback_Key_Lookup ( 
      Fdbk_Key 
    ) 
      SELECT  
        Fdbk_Key 
      FROM  
        CDM_Stg_Feedback 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Feedback', CONCAT(vStart_Cnt,'_4.1'), 'CDM_ETL_Feedback', 1); 
 
     
 
    UPDATE IGNORE CDM_Stg_Feedback 
        SET Fdbk_Id = ( 
      SELECT Fdbk_Id  
            FROM CDM_Feedback_Key_Lookup 
            WHERE 
        CDM_Stg_Feedback.Fdbk_Key = CDM_Feedback_Key_Lookup.Fdbk_Key 
    ) 
    WHERE  
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Fdbk_Id <= 0 AND 
      Rec_Is_Processed = -1; 
     
     
    CALL CDM_Update_Process_Log('CDM_ETL_Feedback', CONCAT(vStart_Cnt,'_4.2'), 'CDM_ETL_Feedback', 1); 
 
    INSERT IGNORE INTO CDM_Feedback( 
      Fdbk_Id, 
      Fdbk_Brand_Id, 
      Fdbk_Brand_LOV_Id, 
      Fdbk_Store_Id, 
      Fdbk_Store_LOV_Id, 
      Fdbk_Customer_Id, 
      Fdbk_Customer_Gender, 
      Fdbk_Customer_Occupation, 
      Fdbk_Score, 
      Fdbk_Product_Variety_Score, 
      Fdbk_Product_Quality_Score, 
      Fdbk_Product_Pricing_Score, 
      Fdbk_Product_Relevance_Score, 
      Fdbk_Product_Availability_Score, 
      Fdbk_Survey_Time, 
      Fdbk_Survey_Date, 
      Fdbk_Survey_Year, 
      Fdbk_Survey_Year_Month, 
      Fdbk_Survey_Day_Week, 
      Fdbk_Staff_Service_Rating, 
      Fdbk_Duration, 
      Fdbk_L1Rsn 
    ) 
      SELECT 
        Fdbk_Id, 
        Fdbk_Brand_Id, 
        Fdbk_Brand_LOV_Id, 
        Fdbk_Store_Id, 
        Fdbk_Store_LOV_Id, 
        Fdbk_Customer_Id, 
        Fdbk_Customer_Gender, 
        Fdbk_Customer_Occupation, 
        Fdbk_Score, 
        Fdbk_Product_Variety_Score, 
        Fdbk_Product_Quality_Score, 
        Fdbk_Product_Pricing_Score, 
        Fdbk_Product_Relevance_Score, 
        Fdbk_Product_Availability_Score, 
        Fdbk_Survey_Time, 
        Fdbk_Survey_Date, 
        IFNULL(YEAR(Fdbk_Survey_Date),''), 
        IFNULL(EXTRACT(YEAR_MONTH FROM Fdbk_Survey_Date),''), 
        IFNULL(WEEKDAY(Fdbk_Survey_Date),-1), 
        Fdbk_Staff_Service_Rating, 
        Fdbk_Duration, 
        Fdbk_L1Rsn 
      FROM  
        CDM_Stg_Feedback 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Feedback', CONCAT(vStart_Cnt,'_5'), 'CDM_ETL_Feedback', 1); 
 
      SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_BH_HIST; 
        END IF; 
         
  END LOOP PROCESS_BH_HIST; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Feedback', 2, 'CDM_ETL_Feedback', 1); 
END$$
DELIMITER ;
