DELIMITER $$
CREATE OR REPLACE PROCEDURE  S_Bill_For_Recommendation (IN vBillStartDate Date, IN vBatchSize BIGINT)
BEGIN
DECLARE vStart_Cnt BIGINT;
DECLARE vEnd_Cnt BIGINT;
DECLARE vRec_Cnt BIGINT;
-- delete from Bill_For_Recommendation where Bill_Date >= vBillStartDate ;
SELECT Customer_id INTO vStart_Cnt FROM CDM_Bill_Details where Bill_Date >= vBillStartDate ORDER BY Customer_id ASC LIMIT 1;
SELECT Customer_id INTO vRec_Cnt FROM CDM_Bill_Details where Bill_Date >= vBillStartDate  ORDER BY Customer_id DESC LIMIT 1;
delete from log_solus where module='S_Bill_For_Recommendation';
insert into log_solus(module,vStartCnt,vEndCnt) values ('S_Bill_For_Recommendation',vStart_Cnt,vRec_Cnt);

PROCESS_PROCESS_LARGE_DATA: LOOP
SET vEnd_Cnt=vStart_Cnt+vBatchSize;
insert into log_solus(module,vStartCnt,vEndCnt) values ('S_Bill_For_Recommendation',vStart_Cnt,vEnd_Cnt);
delete from Bill_For_Recommendation where Customer_Id between vStart_Cnt and vEnd_Cnt and Bill_Date >= vBillStartDate   ;

INSERT IGNORE INTO Bill_For_Recommendation
(Customer_Id,
Cohort_Id,
Segment_Id,
Product_Genome_LOV_Id,
Product_Genome,
Bill_Date,
StoreVisitDate,
Bill_Date_Month,
Sale_Qty,
Sale_Gross_Val,
Sale_Net_Val,
Sale_Disc_Val,
DisByGross)
   SELECT 
        CDM_BD.Customer_Id AS Customer_Id,
		segments.Seg_For_model_1 AS Cohort_Id,
        segments.Seg_For_model_1 AS Segment_Id,
		CDM_PM.Product_Genome_LOV_Id AS Product_Genome_LOV_Id,
        CDM_PM.Product_Genome AS Product_Genome,
        CDM_BD.Bill_Date AS Bill_Date,
        CDM_BD.Bill_Date AS StoreVisitDate,
		MONTH(CDM_BD.Bill_Date) AS Bill_Date_Month,
        Sum(CDM_BD.Sale_Qty) AS Sale_Qty,
        Sum(CDM_BD.Sale_Gross_Val) AS Sale_Gross_Val,
        Sum(CDM_BD.Sale_Net_Val) AS Sale_Net_Val,
        Sum(CDM_BD.Sale_Disc_Val) AS Sale_Disc_Val,
        round(Sum(CDM_BD.Sale_Disc_Val) / Sum(CDM_BD.Sale_Gross_Val),2) AS DisByGross
    FROM
        CDM_Bill_Details CDM_BD, CDM_Product_Master_Original CDM_PM, svoc_segments segments
    WHERE 
			CDM_BD.Customer_Id between vStart_Cnt and vEnd_Cnt 
            AND Bill_Date >= vBillStartDate 
            AND CDM_BD.Product_Id = CDM_PM.Product_Id
            AND CDM_BD.Customer_Id = segments.Customer_Id 
            AND CDM_PM.Product_Genome_LOV_Id > 0
            AND CDM_PM.To_Be_Excluded <> 1
            group by Customer_Id, Product_Genome_LOV_Id,Bill_Date;
            
SET vStart_Cnt = vEnd_Cnt+1;
IF vStart_Cnt  >= vRec_Cnt THEN
     LEAVE PROCESS_PROCESS_LARGE_DATA;
END IF;
END LOOP PROCESS_PROCESS_LARGE_DATA;
END$$
DELIMITER ;
