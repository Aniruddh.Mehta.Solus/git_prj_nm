DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Product_Ingredients () 
BEGIN 
  CALL CDM_Update_Process_Log('CDM_ETL_Product_Ingredients', 1, 'CDM_ETL_Product_Ingredients', 1); 
 
  UPDATE IGNORE CDM_Stg_Product_Ingredients AS A 
    SET Product_Id = ( 
    SELECT Product_Id  
        FROM CDM_Product_Key_Lookup AS B 
        WHERE A.Product_Key = B.Product_Key 
    ); 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Product_Ingredients', 2, 'CDM_ETL_Product_Ingredients', 1); 
 
  UPDATE IGNORE CDM_Stg_Product_Ingredients AS A 
    SET Parent_Product_Id = ( 
    SELECT Product_Id  
        FROM CDM_Product_Key_Lookup AS B 
        WHERE A.Parent_Product_Key = B.Product_Key 
    ); 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Product_Ingredients', 3, 'CDM_ETL_Product_Ingredients', 1); 
 
  INSERT IGNORE INTO CDM_Product_Ingredients ( 
    Product_Id, 
    Parent_Product_Id 
  ) 
    SELECT  
      Product_Id, 
      Parent_Product_Id 
    FROM CDM_Stg_Product_Ingredients 
        WHERE Rec_Is_Processed = -1; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Product_Ingredients', 4, 'CDM_ETL_Product_Ingredients', 1); 
 
END	
mysolus$$
