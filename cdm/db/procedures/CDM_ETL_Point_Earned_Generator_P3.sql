DELIMITER $$
CREATE or replace PROCEDURE  CDM_ETL_Point_Earned_Generator_P3 (IN vTreat_As_Incr TINYINT)
BEGIN
DECLARE done,done2 INT DEFAULT False; 
DECLARE V_ID BIGINT(20);
DECLARE V_Sale_Qty decimal(10,2);
DECLARE V_BillHeader varchar(255);

 -- Fectch Customer Id post svoc run 

     -- IF vTreat_As_Incr = 0 THEN
		DECLARE curCust cursor for 
		select Customer_Key
		from CDM_Bill_Details a,
		CDM_Customer_Master b ,
		CDM_Customer_Key_Lookup c
		where
		a.Customer_Id=b.Customer_Id and
		b.Customer_Id=c.Customer_Id and 
		Sale_Qty>0
		group by a.Customer_Id;


		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;								
		TRUNCATE CDM_Stg_Points_Earned ;
		set SQL_SAFE_UPDATES=0;
		
		 -- Start Cursor for Inserting Points for Customers 
		OPEN curCust;
		LOOP_COMMTEMPLATES: LOOP
			FETCH curCust into V_ID;
			IF done THEN
				LEAVE LOOP_COMMTEMPLATES;
			END IF;
			select V_ID;
			insert into CDM_Stg_Points_Earned (Points_Earned_Rec_Key, Customer_Key, Points_Earned)
			select Customer_Key,Customer_Key,
			CASE 
			WHEN sum(Sale_Qty) BETWEEN 1 AND 10 THEN sum(Sale_Qty)*0.5
			WHEN sum(Sale_Qty) BETWEEN 11 AND 20 THEN sum(Sale_Qty)*1
			WHEN sum(Sale_Qty) BETWEEN 30 AND 40  THEN sum(Sale_Qty)*1.5
			WHEN sum(Sale_Qty) BETWEEN 40 AND 60  THEN sum(Sale_Qty)*2
			ELSE 0
			END
			from CDM_Bill_Details a,
			CDM_Customer_Master b ,
			CDM_Customer_Key_Lookup c
			where
			a.Customer_Id=b.Customer_Id and
			b.Customer_Id=c.Customer_Id and 
			Sale_Qty>0
			group by Customer_Key;
			
			
		END LOOP LOOP_COMMTEMPLATES;
		CLOSE curCust;


	 -- END IF;

END$$
DELIMITER ;
