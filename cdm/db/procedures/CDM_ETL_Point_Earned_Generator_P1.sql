DELIMITER $$
CREATE or replace PROCEDURE  CDM_ETL_Point_Earned_Generator_P1 (IN vTreat_As_Incr TINYINT)
BEGIN
DECLARE done,done2 INT DEFAULT False; 
DECLARE V_ID BIGINT(20);
DECLARE V_Sale_Qty decimal(10,2);
DECLARE V_BillHeader varchar(255);

 -- Fectch Customer Id post svoc run 

     -- IF vTreat_As_Incr = 0 THEN
		DECLARE curCust cursor for 
		SELECT
		d.Customer_Key
		FROM
		CDM_Customer_Time_Dependent_Var a,
		CDM_Customer_Master b,
		CDM_Bill_Details c ,
		CDM_Customer_Key_Lookup d
		WHERE
		a.Customer_Id=b.Customer_Id and 
		a.Customer_Id=c.Customer_Id and
		a.Customer_Id=d.Customer_Id and 
		c.Product_Id in (6,20,22,26,31,76,80,81,98,99,109) and 
		c.Bill_Date between '2021-10-23' and '2021-12-31' and
		b.City in  ('Ahmedabad','Surat','Baroda','Saurashtra') 
		GROUP BY d.Customer_Id;
		
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;								
		TRUNCATE CDM_Stg_Points_Earned ;
		set SQL_SAFE_UPDATES=0;
		
		 -- Start Cursor for Inserting Points for Customers 
		OPEN curCust;
		LOOP_COMMTEMPLATES: LOOP
			FETCH curCust into V_ID;
			IF done THEN
				LEAVE LOOP_COMMTEMPLATES;
			END IF;
			select V_ID,V_BillHeader;
			insert into CDM_Stg_Points_Earned (Points_Earned_Rec_Key, Bill_Header_Key, Customer_Key, Points_Earned,Earned_Date)
			select b.Customer_Key ,c.Bill_Header_Key,b.Customer_Key, 
			CASE 
			WHEN sum(Sale_Qty) BETWEEN 50 AND 149 THEN sum(Sale_Qty)*0.5
			WHEN sum(Sale_Qty) BETWEEN 150 AND 299 THEN sum(Sale_Qty)*1
			WHEN sum(Sale_Qty) >= 300 THEN sum(Sale_Qty)*1.5
			ELSE 0	
			END,
			a.Bill_Date
			from CDM_Bill_Details  a,
			CDM_Customer_Key_Lookup b,
			CDM_Bill_Header_Key_Lookup c,
			CDM_Customer_Master d
			where 
			a.Customer_Id=b.Customer_Id
			AND c.Bill_Header_Id=a.Bill_Header_Id
			AND a.Customer_Id=d.Customer_Id
			AND b.Customer_Key=V_ID
			and a.Product_Id in (6,20,22,26,31,76,80,81,98,99,109) and 
			Bill_Date between '2021-10-23' and '2021-12-31' AND 
			d.City in  ('Ahmedabad','Surat','Baroda','Saurashtra')
			GROUP BY b.Customer_Key;
			
			-- Points for First time Buyer in OND 2021
			select if(min(Bill_Date) is null, 0,min(Bill_Date) ) into @ftd 
			from CDM_Bill_Details a,
			CDM_Customer_Master b,
			CDM_Customer_Key_Lookup c
			where a.Customer_Id=b.Customer_Id and
			b.Customer_Id=c.Customer_Id and
            		c.Customer_Key=V_ID and
			Product_Id in (6,20,22,26,31,76,80,81,98,99,109) and
			Bill_Date between '2021-10-23' and '2021-12-14' AND 
			City in  ('Ahmedabad','Surat','Baroda','Saurashtra');

			select (case when (@ftd>0 AND sum(Sale_Qty>=20)) then 1 else 0 end) into @x
			from Customer_One_View a,
			CDM_Bill_Details b,
			CDM_Customer_Key_Lookup c
			where  a.Customer_Id=b.Customer_Id and
			Product_Id in (6,20,22,26,31,76,80,81,98,99,109) and 
			Bill_Date = @ftd and 
			c.Customer_Id=a.Customer_Id and 
			c.Customer_Key=V_ID
			group by c.Customer_Key;
			
			if @x>=1 THEN
				update CDM_Stg_Points_Earned set Points_Earned=Points_Earned+50 where Customer_Key=V_ID;
				select 'First Time Buyer',V_ID;
			END IF;

			-- Points for Winback 	
			select count(1) into @trans_cnt_1 
			from CDM_Bill_Details a,
			CDM_Customer_Master b,
			CDM_Customer_Key_Lookup c
			where a.Customer_Id=b.Customer_Id and
            		c.Customer_Key=V_ID and
			b.Customer_Id=c.Customer_Id and
			Product_Id in (6,20,22,26,31,76,80,81,98,99,109) and
			Bill_Date between '2021-10-23' and '2021-12-31' AND 
			City in  ('Ahmedabad','Surat','Baroda','Saurashtra');		

			select if(min(Bill_Date) is null,0,min(Bill_Date)) into @min_win_date 
			from CDM_Bill_Details a,
			CDM_Customer_Master b ,
			CDM_Customer_Key_Lookup c
			where a.Customer_Id=b.Customer_Id and
			b.Customer_Id=c.Customer_Id and
            		c.Customer_Key=V_ID and
			Product_Id in (6,20,22,26,31,76,80,81,98,99,109) and
			Bill_Date between '2021-10-23' and '2021-12-31' AND 
			City in  ('Ahmedabad','Surat','Baroda','Saurashtra');	

			select count(1) into @trans_cnt_2 
			from CDM_Bill_Details a,
			CDM_Customer_Master b,
			CDM_Customer_Key_Lookup c
			where a.Customer_Id=b.Customer_Id and
			b.Customer_Id=c.Customer_Id and
            		c.Customer_Key=V_ID and
			Product_Id in (6,20,22,26,31,76,80,81,98,99,109) and
			Bill_Date between date_sub(@min_win_date,interval 91 day) and date_sub(@min_win_date,interval 1 day) AND 
			City in  ('Ahmedabad','Surat','Baroda','Saurashtra');	


			select case when (@trans_cnt_1>=1 AND @trans_cnt_2=0 AND sum(Sale_Qty)>=50 AND @min_win_date>0 )then 1 else 0 end into @x
			from CDM_Customer_Master a,
			CDM_Bill_Details b,
			CDM_Customer_Key_Lookup c
			where  a.Customer_Id=b.Customer_Id and
			c.Customer_Id=a.Customer_Id and  
			c.Customer_Key=V_ID
			AND b.Bill_Date=@min_win_date
			AND Product_Id in (6,20,22,26,31,76,80,81,98,99,109)
			AND City in  ('Ahmedabad','Surat','Baroda','Saurashtra');	

			if @x>=1 THEN
				update CDM_Stg_Points_Earned set Points_Earned=Points_Earned+40 where Customer_Key=V_ID;
				select 'Winback',V_ID;
			END IF;			

			-- Buying first time in 15-31 December 
			select if(min(Bill_Date) is null, 0,min(Bill_Date) ) into @ftd 
			from CDM_Bill_Details a,
			CDM_Customer_Master b,
			CDM_Customer_Key_Lookup c
			where a.Customer_Id=b.Customer_Id and
			b.Customer_Id=c.Customer_Id and
            		c.Customer_Key=V_ID and
			Product_Id in (6,20,22,26,31,76,80,81,98,99,109) and
			Bill_Date between '2021-12-15' and '2021-12-31' AND 
			City in  ('Ahmedabad','Surat','Baroda','Saurashtra');		

			select (case when ( @ftd>0  AND sum(Sale_Qty>=20)) then 1 else 0 end) into @x
			from Customer_One_View a,CDM_Bill_Details b, CDM_Customer_Key_Lookup c
			where  a.Customer_Id=b.Customer_Id and 
			c.Customer_Id=a.Customer_Id and
			Bill_Date = @ftd and 
			Product_Id in (6,20,22,26,31,76,80,81,98,99,109) and 
			c.Customer_Key=V_ID
			group by c.Customer_Key;	

			if @x>=1 THEN
				update CDM_Stg_Points_Earned set Points_Earned=Points_Earned+90 where Customer_Key=V_ID;
				select '15-31 December',V_ID;
			END IF;			


			
			-- Diwali 
			
			select sum(case when Bill_Date  BETWEEN '2021-10-30' AND '2021-11-15' then 1 else 0 end) into @x
			from Customer_One_View a,
			CDM_Bill_Details b,
			CDM_Customer_Key_Lookup c
			where  a.Customer_Id=b.Customer_Id and
			a.Customer_Id=c.Customer_Id and  
			c.Customer_Key=V_ID and
			Product_Id in (6,20,22,26,31,76,80,81,98,99,109)  and
			Bill_Date between '2021-10-23' and '2021-12-31' AND 
			City in   ('Ahmedabad','Surat','Baroda','Saurashtra')
			group by c.Customer_Key;	

			if @x>=1 THEN
				update CDM_Stg_Points_Earned set Points_Earned=Points_Earned+40 where Customer_Key=V_ID;
				select 'Diwali',V_ID;
			END IF;						
			
		END LOOP LOOP_COMMTEMPLATES;
		CLOSE curCust;


	 -- END IF;

END$$
DELIMITER ;
