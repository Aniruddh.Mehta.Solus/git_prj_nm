DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  ETL_SDL_to_CDM_Client_Cust_Var (IN vBatchSize BIGINT) 
BEGIN 
    DECLARE vSuccessRet TINYINT DEFAULT 0; 
  DECLARE vBatchSize BIGINT DEFAULT 500000; 
   
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
 
   
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET vSuccessRet = 0; 
 
  SELECT CAST(LOV_Value AS UNSIGNED) INTO vBatchSize  
  FROM LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'UPPERBATCHSIZE'; 
 
   
  SET @Rec_Cnt = 0; 
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Client_Customer_Variables ORDER BY SDL_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
   
  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
   
  SET @Rec_Cnt = 0; 
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Client_Customer_Variables ORDER BY SDL_Id DESC LIMIT 1; 
 
  IF @Rec_Cnt IS NULL THEN 
    SET @Rec_Cnt = 0; 
  END IF; 
 
  TRUNCATE CDM_Stg_Client_Customer_Variables; 
   
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM_Client_Cust_Var', 1, 'Load into Client Customer Var', 1); 
 
  INSERT_INTO_STG_CLIENT_VAR : LOOP 
   
    CALL CDM_Update_Process_Log('ETL_SDL_to_CDM_Client_Cust_Var', CONCAT(vStart_Cnt,'_0'), 'ETL_SDL_to_CDM_Client_Cust_Var', 1); 
 
    INSERT IGNORE INTO CDM_Stg_Client_Customer_Variables 
    ( 
      Customer_Key, 
      HML_Tag_Last_Calendar_Year, 
      HML_Tag_Last_To_Last_Calendar_Year 
    ) 
      SELECT 
        TRIM(Customer_Key), 
        TRIM(HML_Tag_Last_Calendar_Year), 
        TRIM(HML_Tag_Last_To_Last_Calendar_Year) 
      FROM SDL_Stg_Client_Customer_Variables 
      WHERE 
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
 
    SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
 
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE INSERT_INTO_STG_CLIENT_VAR; 
        END IF; 
 
  END LOOP INSERT_INTO_STG_CLIENT_VAR; 
   
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM_Client_Cust_Var', 2, 'Load into Client Customer Var', 1); 
   
END	
mysolus$$
