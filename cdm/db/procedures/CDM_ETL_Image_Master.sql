DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Image_Master () 
BEGIN 
 
set sql_safe_updates=0; 
  CALL CDM_Update_Process_Log('CDM_ETL_Image_Master', 0, 'CDM_ETL_Image_Master', 1); 
     
  INSERT IGNORE INTO CDM_Image_Key_Lookup ( 
    Image_Key 
    ) 
    SELECT  
      Image_Key 
    FROM CDM_Stg_Image_Master  
    WHERE Image_Key <> '' AND Rec_Is_Processed = -1; 
         
  CALL CDM_Update_Process_Log('CDM_ETL_Image_Master', 1, 'CDM_ETL_Image_Master', 1); 
 
  UPDATE IGNORE CDM_Stg_Image_Master AS A 
        SET Image_Id = ( 
      SELECT Image_Id  
            FROM CDM_Image_Key_Lookup AS B 
            WHERE A.Image_Key = B.Image_Key 
        ); 
 
     
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'IMAGE', 
      Image_Key, 
      Image_Key, 
      fn 
    FROM CDM_Stg_Image_Master  
    WHERE  
      Image_Key <> '' AND Rec_Is_Processed = -1; 
 
  UPDATE IGNORE CDM_Stg_Image_Master AS A 
  SET LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Image_Key = B.LOV_Key 
      AND B.LOV_Attribute = 'IMAGE'  
  ) 
  WHERE LOV_Id <= 0; 
 
 
 
     
     
  CALL CDM_Update_Process_Log('CDM_ETL_Image_Master', 2, 'CDM_ETL_Image_Master', 1); 
   
  INSERT IGNORE INTO CDM_Image_Master ( 
    Image_Id, 
    LOV_Id, 
    product_key, 
        articles, 
        category, 
        gender, 
        fn, 
        brand, 
        season, 
        style_code, 
        features, 
        color, 
        size, 
        mrp, 
        discount, 
        selling_price, 
        product_availability, 
        link, 
        image_link, 
        all_image_link 
     
    ) 
    SELECT  
      Image_Id, 
      LOV_Id, 
      product_key, 
      articles, 
      category, 
        gender, 
        fn, 
        brand, 
        season, 
        style_code, 
        features, 
        color, 
        size, 
        mrp, 
        discount, 
        selling_price, 
        product_availability, 
        link, 
        image_link, 
        all_image_link       
       
    FROM  CDM_Stg_Image_Master  
    WHERE   CDM_Stg_Image_Master.Rec_Is_Processed = -1; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Image_Master', 3, 'CDM_ETL_Image_Master', 1); 
     
    update CDM_Image_Master t1 , CDM_Product_Key_Lookup t2 
    set t1.Product_Id =t2.Product_Id 
    where t1.product_key= t2.Product_Key; 
     
   
         
    update CDM_Image_Master t1 , CDM_Product_Master_Original t2 
    set t1.Product_LOV_Id =t2.Product_LOV_Id, 
    t1.Product_Genome_LOV_Id =t2.Product_Genome_LOV_Id 
    where t1.Product_Id= t2.Product_Id; 
     
    update CDM_Image_Master t1 , CDM_Product_Master_Original t2 
    set t1.Product_Genome_LOV_Id =t2.Product_Genome1_LOV_Id 
    where t1.Product_Id= t2.Product_Id and t1.Product_Genome_LOV_Id =-1 ; 
     
    update CDM_Image_Master t1 , CDM_Product_Master_Original t2 
    set t1.Product_Genome_LOV_Id =t2.Product_Genome2_LOV_Id 
    where t1.Product_Id= t2.Product_Id and t1.Product_Genome_LOV_Id =-1 ; 
     
     update CDM_Image_Master t1 , CDM_Product_Master_Original t2 
    set t1.Product_Genome_LOV_Id =t2.Product_Genome3_LOV_Id 
    where t1.Product_Id= t2.Product_Id and t1.Product_Genome_LOV_Id =-1 ; 
 
   
 
   
   
  update CDM_Image_Master t1 , CDM_Product_Master_Original t2 
    set t2.Image_LOV_Id =t1.LOV_Id 
    where t1.Product_Id= t2.Product_Id; 
   
  update  (select  LOV_Id,Product_Genome_LOV_Id from CDM_Image_Master group by Product_Genome_LOV_Id)  t1 , CDM_Product_Master_Original t2 
    set t2.Image_LOV_Id =t1.LOV_Id 
    where t1.Product_Genome_LOV_Id= t2.Product_Genome_LOV_Id and t2.Image_LOV_Id = -1; 
   
     
    update  (select  LOV_Id,Product_Genome_LOV_Id from CDM_Image_Master group by Product_Genome_LOV_Id)  t1 , CDM_Product_Master_Original t2 
    set t2.Image_LOV_Id =t1.LOV_Id 
    where t1.Product_Genome_LOV_Id= t2.Product_Genome1_LOV_Id and t2.Image_LOV_Id = -1; 
   
  update  (select  LOV_Id,Product_Genome_LOV_Id from CDM_Image_Master group by Product_Genome_LOV_Id)  t1 , CDM_Product_Master_Original t2 
    set t2.Image_LOV_Id =t1.LOV_Id 
    where t1.Product_Genome_LOV_Id= t2.Product_Genome2_LOV_Id and t2.Image_LOV_Id = -1; 
   
  update (select  LOV_Id,Product_Genome_LOV_Id from CDM_Image_Master group by Product_Genome_LOV_Id) t1 , CDM_Product_Master_Original t2 
    set t2.Image_LOV_Id =t1.LOV_Id 
    where t1.Product_Genome_LOV_Id= t2.Product_Genome3_LOV_Id and t2.Image_LOV_Id = -1; 
        
   
 
        
         
END	
mysolus$$
