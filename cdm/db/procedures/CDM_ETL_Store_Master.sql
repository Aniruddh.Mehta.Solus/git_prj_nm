DELIMITER $$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Store_Master () 
BEGIN 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Store_Master', 0, 'CDM_ETL_Store_Master', 1); 
     
  INSERT IGNORE INTO CDM_Store_Key_Lookup ( 
    Store_Key 
    ) 
    SELECT  
      Store_Key 
    FROM CDM_Stg_Store_Master  
    WHERE Store_Key <> '' AND Rec_Is_Processed = -1; 
         
  CALL CDM_Update_Process_Log('CDM_ETL_Store_Master', 1, 'CDM_ETL_Store_Master', 1); 
 
  UPDATE IGNORE CDM_Stg_Store_Master AS A 
        SET Store_Id = ( 
      SELECT Store_Id  
            FROM CDM_Store_Key_Lookup AS B 
            WHERE A.Store_Key = B.Store_Key 
        ); 
 
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'STORE', 
      Store_Key, 
      Store_Key, 
      Store_Comm_Name 
    FROM CDM_Stg_Store_Master  
    WHERE  
      Store_Key <> '' AND Rec_Is_Processed = -1; 
 
  UPDATE IGNORE CDM_Stg_Store_Master AS A 
  SET LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Store_Key = B.LOV_Key 
      AND B.LOV_Attribute = 'STORE'  
  ) 
  WHERE LOV_Id <= 0; 
 
    
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'STORE STATE', 
      Store_State_Key, 
      Store_State_Key, 
      Store_State_Key 
    FROM CDM_Stg_Store_Master  
    WHERE  
      Store_State_Key <> '' AND Rec_Is_Processed = -1; 
 
  UPDATE IGNORE CDM_Stg_Store_Master AS A 
  SET Store_State_LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Store_State_Key = B.LOV_Key 
      AND B.LOV_Attribute = 'STORE STATE'  
  ); 
 
     
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'STORE STATUS', 
      Store_Status, 
      Store_Status, 
      Store_Status 
    FROM CDM_Stg_Store_Master  
    WHERE  
      Store_Status <> '' AND Rec_Is_Processed = -1; 
 
  UPDATE IGNORE CDM_Stg_Store_Master AS A 
  SET Store_Status_LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Store_Status = B.LOV_Key 
      AND B.LOV_Attribute = 'STORE STATUS'  
  ); 
 
     
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'STORE_FORMAT', 
      Store_Format, 
      Store_Format, 
      Store_Format 
    FROM CDM_Stg_Store_Master  
    WHERE Store_Format <> '' AND Rec_Is_Processed = -1 
    GROUP BY Store_Format; 
 
  UPDATE IGNORE CDM_Stg_Store_Master AS A 
  SET Store_Format_LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Store_Format = B.LOV_Key 
      AND B.LOV_Attribute = 'STORE_FORMAT'  
  ); 
 
     
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'STORE_LOCATION_TYPE', 
      Store_Location_Type, 
      Store_Location_Type, 
      Store_Location_Type 
    FROM CDM_Stg_Store_Master  
    WHERE Store_Location_Type <> '' AND Rec_Is_Processed = -1 
    GROUP BY Store_Location_Type; 
 
  UPDATE IGNORE CDM_Stg_Store_Master AS A 
  SET Store_Location_Type_LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Store_Location_Type = B.LOV_Key 
      AND B.LOV_Attribute = 'STORE_LOCATION_TYPE'  
  ); 
 
     
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'STORE_CHANNEL_TYPE', 
      Store_Channel_Type, 
      Store_Channel_Type, 
      Store_Channel_Type 
    FROM CDM_Stg_Store_Master  
    WHERE Store_Channel_Type <> '' AND Rec_Is_Processed = -1 
    GROUP BY Store_Channel_Type; 
 
  UPDATE IGNORE CDM_Stg_Store_Master AS A 
  SET Store_Channel_Type_LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Store_Channel_Type = B.LOV_Key 
      AND B.LOV_Attribute = 'STORE_CHANNEL_TYPE'  
  ); 
 
     
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'STORE_TYPE_TAG1', 
      Store_Type_Tag1, 
      Store_Type_Tag1, 
      Store_Type_Tag1 
    FROM CDM_Stg_Store_Master  
    WHERE Store_Type_Tag1 <> '' AND Rec_Is_Processed = -1 
    GROUP BY Store_Type_Tag1; 
 
  UPDATE IGNORE CDM_Stg_Store_Master AS A 
  SET Store_Type_Tag1_LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Store_Type_Tag1 = B.LOV_Key 
      AND B.LOV_Attribute = 'STORE_TYPE_TAG1'  
  ); 
 
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'STORE_TYPE_TAG2', 
      Store_Type_Tag2, 
      Store_Type_Tag2, 
      Store_Type_Tag2 
    FROM CDM_Stg_Store_Master  
    WHERE Store_Type_Tag2 <> '' AND Rec_Is_Processed = -1 
    GROUP BY Store_Type_Tag2; 
 
  UPDATE IGNORE CDM_Stg_Store_Master AS A 
  SET Store_Type_Tag2_LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Store_Type_Tag2 = B.LOV_Key 
      AND B.LOV_Attribute = 'STORE_TYPE_TAG2'  
  ); 
   
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'STORE_TYPE_TAG3', 
      Store_Type_Tag3, 
      Store_Type_Tag3, 
      Store_Type_Tag3 
    FROM CDM_Stg_Store_Master  
    WHERE Store_Type_Tag3 <> '' AND Rec_Is_Processed = -1 
    GROUP BY Store_Type_Tag3; 
 
  UPDATE IGNORE CDM_Stg_Store_Master AS A 
  SET Store_Type_Tag3_LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Store_Type_Tag3 = B.LOV_Key 
      AND B.LOV_Attribute = 'STORE_TYPE_TAG3'  
  ); 

  UPDATE IGNORE CDM_Stg_Store_Master AS A,CDM_Store_Master AS B 
  SET B.Store_Name = A.Store_Name 
    WHERE  
      A.Store_Id=B.Store_Id  
      AND Rec_Is_Processed=-1;      
     
  CALL CDM_Update_Process_Log('CDM_ETL_Store_Master', 2, 'CDM_ETL_Store_Master', 1); 
   
  INSERT IGNORE INTO CDM_Store_Master ( 
    Store_Id, 
    LOV_Id, 
    Store_Status_LOV_Id, 
    Open_Date, 
    Open_Year, 
    Open_Year_Month, 
    Open_Day_Week, 
    Closure_Date, 
    Closure_Year, 
    Closure_Year_Month, 
    Closure_Day_Week, 
    Store_Size, 
    Store_Region, 
    Store_Address1, 
    Store_Address2, 
    Store_Area, 
    Store_City, 
    Store_Pincode, 
    Store_State_LOV_Id, 
    Store_Lat, 
    Store_Long, 
    Store_Name, 
    Store_Comm_Name, 
    Store_Ownership, 
    Store_Format_LOV_Id, 
    Store_Location_Type_LOV_Id, 
    Store_Channel_Type_LOV_Id, 
    Store_Type_Tag1_LOV_Id, 
    Store_Type_Tag2_LOV_Id, 
    Store_Type_Tag3_LOV_Id 
    ) 
    SELECT  
      Store_Id, 
      LOV_Id, 
      Store_Status_LOV_Id, 
      Open_Date, 
      IFNULL(YEAR(Open_Date),''), 
      IFNULL(EXTRACT(YEAR_MONTH FROM Open_Date),''),   
      IFNULL(WEEKDAY(Open_Date),-1), 
      Closure_Date, 
      IFNULL(YEAR(Closure_Date),''), 
      IFNULL(EXTRACT(YEAR_MONTH FROM Closure_Date),''), 
      IFNULL(WEEKDAY(Closure_Date),-1), 
      Store_Size, 
      Store_Region, 
      Store_Address1, 
      Store_Address2, 
      Store_Area, 
      Store_City, 
      Store_Pincode, 
      Store_State_LOV_Id, 
      Store_Lat, 
      Store_Long, 
      Store_Name, 
      Store_Comm_Name, 
      Store_Ownership, 
      Store_Format_LOV_Id, 
      Store_Location_Type_LOV_Id, 
      Store_Channel_Type_LOV_Id, 
      Store_Type_Tag1_LOV_Id, 
      Store_Type_Tag2_LOV_Id, 
      Store_Type_Tag3_LOV_Id 
    FROM  CDM_Stg_Store_Master  
    WHERE   CDM_Stg_Store_Master.Rec_Is_Processed = -1; 
	
	UPDATE IGNORE 
	CDM_Store_Master A, CDM_Stg_Store_Master B
	SET 
	A.LOV_Id=B.LOV_Id,
	A.Is_Processed=-1,
	A.Store_Status_LOV_Id=B.Store_Status_LOV_Id,
	A.Open_Date=B.Open_Date,
	A.Closure_Date=B.Closure_Date,
	A.Store_Size=B.Store_Size,
	A.Store_Region=B.Store_Region,
	A.Store_Address1=B.Store_Address1,
	A.Store_Address2=B.Store_Address2,
	A.Store_Area=B.Store_Area,
	A.Store_City=B.Store_City,
	A.Store_Pincode=B.Store_Pincode,
	A.Store_State_LOV_Id=B.Store_State_LOV_Id,
	A.Store_Lat=B.Store_Lat,
	A.Store_Long=B.Store_Long,
	A.Store_Name=B.Store_Name,
	A.Store_Comm_Name=B.Store_Comm_Name,
	A.Store_Ownership=B.Store_Ownership,
	A.Store_Format_LOV_Id=B.Store_Format_LOV_Id,
	A.Store_Location_Type_LOV_Id=B.Store_Location_Type_LOV_Id,
	A.Store_Channel_Type_LOV_Id=B.Store_Channel_Type_LOV_Id,
	A.Store_Type_Tag1_LOV_Id=B.Store_Type_Tag1_LOV_Id,
	A.Store_Type_Tag2_LOV_Id=B.Store_Type_Tag2_LOV_Id,
	A.Store_Type_Tag3_LOV_Id=B.Store_Type_Tag3_LOV_Id
	WHERE A.Store_Id=B.Store_Id;
 
  CALL CDM_Update_Process_Log('CDM_ETL_Store_Master', 3, 'CDM_ETL_Store_Master', 1); 
         
END$$
DELIMITER ;