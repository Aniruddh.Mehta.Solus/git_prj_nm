DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_ETL_Customer_Portfolio (IN vBatchSize BIGINT,IN vTreat_As_Incr TINYINT)
BEGIN 
/* Version 1.0 20221202 */

	  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
	  DECLARE vEnd_Cnt BIGINT; 
     
	  SET vEnd_Cnt = vBatchSize; 
      
    /* Getting the Min Rec_Id*/
    
	  SET @Rec_Cnt = 0; 
	  SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Customer_Portfolio ORDER BY Rec_Id ASC LIMIT 1; 
	  SET vStart_Cnt = @Rec_Cnt; 
	  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
   
   /* Getting the Max Rec_Id */
   
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Customer_Portfolio ORDER BY Rec_Id DESC LIMIT 1; 
    
    IF @Rec_Cnt IS NULL THEN  
		SET @Rec_Cnt = 0; 
    END IF; 
    
    
  
    
  /* Creating Temp Table For Each Batch */  
  
	CREATE OR REPLACE TABLE Temp_CDM_Customer_Portfolio(
    Rec_Id bigint(20) DEFAULT -1,
    Customer_Portfolio_Key varchar(128) NOT NULL DEFAULT '-1',
    Customer_Portfolio_Id bigint(20) NOT NULL DEFAULT -1,
	Customer_Key varchar(128) NOT NULL, 
	Customer_Id bigint(20) DEFAULT -1,
	Product_Key varchar(128) NOT NULL,
	Product_Id bigint(20) DEFAULT -1,
	Holding_Date datetime,
	Stock_In_Hand decimal(15,2) DEFAULT -1.0,
	Buy_Qty decimal(15,2) DEFAULT -1.0,
	Sell_Qty decimal(15,2) DEFAULT -1.0,
	Custom_Tag1 varchar(128) DEFAULT NULL,
	Custom_Tag2 varchar(128) DEFAULT NULL,
	Custom_Tag3 varchar(128) DEFAULT NULL,
	Custom_Tag4 varchar(128) DEFAULT NULL,
	Custom_Tag5 varchar(128) DEFAULT NULL,
	PRIMARY KEY (Rec_Id),
	KEY Customer_Key (Customer_Key),
	KEY Product_Key (Product_Key),
	KEY Holding_Date (Holding_Date),
	KEY Customer_Id (Customer_Id),
	KEY Product_Id (Product_Id));
   
  CALL CDM_Update_Process_Log('CDM_ETL_Customer_Portfolio', 1, 'CDM_ETL_Customer_Portfolio', 1); 
     
  PROCESS_CP_HIST: LOOP 
     
    TRUNCATE TABLE Temp_CDM_Customer_Portfolio; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Portfolio', CONCAT(vStart_Cnt,'_0'), 'CDM_ETL_Customer_Portfolio', 1); 
    
    /* Insert Into Temp Table */
    
	   INSERT INTO  Temp_CDM_Customer_Portfolio 
	  (  Rec_Id ,
		 Customer_Portfolio_Key ,
		 Customer_Key ,
		 Customer_Id ,
		 Product_Key ,
		 Product_Id ,
		 Holding_Date ,
		 Stock_In_Hand ,
		 Buy_Qty ,
		 Sell_Qty ,
		 Custom_Tag1 ,
		 Custom_Tag2 ,
		 Custom_Tag3 ,
		 Custom_Tag4 ,
		 Custom_Tag5 
		) 
		  
	SELECT 
		 Rec_Id ,
        CONCAT(Customer_Key,'_',Product_Key,'_',DATE_FORMAT(Holding_Date,'%Y%m%d')),
		 Customer_Key ,
		 Customer_Id ,
		 Product_Key ,
		 Product_Id ,
		DATE_FORMAT(Holding_Date,'%Y-%m-%d'),
		 Stock_In_Hand ,
		 Buy_Qty ,
		 Sell_Qty ,
		 Custom_Tag1 ,
		 Custom_Tag2 ,
		 Custom_Tag3 ,
		 Custom_Tag4 ,
		 Custom_Tag5 
	FROM  CDM_Stg_Customer_Portfolio 
	WHERE 
		Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt
	AND Customer_Key is not null
    AND Product_Key is not null
    AND Holding_Date is not null; 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Portfolio', CONCAT(vStart_Cnt,'_1'), 'CDM_ETL_Customer_Portfolio', 1); 
 
     
         
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Portfolio', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Customer_Portfolio', 1); 
    
    /* Insert Into Customer Portfolio Key Lookup Table */
     
    INSERT IGNORE INTO CDM_Customer_Portfolio_Key_Lookup ( 
      Customer_Portfolio_Key 
    ) 
      SELECT  
        Customer_Portfolio_Key 
      FROM  
        Temp_CDM_Customer_Portfolio TCP
      WHERE 
        Customer_Portfolio_Key <> '';
        
        
	/* Insert Into Customer Key Lookup Table */
        
	INSERT IGNORE INTO CDM_Customer_Key_Lookup(Customer_Key)
    
    SELECT Customer_Key 
    FROM Temp_CDM_Customer_Portfolio
    WHERE Customer_Key <> '';
    
    
    /* Insert Into Product Key Lookup Table */
    
    INSERT IGNORE INTO CDM_Product_Key_Lookup(Product_Key)
    
    SELECT Product_Key 
	FROM Temp_CDM_Customer_Portfolio
    WHERE Product_Key <> '';
        
	
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Portfolio', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Customer_Portfolio', 1); 
 
 
    /* Update the Customer Portfolio Id in Temp Table */ 
    UPDATE IGNORE Temp_CDM_Customer_Portfolio TGT, CDM_Customer_Portfolio_Key_Lookup SRC
    SET TGT.Customer_Portfolio_Id = SRC.Customer_Portfolio_Id
    WHERE TGT.Customer_Portfolio_Key = SRC.Customer_Portfolio_Key;


    /* Update the Customer Id in Temp Table */
    UPDATE IGNORE Temp_CDM_Customer_Portfolio TGT, CDM_Customer_Key_Lookup SRC
    SET TGT.Customer_Id = SRC.Customer_Id
    WHERE TGT.Customer_Key = SRC.Customer_Key
    AND TGT.Customer_Key <> '';
    
    /* Update the Product Id in Temp Table */
    UPDATE IGNORE Temp_CDM_Customer_Portfolio TGT, CDM_Product_Key_Lookup SRC
    SET TGT.Product_Id = SRC.Product_Id
    WHERE TGT.Product_Key = SRC.Product_Key
    AND TGT.Product_Key <> '';

    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Portfolio', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Customer_Portfolio', 1); 

     IF vTreat_As_Incr = 1 -- Incremental Mode 
    THEN 
       
       UPDATE CDM_Customer_Portfolio TGT, Temp_CDM_Customer_Portfolio SRC
       SET TGT.Stock_In_Hand = SRC.Stock_In_Hand,
           TGT.Buy_Qty = SRC.Buy_Qty,
           TGT.Sell_Qty = SRC.Sell_Qty,
           TGT.Custom_Tag1 = SRC.Custom_Tag1,
           TGT.Custom_Tag2 = SRC.Custom_Tag2,
           TGT.Custom_Tag3 = SRC.Custom_Tag3,
           TGT.Custom_Tag4 = SRC.Custom_Tag4,
           TGT.Custom_Tag5 = SRC.Custom_Tag5
       WHERE TGT.Customer_Portfolio_Id = SRC.Customer_Portfolio_Id;  
    
     END IF; 
    
   
         
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Portfolio', CONCAT(vStart_Cnt,'_5'), 'CDM_ETL_Customer_Portfolio', 1); 
 
 /* Insert into CDM_Customer_Portfolio */
 
    INSERT IGNORE INTO  CDM_Customer_Portfolio 
 ( Customer_Portfolio_Id ,
		 Customer_Id ,
		 Product_Id ,
		 Holding_Date ,
		 Stock_In_Hand ,
		 Buy_Qty ,
		 Sell_Qty ,
		 Custom_Tag1 ,
		 Custom_Tag2 ,
		 Custom_Tag3 ,
		 Custom_Tag4 ,
		 Custom_Tag5 
    ) 
      SELECT
      
      Customer_Portfolio_Id,
		Customer_Id,
		Product_Id,
		DATE(Holding_Date),
		Stock_In_Hand,
		Buy_Qty,
		Sell_Qty,
		Custom_Tag1,
		Custom_Tag2,
		Custom_Tag3,
		Custom_Tag4,
		Custom_Tag5
      
      FROM Temp_CDM_Customer_Portfolio; 
 

        SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_CP_HIST; 
        END IF; 
         
  END LOOP PROCESS_CP_HIST; 
 
  DROP TABLE Temp_CDM_Customer_Portfolio; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Customer_Portfolio', 6, 'CDM_ETL_Customer_Portfolio', 1); 
END$$
DELIMITER ;
