DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_DQC_Points_Redemption (OUT msg text)
dqc_points_redeemed:BEGIN
declare msg varchar(1024);
select count(1) into @n1 from CDM_Customer_Master ;
select count(distinct Customer_Id) into @ncust_points from CDM_Points_Redemption ;

if @n1<=@ncust_points  then
  set msg='Number of Customers in Points Redeemed is Greater than Customers  Master ; please reload Customer Master  ';
  leave dqc_points_redeemed;
end if ;

set msg=""; 
END$$
DELIMITER ;
