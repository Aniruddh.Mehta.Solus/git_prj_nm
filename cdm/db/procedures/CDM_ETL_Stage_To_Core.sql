DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Stage_To_Core (IN vTreat_As_Incr TINYINT) 
BEGIN 
  DECLARE vTxnStartDate DATE; 
    DECLARE  vTxnEndDate DATE; 
 
  DECLARE vPercentileForAP DECIMAL(2,2); 
  DECLARE vTxnStartDateForThreshold DATE; 
  DECLARE vTxnEndDateForThreshold DATE; 
  DECLARE vPercentileForThesholds DECIMAL(2,2); 
  DECLARE vLowerBatchSize BIGINT; 
  DECLARE vUpperBatchSize BIGINT; 
     
  SET SQL_SAFE_UPDATES = 0; 
 
  SELECT CAST(LOV_Value AS DECIMAL) INTO vPercentileForAP  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'PERCENTILEFORAP'; 
 
  SELECT CAST(LOV_Value AS DATE) INTO vTxnStartDateForThreshold  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'TXNSTARTDATEFORTHRESHOLD'; 
 
  SELECT CAST(LOV_Value AS DATE) INTO vTxnEndDateForThreshold  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'TXNENDDATEFORTHRESHOLD'; 
 
  SELECT CAST(LOV_Value AS DECIMAL) INTO vPercentileForThesholds  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'PERCENTILEFORTHESHOLDS'; 
 
  SELECT CAST(LOV_Value AS UNSIGNED) INTO vLowerBatchSize  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'LOWERBATCHSIZE'; 
 
  SELECT CAST(LOV_Value AS UNSIGNED) INTO vUpperBatchSize  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'UPPERBATCHSIZE'; 
 
   
  TRUNCATE CDM_PROCESS_LOG; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 0, 'CDM_ETL_Stage_To_Core', 1); 
 
 
   
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 1, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Store_Master(); 
   
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 2, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Brand_Master(); 
 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 3, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_BU_Master(); 
 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 4, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Cat_Master(); 
 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 5, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Dep_Master(); 
 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 6, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Div_Master(); 
 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 7, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Season_Master(); 
 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 8, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Section_Master(); 
 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 9, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Manuf_Master(); 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 10, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Broker_Master(); 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 11, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Product_Master(1); 
 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 12, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Product_Ingredients(); 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 13, 'CDM_ETL_Stage_To_Core', 1); 
  CALL CDM_ETL_Inventory_Master(vUpperBatchSize, vTreat_As_Incr); 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 14, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Customer_Master(vUpperBatchSize, vTreat_As_Incr); 
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 15, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Bill_Detail(vUpperBatchSize,vTreat_As_Incr); 
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 16, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Bill_Header(vUpperBatchSize, vTreat_As_Incr); 
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 17, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Bill_Payments(vUpperBatchSize, vTreat_As_Incr); 
   
   
  CALL CDM_Update_Process_Log('ETL_Customer_Membership', 18, 'ETL_Customer_Membership', 1); 
    CALL CDM_ETL_Customer_Membership(vUpperBatchSize); 
   
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 19, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Points_Earned(vUpperBatchSize); 
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 20, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Points_Redemption(vUpperBatchSize); 
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 21, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Offers(vUpperBatchSize); 
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 22, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Voucher(vUpperBatchSize); 
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 23, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Voucher_Redemption(vUpperBatchSize); 
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 24, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Points_Expiry(vUpperBatchSize); 
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 25, 'CDM_ETL_Stage_To_Core', 1); 
    CALL CDM_ETL_Feedback(vUpperBatchSize); 
 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 26, 'CDM_ETL_Stage_To_Core', 1); 
  Call CDM_ETL_Var_List(); 
 
 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Stage_To_Core', 27, 'CDM_ETL_Stage_To_Core', 1); 
 
END	
mysolus$$
