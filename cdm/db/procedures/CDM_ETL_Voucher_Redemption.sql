DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Voucher_Redemption (IN vBatchSize BIGINT) 
BEGIN 
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
     
  SET vEnd_Cnt = vBatchSize; 
 
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Voucher_Redemption ORDER BY Rec_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
   
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Voucher_Redemption ORDER BY Rec_Id DESC LIMIT 1; 
    IF @Rec_Cnt IS NULL THEN  
    SET @Rec_Cnt = 0; 
  END IF; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Voucher_Redemption', 1, 'CDM_ETL_Voucher_Redemption', 1); 
     
  PROCESS_VR: LOOP 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Voucher_Redemption', CONCAT(vStart_Cnt,'_1'), 'CDM_ETL_Voucher_Redemption', 1); 
 
    UPDATE IGNORE CDM_Stg_Voucher_Redemption 
    SET Bill_Header_Id = ( 
      SELECT Bill_Header_Id  
      FROM CDM_Bill_Header_Key_Lookup 
      WHERE 
        CDM_Stg_Voucher_Redemption.Bill_Header_Key = CDM_Bill_Header_Key_Lookup.Bill_Header_Key 
    ) 
    WHERE Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Bill_Header_Key  <> '' AND 
      Rec_Is_Processed = -1; 
 
    UPDATE IGNORE CDM_Stg_Voucher_Redemption -- ashutosh 
    SET Customer_Id = ( 
      SELECT Customer_Id  
      FROM CDM_Customer_Key_Lookup 
      WHERE 
        CDM_Stg_Voucher_Redemption.Customer_Key = CDM_Customer_Key_Lookup.Customer_Key 
    ) 
    WHERE Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Customer_Key  <> '' AND 
      Rec_Is_Processed = -1; 
         
    CALL CDM_Update_Process_Log('CDM_ETL_Voucher_Redemption', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Voucher_Redemption', 1); 
 
     
    INSERT IGNORE INTO CDM_Voucher_Redemption_Key_Lookup ( 
      Voucher_Redemption_Rec_Key 
    ) 
      SELECT  
        Voucher_Redemption_Rec_Key 
      FROM  
        CDM_Stg_Voucher_Redemption 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
     
     
    CALL CDM_Update_Process_Log('CDM_ETL_Voucher_Redemption', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Voucher_Redemption', 1); 
 
    UPDATE IGNORE CDM_Stg_Voucher_Redemption 
        SET Voucher_Redemption_Rec_Id = ( 
      SELECT Voucher_Redemption_Rec_Id  
            FROM CDM_Voucher_Redemption_Key_Lookup 
            WHERE 
        CDM_Stg_Voucher_Redemption.Voucher_Redemption_Rec_Key = CDM_Voucher_Redemption_Key_Lookup.Voucher_Redemption_Rec_Key 
    ) 
    WHERE  
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Voucher_Redemption_Rec_Id <= 0 AND 
      Rec_Is_Processed = -1; 
       
     
     
    INSERT IGNORE INTO CDM_Voucher_Key_Lookup ( 
      Voucher_Key 
    ) 
      SELECT  
        Voucher_Key 
      FROM  
        CDM_Stg_Voucher_Redemption 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Voucher_Redemption', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Voucher_Redemption', 1); 
 
    UPDATE IGNORE CDM_Stg_Voucher_Redemption 
        SET Voucher_Id = ( 
      SELECT Voucher_Id  
            FROM CDM_Voucher_Key_Lookup 
            WHERE 
        CDM_Stg_Voucher_Redemption.Voucher_Key = CDM_Voucher_Key_Lookup.Voucher_Key 
    ) 
    WHERE  
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Voucher_Id <= 0 AND 
      Rec_Is_Processed = -1; 
       
     
    INSERT IGNORE INTO CDM_Vouchers ( 
      Voucher_Id 
    ) 
      SELECT  
        Voucher_Id 
      FROM  
        CDM_Stg_Voucher_Redemption 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
 
         
    CALL CDM_Update_Process_Log('CDM_ETL_Voucher_Redemption', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Voucher_Redemption', 1); 
 
    INSERT IGNORE INTO CDM_Voucher_Redemption ( 
      Voucher_Redemption_Rec_Id, 
      Bill_Header_Id, 
      Customer_Id, 
      Voucher_Id, 
      Voucher_Value, 
      Redemption_Date, 
      Redemption_Year, 
      Redemption_Year_Month, 
      Redemption_Day_Week 
    ) 
      SELECT  
        Voucher_Redemption_Rec_Id, 
        Bill_Header_Id, 
        Customer_Id, 
        Voucher_Id, 
        Voucher_Value, 
        Redemption_Date, 
        IFNULL(YEAR(Redemption_Date),''), 
        IFNULL(EXTRACT(YEAR_MONTH FROM Redemption_Date),''), 
        IFNULL(WEEKDAY(Redemption_Date),-1) 
      FROM CDM_Stg_Voucher_Redemption 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
 
      SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_VR; 
        END IF; 
         
  END LOOP PROCESS_VR; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Voucher_Redemption', 2, 'CDM_ETL_Voucher_Redemption', 1); 
   
END	
mysolus$$
