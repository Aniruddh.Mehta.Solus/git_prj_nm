DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_DQC_Points_Expiry (OUT msg text)
dqc_points_expiry:BEGIN
declare msg varchar(1024);
select count(1) into @n1 from CDM_Customer_Master  ;
select count(distinct Customer_Id) into @ncust_points from CDM_Points_Expiry ;

if @n1<=@ncust_points  then
  set msg='Number of Customers in Points Expiry is Greater than Customers in Customer Master ; please relaod Customer Master  ';
  leave dqc_points_expiry;
end if ;

set msg=""; 
END$$
DELIMITER ;
