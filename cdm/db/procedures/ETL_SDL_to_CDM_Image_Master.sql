DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  ETL_SDL_to_CDM_Image_Master () 
BEGIN 
DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
    DECLARE vBatchSize BIGINT DEFAULT 5000; 
    SET vEnd_Cnt = vBatchSize; 
   
   SET @Rec_Cnt = 0; 
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Image_Master ORDER BY SDL_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
 
  SET @Rec_Cnt = 0; 
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Image_Master ORDER BY SDL_Id DESC LIMIT 1; 
 
  IF @Rec_Cnt IS NULL THEN 
    SET @Rec_Cnt = 0; 
  END IF; 
 
  SET vEnd_Cnt = vBatchSize; 
 
  TRUNCATE CDM_Stg_Image_Master; 
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 2, 'Load into Image Master', 1); 
 
  INSERT_INTO_STG_IMAGE_MASTER : LOOP 
 
    INSERT IGNORE INTO CDM_Stg_Image_Master 
    ( 
      image_key, 
      product_key, 
      articles, 
      category, 
      gender, 
      fn, 
      brand, 
      season, 
      style_code, 
      features, 
      color, 
      size, 
      mrp, 
      discount, 
      selling_price, 
      product_availability, 
      link, 
      image_link, 
      all_image_link 
    ) 
      SELECT 
        image_key, 
        product_key, 
        articles, 
        category, 
        gender, 
        fn, 
        brand, 
        season, 
        style_code, 
        features, 
        color, 
        size, 
        mrp, 
        discount, 
        selling_price, 
        product_availability, 
        link, 
        image_link, 
        all_image_link 
      FROM SDL_Stg_Image_Master 
      WHERE 
      SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
      Rec_Is_Processed = -1; 
 
    SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
 
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE INSERT_INTO_STG_IMAGE_MASTER; 
        END IF; 
 
  END LOOP INSERT_INTO_STG_IMAGE_MASTER; 
   
   
end	
mysolus$$
