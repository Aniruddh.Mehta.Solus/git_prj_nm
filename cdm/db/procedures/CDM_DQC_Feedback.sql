DELIMITER $$
CREATE or replace PROCEDURE  CDM_DQC_Feedback (OUT msg text)
dqc_feedback:BEGIN
declare msg varchar(1024);
select count(1) into @n1 from CDM_Customer_Master  ;
select count(distinct Fdbk_Customer_Id) into @ncust_feedback from CDM_Feedback ;

if @n1<=@ncust_feedback  then
  set msg='Number of Customers in CDM_Feedback is Greater than Customers in Customer Master ; please relaod Customer Master  ';
  leave dqc_feedback;
end if ;

set msg=""; 
END$$
DELIMITER ;
