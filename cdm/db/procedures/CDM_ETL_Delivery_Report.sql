DELIMITER $$
CREATE or REPLACE PROCEDURE  CDM_ETL_Delivery_Report ()
BEGIN 
 
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
DECLARE vBatchSize BIGINT DEFAULT 100000; 

  CALL CDM_Update_Process_Log('CDM_ETL_Delivery_Report', 0, 'CDM_ETL_Delivery_Report', 1); 
 
SELECT Rec_Id INTO vStart_Cnt FROM CDM_Stg_Del_Rep ORDER BY Rec_Id ASC LIMIT 1;
SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Del_Rep ORDER BY Rec_Id DESC LIMIT 1;

 
INSERT_LOOP: LOOP

SET vEnd_Cnt = vStart_Cnt + vBatchSize;

 insert ignore into CDM_Delivery_Report(
Customer_Id,
 Event_Execution_ID,
 Event_Execution_Date_ID,
 Delivery_Date, 
 Delivery_Status,
 Open_Date,
 Open_Status,
 Communication_Channel
)
select Customer_Id,
Event_Execution_ID,
Sent_Date,
Delivery_Date,
Delivery_Status,
Open_Date,
Open_Status,
Communication_Channel
from CDM_Stg_Del_Rep 
where Event_Execution_ID != ''
and Rec_Id between vStart_Cnt and vEnd_Cnt;

SET vStart_Cnt = vEnd_Cnt;

			IF vStart_Cnt  >= @Rec_Cnt THEN
				LEAVE INSERT_LOOP;
	
			END IF;
            
END LOOP INSERT_LOOP;  
 
 

  
END$$
DELIMITER ;

