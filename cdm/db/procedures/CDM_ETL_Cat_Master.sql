DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Cat_Master () 
BEGIN 
  CALL CDM_Update_Process_Log('CDM_ETL_Cat_Master', 1, 'CDM_ETL_Cat_Master', 1); 
 
  INSERT IGNORE INTO CDM_Cat_Key_Lookup ( 
    Cat_Key 
  ) 
    SELECT  
      Cat_Key 
    FROM CDM_Stg_Cat_Master 
        WHERE Cat_Key <>'' AND Rec_Is_Processed = -1; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Cat_Master', 2, 'CDM_ETL_Cat_Master', 1); 
 
  UPDATE IGNORE CDM_Stg_Cat_Master AS A 
    SET Cat_Id = ( 
    SELECT Cat_Id  
        FROM CDM_Cat_Key_Lookup AS B 
        WHERE A.Cat_Key = B.Cat_Key 
    ); 
 
     
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'CAT', 
      Cat_Key, 
      Cat_Key, 
      Cat_Comm_Name 
    FROM CDM_Stg_Cat_Master  
    WHERE Cat_Key <> '' AND Rec_Is_Processed = -1; 
 
  UPDATE IGNORE CDM_Stg_Cat_Master AS A 
        SET LOV_Id = ( 
      SELECT LOV_Id   
            FROM CDM_LOV_Master AS B 
            WHERE  
        A.Cat_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'CAT'  
        ) 
  WHERE LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Cat_Master AS A 
  SET  
    Comp_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'CAT'  
    ) 
  WHERE Comp_LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Cat_Master AS A 
  SET  
    Up_Sell_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'CAT'  
    ) 
  WHERE Up_Sell_LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Cat_Master AS A 
  SET  
    Cross_Sell_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'CAT'  
    ) 
  WHERE Cross_Sell_LOV_Id <= 0; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Cat_Master', 3, 'CDM_ETL_Cat_Master', 1); 
     
  INSERT IGNORE INTO CDM_Cat_Master ( 
    Cat_Id, 
    LOV_Id, 
    Comp_LOV_Id, 
    Up_Sell_LOV_Id, 
    Cross_Sell_LOV_Id, 
    Cat_Name, 
    Cat_Type_Tag1, 
    Cat_Type_Tag2, 
    Cat_Type_Tag3, 
    Cat_Comm_Name 
  ) 
    SELECT  
      Cat_Id, 
      LOV_Id, 
      Comp_LOV_Id, 
      Up_Sell_LOV_Id, 
      Cross_Sell_LOV_Id, 
      Cat_Name, 
      Cat_Type_Tag1, 
      Cat_Type_Tag2, 
      Cat_Type_Tag3, 
      Cat_Comm_Name 
    FROM CDM_Stg_Cat_Master 
        WHERE Rec_Is_Processed = -1; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Cat_Master', 4, 'CDM_ETL_Cat_Master', 1); 
END	
mysolus$$
