DELIMITER $$
CREATE or REPLACE PROCEDURE  CDM_ETL_LOV_Master ()
BEGIN 
 
  CALL CDM_Update_Process_Log('CDM_ETL_LOV_Master', 1, 'CDM_ETL_LOV_Master', 1); 
     
 
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
     LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value
    FROM CDM_Stg_LOV_Master  
    WHERE  
      LOV_Attribute <> '' AND Rec_Is_Processed = -1; 
 
 
  CALL CDM_Update_Process_Log('CDM_ETL_LOV_Master', 2, 'CDM_ETL_LOV_Master', 1); 
         
END$$
DELIMITER ;

