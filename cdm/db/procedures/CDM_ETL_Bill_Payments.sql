DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Bill_Payments (IN vBatchSize BIGINT,IN vTreat_As_Incr TINYINT) 
BEGIN 
 
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
     
  SET vEnd_Cnt = vBatchSize; 
 
  SET @Rec_Cnt = 0; 
  SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Bill_Payments ORDER BY Rec_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
   
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Bill_Payments ORDER BY Rec_Id DESC LIMIT 1; 
 
    IF @Rec_Cnt IS NULL THEN  
    SET @Rec_Cnt = 0; 
  END IF; 
 
  CREATE TABLE IF NOT EXISTS Temp_CDM_Bill_Payments ( 
    Rec_Id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    Rec_Is_Processed TINYINT DEFAULT - 1 NOT NULL, 
    Bill_Header_Id BIGINT DEFAULT - 1 NOT NULL, 
    Bill_Header_Key VARCHAR(128) NOT NULL, 
    Customer_Key VARCHAR(128) DEFAULT '' NOT NULL, 
    Customer_Id BIGINT DEFAULT -1 NOT NULL, 
    Bill_Date DATE, 
    Online_Payment DECIMAL(15,2) DEFAULT 0 NOT NULL, 
    Gift_Card DECIMAL(15,2) DEFAULT 0 NOT NULL, 
    Cash_Payment DECIMAL(15,2) DEFAULT 0 NOT NULL, 
    Card_Payment DECIMAL(15,2) DEFAULT 0 NOT NULL, 
    Coupon_Payment DECIMAL(15,2) DEFAULT 0 NOT NULL, 
    Loy_Redemption DECIMAL(15,2) DEFAULT 0 NOT NULL, 
    Wallet DECIMAL(15,2) DEFAULT 0 NOT NULL 
  )  ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
  ALTER TABLE Temp_CDM_Bill_Payments 
    ADD INDEX (Bill_Header_Key), 
    ADD INDEX (Customer_Key), 
    ADD INDEX (Rec_Is_Processed), 
    ADD INDEX (Bill_Date); 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Bill_Payments', 1, 'CDM_ETL_Bill_Payments', 1); 
     
  PROCESS_BH_HIST: LOOP 
    TRUNCATE TABLE Temp_CDM_Bill_Payments; 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Payments', CONCAT(vStart_Cnt,'_0'), 'CDM_ETL_Bill_Payments', 1); 
 
    INSERT INTO Temp_CDM_Bill_Payments( 
      Bill_Header_Id, 
      Bill_Header_Key, 
      Customer_Key, 
      Customer_Id, 
      Bill_Date, 
      Online_Payment, 
      Gift_Card, 
      Cash_Payment, 
      Card_Payment, 
      Coupon_Payment, 
      Loy_Redemption, 
      Wallet 
    ) 
      SELECT 
        Bill_Header_Id, 
        Bill_Header_Key, 
        Customer_Key, 
        Customer_Id, 
        Bill_Date, 
        Online_Payment, 
        Gift_Card, 
        Cash_Payment, 
        Card_Payment, 
        Coupon_Payment, 
        Loy_Redemption, 
        Wallet 
      FROM CDM_Stg_Bill_Payments 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Payments', CONCAT(vStart_Cnt,'_1'), 'CDM_ETL_Bill_Payments', 1); 
 
             
    INSERT IGNORE INTO CDM_Customer_Key_Lookup ( 
      Customer_Key 
    )     
      SELECT  
        Customer_Key 
      FROM Temp_CDM_Bill_Payments 
      WHERE 
        Customer_Key <> '' 
      GROUP BY Customer_Key; 
 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Payments', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Bill_Payments', 1); 
 
    INSERT IGNORE INTO CDM_Customer_PII_Master ( 
      Customer_Id 
    )     
      SELECT  
        Customer_Id 
      FROM Temp_CDM_Bill_Payments 
      WHERE  
        Customer_Key <> '' 
      GROUP BY Customer_Id; 
                     
     
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Payments', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Bill_Payments', 1); 
 
    INSERT IGNORE INTO  CDM_Customer_Master ( 
      Customer_Id, 
      Is_New_Cust_Flag 
    ) 
      SELECT  
        Customer_Id, 
        1 
      FROM Temp_CDM_Bill_Payments                         
      WHERE  
        Customer_Key <> '' 
      GROUP BY Customer_Id; 
 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Payments', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Bill_Payments', 1); 
 
    UPDATE IGNORE Temp_CDM_Bill_Payments 
    SET Customer_Id = ( 
        SELECT Customer_Id 
        FROM CDM_Customer_Key_Lookup 
        WHERE  
          CDM_Customer_Key_Lookup.Customer_Key = Temp_CDM_Bill_Payments.Customer_Key 
      ) 
    WHERE  
      Customer_Key <> ''; 
     
     
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Payments', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Bill_Payments', 1); 
 
    UPDATE IGNORE Temp_CDM_Bill_Payments 
    SET Bill_Header_Id = ( 
        SELECT Bill_Header_Id 
        FROM CDM_Bill_Header_Key_Lookup 
        WHERE CDM_Bill_Header_Key_Lookup.Bill_Header_Key = Temp_CDM_Bill_Payments.Bill_Header_Key 
      ) 
    WHERE 
      Bill_Header_Id <= 0; 
     
     
    UPDATE IGNORE Temp_CDM_Bill_Payments 
    SET Customer_Id = ( 
        SELECT Customer_Id 
        FROM CDM_Bill_Header 
        WHERE  
          CDM_Bill_Header.Bill_Header_Id = Temp_CDM_Bill_Payments.Bill_Header_Id 
      ) 
    WHERE 
            Customer_Key = ''; 
 
    UPDATE IGNORE Temp_CDM_Bill_Payments 
    SET Bill_Header_Id = ( 
        SELECT Bill_Header_Id 
        FROM CDM_NM_Bill_Header_Key_Lookup 
        WHERE CDM_NM_Bill_Header_Key_Lookup.Bill_Header_Key = Temp_CDM_Bill_Payments.Bill_Header_Key 
      ) 
    WHERE 
      Bill_Header_Id <= 0; 
     
     
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Payments', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Bill_Payments', 1); 
 
    INSERT IGNORE INTO CDM_Bill_Payments( 
        Bill_Header_Id, 
        Customer_Id, 
        Bill_Date, 
        Online_Payment, 
        Gift_Card, 
        Cash_Payment, 
        Card_Payment, 
        Coupon_Payment, 
        Loy_Redemption, 
        Wallet 
      )  
      SELECT 
        Bill_Header_Id, 
        Customer_Id, 
        Bill_Date, 
        Online_Payment, 
        Gift_Card, 
        Cash_Payment, 
        Card_Payment, 
        Coupon_Payment, 
        Loy_Redemption, 
        Wallet 
      FROM  
        Temp_CDM_Bill_Payments 
      WHERE 
        Customer_Id > 0; 
 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Payments', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Bill_Payments', 1); 
 
    INSERT IGNORE INTO CDM_NM_Bill_Payments( 
        Bill_Header_Id, 
        Online_Payment, 
        Gift_Card, 
        Cash_Payment, 
        Card_Payment, 
        Coupon_Payment, 
        Loy_Redemption, 
        Wallet 
      )  
      SELECT 
        Bill_Header_Id, 
        Online_Payment, 
        Gift_Card, 
        Cash_Payment, 
        Card_Payment, 
        Coupon_Payment, 
        Loy_Redemption, 
        Wallet 
      FROM  
        Temp_CDM_Bill_Payments 
      WHERE 
        Customer_Id <= 0; 
 
      SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_BH_HIST; 
        END IF; 
         
  END LOOP PROCESS_BH_HIST; 
   
  DROP TABLE Temp_CDM_Bill_Payments; 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Bill_Payments', 2, 'CDM_ETL_Bill_Payments', 1); 
 
END	
mysolus$$
