DELIMITER mysolus$$

CREATE OR REPLACE PROCEDURE `CDM_ETL_Voucher`(IN vBatchSize BIGINT)
BEGIN
  DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT;
    
  SET vEnd_Cnt = vBatchSize;

    SET @Rec_Cnt = 0;
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Voucher_Master ORDER BY Rec_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;
  SET vEnd_Cnt = vStart_Cnt + vBatchSize;
  
    SET @Rec_Cnt = 0;
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Voucher_Master ORDER BY Rec_Id DESC LIMIT 1;
    IF @Rec_Cnt IS NULL THEN 
    SET @Rec_Cnt = 0;
  END IF;

  CALL CDM_Update_Process_Log('CDM_ETL_Voucher', 1, 'CDM_ETL_Voucher', 1);
    
  PROCESS_Voucher: LOOP
    
    INSERT IGNORE INTO CDM_Voucher_Key_Lookup (
      Voucher_Key
    )
      SELECT 
        Voucher_Key
      FROM 
        CDM_Stg_Voucher_Master
      WHERE 
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;
    
    
    CALL CDM_Update_Process_Log('CDM_ETL_Voucher', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Voucher', 1);

    UPDATE IGNORE CDM_Stg_Voucher_Master
        SET Voucher_Id = (
      SELECT Voucher_Id 
            FROM CDM_Voucher_Key_Lookup
            WHERE
        CDM_Stg_Voucher_Master.Voucher_Key = CDM_Voucher_Key_Lookup.Voucher_Key
    )
    WHERE 
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
            Voucher_Id <= 0 AND
      Rec_Is_Processed = -1;
      
    UPDATE IGNORE CDM_Stg_Voucher_Master -- ashutosh
    SET Customer_Id = (
      SELECT Customer_Id 
      FROM CDM_Customer_Key_Lookup
      WHERE
        CDM_Stg_Voucher_Master.Customer_Key = CDM_Customer_Key_Lookup.Customer_Key
    )
    WHERE Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
            Customer_Key  <> '' AND
      Rec_Is_Processed = -1;
   

        
    CALL CDM_Update_Process_Log('CDM_ETL_Voucher', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Voucher', 1);

    INSERT IGNORE INTO CDM_Vouchers (
      Voucher_Id,
      Issue_Date,
      Issue_Year,
      Issue_Year_Month,
      Issue_Day_Week,
      Validity_Period,
      Expiry_Date,
      Expiry_Year,
      Expiry_Year_Month,
      Expiry_Day_Week,
      Customer_Id
    )
      SELECT 
        Voucher_Id,
        Issue_Date,
        IFNULL(YEAR(Issue_Date),''),
        IFNULL(EXTRACT(YEAR_MONTH FROM Issue_Date),''),
        IFNULL(WEEKDAY(Issue_Date),-1),     
        Validity_Period,
        Expiry_Date,
        IFNULL(YEAR(Expiry_Date),''),
        IFNULL(EXTRACT(YEAR_MONTH FROM Expiry_Date),''),
        IFNULL(WEEKDAY(Expiry_Date),-1),
        Customer_Id
      FROM CDM_Stg_Voucher_Master
      WHERE 
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

      SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;
        
        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE PROCESS_Voucher;
        END IF;
        
  END LOOP PROCESS_Voucher;

  CALL CDM_Update_Process_Log('CDM_ETL_Voucher', 2, 'CDM_ETL_Voucher', 1);
  
END
mysolus$$
