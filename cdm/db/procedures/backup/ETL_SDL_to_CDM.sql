DELIMITER mysolus$$

 CREATE  OR REPLACE  PROCEDURE `ETL_SDL_to_CDM`(IN vBatchSize BIGINT)
BEGIN

 
    DECLARE vSuccessRet TINYINT DEFAULT 0;
  DECLARE vBatchSize BIGINT DEFAULT 500000;
  
  DECLARE vStart_Cnt BIGINT DEFAULT 0;
    DECLARE vEnd_Cnt BIGINT;

  
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET vSuccessRet = 0;

  SELECT CAST(LOV_Value AS UNSIGNED) INTO vBatchSize 
  FROM LOV_Master 
  WHERE 
    LOV_Attribute = 'SOLUS_SEED'
    AND LOV_Key = 'UPPERBATCHSIZE';
  
  SET vEnd_Cnt = vBatchSize;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_LOV_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_LOV_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  TRUNCATE CDM_Stg_LOV_Master;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 1, 'Load into LOV Master', 1);

  INSERT_INTO_STG_LOV_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_LOV_Master (
      LOV_Attribute,
      LOV_Key,
      LOV_Value,
      LOV_Comm_Value,
      LOV_Value_Code,
      LOV_Start_Date,
      LOV_End_Date
    )
      SELECT
        LOV_Attribute,
        LOV_Key,
        LOV_Value,
        LOV_Comm_Value,
        LOV_Value_Code,
        IF(DAYNAME(LOV_Start_Date)IS NULL,NULL,LOV_Start_Date),
        IF(DAYNAME(LOV_End_Date)IS NULL,NULL,LOV_End_Date)
      FROM SDL_Stg_LOV_Master
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

      SET vStart_Cnt = vStart_Cnt + vBatchSize;
      SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

    IF vStart_Cnt  >= @Rec_Cnt THEN
      LEAVE INSERT_INTO_STG_LOV_MASTER;
    END IF;

  END LOOP INSERT_INTO_STG_LOV_MASTER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Store_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Store_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Store_Master;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 2, 'Load into Store Master', 1);

  INSERT_INTO_STG_STORE_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Store_Master
    (
      Store_Key,
      Store_Status,
      Open_Date,
      Closure_Date,
      Store_Size,
      Store_Region,
      Store_Address1,
      Store_Address2,
      Store_Area,
      Store_City,
      Store_Pincode,
      Store_State_Key,
      Store_Lat,
      Store_Long,
      Store_Name,
      Store_Comm_Name,
      Store_Ownership,
      Store_Format,
      Store_Location_Type,
      Store_Channel_Type,
      Store_Type_Tag1,
      Store_Type_Tag2,
      Store_Type_Tag3
    )
      SELECT
        TRIM(Store_Key),
        TRIM(Store_Status),
        IF(DAYNAME(Open_Date)IS NULL,NULL,Open_Date),
        IF(DAYNAME(Closure_Date)IS NULL,NULL,Closure_Date),
        Store_Size,
        TRIM(Store_Region),
        TRIM(Store_Address1),
        TRIM(Store_Address2),
        TRIM(Store_Area),
        TRIM(Store_City),
        TRIM(Store_Pincode),
        TRIM(Store_State_Key),
        TRIM(Store_Lat),
        TRIM(Store_Long),
        TRIM(Store_Name),
        TRIM(Store_Comm_Name),
        TRIM(Store_Ownership),
        TRIM(Store_Format),
        TRIM(Store_Location_Type),
        TRIM(Store_Channel_Type),
        Store_Type_Tag1,
        Store_Type_Tag2,
        Store_Type_Tag3
      FROM SDL_Stg_Store_Master
      WHERE
      SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
      Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_STORE_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_STORE_MASTER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Brand_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Brand_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Brand_Master;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 3, 'Load into Brand Master', 1);

  INSERT_INTO_STG_BRAND_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Brand_Master (
      Brand_Key,
      Brand_Name,
      Is_Premium,
      Brand_Type_Tag1,
      Brand_Type_Tag2,
      Brand_Type_Tag3,
      Brand_Comm_Name
    )
      SELECT
        Brand_Key,
        Brand_Name,
        Is_Premium,
        Brand_Type_Tag1,
        Brand_Type_Tag2,
        Brand_Type_Tag3,
        Brand_Comm_Name
      FROM SDL_Stg_Brand_Master
      WHERE
      SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
      Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_BRAND_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_BRAND_MASTER;
  
  
  

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_BU_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_BU_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 4, 'Load into BU Master', 1);
  TRUNCATE CDM_Stg_BU_Master;

  INSERT_INTO_STG_BU_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_BU_Master (
      BU_Key,
      BU_Name,
      BU_Type_Tag1,
      BU_Type_Tag2,
      BU_Type_Tag3,
      BU_Comm_Name
    )
      SELECT
        BU_Key,
        BU_Name,
        BU_Type_Tag1,
        BU_Type_Tag2,
        BU_Type_Tag3,
        BU_Comm_Name
      FROM SDL_Stg_BU_Master
      WHERE
      SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
      Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_BU_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_BU_MASTER;
  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Cat_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Cat_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 5, 'Load into Cat Master', 1);
  TRUNCATE CDM_Stg_Cat_Master;

  INSERT_INTO_STG_CAT_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Cat_Master (
      Cat_Key,
      Cat_Name,
      Cat_Type_Tag1,
      Cat_Type_Tag2,
      Cat_Type_Tag3,
      Cat_Comm_Name
    )
      SELECT
        Cat_Key,
        Cat_Name,
        Cat_Type_Tag1,
        Cat_Type_Tag2,
        Cat_Type_Tag3,
        Cat_Comm_Name
      FROM SDL_Stg_Cat_Master
      WHERE
      SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
      Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_CAT_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_CAT_MASTER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Dep_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Dep_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 6, 'Load into Dep Master', 1);
  TRUNCATE CDM_Stg_Dep_Master;

  INSERT_INTO_STG_DEP_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Dep_Master (
      Dep_Key,
      Dep_Name,
      Dep_Type_Tag1,
      Dep_Type_Tag2,
      Dep_Type_Tag3,
      Dep_Comm_Name
    )
      SELECT
        Dep_Key,
        Dep_Name,
        Dep_Type_Tag1,
        Dep_Type_Tag2,
        Dep_Type_Tag3,
        Dep_Comm_Name
      FROM SDL_Stg_Dep_Master
      WHERE
      SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
      Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_DEP_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_DEP_MASTER;

  
  
  

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Div_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Div_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 7, 'Load into Div Master', 1);
  TRUNCATE CDM_Stg_Div_Master;

  INSERT_INTO_STG_DIV_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Div_Master (
      Div_Key,
      Div_Name,
      Div_Type_Tag1,
      Div_Type_Tag2,
      Div_Type_Tag3,
      Div_Comm_Name
    )
      SELECT
        Div_Key,
        Div_Name,
        Div_Type_Tag1,
        Div_Type_Tag2,
        Div_Type_Tag3,
        Div_Comm_Name
      FROM SDL_Stg_Div_Master
      WHERE
      SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
      Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_DIV_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_DIV_MASTER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Season_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Season_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 8, 'Load into Season Master', 1);
  TRUNCATE CDM_Stg_Season_Master;

  INSERT_INTO_STG_SEASON_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Season_Master (
      Season_Key,
      Season_Name,
      Season_Type_Tag1,
      Season_Type_Tag2,
      Season_Type_Tag3,
      Season_Comm_Name
    )
      SELECT
        Season_Key,
        Season_Name,
        Season_Type_Tag1,
        Season_Type_Tag2,
        Season_Type_Tag3,
        Season_Comm_Name
      FROM SDL_Stg_Season_Master
      WHERE
      SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
      Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_SEASON_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_SEASON_MASTER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Section_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Section_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 9, 'Load into Section Master', 1);
  TRUNCATE CDM_Stg_Section_Master;

  INSERT_INTO_STG_SECTION_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Section_Master (
      Section_Key,
      Section_Name,
      Section_Type_Tag1,
      Section_Type_Tag2,
      Section_Type_Tag3,
      Section_Comm_Name
    )
      SELECT
        Section_Key,
        Section_Name,
        Section_Type_Tag1,
        Section_Type_Tag2,
        Section_Type_Tag3,
        Section_Comm_Name
      FROM SDL_Stg_Section_Master
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_SECTION_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_SECTION_MASTER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Manuf_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Manuf_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 10, 'Load into Manuf Master', 1);
  TRUNCATE CDM_Stg_Manuf_Master;

  INSERT_INTO_STG_MANUF_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Manuf_Master (
      Manuf_Key,
      Manuf_Name,
      Manuf_Type_Tag1,
      Manuf_Type_Tag2,
      Manuf_Type_Tag3,
      Manuf_Comm_Name
    )
      SELECT
        Manuf_Key,
        Manuf_Name,
        Manuf_Type_Tag1,
        Manuf_Type_Tag2,
        Manuf_Type_Tag3,
        Manuf_Comm_Name
      FROM SDL_Stg_Manuf_Master
      WHERE
      SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
      Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_MANUF_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_MANUF_MASTER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Product_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Product_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 11, 'Load into Product Master', 1);
  TRUNCATE CDM_Stg_Product_Master;

  INSERT_INTO_STG_PRODUCT_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Product_Master
    (
      Product_Key,
      Is_Private_Label,
      Is_Premium,
      Unit_Price,
      Launch_Date,
      Brand_Key,
      Div_Key,
      Dep_Key,
      Cat_Key,
      Season_Key,
      BU_Key,
      Section_Key,
      Manuf_Key,
      Brand_Name,
      Div_Name,
      Dep_Name,
      Cat_Name,
      Season_Name,
      BU_Name,
      Section_Name,
      Manuf_Name,
      Uom,
      Tax_Category,
      Product_Name,
      Product_Desc,
      Product_Comm_Name,
      To_Be_Excluded,
      Is_Combo,
      Is_Ingredient,
      Festival_During_Launch,
      Festival_Id_During_Launch,
      Brand_Event_During_Launch,
      Fabric_Base,
      Fit,
      Design,
      Colour,
      Associated_Age,
      Replenish_Days,
      Custom_Attrib,
      Custom_Tag1,
      Custom_Tag2,
      Custom_Tag3,
      Custom_Tag4,
      Custom_Tag5,
      Product_Genome,
         `Bottom_Cut`,
 `Sleeve`,
 `Collar`,
 `Collar_Top_Button`,
 `Collar_Stiffness`,
 `Collar_Contrast_Type`,
  `Collar_Piping_Type`,
  `Cuffs`,
 `Cuffs_Shape`,
 `Cuffs_Stiffness`,
 `Cuffs_Contrast_Type`,
 `Cuffs_Piping_Type`,
 `Button`,
 `Thread_Color`,
 `Pocket_Placement`,
 `Pocket_Contrast_Type`,
 `Placket`,
 `Placket_Contrast_Type`,
 `Placket_Piping_Type`,
 `Epaulette`,
 `Collar_Piping_Twill_Tape`,
 `Pocket_Contrast_Leather`,
 `Pocket_Contrast_Suede`,
 `Cuffs_Piping_Twill_Tape`,
 `Placket_Button`,
 `Placket_Piping_Twill_Tape`,
 `Epaulette_Contrast_Leather`,
 `Epaulette_Contrast_Suede`,
 `Elbow_Patch`,
 `Elbow_Patch_Contrast_Leather`,
 `Elbow_Patch_Contrast_Suede`,
 `Back`,
 `Monogram`,
 `Monogram_Color`,
 `Gusset`, 
 `Gusset_Contrast_Leather`, 
 `Gusset_Contrast_Suede` 

    )
      SELECT
        TRIM(Product_Key),
        Is_Private_Label,
        Is_Premium,
        Unit_Price,
        Launch_Date,
        TRIM(Brand_Key),
        TRIM(Div_Key),
        TRIM(Dep_Key),
        TRIM(Cat_Key),
        TRIM(Season_Key),
        TRIM(BU_Key),
        TRIM(Section_Key),
        TRIM(Manuf_Key),
        TRIM(Brand_Name),
        TRIM(Div_Name),
        TRIM(Dep_Name),
        TRIM(Cat_Name),
        TRIM(Season_Name),
        TRIM(BU_Name),
        TRIM(Section_Name),
        TRIM(Manuf_Name),
        Uom,
        TRIM(Tax_Category),
        TRIM(Product_Name),
        TRIM(Product_Desc),
        TRIM(Product_Comm_Name),
        To_Be_Excluded,
        Is_Combo,
        Is_Ingredient,
        TRIM(Festival_During_Launch),
        Festival_Id_During_Launch,
        TRIM(Brand_Event_During_Launch),
        TRIM(Fabric_Base),
        TRIM(Fit),
        TRIM(Design),
        TRIM(Colour),
        Associated_Age,
        Replenish_Days,
        Custom_Attrib,
        Custom_Tag1,
        Custom_Tag2,
        Custom_Tag3,
        Custom_Tag4,
        Custom_Tag5,
        Product_Genome,
           `Bottom_Cut`,
 `Sleeve`,
 `Collar`,
 `Collar_Top_Button`,
 `Collar_Stiffness`,
 `Collar_Contrast_Type`,
  `Collar_Piping_Type`,
  `Cuffs`,
 `Cuffs_Shape`,
 `Cuffs_Stiffness`,
 `Cuffs_Contrast_Type`,
 `Cuffs_Piping_Type`,
 `Button`,
 `Thread_Color`,
 `Pocket_Placement`,
 `Pocket_Contrast_Type`,
 `Placket`,
 `Placket_Contrast_Type`,
 `Placket_Piping_Type`,
 `Epaulette`,
 `Collar_Piping_Twill_Tape`,
 `Pocket_Contrast_Leather`,
 `Pocket_Contrast_Suede`,
 `Cuffs_Piping_Twill_Tape`,
 `Placket_Button`,
 `Placket_Piping_Twill_Tape`,
 `Epaulette_Contrast_Leather`,
 `Epaulette_Contrast_Suede`,
 `Elbow_Patch`,
 `Elbow_Patch_Contrast_Leather`,
 `Elbow_Patch_Contrast_Suede`,
 `Back`,
 `Monogram`,
 `Monogram_Color`,
 `Gusset`, 
 `Gusset_Contrast_Leather`, 
 `Gusset_Contrast_Suede` 

      FROM SDL_Stg_Product_Master
      WHERE
      SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
      Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_PRODUCT_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_PRODUCT_MASTER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Product_Ingredients ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Product_Ingredients ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Product_Ingredients;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 12, 'Load into Product Ingredients', 1);

  INSERT_INTO_STG_PRODUCT_INGREDIENTS : LOOP

    INSERT IGNORE INTO CDM_Stg_Product_Ingredients (
      Product_Key,
      Parent_Product_Key
    )
      SELECT
        Product_Key,
        Parent_Product_Key
      FROM SDL_Stg_Product_Ingredients
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_PRODUCT_INGREDIENTS;
        END IF;

  END LOOP INSERT_INTO_STG_PRODUCT_INGREDIENTS;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Customer_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Customer_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Customer_Master;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 13, 'Load into Customer Master', 1);

  INSERT_INTO_STG_CUSTOMER_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Customer_Master
    (
      Customer_Key,
      Mobile,
      Email,
      First_Name,
      Middle_Name,
      Last_Name,
      Full_Name,
      Address_Line1,
      Address_Line2,
      Latitude,
      Longitude,
      Comm_Name,
      City,
      Doj,
      Enrolled_Store_Key,
      Enrolled_Channel,
      Gender,
      Dob,
      Pincode,
      Marital_Status,
      Members_In_Household,
      Email_Consent,
      SMS_Consent,
      Customer_State,
      DoA,
      DND,
      Ethnicity,
      Tier_Name,
      Point_Balance,
      Customer_Custom_Tag1,
      Customer_Custom_Tag2,
      Customer_Custom_Tag3
    )
      SELECT
        TRIM(Customer_Key),
        TRIM(Mobile),
        TRIM(Email),
        TRIM(First_Name),
        TRIM(Middle_Name),
        TRIM(Last_Name),
        TRIM(Full_Name),
        TRIM(Address_Line1),
        TRIM(Address_Line2),
        Latitude,
        Longitude,
        TRIM(Comm_Name),
        TRIM(City),
        IF(DAYNAME(Doj)IS NULL,NULL,Doj),
        TRIM(Enrolled_Store_Key),
        TRIM(Enrolled_Channel),
        TRIM(Gender),
        IF(DAYNAME(Dob)IS NULL,NULL,Dob),
        Pincode,
        TRIM(Marital_Status),
        Members_In_Household,
        Email_Consent,
        SMS_Consent,
        TRIM(Customer_State),
        IF(DAYNAME(DoA)IS NULL,NULL,DoA),
        TRIM(DND),
                TRIM(Ethnicity),
        TRIM(Tier_Name),
        Point_Balance,
        Customer_Custom_Tag1,
        Customer_Custom_Tag2,
        Customer_Custom_Tag3
      FROM SDL_Stg_Customer_Master
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_CUSTOMER_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_CUSTOMER_MASTER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Bill_Header ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Bill_Header ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Bill_Header;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 14, 'Load into Bill Header', 1);

  INSERT_INTO_STG_BILL_HEADER : LOOP

    INSERT IGNORE INTO CDM_Stg_Bill_Header
    (   Bill_Header_Key,
      Customer_Key,
      Store_Key,
      Bill_Date,
      Bill_Time,
      Bill_Qty,
      Bill_Gross_Val,
      Bill_Disc,
      Bill_Tax,
      Bill_Total_Val,
      Txn_Channel
    )
      SELECT
        Bill_Header_Key,
        Customer_Key,
        Store_Key,
        Bill_Date,
        Bill_Time,
        Bill_Qty,
        Bill_Gross_Val,
        Bill_Disc,
        Bill_Tax,
        Bill_Total_Val,
        Txn_Channel
      FROM
        SDL_Stg_Bill_Header
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_BILL_HEADER;
        END IF;

  END LOOP INSERT_INTO_STG_BILL_HEADER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Bill_Payments ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Bill_Payments ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Bill_Payments;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 15, 'Load into Bill Payments', 1);

  INSERT_INTO_STG_BILL_PAYMENTS : LOOP

    INSERT IGNORE INTO CDM_Stg_Bill_Payments
    (
      Bill_Header_Key,
      Customer_Key,
      Bill_Date,
      Online_Payment,
      Gift_Card,
      Cash_Payment,
      Card_Payment,
      Coupon_Payment,
      Loy_Redemption,
      Wallet
    )
      SELECT
        Bill_Header_Key,
        Customer_Key,
        Bill_Date,
        Online_Payment,
        Gift_Card,
        Cash_Payment,
        Card_Payment,
        Coupon_Payment,
        Loy_Redemption,
        Wallet
      FROM
        SDL_Stg_Bill_Payments
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_BILL_PAYMENTS;
        END IF;

  END LOOP INSERT_INTO_STG_BILL_PAYMENTS;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Bill_Details ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Bill_Details ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Bill_Details;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 16, 'Load into Bill Details', 1);

  INSERT_INTO_STG_BILL_DETAILS : LOOP

    INSERT IGNORE INTO CDM_Stg_Bill_Details
    (
      Bill_Details_Key,
      Bill_Header_Key,
      Line_No,
      Store_Key,
      Product_Key,
      Customer_Key,
      Broker_Key,
      Bill_Date,
      Bill_Time,
      Sale_Qty,
      Sale_Gross_Val,
      Sale_Disc_Val,
      Sale_Tax_Val,
      Sale_Net_Val,
      Mrp,
            Trans_Type,
            Trans_Sub_Type,
            Source_Program,
            Cat_Size,
      Txn_Status_Date,
       `Bust`,
      `Waist`,
       `Sleeve`,
        `Neck`,
        `Tuniclength`,
        `SizeOption`,
        `StyleOption`, 
        `SizeType`
    )
      SELECT
        TRIM(Bill_Details_Key),
        TRIM(Bill_Header_Key),
        TRIM(Line_No),
        TRIM(Store_Key),
        TRIM(Product_Key),
        TRIM(Customer_Key),
        TRIM(Broker_Key),
        Bill_Date,
        Bill_Time,
        Sale_Qty,
        Sale_Gross_Val,
        Sale_Disc_Val,
        Sale_Tax_Val,
        Sale_Net_Val,
        Mrp,
                TRIM(Trans_Type),
                TRIM(Trans_Sub_Type),
                TRIM(Source_Program),
                Cat_Size,
        Txn_Status_Date,
        `Bust`,
      `Waist`,
       `Sleeve`,
        `Neck`,
        `Tuniclength`,
        `SizeOption`,
        `StyleOption`, 
        `SizeType`
      FROM
        SDL_Stg_Bill_Details
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_BILL_DETAILS;
        END IF;

  END LOOP INSERT_INTO_STG_BILL_DETAILS;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Points_Earned ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Points_Earned ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Points_Earned;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 17, 'Load into Points Earned', 1);

  INSERT_INTO_STG_POINTS_EARNED : LOOP

    INSERT IGNORE INTO CDM_Stg_Points_Earned
    (
      Points_Earned_Rec_Key,
      Bill_Header_Key,
      Customer_Key,
      Points_Earned,
      Earned_Date
    )
      SELECT
        Points_Earned_Rec_Key,
        Bill_Header_Key,
        Customer_Key,
        Points_Earned,
        IF(DAYNAME(Earned_Date)IS NULL,NULL,Earned_Date)
      FROM SDL_Stg_Points_Earned
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_POINTS_EARNED;
        END IF;

  END LOOP INSERT_INTO_STG_POINTS_EARNED;

  
  
  

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Points_Redemption ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Points_Redemption ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Points_Redemption;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 18, 'Load Point Redeemed', 1);

  INSERT_INTO_STG_POINTS_REDEMPTION : LOOP

    INSERT IGNORE INTO CDM_Stg_Points_Redemption (
      Points_Redemption_Rec_Key,
      Bill_Header_Key,
      Customer_Key,
      Points_Redemption,
      Redemption_Date
    )
      SELECT
        Points_Redemption_Rec_Key,
        Bill_Header_Key,
        Customer_Key,
        Points_Redemption,
        IF(DAYNAME(Redemption_Date)IS NULL,NULL,Redemption_Date)
      FROM SDL_Stg_Points_Redemption
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_POINTS_REDEMPTION;
        END IF;

  END LOOP INSERT_INTO_STG_POINTS_REDEMPTION;

  
  
  

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Offers ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Offers ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Offers;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 19, 'Load Offers', 1);

  INSERT_INTO_STG_OFFERS : LOOP

    INSERT IGNORE INTO CDM_Stg_Offers
    (
      Offer_Key,
      Offer_Code,
      Validity_Period,
      Offer_Score,
      Offer_Type,
      Offer_Desc
    )
      SELECT
        Offer_Key,
        Offer_Code,
        Validity_Period,
        Offer_Score,
        Offer_Type,
        Offer_Desc
      FROM SDL_Stg_Offers
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_OFFERS;
        END IF;

  END LOOP INSERT_INTO_STG_OFFERS;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT Rec_Id INTO @Rec_Cnt FROM SDL_Stg_Voucher_Master ORDER BY Rec_Id ASC LIMIT 1; -- ashutosh
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT Rec_Id INTO @Rec_Cnt FROM SDL_Stg_Voucher_Master ORDER BY Rec_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Voucher_Master;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 20, 'Load Voucher', 1);

  INSERT_INTO_STG_VOUCHER : LOOP

    INSERT IGNORE INTO CDM_Stg_Voucher_Master (
      Voucher_Key,
      Issue_Date,
      Validity_Period,
      Expiry_Date,
      Customer_Key
    )
      SELECT
        Voucher_Key,
        IF(DAYNAME(Issue_Date)IS NULL,NULL,Issue_Date),
        Validity_Period,
        IF(DAYNAME(Expiry_Date)IS NULL,NULL,Expiry_Date),
        Customer_Key
      FROM SDL_Stg_Voucher_Master
      WHERE
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_VOUCHER;
        END IF;

  END LOOP INSERT_INTO_STG_VOUCHER;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT Rec_Id INTO @Rec_Cnt FROM SDL_Stg_Voucher_Redemption ORDER BY Rec_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT Rec_Id INTO @Rec_Cnt FROM SDL_Stg_Voucher_Redemption ORDER BY Rec_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Voucher_Redemption;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 21, 'Load Voucher Redemption', 1);

  INSERT_INTO_STG_VOUCHER_REDEMPTION : LOOP

    INSERT IGNORE INTO CDM_Stg_Voucher_Redemption (
      Voucher_Redemption_Rec_Key,
      Bill_Header_Key,
      Customer_Key,
      Voucher_Value,
      Redemption_Date,
      Voucher_Key
    )
      SELECT
        Voucher_Redemption_Rec_Key,
        Bill_Header_Key,
        Customer_Key,
        Voucher_Value,
        IF(DAYNAME(Redemption_Date)IS NULL,NULL,Redemption_Date),
        Voucher_Key
      FROM SDL_Stg_Voucher_Redemption
      WHERE
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_VOUCHER_REDEMPTION;
        END IF;

  END LOOP INSERT_INTO_STG_VOUCHER_REDEMPTION;

  
  
  
  
    SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Customer_Membership ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Customer_Membership ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Customer_Membership;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 22, 'Load into membership', 1);

  INSERT_INTO_STG_MEMBERSHIP : LOOP

    INSERT IGNORE INTO CDM_Stg_Customer_Membership (
      Membership_Rec_Key,
      Customer_Key,
      Mobile,
      Tier_Name,
            Tier_Expiry_Date,
            Tier_Updated_Date

    )
      SELECT
        TRIM(Membership_Rec_Key),
        TRIM(Customer_Key),
        TRIM(Mobile),
        TRIM(Tier_Name),
        Tier_Expiry_Date,
        Tier_Updated_Date
      FROM SDL_Stg_Customer_Membership
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_MEMBERSHIP;
        END IF;

  END LOOP INSERT_INTO_STG_MEMBERSHIP;

  
  
  
  

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Points_Expiry ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Points_Expiry ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Points_Expiry;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 23, 'Load Points Expiry', 1);

  INSERT_INTO_STG_POINTS_EXPIRY : LOOP

    INSERT IGNORE INTO CDM_Stg_Points_Expiry
    (
      Points_Expiry_Rec_Key,
      Customer_Key,
      Expiring_Points,
      Expiry_Date
    )
      SELECT
        Points_Expiry_Rec_Key,
        Customer_Key,
        Points_Expired,
        IF(DAYNAME(Expired_Date)IS NULL,NULL,Expired_Date)
      FROM SDL_Stg_Points_Expiry
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_POINTS_EXPIRY;
        END IF;

  END LOOP INSERT_INTO_STG_POINTS_EXPIRY;

  
  
  

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Feedback ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Feedback ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Feedback;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 24, 'Load into Feedback', 1);

  INSERT_INTO_STG_FEEDBACK : LOOP

    INSERT IGNORE INTO CDM_Stg_Feedback
    (
      Fdbk_Key,
      Fdbk_Brand_Key,
      Fdbk_Store_Key,
      Fdbk_Customer_Key,
      Fdbk_Customer_Mobile,
      Fdbk_Customer_Email,
      Fdbk_Customer_Gender,
      Fdbk_Customer_Age_Group,
      Fdbk_Customer_Occupation,
      Fdbk_Score,
      Fdbk_Product_Variety_Score,
      Fdbk_Product_Quality_Score,
      Fdbk_Product_Pricing_Score,
      Fdbk_Product_Relevance_Score,
      Fdbk_Product_Availability_Score,
      Fdbk_Survey_Time,
      Fdbk_Survey_Date,
      Fdbk_Staff_Service_Rating,
      Fdbk_Duration,
      FDBK_L1RSN
    )
    SELECT
      TRIM(Fdbk_Key),
      TRIM(Fdbk_Brand_Key),
      TRIM(Fdbk_Store_Key),
      TRIM(Fdbk_Customer_Key),
      TRIM(Fdbk_Customer_Mobile),
      TRIM(Fdbk_Customer_Email),
      TRIM(Fdbk_Customer_Gender),
      TRIM(Fdbk_Customer_Age_Group),
      TRIM(Fdbk_Customer_Occupation),
      Fdbk_Score,
      Fdbk_Product_Variety_Score,
      Fdbk_Product_Quality_Score,
      Fdbk_Product_Pricing_Score,
      Fdbk_Product_Relevance_Score,
      Fdbk_Product_Availability_Score,
      Fdbk_Survey_Time,
      IF(DAYNAME(Fdbk_Survey_Date) IS NULL,NULL,Fdbk_Survey_Date),
      Fdbk_Staff_Service_Rating,
      Fdbk_Duration,
      FDBK_L1RSN
    FROM
    SDL_Stg_Feedback
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_FEEDBACK;
        END IF;

  END LOOP INSERT_INTO_STG_FEEDBACK;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_SMS_Del_Rep ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_SMS_Del_Rep ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_SMS_Del_Rep;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 25, 'Load into SMS Delivery Report', 1);

  INSERT_INTO_STG_SMS_DEL_REP : LOOP

    INSERT IGNORE INTO CDM_Stg_SMS_Del_Rep (
      Customer_Mobile_Number,
      Sent_Date,
      Sent_Time,
      Delivery_Date,
      Delivery_Time,
      Delivery_Status,
      Open_Date,
      Open_Time,
      Open_Status,
      SMS_Type,
      SMS_Operator,
      SMS_Circle,
      SMS_No_Per_Message,
      Event_Execution_ID
    )
      SELECT
        Customer_Mobile_Number,
        Sent_Date,
        Sent_Time,
        Delivery_Date,
        Delivery_Time,
        Delivery_Status,
        Open_Date,
        Open_Time,
        Open_Status,
        SMS_Type,
        SMS_Operator,
        SMS_Circle,
        SMS_No_Per_Message,
        Event_Execution_ID
      FROM SDL_Stg_SMS_Del_Rep
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_SMS_DEL_REP;
        END IF;

  END LOOP INSERT_INTO_STG_SMS_DEL_REP;

  
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Email_Del_Rep ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Email_Del_Rep ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Email_Del_Rep;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 26, 'Load into SMS Delivery Report', 1);

  INSERT_INTO_STG_EMAIL_DEL_REP : LOOP

    INSERT IGNORE INTO CDM_Stg_Email_Del_Rep (
      Customer_Email,
      Sent_Date,
      Sent_Time,
      Delivery_Date,
      Delivery_Time,
      Delivery_Status,
      Open_Date,
      Open_Time,
      Open_Status,
      Event_Execution_ID
    )
      SELECT
        Customer_Email,
        Sent_Date,
        Sent_Time,
        Delivery_Date,
        Delivery_Time,
        Delivery_Status,
        Open_Date,
        Open_Time,
        Open_Status,
        Event_Execution_ID
      FROM SDL_Stg_Email_Del_Rep
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_EMAIL_DEL_REP;
        END IF;

  END LOOP INSERT_INTO_STG_EMAIL_DEL_REP;

  
  
  

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_PN_Del_Rep ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_PN_Del_Rep ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_PN_Del_Rep;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 27, 'Load into SMS Delivery Report', 1);

  INSERT_INTO_STG_PN_DEL_REP : LOOP

    INSERT IGNORE INTO CDM_Stg_PN_Del_Rep (
      Customer_Mobile_Number,
      Sent_Date,
      Sent_Time,
      Delivery_Date,
      Delivery_Time,
      Delivery_Status,
      Open_Date,
      Open_Time,
      Open_Status,
      Event_Execution_ID
    )
      SELECT
        Customer_Mobile_Number,
        Sent_Date,
        Sent_Time,
        Delivery_Date,
        Delivery_Time,
        Delivery_Status,
        Open_Date,
        Open_Time,
        Open_Status,
        Event_Execution_ID
      FROM SDL_Stg_PN_Del_Rep
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_PN_DEL_REP;
        END IF;

  END LOOP INSERT_INTO_STG_PN_DEL_REP;

  
  
    SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Broker_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Broker_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Broker_Master;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 28, 'Load into CDM_Stg_Broker_Master ', 1);

  INSERT_INTO_STG_BROKER_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Broker_Master (
      Broker_Key ,
      Broker_Comm_Name ,
      Doj ,
      Dob ,
      Pincode ,
      Mobile ,
      Email ,
      Performance_Evaluation_Date,
      Client_Assets ,
      Overall_Assets ,
      Client_Gross_Sales ,
      Overall_Gross_Sales ,
      Client_Net_Sales ,
      Overall_Net_Sales ,
      Client_New_SIP ,
      Overall_New_SIP ,
      Client_Total_SIP ,
      Overall_Total_SIP ,
      Broker_Bank_Key ,
      Broker_Category_Key ,
      Broker_Channel_Key ,
      Broker_Region_Key ,
      Broker_Store_Key ,
      Broker_Type_Tag1 ,
      Broker_Type_Tag2 ,
      Broker_Type_Tag3 
    )
      SELECT
            TRIM(Broker_Key) ,
          TRIM(Broker_Comm_Name),
          Doj ,
          Dob ,
          Pincode ,
          TRIM(Mobile),
          Email ,
          Performance_Evaluation_Date,
          Client_Assets ,
          Overall_Assets ,
          Client_Gross_Sales ,
          Overall_Gross_Sales ,
          Client_Net_Sales ,
          Overall_Net_Sales ,
          Client_New_SIP ,
          Overall_New_SIP ,
          Client_Total_SIP ,
          Overall_Total_SIP ,
          TRIM(Broker_Bank_Key) ,
          TRIM(Broker_Category_Key) ,
          TRIM(Broker_Channel_Key),
          TRIM(Broker_Region_Key),
          TRIM(Broker_Store_Key) ,
          Broker_Type_Tag1 ,
          Broker_Type_Tag2 ,
          Broker_Type_Tag3  
      FROM SDL_Stg_Broker_Master
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_BROKER_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_BROKER_MASTER;
  
  
  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Inventory_Master ORDER BY SDL_Id ASC LIMIT 1;
  SET vStart_Cnt = @Rec_Cnt;

  SET @Rec_Cnt = 0;
  SELECT SDL_Id INTO @Rec_Cnt FROM SDL_Stg_Inventory_Master ORDER BY SDL_Id DESC LIMIT 1;

  IF @Rec_Cnt IS NULL THEN
    SET @Rec_Cnt = 0;
  END IF;

  SET vEnd_Cnt = vBatchSize;

  TRUNCATE CDM_Stg_Inventory_Master;
  CALL CDM_Update_Process_Log('ETL_SDL_to_CDM', 29, 'Load into CDM_Stg_Inventory_Master ', 1);

  INSERT_INTO_STG_INVENTORY_MASTER : LOOP

    INSERT IGNORE INTO CDM_Stg_Inventory_Master (
      Inventory_Key ,
      Product_Key ,
      Store_Key ,
      Stock_Qty ,
      Refresh_Date 
    )
      SELECT
            Inventory_Key ,
          Product_Key ,
          Store_Key ,
          Stock_Qty ,
          Refresh_Date 
      FROM SDL_Stg_Inventory_Master
      WHERE
        SDL_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND
        Rec_Is_Processed = -1;

    SET vStart_Cnt = vStart_Cnt + vBatchSize;
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize;

        IF vStart_Cnt  >= @Rec_Cnt THEN
            LEAVE INSERT_INTO_STG_INVENTORY_MASTER;
        END IF;

  END LOOP INSERT_INTO_STG_INVENTORY_MASTER;
  
  
END
mysolus$$


