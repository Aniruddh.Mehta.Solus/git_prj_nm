DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_ETL_Sip_Master (IN vBatchSizeStagingRecId BIGINT,IN vBatchSizeCustomerId BIGINT,IN vAllow_Update TINYINT)
BEGIN 
    DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
    DECLARE vMax_Cnt BIGINT; 
	DECLARE vProc_Name VARCHAR(128) DEFAULT ''; 
	
	SET sql_safe_updates = 0;  
	SET vProc_Name = 'CDM_ETL_Sip_Master';
	TRUNCATE CDM_PROCESS_LOG;
	CALL CDM_Update_Process_Log(vProc_Name, 1, 'START.', 1); 	
	SELECT concat(vProc_Name,' SATRTED !! ', now()) as '';
    
    
	SELECT Sip_Id into @vSip_Id FROM CDM_Sip_Master  order by Sip_Id desc limit 1;
	SELECT Customer_Id into @vCustomer_Id FROM CDM_Customer_Master  order by Customer_Id  desc limit 1;
	SELECT Product_Id into @vProduct_Id FROM CDM_Product_Master  order by Product_Id desc limit 1;
	
    
	SET vStart_Cnt=0; 

    SELECT Rec_Id INTO vStart_Cnt FROM CDM_Stg_Sip_Master ORDER BY Rec_Id ASC LIMIT 1;
	IF vStart_Cnt > 0 THEN
		SELECT Rec_Id INTO vMax_Cnt FROM CDM_Stg_Sip_Master ORDER BY Rec_Id DESC LIMIT 1;
		IF vMax_Cnt > 0 THEN
			SET vEnd_Cnt = vStart_Cnt + vBatchSizeStagingRecId; 
			SELECT concat(vProc_Name,' Updating Customer Id in Staging. No of Records in Staging: ',vMax_Cnt,' ', now()) as '';
						
					DROP TABLE IF EXISTS Temp_CDM_Sip_Master ; 
					CREATE TABLE IF NOT EXISTS Temp_CDM_Sip_Master   ( 
							  Rec_Id bigint(20) NOT NULL AUTO_INCREMENT,
							  Rec_Is_Processed tinyint(4) NOT NULL DEFAULT -1,
							  Sip_Key varchar(256) NOT NULL,
							  Sip_Id bigint(20) NOT NULL DEFAULT -1,
							  Customer_Key varchar(256) NOT NULL DEFAULT '',
							  Customer_Id bigint(20) NOT NULL DEFAULT -1,
							  Product_Key varchar(256) NOT NULL,
							  Product_Id bigint(20) NOT NULL DEFAULT -1,
							  pan varchar(100) NOT NULL,
							  Sip_Fund varchar(100) NOT NULL,
							  Sip_Scheme varchar(100) NOT NULL,
							  Sip_Plan varchar(100) NOT NULL,
							  Sip_Schemeplan varchar(100) DEFAULT NULL,
							  Sip_Acno varchar(100) NOT NULL,
							  Sip_StartDate varchar(100) DEFAULT NULL,
							  Sip_Enddt varchar(100) DEFAULT NULL,
							  Sip_Ihno varchar(100) NOT NULL,
							  Sip_Branch varchar(100) NOT NULL,
							  Sip_Agent varchar(100) DEFAULT NULL,
							  Sip_Regdt varchar(100) DEFAULT NULL,
							  Sip_Frequency varchar(100) DEFAULT NULL,
							  Sip_Insttype varchar(100) DEFAULT NULL,
							  Sip_Instamt decimal(15,4) NOT NULL DEFAULT 0.00,
							  Sip_Noinstall decimal(15,4) NOT NULL DEFAULT 0.00,
							  Sip_Terminationdt varchar(100) DEFAULT NULL,
							  Sip_Status varchar(100) DEFAULT NULL,
							  Sip_Subbroker varchar(100) DEFAULT NULL,
							  Sip_Mocode varchar(100) DEFAULT NULL,
							  Sip_Remarks varchar(255) DEFAULT NULL,
							  Sip_Kboltregdt varchar(100) DEFAULT NULL,
							  Sip_Subtrtype varchar(100) DEFAULT NULL,
							  Sip_Polstatus varchar(100) DEFAULT NULL,
							  Sip_Urnno varchar(100) NOT NULL,
							  Sip_Prxy_branch varchar(100) DEFAULT NULL,
							  Sip_Riacode varchar(100) DEFAULT NULL,
							  Sip_Otmtype varchar(100) DEFAULT NULL,
							  Sip_Locationid varchar(100) DEFAULT NULL,
							  Created_Date timestamp NOT NULL DEFAULT current_timestamp(),
							  Modified_Date timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
							  PRIMARY KEY (Rec_Id),
							  KEY Cust_Id (Customer_Id),
							  KEY Prod_Id (Product_Id),
							  KEY Sip_STATUS (Sip_Status),
							  KEY Sip_ihno (Sip_Ihno),
							  KEY Sip_urnno (Sip_Urnno)
							) ENGINE=InnoDB AUTO_INCREMENT=1024 DEFAULT CHARSET=latin1;
		
               
			PROCESS_CUST_KEY: LOOP 
				CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_0'), 'PROCESS_CUST_KEY', 1); 
				SELECT concat('CDM_Stg_Sip_Master: ',vStart_Cnt,' ',vEnd_Cnt,' ',now()) as '';
                
				INSERT IGNORE INTO CDM_Customer_Key_Lookup ( 
					Customer_Key 
				) 
					SELECT Customer_Key 
					FROM CDM_Stg_Sip_Master
					WHERE Customer_Key <> '' 
					AND Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt
					AND Rec_Is_Processed = -1
					GROUP BY Customer_Key; 
			

				INSERT IGNORE INTO CDM_Product_Key_Lookup ( 
					Product_Key 
				) 
					SELECT Product_Key 
					FROM CDM_Stg_Sip_Master
					WHERE Product_Key <> '' 
					AND Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt
					AND Rec_Is_Processed = -1
					GROUP BY Product_Key; 

				INSERT IGNORE INTO CDM_Sip_Key_Lookup ( 
					Sip_Key 
				) 
					SELECT Sip_Key 
					FROM CDM_Stg_Sip_Master
					WHERE Sip_Key <> '' 
					AND Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt
					AND Rec_Is_Processed = -1
					GROUP BY Sip_Key; 
            
            
            
            
				UPDATE IGNORE CDM_Stg_Sip_Master AS TGT, CDM_Customer_Key_Lookup AS SRC
				SET TGT.Customer_Id = SRC.Customer_Id
				WHERE
					TGT.Customer_Key = SRC.Customer_Key
					AND Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt
					AND Rec_Is_Processed = -1
					AND SRC.Customer_Key  <> ''; 

				UPDATE IGNORE CDM_Stg_Sip_Master AS TGT, CDM_Product_Key_Lookup AS SRC
				SET TGT.Product_Id = SRC.Product_Id
				WHERE
					TGT.Product_Key = SRC.Product_Key
					AND Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt
					AND Rec_Is_Processed = -1
					AND SRC.Product_Key  <> ''; 
                    
				UPDATE IGNORE CDM_Stg_Sip_Master AS TGT, CDM_Sip_Key_Lookup AS SRC
				SET TGT.Sip_Id = SRC.Sip_Id
				WHERE
					TGT.Sip_Key = SRC.Sip_Key
					AND Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt
					AND Rec_Is_Processed = -1
					AND SRC.Sip_Key  <> ''; 
                
				SET vStart_Cnt = vStart_Cnt + vBatchSizeStagingRecId; 
				SET vEnd_Cnt = vEnd_Cnt + vBatchSizeStagingRecId; 
         
				IF vStart_Cnt  >= vMax_Cnt THEN 
					LEAVE PROCESS_CUST_KEY;
				END IF; 
			END LOOP PROCESS_CUST_KEY; 
		    SELECT concat(vProc_Name,' Updating Sip_Id, Customer_ID, Product_Id in Staging Completed. LOADING CDM STARTED !!   ',now()) as '';
			
            				
			SET vStart_Cnt = 0; 
            SELECT Customer_Id INTO vStart_Cnt FROM CDM_Stg_Sip_Master WHERE Customer_Id > 0 ORDER BY Customer_Id ASC LIMIT 1; 
			IF vStart_Cnt > 0 THEN
				SELECT Customer_Id INTO vMax_Cnt FROM CDM_Stg_Sip_Master ORDER BY Customer_Id DESC LIMIT 1; 
				IF vMax_Cnt >= vStart_Cnt THEN
					SET vEnd_Cnt = vStart_Cnt + vBatchSizeCustomerId; 			 
                    PROCESS_BD_HIST: LOOP 
                    	SELECT concat('CDM_Sip_Master: ', vStart_Cnt,' ',vEnd_Cnt,' ',now()) as '';
						TRUNCATE TABLE Temp_CDM_Sip_Master;
						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_0'), 'PROCESS_Sip_HIST', 1); 
						
						INSERT IGNORE INTO Temp_CDM_Sip_Master   ( 
											Sip_Key,
											Sip_Id,
											Customer_Key,
											Customer_Id,
											Product_Key,
											Product_Id,
											pan,
											Sip_Fund,
											Sip_Scheme,
											Sip_Plan,
											Sip_Schemeplan,
											Sip_Acno,
											Sip_StartDate,
											Sip_Enddt,
											Sip_Ihno,
											Sip_Branch,
											Sip_Agent,
											Sip_Regdt,
											Sip_Frequency,
											Sip_Insttype,
											Sip_Instamt,
											Sip_Noinstall,
											Sip_Terminationdt,
											Sip_Status,
											Sip_Subbroker,
											Sip_Mocode,
											Sip_Remarks,
											Sip_Kboltregdt,
											Sip_Subtrtype,
											Sip_Polstatus,
											Sip_Urnno,
											Sip_Prxy_branch,
											Sip_Riacode,
											Sip_Otmtype,
											Sip_Locationid
						)
							SELECT
											Sip_Key,
											Sip_Id,
											Customer_Key,
											Customer_Id,
											Product_Key,
											Product_Id,
											pan,
											Sip_Fund,
											Sip_Scheme,
											Sip_Plan,
											Sip_Schemeplan,
											Sip_Acno,
											Sip_StartDate,
											Sip_Enddt,
											Sip_Ihno,
											Sip_Branch,
											Sip_Agent,
											Sip_Regdt,
											Sip_Frequency,
											Sip_Insttype,
											Sip_Instamt,
											Sip_Noinstall,
											Sip_Terminationdt,
											Sip_Status,
											Sip_Subbroker,
											Sip_Mocode,
											Sip_Remarks,
											Sip_Kboltregdt,
											Sip_Subtrtype,
											Sip_Polstatus,
											Sip_Urnno,
											Sip_Prxy_branch,
											Sip_Riacode,
											Sip_Otmtype,
											Sip_Locationid
							FROM CDM_Stg_Sip_Master
							WHERE 
								Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;
								
						
						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_1'), 'PROCESS_Sip_HIST', 1); 

							
						
							
						IF vAllow_Update = 1 THEN 
							SELECT concat(vProc_Name,' LOADING CDM Sip IN UPDATE MODE. This will increase load time !! ',now()) as '';

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_2'), 'PROCESS_Sip_HIST', 1); 
				
							UPDATE IGNORE CDM_Sip_Master AS TGT, Temp_CDM_Sip_Master AS SRC
							SET 
							TGT.pan=SRC.pan,
							TGT.Sip_Fund=SRC.Sip_Fund,
							TGT.Sip_Scheme=SRC.Sip_Scheme,
							TGT.Sip_Plan=SRC.Sip_Plan,
							TGT.Sip_Schemeplan=SRC.Sip_Schemeplan,
							TGT.Sip_Acno=SRC.Sip_Acno,
							TGT.Sip_StartDate=SRC.Sip_StartDate,
							TGT.Sip_Enddt=SRC.Sip_Enddt,
							TGT.Sip_Ihno=SRC.Sip_Ihno,
							TGT.Sip_Branch=SRC.Sip_Branch,
							TGT.Sip_Agent=SRC.Sip_Agent,
							TGT.Sip_Regdt=SRC.Sip_Regdt,
							TGT.Sip_Frequency=SRC.Sip_Frequency,
							TGT.Sip_Insttype=SRC.Sip_Insttype,
							TGT.Sip_Instamt=SRC.Sip_Instamt,
							TGT.Sip_Noinstall=SRC.Sip_Noinstall,
							TGT.Sip_Terminationdt=SRC.Sip_Terminationdt,
							TGT.Sip_Status=SRC.Sip_Status,
							TGT.Sip_Subbroker=SRC.Sip_Subbroker,
							TGT.Sip_Mocode=SRC.Sip_Mocode,
							TGT.Sip_Remarks=SRC.Sip_Remarks,
							TGT.Sip_Kboltregdt=SRC.Sip_Kboltregdt,
							TGT.Sip_Subtrtype=SRC.Sip_Subtrtype,
							TGT.Sip_Polstatus=SRC.Sip_Polstatus,
							TGT.Sip_Urnno=SRC.Sip_Urnno,
							TGT.Sip_Prxy_branch=SRC.Sip_Prxy_branch,
							TGT.Sip_Riacode=SRC.Sip_Riacode,
							TGT.Sip_Otmtype=SRC.Sip_Otmtype,
							TGT.Sip_Locationid=SRC.Sip_Locationid								
							WHERE  
								TGT.Sip_Id = SRC.Sip_Id;
								
							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_3'), 'PROCESS_Sip_HIST', 1); 
						
						END IF;
	
							
					INSERT IGNORE INTO CDM_LOV_Master 
						( 
						LOV_Attribute, 
						LOV_Key, 
						LOV_Value 
						) 
						SELECT  
						'PRODUCT', 
						Product_Key, 
						Product_Key 
						FROM Temp_CDM_Sip_Master 
						WHERE 
						Product_Key <> '' 
						GROUP BY Product_Key; 	
							
						INSERT IGNORE INTO CDM_Product_Master_Original ( 
						Product_Id, 
						Product_LOV_Id 
						) 
						SELECT  
						C.Product_Id, 
						B.LOV_Id 
						FROM CDM_LOV_Master AS B, Temp_CDM_Sip_Master AS C 
						WHERE  
						B.LOV_Attribute = 'PRODUCT' AND 
						B.LOV_Key = C.Product_Key 
						GROUP BY C.Product_Id, B.LOV_Id; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_4'), 'PROCESS_Sip_HIST', 1); 
						
						
						INSERT IGNORE INTO CDM_Customer_PII_Master ( 
							Customer_Id 
						)     
							SELECT  
								Customer_Id 
							FROM Temp_CDM_Sip_Master
							WHERE  
								Customer_Key <> '' 
							GROUP BY Customer_Id; 
						
						INSERT IGNORE INTO  CDM_Customer_Master ( 
							Customer_Id, 
							Is_New_Cust_Flag 
						) 
							SELECT  
								Customer_Id, 
								1 
							FROM Temp_CDM_Sip_Master
							WHERE  
								Customer_Key <> '' 
							GROUP BY Customer_Id; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_5'), 'PROCESS_Sip_HIST', 1); 
 
						INSERT IGNORE INTO CDM_Sip_Master ( 
						Sip_Id,
						Customer_Id,
						Product_Id,
						pan,
						Sip_Fund,
						Sip_Scheme,
						Sip_Plan,
						Sip_Schemeplan,
						Sip_Acno,
						Sip_StartDate,
						Sip_Enddt,
						Sip_Ihno,
						Sip_Branch,
						Sip_Agent,
						Sip_Regdt,
						Sip_Frequency,
						Sip_Insttype,
						Sip_Instamt,
						Sip_Noinstall,
						Sip_Terminationdt,
						Sip_Status,
						Sip_Subbroker,
						Sip_Mocode,
						Sip_Remarks,
						Sip_Kboltregdt,
						Sip_Subtrtype,
						Sip_Polstatus,
						Sip_Urnno,
						Sip_Prxy_branch,
						Sip_Riacode,
						Sip_Otmtype,
						Sip_Locationid
                        ) 
							SELECT  
						Sip_Id,
						Customer_Id,
						Product_Id,
						pan,
						Sip_Fund,
						Sip_Scheme,
						Sip_Plan,
						Sip_Schemeplan,
						Sip_Acno,
						Sip_StartDate,
						Sip_Enddt,
						Sip_Ihno,
						Sip_Branch,
						Sip_Agent,
						Sip_Regdt,
						Sip_Frequency,
						Sip_Insttype,
						Sip_Instamt,
						Sip_Noinstall,
						Sip_Terminationdt,
						Sip_Status,
						Sip_Subbroker,
						Sip_Mocode,
						Sip_Remarks,
						Sip_Kboltregdt,
						Sip_Subtrtype,
						Sip_Polstatus,
						Sip_Urnno,
						Sip_Prxy_branch,
						Sip_Riacode,
						Sip_Otmtype,
						Sip_Locationid
					FROM  Temp_CDM_Sip_Master 
                    WHERE (Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt)
                    AND   (Sip_Key <> '');
							

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_6'), 'PROCESS_Sip_HIST', 1); 
						
						
						SET vStart_Cnt = vStart_Cnt + vBatchSizeCustomerId; 
						SET vEnd_Cnt = vEnd_Cnt + vBatchSizeCustomerId; 
				 
						IF vStart_Cnt  >= vMax_Cnt THEN 
							LEAVE PROCESS_BD_HIST;
						END IF; 
					END LOOP PROCESS_BD_HIST; 
					
                                        
					DROP TABLE Temp_CDM_Sip_Master  ; 
					
				ELSE
					CALL CDM_Update_Process_Log(vProc_Name, 6, 'No records to process.', 1);
				END IF;
			ELSE
				CALL CDM_Update_Process_Log(vProc_Name, 7, 'No records to process.', 1); 
			END IF;
		ELSE
			CALL CDM_Update_Process_Log(vProc_Name, 8, 'No records to process.', 1); 
		END IF;
	ELSE
		CALL CDM_Update_Process_Log(vProc_Name, 9, 'No records to process.', 1); 	
	END IF;
 
    CALL CDM_Update_Process_Log('CDM_ETL_Sip_Master', 'Done', 'CDM_ETL_Sip_Master', 1); 

    SELECT concat(vProc_Name,' COMPLETED !! .',now()) as '';    
    SELECT concat( 'Records in csv_Sip_Master ' , count(1)) as ''  from csv_Sip_Master;
    select concat( 'Records in CDM_Stg_Sip_Master ' , count(1)) as ''from CDM_Stg_Sip_Master;
    SELECT COUNT(1) into @vcnt FROM CDM_Sip_Master where Sip_Id > @vSip_Id  ;
	SELECT concat(vProc_Name,' New Records Loaded to   CDM_Sip_Master !! ',@vcnt ) as '';
	SELECT COUNT(1) into @vcnt FROM CDM_Customer_Master where Customer_Id > @vCustomer_Id  ;
    SELECT concat(vProc_Name,' New Records Loaded to  CDM_Customer_Master : ',@vcnt ) as '';
	SELECT COUNT(1) into @vcnt FROM CDM_Product_Master where Product_Id > @vProduct_Id  ;
    SELECT concat(vProc_Name,' New Records Loaded to CDM_Product_Master : ',@vcnt ) as '';

END$$
DELIMITER ;

