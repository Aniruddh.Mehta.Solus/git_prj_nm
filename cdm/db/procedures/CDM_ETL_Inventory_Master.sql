DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Inventory_Master (IN vBatchSize BIGINT,IN vTreat_As_Incr TINYINT) 
BEGIN 
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
     
    SET vEnd_Cnt = vBatchSize; 
   
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Inventory_Master', 0, 'CDM_ETL_Inventory_Master', 1); 
     
  INSERT IGNORE INTO CDM_Inventory_Key_Lookup ( 
    Inventory_Key 
    ) 
    SELECT  
      Inventory_Key 
    FROM CDM_Stg_Inventory_Master  
    WHERE Inventory_Key <> '' AND Rec_Is_Processed = -1; 
         
   
 
  UPDATE IGNORE CDM_Stg_Inventory_Master AS A 
        SET Inventory_Id = ( 
      SELECT  Inventory_Id 
            FROM CDM_Inventory_Key_Lookup AS B 
            WHERE A.Inventory_Key = B.Inventory_Key 
        ); 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Inventory_Master', 1, 'CDM_ETL_Inventory_Master', 1); 
 
     
     
      INSERT IGNORE INTO CDM_Product_Key_Lookup ( 
    Product_Key 
    ) 
    SELECT  
      Product_Key 
    FROM CDM_Stg_Inventory_Master  
    WHERE Product_Key <> '' AND Rec_Is_Processed = -1; 
         
        UPDATE IGNORE CDM_Stg_Inventory_Master AS A 
        SET Product_Id = ( 
      SELECT  Product_Id 
            FROM CDM_Product_Key_Lookup AS B 
            WHERE A.Product_Key = B.Product_Key 
        ); 
         
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'PRODUCT', 
      Product_Key, 
      Product_Key, 
      '' 
    FROM CDM_Stg_Inventory_Master  
    WHERE  
      Product_Key <> '' AND Rec_Is_Processed = -1 
      GROUP BY Product_Key; 
 
  UPDATE IGNORE CDM_Stg_Inventory_Master AS A 
  SET Product_LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Product_Key = B.LOV_Key 
      AND B.LOV_Attribute = 'PRODUCT'  
  ) 
  WHERE Product_LOV_Id <= 0; 
   
  INSERT IGNORE INTO CDM_Product_Master_Original 
  ( 
    Product_Id, 
    Product_LOV_Id 
  ) 
    SELECT  
      Product_Id, 
      Product_LOV_Id 
    FROM CDM_Stg_Inventory_Master  
    WHERE  
      Product_Id <> -1 AND Rec_Is_Processed = -1 
      GROUP BY Product_Id; 
      
  UPDATE IGNORE CDM_Stg_Inventory_Master AS A ,CDM_Product_Master_Original B 
  SET A.Brand_LOV_Id = B.Brand_LOV_Id , 
    A.Div_LOV_Id = B.Div_LOV_Id , 
    A.Dep_LOV_Id = B.Dep_LOV_Id , 
    A.Cat_LOV_Id = B.Cat_LOV_Id , 
    A.Season_LOV_Id = B.Season_LOV_Id , 
    A.BU_LOV_Id = B.BU_LOV_Id , 
    A.Section_LOV_Id = B.Section_LOV_Id , 
    A.Manuf_LOV_Id = B.Manuf_LOV_Id  
  WHERE A.Product_Id = B.Product_Id; 
   
 
    
   INSERT IGNORE INTO CDM_Store_Key_Lookup ( 
    Store_Key 
    ) 
    SELECT  
      Store_Key 
    FROM CDM_Stg_Inventory_Master  
    WHERE Store_Key <> '' AND Rec_Is_Processed = -1; 
         
        UPDATE IGNORE CDM_Stg_Inventory_Master AS A 
        SET Store_Id = ( 
      SELECT  Store_Id 
            FROM CDM_Store_Key_Lookup AS B 
            WHERE A.Store_Key = B.Store_Key 
        ); 
         
    
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'STORE', 
      Store_Key, 
      Store_Key, 
      '' 
    FROM CDM_Stg_Inventory_Master  
    WHERE  
      Store_Key <> '' AND Rec_Is_Processed = -1; 
 
  UPDATE IGNORE CDM_Stg_Inventory_Master AS A 
  SET Store_LOV_Id = ( 
    SELECT LOV_Id   
    FROM CDM_LOV_Master AS B 
    WHERE  
      A.Store_Key = B.LOV_Key 
      AND B.LOV_Attribute = 'STORE'  
  ); 
   
  INSERT IGNORE INTO CDM_Store_Master 
  ( 
    Store_Id, 
    LOV_Id 
  ) 
    SELECT  
      Store_Id, 
      Store_LOV_Id 
    FROM CDM_Stg_Inventory_Master  
    WHERE  
      Store_Id <> -1 AND Rec_Is_Processed = -1 
      GROUP BY Store_Id; 
   
     
  CALL CDM_Update_Process_Log('CDM_ETL_Inventory_Master', 2, 'CDM_ETL_Inventory_Master', 1); 
   
  SET @Rec_Cnt = 0; 
  SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Inventory_Master ORDER BY Rec_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
 
  SET @Rec_Cnt = 0; 
  SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Inventory_Master ORDER BY Rec_Id DESC LIMIT 1; 
 
  IF @Rec_Cnt IS NULL THEN 
    SET @Rec_Cnt = 0; 
  END IF; 
   
  UPDATE_AND_INSERT_LOOP : LOOP 
   
    IF vTreat_As_Incr = 1 THEN 
      UPDATE IGNORE  CDM_Inventory_Master A , CDM_Stg_Inventory_Master B 
      SET  
      A.Inventory_Id = B.Inventory_Id, 
      A.Product_Id = B.Product_Id, 
      A.Is_Processed = -1, 
      A.Product_LOV_Id = B.Product_LOV_Id, 
      A.Store_Id = B.Store_Id, 
      A.Store_LOV_Id = B.Store_LOV_Id, 
      A.Stock_Qty = B.Stock_Qty, 
      A.Refresh_Date = B.Refresh_Date, 
      A.Brand_LOV_Id = B.Brand_LOV_Id , 
      A.Div_LOV_Id = B.Div_LOV_Id , 
      A.Dep_LOV_Id = B.Dep_LOV_Id , 
      A.Cat_LOV_Id = B.Cat_LOV_Id , 
      A.Season_LOV_Id = B.Season_LOV_Id , 
      A.BU_LOV_Id = B.BU_LOV_Id , 
      A.Section_LOV_Id = B.Section_LOV_Id , 
      A.Manuf_LOV_Id = B.Manuf_LOV_Id  
      WHERE 
	A.Product_Id = B.Product_Id OR A.Store_Id = B.Store_Id  
	AND  B.Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt; 
    END IF; 
  CALL CDM_Update_Process_Log('CDM_ETL_Inventory_Master', CONCAT('3_',vStart_Cnt), 'CDM_ETL_Inventory_Master', 1); 
   
  INSERT IGNORE INTO CDM_Inventory_Master ( 
    Inventory_Id, 
    Product_Id, 
    Product_LOV_Id, 
    Brand_LOV_Id ,     
    Div_LOV_Id ,     
    Dep_LOV_Id ,     
    Cat_LOV_Id ,     
    Season_LOV_Id ,     
    BU_LOV_Id ,     
    Section_LOV_Id ,     
    Manuf_LOV_Id, 
    Store_Id, 
    Store_LOV_Id, 
    Stock_Qty, 
    Refresh_Date 
    ) 
    SELECT  
    Inventory_Id, 
    Product_Id, 
    Product_LOV_Id, 
    Brand_LOV_Id ,     
    Div_LOV_Id ,     
    Dep_LOV_Id ,     
    Cat_LOV_Id ,     
    Season_LOV_Id ,     
    BU_LOV_Id ,     
    Section_LOV_Id ,     
    Manuf_LOV_Id, 
    Store_Id, 
    Store_LOV_Id, 
    Stock_Qty, 
    Refresh_Date 
    FROM  CDM_Stg_Inventory_Master  
    WHERE   CDM_Stg_Inventory_Master.Rec_Is_Processed = -1 AND 
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt; 
      
 
    update CDM_Product_Master_Original A, CDM_Stg_Inventory_Master B 
    set A.Inventory_Qty=B.Stock_Qty 
    where A.Product_Id=B.Product_Id and  
     B.Rec_Is_Processed = -1 AND 
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt; 
      
      SELECT vStart_Cnt,vEnd_Cnt; 
 
      SET vStart_Cnt = vStart_Cnt + vBatchSize; 
      SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Inventory_Master', CONCAT('4_',vStart_Cnt), 'CDM_ETL_Inventory_Master', 1); 
   
    IF vStart_Cnt  >= @Rec_Cnt THEN 
      LEAVE UPDATE_AND_INSERT_LOOP; 
    END IF; 
     
  END LOOP UPDATE_AND_INSERT_LOOP; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Inventory_Master', 5, 'CDM_ETL_Inventory_Master', 1); 
         
END	
mysolus$$
