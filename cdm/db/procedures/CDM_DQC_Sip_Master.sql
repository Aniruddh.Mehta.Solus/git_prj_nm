DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_DQC_Sip_Master (OUT msg text)
dqc_sip:BEGIN
declare msg varchar(1024);
select count(1) 					into @n1 from CDM_Customer_Master ;
select count(distinct Customer_Id ) into @n2 from CDM_Sip_Master  ;
select count(1) 				    into @nproducts from CDM_Product_Master_Original ;
select count(distinct Product_Id)   into @nproductsip from CDM_Sip_Master ;


if @n1<=0.8*(@n2)  then
  set msg=' Number of Customers in Sip_Master is Greater than Customers in Customer Master . Please make sure to load full Customer Master ';
  leave dqc_sip;
end if ;

if @nproducts < @nproductsip  then
  set msg=' Products in Sip_Master is greater than Products in Product Master. Please make sure to load full Product Master ';
  leave dqc_sip;
end if ;

set msg="";

END$$
DELIMITER ;

