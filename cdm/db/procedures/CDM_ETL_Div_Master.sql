DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Div_Master () 
BEGIN 
  CALL CDM_Update_Process_Log('CDM_ETL_Div_Master', 1, 'CDM_ETL_Div_Master', 1); 
 
  INSERT IGNORE INTO CDM_Div_Key_Lookup ( 
    Div_Key 
  ) 
    SELECT  
      Div_Key 
    FROM CDM_Stg_Div_Master 
        WHERE Div_Key <> '' AND Rec_Is_Processed = -1; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Div_Master', 2, 'CDM_ETL_Div_Master', 1); 
 
  UPDATE IGNORE CDM_Stg_Div_Master AS A 
    SET Div_Id = ( 
    SELECT Div_Id  
        FROM CDM_Div_Key_Lookup AS B 
        WHERE A.Div_Key = B.Div_Key 
    ); 
 
     
  INSERT IGNORE INTO CDM_LOV_Master 
  ( 
    LOV_Attribute, 
    LOV_Key, 
    LOV_Value, 
    LOV_Comm_Value 
  ) 
    SELECT  
      'DIV', 
      Div_Key, 
      Div_Key, 
      Div_Comm_Name 
    FROM CDM_Stg_Div_Master  
    WHERE Div_Key <> '' AND Rec_Is_Processed = -1; 
 
  UPDATE IGNORE CDM_Stg_Div_Master AS A 
        SET LOV_Id = ( 
      SELECT LOV_Id   
            FROM CDM_LOV_Master AS B 
            WHERE  
        A.Div_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'DIV'  
        ) 
  WHERE LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Div_Master AS A 
  SET  
    Comp_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'DIV'  
    ) 
  WHERE Comp_LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Div_Master AS A 
  SET  
    Up_Sell_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'DIV'  
    ) 
  WHERE Up_Sell_LOV_Id <= 0; 
 
   
  UPDATE IGNORE CDM_Stg_Div_Master AS A 
  SET  
    Cross_Sell_LOV_Id = ( 
      SELECT LOV_Id  
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Comp_Key = B.LOV_Key 
        AND B.LOV_Attribute = 'DIV'  
    ) 
  WHERE Cross_Sell_LOV_Id <= 0; 
     
  CALL CDM_Update_Process_Log('CDM_ETL_Div_Master', 3, 'CDM_ETL_Div_Master', 1); 
 
  INSERT IGNORE INTO CDM_Div_Master ( 
    Div_Id, 
    LOV_Id, 
    Comp_LOV_Id, 
    Up_Sell_LOV_Id, 
    Cross_Sell_LOV_Id, 
    Div_Name, 
    Div_Type_Tag1, 
    Div_Type_Tag2, 
    Div_Type_Tag3, 
    Div_Comm_Name 
  ) 
    SELECT  
      Div_Id, 
      LOV_Id, 
      Comp_LOV_Id, 
      Up_Sell_LOV_Id, 
      Cross_Sell_LOV_Id, 
      Div_Name, 
      Div_Type_Tag1, 
      Div_Type_Tag2, 
      Div_Type_Tag3, 
      Div_Comm_Name 
    FROM CDM_Stg_Div_Master 
        WHERE Rec_Is_Processed = -1; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Div_Master', 4, 'CDM_ETL_Div_Master', 1); 
 
END	
mysolus$$
