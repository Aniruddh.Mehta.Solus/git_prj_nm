DELIMITER mysolus$$

CREATE OR REPLACE PROCEDURE  CDM_Update_Process_Log (
    vProc_Name VARCHAR(128),
    vStep_Id VARCHAR(128),
    vStep_Desc VARCHAR(128),
    vStep_Status TINYINT
)
BEGIN
  INSERT IGNORE INTO CDM_PROCESS_LOG 
    ( Proc_Name, Step_Id, Step_Desc, Step_Status )
  VALUES 
    ( vProc_Name, vStep_Id, vStep_Desc, vStep_Status );
END
mysolus$$
