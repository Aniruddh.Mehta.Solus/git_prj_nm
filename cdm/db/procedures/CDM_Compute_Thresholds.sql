DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_Compute_Thresholds (IN vBatchSize BIGINT) 
BEGIN 
  DECLARE vTxnStartDate DATE; 
    DECLARE  vTxnEndDate DATE; 
 
  DECLARE vPercentileForAP DECIMAL(2,2); 
  DECLARE vTxnStartDateForThreshold DATE; 
  DECLARE vTxnEndDateForThreshold DATE; 
  DECLARE vPercentileForThesholds DECIMAL(2,2); 
  DECLARE vLowerBatchSize BIGINT; 
  DECLARE vUpperBatchSize BIGINT; 
     
  SET SQL_SAFE_UPDATES = 0; 
 
  SELECT CAST(LOV_Value AS DECIMAL) INTO vPercentileForAP  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'PERCENTILEFORAP'; 
 
  SELECT CAST(LOV_Value AS DATE) INTO vTxnStartDateForThreshold  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'TXNSTARTDATEFORTHRESHOLD'; 
 
  SELECT CAST(LOV_Value AS DATE) INTO vTxnEndDateForThreshold  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'TXNENDDATEFORTHRESHOLD'; 
 
  SELECT CAST(LOV_Value AS DECIMAL) INTO vPercentileForThesholds  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'PERCENTILEFORTHESHOLDS'; 
 
  SELECT CAST(LOV_Value AS UNSIGNED) INTO vLowerBatchSize  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'LOWERBATCHSIZE'; 
 
  SELECT CAST(LOV_Value AS UNSIGNED) INTO vUpperBatchSize  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'UPPERBATCHSIZE'; 
 
   
  UPDATE CDM_LOV_Master  
  SET LOV_Value = CDM_Get_Active_Period_Duration(vPercentileForAP,vTxnStartDateForThreshold, vTxnEndDateForThreshold) 
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'ACTIVE_PERIOD_IN_DAYS'; 
 
   
  UPDATE CDM_LOV_Master  
  SET LOV_Value = CDM_Get_HighValue_Threshold(vPercentileForThesholds,vTxnStartDateForThreshold, vTxnEndDateForThreshold) 
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'HV_THRESHOLD'; 
 
   
  UPDATE CDM_LOV_Master  
  SET LOV_Value = CDM_Get_HighFrequency_Threshold(vPercentileForThesholds,vTxnStartDateForThreshold, vTxnEndDateForThreshold) 
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'HF_THRESHOLD'; 
 
END	
mysolus$$
