DELIMITER $$
CREATE or REPLACE PROCEDURE  S_Stg_Del_Rep ()
BEGIN 

DECLARE vStart_Cnt1 BIGINT DEFAULT 0;
DECLARE vEnd_Cnt1 BIGINT;
DECLARE vBatchSize1 BIGINT DEFAULT 10000; 

DECLARE vStart_Cnt2 BIGINT DEFAULT 0;
DECLARE vEnd_Cnt2 BIGINT;
DECLARE vBatchSize2 BIGINT DEFAULT 10000; 

SELECT Rec_Id INTO vStart_Cnt1 FROM CDM_Stg_Del_Rep ORDER BY Rec_Id ASC LIMIT 1;
SELECT Rec_Id INTO @Rec_Cnt1 FROM CDM_Stg_Del_Rep ORDER BY Rec_Id DESC LIMIT 1;

SELECT Communication_Channel INTO @Channel FROM CDM_Stg_Del_Rep LIMIT 1;


IF @Channel = 'SMS' then
PROCESS_LOOP: LOOP
			SET vEnd_Cnt1 = vStart_Cnt1 + vBatchSize1;
            
			update CDM_Stg_Del_Rep a, CDM_Customer_PII_Master b
			set a.Customer_Id = b.Customer_Id
			where a.Customer_Key = trim(replace(b.Mobile,' ',''))
			and a.Rec_Id between vStart_Cnt1 and vEnd_Cnt1;

			SET vStart_Cnt1 = vEnd_Cnt1;

			IF vStart_Cnt1  >= @Rec_Cnt1 THEN
				LEAVE PROCESS_LOOP;
	
			END IF;
            
END LOOP PROCESS_LOOP;  

END IF;


IF @Channel = 'EMAIL' then
PROCESS_LOOP: LOOP
			SET vEnd_Cnt1 = vStart_Cnt1 + vBatchSize1;
            
            
			update CDM_Stg_Del_Rep a, CDM_Customer_PII_Master b
			set a.Customer_Id = b.Customer_Id
			where a.Customer_Key = b.Email
			and a.Rec_Id between vStart_Cnt1 and vEnd_Cnt1;

			SET vStart_Cnt1 = vEnd_Cnt1;

			IF vStart_Cnt1  >= @Rec_Cnt1 THEN
				LEAVE PROCESS_LOOP;
	
			END IF;
            
END LOOP PROCESS_LOOP;  

END IF;

IF @Channel = 'WHATSAPP' then
PROCESS_LOOP: LOOP
			SET vEnd_Cnt1 = vStart_Cnt1 + vBatchSize1;
            
			update CDM_Stg_Del_Rep a, CDM_Customer_PII_Master b
			set a.Customer_Id = b.Customer_Id
			where a.Customer_Key = trim(replace(b.Mobile,' ',''))
			and a.Rec_Id between vStart_Cnt1 and vEnd_Cnt1;

			SET vStart_Cnt1 = vEnd_Cnt1;

			IF vStart_Cnt1  >= @Rec_Cnt1 THEN
				LEAVE PROCESS_LOOP;
	
			END IF;
            
END LOOP PROCESS_LOOP;  

END IF;


IF @Channel = 'PN' then
PROCESS_LOOP: LOOP
			SET vEnd_Cnt1 = vStart_Cnt1 + vBatchSize1;
            
			update CDM_Stg_Del_Rep a, CDM_Customer_PII_Master b
			set a.Customer_Id = b.Customer_Id
			where a.Customer_Key = b.Email
			and a.Rec_Id between vStart_Cnt1 and vEnd_Cnt1;

			SET vStart_Cnt1 = vEnd_Cnt1;

			IF vStart_Cnt1  >= @Rec_Cnt1 THEN
				LEAVE PROCESS_LOOP;
	
			END IF;
            
END LOOP PROCESS_LOOP;  

END IF;



SELECT Customer_Id INTO vStart_Cnt2 FROM CDM_Stg_Del_Rep where Customer_Id is not NULL ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt2 FROM CDM_Stg_Del_Rep where Customer_Id is not NULL ORDER BY Customer_Id DESC LIMIT 1;



Update_LOOP: LOOP
		SET vEnd_Cnt2 = vStart_Cnt2 + vBatchSize2;
        
            update CDM_Stg_Del_Rep a
		set a.Event_Execution_ID = ( select Event_Execution_ID from Event_Execution_History b where a.Customer_Id = b.Customer_Id
            and a.Sent_Date = b.Event_Execution_Date_ID
            and b.Communication_Channel = a.Communication_Channel
            AND a.Customer_Id between vStart_Cnt2 and vEnd_Cnt2
            limit 1
            )
            where a.Customer_Id between vStart_Cnt2 and vEnd_Cnt2;
            
            SET vStart_Cnt2 = vEnd_Cnt2;

			IF vStart_Cnt2  >= @Rec_Cnt2 THEN
				LEAVE Update_LOOP;
	
			END IF;
            
END LOOP Update_LOOP; 

END$$
DELIMITER ;

