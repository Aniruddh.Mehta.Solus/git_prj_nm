DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Cilent_Cust_Var (IN vBatchSize BIGINT) 
BEGIN 
 
  DECLARE vBatchSize BIGINT; 
 
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
    
  SELECT CAST(LOV_Value AS UNSIGNED) INTO vBatchSize  
  FROM CDM_LOV_Master  
  WHERE  
    LOV_Attribute = 'SOLUS_SEED' 
    AND LOV_Key = 'UPPERBATCHSIZE'; 
    
  SET sql_safe_updates=0; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Cilent_Cust_Var', 1, 'CDM_Prepare_Customer_Comm_Details', 1); 
   
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Client_Customer_Variables ORDER BY Customer_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
 
  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
     
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Client_Customer_Variables ORDER BY Customer_Id DESC LIMIT 1; 
 
    IF @Rec_Cnt IS NULL THEN  
    SET @Rec_Cnt = 0; 
  END IF; 
 
  PROCESS_CLIENT_CUST_VAR: LOOP   
    CALL CDM_Update_Process_Log('CDM_ETL_Cilent_Cust_Var', CONCAT(vStart_Cnt,'_1'), 'CDM_ETL_Cilent_Cust_Var', 1); 
 
    UPDATE IGNORE CDM_Stg_Client_Customer_Variables 
    SET Customer_Id = ( 
      SELECT Customer_Id 
      FROM CDM_Customer_Key_Lookup 
      WHERE CDM_Stg_Client_Customer_Variables.Customer_Key = CDM_Customer_Key_Lookup.Customer_Key 
    ) 
    WHERE  
      Customer_Id <=0 AND 
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_Cilent_Cust_Var', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Cilent_Cust_Var', 1); 
 
    UPDATE IGNORE CDM_Client_Customer_Variables CDM_CCV, CDM_Stg_Client_Customer_Variables CDM_Stg_CCV 
    SET 
      CDM_CCV.HML_Tag_Last_Calendar_Year = CDM_Stg_CCV.HML_Tag_Last_Calendar_Year,    
      CDM_CCV.HML_Tag_Last_To_Last_Calendar_Year = CDM_Stg_CCV.HML_Tag_Last_To_Last_Calendar_Year 
    WHERE  
      CDM_CCV.Customer_Id = CDM_Stg_CCV.Customer_Id AND 
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt;          
 
    CALL CDM_Update_Process_Log('CDM_ETL_Cilent_Cust_Var', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Cilent_Cust_Var', 1); 
 
    INSERT IGNORE INTO CDM_Client_Customer_Variables ( 
      Customer_Id, 
      HML_Tag_Last_Calendar_Year,    
      HML_Tag_Last_To_Last_Calendar_Year 
    )  
      SELECT  
        Customer_Id, 
        HML_Tag_Last_Calendar_Year,    
        HML_Tag_Last_To_Last_Calendar_Year 
      FROM CDM_Stg_Client_Customer_Variables 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt; 
 
    SET vStart_Cnt = vStart_Cnt + vBatchSize; 
    SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
     
    IF vStart_Cnt  >= @Rec_Cnt THEN 
      LEAVE PROCESS_CLIENT_CUST_VAR; 
    END IF; 
     
  END LOOP PROCESS_CLIENT_CUST_VAR; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Cilent_Cust_Var', 2, 'CDM_Prepare_Customer_Comm_Details', 1); 
   
END	
mysolus$$
