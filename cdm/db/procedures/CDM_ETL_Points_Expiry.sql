DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Points_Expiry (IN vBatchSize BIGINT) 
BEGIN 
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
     
  SET vEnd_Cnt = vBatchSize; 
 
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Points_Expiry ORDER BY Rec_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
   
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Points_Expiry ORDER BY Rec_Id DESC LIMIT 1; 
    IF @Rec_Cnt IS NULL THEN  
    SET @Rec_Cnt = 0; 
  END IF; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Points_Expiry', 1, 'CDM_ETL_Points_Expiry', 1); 
     
  PROCESS_PE: LOOP 
 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Points_Expiry', CONCAT(vStart_Cnt,'_1'), 'CDM_ETL_Points_Expiry', 1); 
 
 	  
	   INSERT IGNORE INTO CDM_Customer_Key_Lookup ( 
      Customer_Key 
    ) 
      SELECT Customer_Key 
      FROM CDM_Stg_Points_Expiry 
      WHERE 
        Customer_Key <> '' 
      GROUP BY Customer_Key; 
 
    UPDATE IGNORE CDM_Stg_Points_Expiry 
    SET Customer_Id = ( 
      SELECT Customer_Id  
      FROM CDM_Customer_Key_Lookup 
      WHERE 
        CDM_Stg_Points_Expiry.Customer_Key = CDM_Customer_Key_Lookup.Customer_Key 
    ) 
    WHERE Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Customer_Key  <> '' AND 
      Rec_Is_Processed = -1; 
	  
 
	  	  
	  INSERT IGNORE INTO  CDM_Customer_Master ( 
      Customer_Id, 
      Is_New_Cust_Flag 
    ) 
      SELECT  
        Customer_Id, 
        1 
      FROM CDM_Stg_Points_Expiry                         
      WHERE  
        Customer_Key <> '' 
      GROUP BY Customer_Id; 
 
         
    CALL CDM_Update_Process_Log('CDM_ETL_Points_Expiry', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Points_Expiry', 1); 
 
     
    INSERT IGNORE INTO CDM_Points_Expiry_Key_Lookup ( 
      Points_Expiry_Rec_Key 
    ) 
      SELECT  
        Points_Expiry_Rec_Key 
      FROM  
        CDM_Stg_Points_Expiry 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
     
     
    CALL CDM_Update_Process_Log('CDM_ETL_Points_Expiry', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Points_Expiry', 1); 
 
    UPDATE IGNORE CDM_Stg_Points_Expiry 
        SET Points_Expiry_Rec_Id = ( 
      SELECT Points_Expiry_Rec_Id  
            FROM CDM_Points_Expiry_Key_Lookup 
            WHERE 
        CDM_Stg_Points_Expiry.Points_Expiry_Rec_Key = CDM_Points_Expiry_Key_Lookup.Points_Expiry_Rec_Key 
    ) 
    WHERE  
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Points_Expiry_Rec_Id <= 0 AND 
      Rec_Is_Processed = -1; 
 
         
    CALL CDM_Update_Process_Log('CDM_ETL_Points_Expiry', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Points_Expiry', 1); 
 
    INSERT IGNORE INTO CDM_Points_Expiry ( 
      Points_Expiry_Rec_Id, 
      Customer_Id, 
      Expiring_Points, 
      Expiry_Date, 
      Expiry_Year, 
      Expiry_Year_Month, 
      Expiry_Day_Week 
    ) 
      SELECT  
        Points_Expiry_Rec_Id, 
        Customer_Id, 
        Expiring_Points, 
        Expiry_Date, 
        IFNULL(YEAR(Expiry_Date),''), 
        IFNULL(EXTRACT(YEAR_MONTH FROM Expiry_Date),''), 
        IFNULL(WEEKDAY(Expiry_Date),-1) 
      FROM CDM_Stg_Points_Expiry 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
 
      SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_PE; 
        END IF; 
         
  END LOOP PROCESS_PE; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Points_Expiry', 2, 'CDM_ETL_Points_Expiry', 1); 
   
END	
mysolus$$
