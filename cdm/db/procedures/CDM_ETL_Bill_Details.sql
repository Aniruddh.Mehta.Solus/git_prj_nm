DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_ETL_Bill_Details (IN vBatchSizeStagingRecId BIGINT,IN vBatchSizeCustomerId BIGINT,IN vAllow_Update TINYINT, OUT vErrMsg TEXT, OUT vSuccess TINYINT)
BEGIN 
    DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
    DECLARE vMax_Cnt BIGINT; 
	DECLARE vProc_Name VARCHAR(128) DEFAULT ''; 
	
	/*
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
		SET @logmsg=CONCAT(IFNULL(@errsqlstate,'ERROR') , IFNULL(@errno,'1'), IFNULL(@errtext,'ERROR MESSAGE'));
		SET vErrMsg=@logmsg;
		SET vSuccess=0;
		INSERT INTO ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time, Status , Load_Execution_ID) 
		VALUES (vProc_Name, @logmsg ,NOW(),NULL,@errno, IFNULL(1,1));
        SELECT concat(vProc_Name, ' Error Message : ', @logmsg) AS '';
	END;
	*/

    
    SET sql_safe_updates = 0;  
	SET vProc_Name = 'CDM_ETL_Bill_Details';
	TRUNCATE CDM_PROCESS_LOG;
	CALL CDM_Update_Process_Log(vProc_Name, 1, 'START.', 1); 	
	SELECT "==================================================" as '';
	SELECT concat(vProc_Name,' VERSION 23-Jan-2023. SATRTED !! ', now()) as '';
    SELECT "==================================================" as '';

    SELECT Bill_Details_Id into @vNM_Bill_Details_Id FROM CDM_NM_Bill_Details order by Bill_Details_Id desc limit 1;
	SELECT Bill_Details_Id into @vBill_Details_Id FROM CDM_Bill_Details  order by Bill_Details_Id desc limit 1;
	SELECT Bill_Details_Id into @vNon_Sales_Bill_Details_Id FROM CDM_Non_Sales_Bill_Details  order by Bill_Details_Id desc limit 1;
    SELECT Customer_Id into @vCustomer_Id FROM CDM_Customer_Master  order by Customer_Id  desc limit 1;
	SELECT Product_Id into @vProduct_Id FROM CDM_Product_Master  order by Product_Id desc limit 1;
	SELECT Store_Id into @vStore_Id FROM CDM_Store_Master  order by Store_Id desc limit 1;    
    
	SET vStart_Cnt=0; 

    SELECT Rec_Id INTO vStart_Cnt FROM CDM_Stg_Bill_Details ORDER BY Rec_Id ASC LIMIT 1;
	IF vStart_Cnt > 0 THEN
		SELECT Rec_Id INTO vMax_Cnt FROM CDM_Stg_Bill_Details ORDER BY Rec_Id DESC LIMIT 1;
		IF vMax_Cnt > 0 THEN
			SET vEnd_Cnt = vStart_Cnt + vBatchSizeStagingRecId; 
			SELECT concat(vProc_Name,' Updating Customer Id in Staging. No of Records in Staging: ',vMax_Cnt,' ', now()) as '';
			SELECT concat('CDM_Bill_Details: ',' Loading NM Txns Into Temp Table ',' ',now()) as '';
			
					DROP TABLE IF EXISTS Temp_CDM_Bill_Details ; 
					CREATE TABLE IF NOT EXISTS Temp_CDM_Bill_Details   ( 
						Rec_Id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY
						,Rec_Is_Processed TINYINT NOT NULL DEFAULT -1
						,Bill_Details_Key VARCHAR(128) NOT NULL
						,Bill_Details_Id BIGINT NOT NULL DEFAULT -1
						,Bill_Header_Key VARCHAR(128) NOT NULL
						,Bill_Header_Id BIGINT NOT NULL DEFAULT -1
						,Line_No SMALLINT NOT NULL DEFAULT -1
						,Store_Key VARCHAR(128) NOT NULL
						,Store_Id BIGINT NOT NULL DEFAULT -1
						,Store_LOV_Id BIGINT NOT NULL DEFAULT -1
						,Product_Key VARCHAR(128) NOT NULL
						,Broker_Key VARCHAR(128)
						,Broker_Id BIGINT NOT NULL DEFAULT -1
						,Product_Id BIGINT NOT NULL DEFAULT -1
						,Product_LOV_Id BIGINT NOT NULL DEFAULT -1
						,Customer_Key VARCHAR(128) NOT NULL DEFAULT ''
						,Customer_Id BIGINT NOT NULL DEFAULT -1
						,Bill_Date DATE
						,Bill_Time TIME
						,Cust_Txn_Number SMALLINT NOT NULL DEFAULT -1
						,Sale_Qty DECIMAL(15,2) NOT NULL DEFAULT 0.00
						,Sale_Gross_Val DECIMAL(15,2) NOT NULL DEFAULT 0.00
						,Sale_Disc_Val DECIMAL(15,2) NOT NULL DEFAULT 0.00
						,Sale_Tax_Val DECIMAL(15,2) NOT NULL DEFAULT 0.00
						,Sale_Net_Val DECIMAL(15,2) NOT NULL DEFAULT 0.00
						,Mrp DECIMAL(15,2) NOT NULL DEFAULT -1.00
						,Created_Date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP()
						,Modified_Date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
						,Trans_Type VARCHAR(128)
						,Trans_Sub_Type VARCHAR(128)
						,Source_Program VARCHAR(128)
						,KEY Customer_Id (Customer_Id)
						,KEY Broker_Key (Broker_Key)
						,KEY Bill_Details_Key (Bill_Details_Key)
						,KEY Bill_Header_Key (Bill_Header_Key)
						,KEY Store_Key (Store_Key)
						,KEY Product_Key (Product_Key)
						,KEY Rec_Is_Processed (Rec_Is_Processed)
					) ENGINE=InnoDB;
					
			
                 
			PROCESS_CUST_KEY: LOOP 
				CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_0'), 'PROCESS_CUST_KEY', 1); 
				SELECT concat('CDM_Stg_Bill_Details: ',vStart_Cnt,' ',vEnd_Cnt,' ',now()) as '';
                
				INSERT IGNORE INTO CDM_Customer_Key_Lookup ( 
					Customer_Key 
				) 
					SELECT Customer_Key 
					FROM CDM_Stg_Bill_Details
					WHERE Customer_Key <> '' 
					AND Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt
					AND Rec_Is_Processed = -1
					GROUP BY Customer_Key; 
			
            
				UPDATE IGNORE CDM_Stg_Bill_Details AS TGT, CDM_Customer_Key_Lookup AS SRC
				SET TGT.Customer_Id = SRC.Customer_Id
				WHERE
					TGT.Customer_Key = SRC.Customer_Key
					AND Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt
					AND Rec_Is_Processed = -1
					AND SRC.Customer_Key  <> ''; 
				
            
				INSERT IGNORE INTO Temp_CDM_Bill_Details   ( 
							Bill_Details_Key
							,Bill_Header_Key
							,Line_No
							,Store_Key
							,Product_Key
							,Broker_Key
							,Customer_Key
							,Customer_Id
							,Bill_Date
							,Bill_Time
							,Cust_Txn_Number
							,Sale_Qty
							,Sale_Gross_Val
							,Sale_Disc_Val
							,Sale_Tax_Val
							,Sale_Net_Val
							,Mrp
							,Trans_Type
							,Trans_Sub_Type
							,Source_Program
						)
							SELECT
								Bill_Details_Key
								,Bill_Header_Key
								,Line_No
								,Store_Key
								,Product_Key
								,Broker_Key
								,Customer_Key
								,Customer_Id
								,Bill_Date
								,Bill_Time
								,Cust_Txn_Number
								,Sale_Qty
								,Sale_Gross_Val
								,Sale_Disc_Val
								,Sale_Tax_Val
								,Sale_Net_Val
								,Mrp
								,Trans_Type
								,Trans_Sub_Type
								,Source_Program
							FROM CDM_Stg_Bill_Details
							WHERE 
								(Customer_Id < 0) AND (Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt);
    

				
				SET vStart_Cnt = vStart_Cnt + vBatchSizeStagingRecId; 
				SET vEnd_Cnt = vEnd_Cnt + vBatchSizeStagingRecId; 
         
				IF vStart_Cnt  >= vMax_Cnt THEN 
					LEAVE PROCESS_CUST_KEY;
				END IF; 
			END LOOP PROCESS_CUST_KEY; 
		    SELECT concat(vProc_Name,' Updating Customer_ID in Staging Completed. LOADING CDM STARTED !!   ',now()) as '';
			SELECT concat('CDM_Bill_Details: ',' Finished NM Txns Into Temp Table ',' ',now()) as '';
             SELECT "=============================================================================================" as '';

				
			
				
			SET vStart_Cnt = 0; 
            -- Considering that there many be NM txns with no customer key for those customer id will be -1 managing that scenario
			SELECT Customer_Id INTO vStart_Cnt FROM CDM_Stg_Bill_Details WHERE Customer_Key <> '' ORDER BY Customer_Id ASC LIMIT 1; 
			IF vStart_Cnt > 0 THEN
				SELECT Customer_Id INTO vMax_Cnt FROM CDM_Stg_Bill_Details ORDER BY Customer_Id DESC LIMIT 1; 
				IF vMax_Cnt >= vStart_Cnt THEN
					SET vEnd_Cnt = vStart_Cnt + vBatchSizeCustomerId; 			 
                    PROCESS_BD_HIST: LOOP 
                    	SELECT concat('CDM_Bill_Details: ', vStart_Cnt,' ',vEnd_Cnt,' ',now()) as '';
						DELETE FROM Temp_CDM_Bill_Details WHERE Customer_Id > 0 ; -- CONVERTING TRUNCATE INTO DELETE AS NM records also will go with it otherwise
						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_0'), 'PROCESS_BD_HIST', 1); 
						
						INSERT IGNORE INTO Temp_CDM_Bill_Details   ( 
							Bill_Details_Key
							,Bill_Header_Key
							,Line_No
							,Store_Key
							,Product_Key
							,Broker_Key
							,Customer_Key
							,Customer_Id
							,Bill_Date
							,Bill_Time
							,Cust_Txn_Number
							,Sale_Qty
							,Sale_Gross_Val
							,Sale_Disc_Val
							,Sale_Tax_Val
							,Sale_Net_Val
							,Mrp
							,Trans_Type
							,Trans_Sub_Type
							,Source_Program
						)
							SELECT
								Bill_Details_Key
								,Bill_Header_Key
								,Line_No
								,Store_Key
								,Product_Key
								,Broker_Key
								,Customer_Key
								,Customer_Id
								,Bill_Date
								,Bill_Time
								,Cust_Txn_Number
								,Sale_Qty
								,Sale_Gross_Val
								,Sale_Disc_Val
								,Sale_Tax_Val
								,Sale_Net_Val
								,Mrp
								,Trans_Type
								,Trans_Sub_Type
								,Source_Program
							FROM CDM_Stg_Bill_Details
							WHERE 
								Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;
								
						
						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_1'), 'PROCESS_BD_HIST', 1); 

							
						
							
						IF vAllow_Update = 1 THEN 
							SELECT concat(vProc_Name,' LOADING CDM IN UPDATE MODE. This will increase load time !! ',now()) as '';

								
							
								
							
							UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_Store_Key_Lookup AS SRC
								SET TGT.Store_Id = SRC.Store_Id
							WHERE 
								TGT.Store_Key = SRC.Store_Key; 

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_2'), 'PROCESS_BD_HIST', 1); 
							
							
							UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_Product_Key_Lookup AS SRC
								SET TGT.Product_Id = SRC.Product_Id
							WHERE 
								TGT.Product_Key = SRC.Product_Key; 

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_3'), 'PROCESS_BD_HIST', 1); 
							
							
							UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_Broker_Key_Lookup AS SRC
								SET TGT.Broker_Id = SRC.Broker_Id
							WHERE 
								TGT.Broker_Key = SRC.Broker_Key;

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_4'), 'PROCESS_BD_HIST', 1); 

							
							UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_Bill_Header_Key_Lookup AS SRC
							SET TGT.Bill_Header_Id = SRC.Bill_Header_Id
							WHERE 
								TGT.Bill_Header_Key = SRC.Bill_Header_Key 
								AND Customer_Id > 0 
								AND Sale_Net_Val >= 0
								AND TGT.Bill_Header_Id <= 0; 

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_5'), 'PROCESS_BD_HIST', 1); 

							
							UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_NM_Bill_Header_Key_Lookup AS SRC
							SET TGT.Bill_Header_Id = SRC.Bill_Header_Id
							WHERE 
								TGT.Bill_Header_Key = SRC.Bill_Header_Key
								AND Customer_Id = ''
								AND TGT.Bill_Header_Id <= 0; 


/* AM : FIX FOR BILL HEADER ID NOT GETTING UPDATED JUNE-05*/						
							UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_NM_Bill_Header_Key_Lookup AS SRC
							SET TGT.Bill_Header_Id = SRC.Bill_Header_Id
							WHERE 
								TGT.Bill_Header_Key = SRC.Bill_Header_Key
								AND Customer_Id < 0
								AND TGT.Bill_Header_Id <= 0; 






							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_6'), 'PROCESS_BD_HIST', 1); 
						
							
							UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_Non_Sales_Bill_Header_Key_Lookup AS SRC
							SET TGT.Bill_Header_Id = SRC.Bill_Header_Id
							WHERE 
								TGT.Bill_Header_Key = SRC.Bill_Header_Key 
								AND Customer_Id > 0
								AND Sale_Net_Val < 0 
								AND TGT.Bill_Header_Id <= 0; 

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_7'), 'PROCESS_BD_HIST', 1); 

							
							UPDATE IGNORE Temp_CDM_Bill_Details AS TGT,CDM_Bill_Details_Key_Lookup AS SRC
							SET TGT.Bill_Details_Id = SRC.Bill_Details_Id
							WHERE TGT.Bill_Details_Key = SRC.Bill_Details_Key;

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_8'), 'PROCESS_BD_HIST', 1); 

							
							UPDATE IGNORE CDM_Bill_Details AS TGT, Temp_CDM_Bill_Details AS SRC
							SET 
								TGT.Broker_Id = IFNULL(SRC.Broker_Id,-1), 
								TGT.Sale_Qty = SRC.Sale_Qty, 
								TGT.Sale_Gross_Val= SRC.Sale_Gross_Val, 
								TGT.Sale_Disc_Val = SRC.Sale_Disc_Val, 
								TGT.Sale_Tax_Val= SRC.Sale_Tax_Val, 
								TGT.Sale_Net_Val = SRC.Sale_Net_Val, 
								TGT.Bill_Date = SRC.Bill_Date, 
								TGT.Bill_Time = SRC.Bill_Time, 
								TGT.Bill_Year = IFNULL(YEAR(SRC.Bill_Date),''), 
								TGT.Bill_Year_Month = IFNULL(EXTRACT(YEAR_MONTH FROM SRC.Bill_Date),''), 
								TGT.Bill_Day_Week = IFNULL(WEEKDAY(SRC.Bill_Date),-1), 
								TGT.Trans_Type = SRC.Trans_Type, 
								TGT.Trans_Sub_Type = SRC.Trans_Sub_Type, 
								TGT.Source_Program = SRC.Source_Program
							WHERE  
								TGT.Bill_Details_Id = SRC.Bill_Details_Id
								AND SRC.Customer_Id > 0
								AND SRC.Sale_Net_Val >= 0; 

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_9'), 'PROCESS_BD_HIST', 1); 
							
							
							UPDATE IGNORE 
								CDM_Bill_Header AS TGT,
								(
									SELECT 
										TCBD.Bill_Header_Id AS Bill_Header_Id
										,TCBD.Customer_Id AS Customer_Id
										,TCBD.Store_Id AS Store_Id
										,TCBD.Bill_Date AS Bill_Date
										,TCBD.Bill_Time AS Bill_Time
										,SUM(TCBD.Sale_Qty) AS Bill_Qty
										,SUM(TCBD.Sale_Gross_Val) AS Bill_Gross_Val
										,SUM(TCBD.Sale_Disc_Val) AS Bill_Disc
										,SUM(TCBD.Sale_Tax_Val) AS Bill_Tax
										,SUM(TCBD.Sale_Net_Val) AS Bill_Total_Val
									FROM  
										Temp_CDM_Bill_Details TCBD
									WHERE  
										Customer_Id > 0 AND 
										Sale_Net_Val >= 0   
									GROUP BY  
										Bill_Header_Id, 
										Customer_Id, 
										Store_Id, 
										Bill_Date,
										Bill_Time
								) AS SRC 
							SET
								TGT.Customer_Id = SRC.Customer_Id
								,TGT.Store_Id = SRC.Store_Id
								,TGT.Bill_Date = SRC.Bill_Date
								,TGT.Bill_Year = IFNULL(YEAR(SRC.Bill_Date),'')
								,TGT.Bill_Year_Month = IFNULL(EXTRACT(YEAR_MONTH FROM SRC.Bill_Date),'')
								,TGT.Bill_Day_Week = IFNULL(WEEKDAY(SRC.Bill_Date),-1)
								,TGT.Bill_Time = SRC.Bill_Time
								,TGT.Bill_Qty = SRC.Bill_Qty
								,TGT.Bill_Gross_Val = SRC.Bill_Gross_Val
								,TGT.Bill_Disc = SRC.Bill_Disc
								,TGT.Bill_Tax = SRC.Bill_Tax
								,TGT.Bill_Total_Val = SRC.Bill_Total_Val
							WHERE 
								TGT.Bill_Header_Id = SRC.Bill_Header_Id;

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_10'), 'PROCESS_BD_HIST', 1); 
							
							
							UPDATE IGNORE Temp_CDM_Bill_Details AS TGT,CDM_NM_Bill_Details_Key_Lookup AS SRC
							SET TGT.Bill_Details_Id = SRC.Bill_Details_Id
							WHERE TGT.Bill_Details_Key = SRC.Bill_Details_Key;

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_11'), 'PROCESS_BD_HIST', 1); 

							
							UPDATE IGNORE CDM_NM_Bill_Details AS TGT, Temp_CDM_Bill_Details AS SRC 
							SET 
								TGT.Broker_Id = IFNULL(SRC.Broker_Id,-1), 
								TGT.Sale_Qty = SRC.Sale_Qty, 
								TGT.Sale_Gross_Val= SRC.Sale_Gross_Val, 
								TGT.Sale_Disc_Val = SRC.Sale_Disc_Val, 
								TGT.Sale_Tax_Val= SRC.Sale_Tax_Val, 
								TGT.Sale_Net_Val = SRC.Sale_Net_Val, 
								TGT.Bill_Date = SRC.Bill_Date, 
								TGT.Bill_Time = SRC.Bill_Time, 
								TGT.Bill_Year = IFNULL(YEAR(SRC.Bill_Date),''), 
								TGT.Bill_Year_Month = IFNULL(EXTRACT(YEAR_MONTH FROM SRC.Bill_Date),''), 
								TGT.Bill_Day_Week = IFNULL(WEEKDAY(SRC.Bill_Date),-1), 
								TGT.Trans_Type = SRC.Trans_Type, 
								TGT.Trans_Sub_Type = SRC.Trans_Sub_Type, 
								TGT.Source_Program = SRC.Source_Program
							WHERE  
								SRC.Bill_Details_Id = TGT.Bill_Details_Id AND 
								SRC.Customer_Id <= 0; 

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_12'), 'PROCESS_BD_HIST', 1); 

							
							UPDATE IGNORE 
								CDM_NM_Bill_Header AS TGT,
								(
									SELECT 
										Bill_Header_Id
										,Store_Id
										,Bill_Date
										,Bill_Time
										,SUM(Sale_Qty) AS Bill_Qty
										,SUM(Sale_Gross_Val) AS Bill_Gross_Val
										,SUM(Sale_Disc_Val) AS Bill_Disc
										,SUM(Sale_Tax_Val) AS Bill_Tax
										,SUM(Sale_Net_Val) AS Bill_Total_Val
									FROM  
										Temp_CDM_Bill_Details
									WHERE  
										Customer_Id <= 0
									GROUP BY  
										Bill_Header_Id, 
										Store_Id, 
										Bill_Date,
										Bill_Time
								) AS SRC 
							SET
								TGT.Store_Id = SRC.Store_Id
								,TGT.Bill_Date = SRC.Bill_Date
								,TGT.Bill_Year = IFNULL(YEAR(SRC.Bill_Date),'')
								,TGT.Bill_Year_Month = IFNULL(EXTRACT(YEAR_MONTH FROM SRC.Bill_Date),'')
								,TGT.Bill_Day_Week = IFNULL(WEEKDAY(SRC.Bill_Date),-1)
								,TGT.Bill_Time = SRC.Bill_Time
								,TGT.Bill_Qty = SRC.Bill_Qty
								,TGT.Bill_Gross_Val = SRC.Bill_Gross_Val
								,TGT.Bill_Disc = SRC.Bill_Disc
								,TGT.Bill_Tax = SRC.Bill_Tax
								,TGT.Bill_Total_Val = SRC.Bill_Total_Val
							WHERE 
								TGT.Bill_Header_Id = SRC.Bill_Header_Id;							

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_13'), 'PROCESS_BD_HIST', 1); 

							
							UPDATE IGNORE Temp_CDM_Bill_Details AS TGT,CDM_Non_Sales_Bill_Details_Key_Lookup AS SRC
							SET TGT.Bill_Details_Id = SRC.Bill_Details_Id
							WHERE TGT.Bill_Details_Key = SRC.Bill_Details_Key;

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_14'), 'PROCESS_BD_HIST', 1); 

							
							UPDATE IGNORE CDM_Non_Sales_Bill_Details AS TGT, Temp_CDM_Bill_Details AS SRC 
							SET 
								TGT.Broker_Id = IFNULL(SRC.Broker_Id,-1), 
								TGT.Sale_Qty = SRC.Sale_Qty, 
								TGT.Sale_Gross_Val= SRC.Sale_Gross_Val, 
								TGT.Sale_Disc_Val = SRC.Sale_Disc_Val, 
								TGT.Sale_Tax_Val= SRC.Sale_Tax_Val, 
								TGT.Sale_Net_Val = SRC.Sale_Net_Val, 
								TGT.Bill_Date = SRC.Bill_Date, 
								TGT.Bill_Time = SRC.Bill_Time, 
								TGT.Bill_Year = IFNULL(YEAR(SRC.Bill_Date),''), 
								TGT.Bill_Year_Month = IFNULL(EXTRACT(YEAR_MONTH FROM SRC.Bill_Date),''), 
								TGT.Bill_Day_Week = IFNULL(WEEKDAY(SRC.Bill_Date),-1), 
								TGT.Trans_Type = SRC.Trans_Type, 
								TGT.Trans_Sub_Type = SRC.Trans_Sub_Type, 
								TGT.Source_Program = SRC.Source_Program
							WHERE  
								SRC.Bill_Details_Id = TGT.Bill_Details_Id
								AND SRC.Customer_Id > 0
								AND SRC.Sale_Net_Val < 0 ; 

							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_15'), 'PROCESS_BD_HIST', 1); 

							
							UPDATE IGNORE 
								CDM_Non_Sales_Bill_Header AS TGT,
								(
									SELECT 
										TCBD.Bill_Header_Id AS Bill_Header_Id
										,TCBD.Customer_Id AS Customer_Id
										,TCBD.Store_Id AS Store_Id
										,TCBD.Bill_Date AS Bill_Date
										,TCBD.Bill_Time AS Bill_Time
										,SUM(TCBD.Sale_Qty) AS Bill_Qty
										,SUM(TCBD.Sale_Gross_Val) AS Bill_Gross_Val
										,SUM(TCBD.Sale_Disc_Val) AS Bill_Disc
										,SUM(TCBD.Sale_Tax_Val) AS Bill_Tax
										,SUM(TCBD.Sale_Net_Val) AS Bill_Total_Val
									FROM  
										Temp_CDM_Bill_Details TCBD
									WHERE  
										Customer_Id > 0 AND 
										Sale_Net_Val < 0 
									GROUP BY  
										Bill_Header_Id, 
										Customer_Id, 
										Store_Id, 
										Bill_Date,
										Bill_Time
								) AS SRC 
							SET
								TGT.Customer_Id = SRC.Customer_Id
								,TGT.Store_Id = SRC.Store_Id
								,TGT.Bill_Date = SRC.Bill_Date
								,TGT.Bill_Year = IFNULL(YEAR(SRC.Bill_Date),'')
								,TGT.Bill_Year_Month = IFNULL(EXTRACT(YEAR_MONTH FROM SRC.Bill_Date),'')
								,TGT.Bill_Day_Week = IFNULL(WEEKDAY(SRC.Bill_Date),-1)
								,TGT.Bill_Time = SRC.Bill_Time
								,TGT.Bill_Qty = SRC.Bill_Qty
								,TGT.Bill_Gross_Val = SRC.Bill_Gross_Val
								,TGT.Bill_Disc = SRC.Bill_Disc
								,TGT.Bill_Tax = SRC.Bill_Tax
								,TGT.Bill_Total_Val = SRC.Bill_Total_Val
							WHERE 
								TGT.Bill_Header_Id = SRC.Bill_Header_Id;
							
							CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_16'), 'PROCESS_BD_HIST', 1); 
						
						END IF;

							
						
							
						
						INSERT IGNORE INTO CDM_Store_Key_Lookup ( 
							Store_Key 
						) 
							SELECT  
								Store_Key 
							FROM Temp_CDM_Bill_Details 
							WHERE 
								Store_Key <> '' 
							GROUP BY Store_Key; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_17'), 'PROCESS_BD_HIST', 1); 

						
						INSERT IGNORE INTO CDM_Product_Key_Lookup ( 
							Product_Key 
						) 
							SELECT  
								Product_Key 
							FROM Temp_CDM_Bill_Details 
							WHERE 
								Product_Key <> '' 
							GROUP BY Product_Key; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_18'), 'PROCESS_BD_HIST', 1); 
						
						
						INSERT IGNORE INTO CDM_Broker_Key_Lookup ( 
							Broker_Key 
						) 
							SELECT  
								Broker_Key 
							FROM Temp_CDM_Bill_Details 
							WHERE 
								Broker_Key <> '' 
							GROUP BY Broker_Key; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_19'), 'PROCESS_BD_HIST', 1); 

							
						
							
						
						INSERT IGNORE INTO CDM_LOV_Master 
						( 
							LOV_Attribute, 
							LOV_Key, 
							LOV_Value 
						) 
							SELECT  
								'STORE', 
								Store_Key, 
								Store_Key 
							FROM Temp_CDM_Bill_Details 
							WHERE 
								Store_Key <> '' 
							GROUP BY Store_Key; 
							
						
						/* AM : Fix for Product LOV Id not getting generated into LOV Master for prd coming through bills  */
						INSERT IGNORE INTO CDM_LOV_Master 
						( 
						LOV_Attribute, 
						LOV_Key, 
						LOV_Value 
						) 
						SELECT  
						'PRODUCT', 
						Product_Key, 
						Product_Key 
						FROM Temp_CDM_Bill_Details 
						WHERE 
						Product_Key <> '' 
						GROUP BY Product_Key; 	
							
							
							
						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_20'), 'PROCESS_BD_HIST', 1); 
						
						
						INSERT IGNORE INTO CDM_LOV_Master 
						( 
							LOV_Attribute, 
							LOV_Key, 
							LOV_Value 
						) 
						SELECT  
							'BROKER', 
							Broker_Key, 
							Broker_Key 
						FROM Temp_CDM_Bill_Details 
						WHERE 
							Broker_Key <> '' 
						GROUP BY Broker_Key; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_21'), 'PROCESS_BD_HIST', 1); 

							
						
							
						
						UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_Store_Key_Lookup AS SRC
							SET TGT.Store_Id = SRC.Store_Id
						WHERE 
							TGT.Store_Key = SRC.Store_Key; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_22'), 'PROCESS_BD_HIST', 1); 
						
						
						UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_Product_Key_Lookup AS SRC
							SET TGT.Product_Id = SRC.Product_Id
						WHERE 
							TGT.Product_Key = SRC.Product_Key; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_23'), 'PROCESS_BD_HIST', 1); 
						
						
						UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_Broker_Key_Lookup AS SRC
							SET TGT.Broker_Id = SRC.Broker_Id
						WHERE 
							TGT.Broker_Key = SRC.Broker_Key;

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_24'), 'PROCESS_BD_HIST', 1); 

							
						
							
						
						INSERT IGNORE INTO CDM_Store_Master ( 
							Store_Id, 
							LOV_Id 
						) 
							SELECT  
								C.Store_Id, 
								B.LOV_Id 
							FROM CDM_LOV_Master AS B, Temp_CDM_Bill_Details AS C 
							WHERE  
								B.LOV_Attribute = 'STORE'  
								AND B.LOV_Key = C.Store_Key 
							GROUP BY C.Store_Id, B.LOV_Id; 
						
						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_25'), 'PROCESS_BD_HIST', 1); 
						
						/* AM : Fix for Product LOV Id not getting inserted into Prd Mast Org for prd coming through bills  */
						INSERT IGNORE INTO CDM_Product_Master_Original ( 
						Product_Id, 
						Product_LOV_Id 
						) 
						SELECT  
						C.Product_Id, 
						B.LOV_Id 
						FROM CDM_LOV_Master AS B, Temp_CDM_Bill_Details AS C 
						WHERE  
						B.LOV_Attribute = 'PRODUCT' AND 
						B.LOV_Key = C.Product_Key 
						GROUP BY C.Product_Id, B.LOV_Id; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_26'), 'PROCESS_BD_HIST', 1); 
						
						
						INSERT IGNORE INTO CDM_Broker_Master ( 
							Broker_Id, 
							LOV_Id 
						) 
							SELECT  
								C.Broker_Id, 
								B.LOV_Id 
							FROM CDM_LOV_Master AS B, Temp_CDM_Bill_Details AS C 
							WHERE  
								B.LOV_Attribute = 'BROKER' AND 
								B.LOV_Key = C.Broker_Key 
							GROUP BY C.Broker_Id, B.LOV_Id; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_27'), 'PROCESS_BD_HIST', 1); 
						
						
						INSERT IGNORE INTO CDM_Customer_PII_Master ( 
							Customer_Id 
						)     
							SELECT  
								Customer_Id 
							FROM Temp_CDM_Bill_Details 
							WHERE  
								Customer_Key <> '' 
							GROUP BY Customer_Id; 
						
						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_28'), 'PROCESS_BD_HIST', 1); 
						
						
						INSERT IGNORE INTO  CDM_Customer_Master ( 
							Customer_Id, 
							Is_New_Cust_Flag 
						) 
							SELECT  
								Customer_Id, 
								1 
							FROM Temp_CDM_Bill_Details                         
							WHERE  
								Customer_Key <> '' 
							GROUP BY Customer_Id; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_29'), 'PROCESS_BD_HIST', 1); 
 
							
						
							
						
						INSERT IGNORE INTO CDM_Bill_Header_Key_Lookup ( 
							Bill_Header_Key 
						) 
							SELECT Bill_Header_Key 
							FROM Temp_CDM_Bill_Details TCBD
							WHERE 
								Bill_Header_Key <> '' AND 
								Customer_Id > 0 AND 
								Sale_Net_Val >= 0  
							GROUP BY Bill_Header_Key; 
						
						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_30'), 'PROCESS_BD_HIST', 1); 
						
						
						UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_Bill_Header_Key_Lookup AS SRC
						SET TGT.Bill_Header_Id = SRC.Bill_Header_Id
						WHERE 
							TGT.Bill_Header_Key = SRC.Bill_Header_Key 
							AND Customer_Id > 0 
							AND Sale_Net_Val >= 0
							AND TGT.Bill_Header_Id <= 0; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_31'), 'PROCESS_BD_HIST', 1); 
  
						
						INSERT IGNORE INTO CDM_NM_Bill_Header_Key_Lookup ( 
							Bill_Header_Key 
						) 
							SELECT Bill_Header_Key 
							FROM Temp_CDM_Bill_Details 
							WHERE 
								Bill_Header_Key <> ''
								AND Customer_Id = ''
							GROUP BY Bill_Header_Key; 


/* AM : FIX FOR BILL HEADER ID NOT GETTING UPDATED JUNE-05*/						
						INSERT IGNORE INTO CDM_NM_Bill_Header_Key_Lookup ( 
							Bill_Header_Key 
						) 
							SELECT Bill_Header_Key 
							FROM Temp_CDM_Bill_Details 
							WHERE 
								Bill_Header_Key <> ''
								AND Customer_Id < 0
							GROUP BY Bill_Header_Key; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_32'), 'PROCESS_BD_HIST', 1); 


/* AM : FIX FOR BILL HEADER ID NOT GETTING UPDATED JUNE-05*/						
						UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_NM_Bill_Header_Key_Lookup AS SRC
						SET TGT.Bill_Header_Id = SRC.Bill_Header_Id
						WHERE 
							TGT.Bill_Header_Key = SRC.Bill_Header_Key
							AND Customer_Id = ''
							AND TGT.Bill_Header_Id <= 0; 

						UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_NM_Bill_Header_Key_Lookup AS SRC
						SET TGT.Bill_Header_Id = SRC.Bill_Header_Id
						WHERE 
							TGT.Bill_Header_Key = SRC.Bill_Header_Key
							AND Customer_Id < 0
							AND TGT.Bill_Header_Id <= 0;                             
                            
                            

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_33'), 'PROCESS_BD_HIST', 1); 
						
						
						INSERT IGNORE INTO CDM_Non_Sales_Bill_Header_Key_Lookup ( 
							Bill_Header_Key 
						) 
							SELECT Bill_Header_Key 
							FROM Temp_CDM_Bill_Details TCBD
							WHERE 
								Customer_Id > 0 AND 
								Bill_Header_Key <> '' AND 
								Sale_Net_Val < 0 
							GROUP BY Bill_Header_Key; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_34'), 'PROCESS_BD_HIST', 1); 
						
						
						UPDATE IGNORE Temp_CDM_Bill_Details AS TGT, CDM_Non_Sales_Bill_Header_Key_Lookup AS SRC 
						SET TGT.Bill_Header_Id = SRC.Bill_Header_Id
						WHERE 
							TGT.Bill_Header_Key = SRC.Bill_Header_Key 
							AND Customer_Id > 0
							AND Sale_Net_Val < 0
							AND TGT.Bill_Header_Id <= 0; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_35'), 'PROCESS_BD_HIST', 1); 
						
							
						
							
						
						INSERT IGNORE INTO CDM_Bill_Details_Key_Lookup ( 
							Bill_Details_Key 
						) 
							SELECT  
								Bill_Details_Key 
							FROM  
								Temp_CDM_Bill_Details TCBD
							WHERE  
								Bill_Details_Key <> '' AND 
								Customer_Id > 0 AND 
								Sale_Net_Val >= 0 ; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_36'), 'PROCESS_BD_HIST', 1); 
						
						UPDATE IGNORE Temp_CDM_Bill_Details AS TGT,CDM_Bill_Details_Key_Lookup AS SRC
						SET TGT.Bill_Details_Id = SRC.Bill_Details_Id
						WHERE TGT.Bill_Details_Key = SRC.Bill_Details_Key;

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_37'), 'PROCESS_BD_HIST', 1); 
						
						INSERT IGNORE INTO CDM_Bill_Details ( 
							Bill_Details_Id, 
							Bill_Header_Id, 
							Customer_Id, 
							Line_No, 
							Store_Id, 
							Product_Id, 
							Broker_Id, 
							Sale_Qty, 
							Sale_Gross_Val, 
							Sale_Disc_Val, 
							Sale_Tax_Val, 
							Sale_Net_Val, 
							Bill_Date, 
							Bill_Time, 
							Bill_Year, 
							Bill_Year_Month, 
							Bill_Day_Week, 
							Mrp, 
							Trans_Type, 
							Trans_Sub_Type, 
							Source_Program
						) 
							SELECT  
								TCBD.Bill_Details_Id, 
								TCBD.Bill_Header_Id,         
								IFNULL(TCBD.Customer_Id,-1), 
								TCBD.Line_No, 
								IFNULL(TCBD.Store_Id,-1), 
								IFNULL(TCBD.Product_Id,-1), 
								IFNULL(TCBD.Broker_Id,-1), 
								TCBD.Sale_Qty, 
								TCBD.Sale_Gross_Val, 
								TCBD.Sale_Disc_Val, 
								TCBD.Sale_Tax_Val, 
								TCBD.Sale_Net_Val, 
								TCBD.Bill_Date, 
								TCBD.Bill_Time, 
								IFNULL(YEAR(TCBD.Bill_Date),''), 
								IFNULL(EXTRACT(YEAR_MONTH FROM TCBD.Bill_Date),''), 
								IFNULL(WEEKDAY(TCBD.Bill_Date),-1), 
								Mrp, 
								Trans_Type, 
								Trans_Sub_Type, 
								Source_Program
							FROM Temp_CDM_Bill_Details AS TCBD
							WHERE  
								Customer_Id > 0 AND 
								Sale_Net_Val >=0; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_38'), 'PROCESS_BD_HIST', 1); 
						
						
						INSERT IGNORE INTO CDM_NM_Bill_Details_Key_Lookup ( 
							Bill_Details_Key 
						) 
							SELECT  
							Bill_Details_Key 
							FROM  
							Temp_CDM_Bill_Details 
							WHERE  
							Bill_Details_Key <> '' AND 
							Customer_Id <= 0; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_39'), 'PROCESS_BD_HIST', 1); 

						UPDATE IGNORE Temp_CDM_Bill_Details AS TGT,CDM_NM_Bill_Details_Key_Lookup AS SRC
						SET TGT.Bill_Details_Id = SRC.Bill_Details_Id
						WHERE TGT.Bill_Details_Key = SRC.Bill_Details_Key;

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_40'), 'PROCESS_BD_HIST', 1); 
						
						INSERT IGNORE INTO CDM_NM_Bill_Details ( 
							Bill_Details_Id, 
							Bill_Header_Id, 
							Customer_Id, 
							Line_No, 
							Store_Id, 
							Product_Id, 
							Broker_Id, 
							Sale_Qty, 
							Sale_Gross_Val, 
							Sale_Disc_Val, 
							Sale_Tax_Val, 
							Sale_Net_Val, 
							Bill_Date, 
							Bill_Time, 
							Bill_Year, 
							Bill_Year_Month, 
							Bill_Day_Week, 
							Mrp, 
							Trans_Type, 
							Trans_Sub_Type, 
							Source_Program
						) 
							SELECT  
								Bill_Details_Id, 
								Bill_Header_Id,         
								IFNULL(Customer_Id,-1), 
								Line_No, 
								IFNULL(Store_Id,-1), 
								IFNULL(Product_Id,-1), 
								IFNULL(Broker_Id,-1), 
								Sale_Qty, 
								Sale_Gross_Val, 
								Sale_Disc_Val, 
								Sale_Tax_Val, 
								Sale_Net_Val, 
								Bill_Date, 
								Bill_Time, 
								IFNULL(YEAR(Bill_Date),''), 
								IFNULL(EXTRACT(YEAR_MONTH FROM Bill_Date),''), 
								IFNULL(WEEKDAY(Bill_Date),-1), 
                                Mrp,
								Trans_Type, 
								Trans_Sub_Type, 
								Source_Program
							FROM Temp_CDM_Bill_Details
							WHERE  
								Customer_Id <= 0; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_41'), 'PROCESS_BD_HIST', 1); 

						
						INSERT IGNORE INTO CDM_Non_Sales_Bill_Details_Key_Lookup ( 
							Bill_Details_Key 
						) 
							SELECT  
								Bill_Details_Key 
							FROM  
								Temp_CDM_Bill_Details TCBD
							WHERE  
								Bill_Details_Key <> '' AND 
								Sale_Net_Val < 0 ; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_42'), 'PROCESS_BD_HIST', 1); 
						
						UPDATE IGNORE Temp_CDM_Bill_Details AS TGT,CDM_Non_Sales_Bill_Details_Key_Lookup AS SRC
						SET TGT.Bill_Details_Id = SRC.Bill_Details_Id
						WHERE TGT.Bill_Details_Key = SRC.Bill_Details_Key;

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_43'), 'PROCESS_BD_HIST', 1); 
						
						INSERT IGNORE INTO CDM_Non_Sales_Bill_Details ( 
							Bill_Details_Id, 
							Bill_Header_Id, 
							Customer_Id, 
							Line_No, 
							Store_Id, 
							Product_Id, 
							Broker_Id, 
							Sale_Qty, 
							Sale_Gross_Val, 
							Sale_Disc_Val, 
							Sale_Tax_Val, 
							Sale_Net_Val, 
							Bill_Date, 
							Bill_Time, 
							Bill_Year, 
							Bill_Year_Month, 
							Bill_Day_Week, 
							Mrp, 
							Trans_Type, 
							Trans_Sub_Type, 
							Source_Program
						) 
							SELECT  
								TCBD.Bill_Details_Id, 
								TCBD.Bill_Header_Id,         
								IFNULL(TCBD.Customer_Id,-1), 
								TCBD.Line_No, 
								IFNULL(TCBD.Store_Id,-1), 
								IFNULL(TCBD.Product_Id,-1), 
								IFNULL(TCBD.Broker_Id,-1), 
								TCBD.Sale_Qty, 
								TCBD.Sale_Gross_Val, 
								TCBD.Sale_Disc_Val, 
								TCBD.Sale_Tax_Val, 
								TCBD.Sale_Net_Val, 
								TCBD.Bill_Date, 
								TCBD.Bill_Time, 
								IFNULL(YEAR(TCBD.Bill_Date),''), 
								IFNULL(EXTRACT(YEAR_MONTH FROM TCBD.Bill_Date),''), 
								IFNULL(WEEKDAY(TCBD.Bill_Date),-1), 
								Mrp, 
								Trans_Type, 
								Trans_Sub_Type, 
								Source_Program
							FROM Temp_CDM_Bill_Details AS TCBD
							WHERE  
								TCBD.Customer_Id > 0 AND 
								TCBD.Sale_Net_Val < 0 ;
						
						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_44'), 'PROCESS_BD_HIST', 1); 

						
						INSERT IGNORE INTO CDM_Bill_Header( 
							Bill_Header_Id
							,Is_New
							,Customer_Id
							,Store_Id
							,Bill_Date
							,Bill_Year
							,Bill_Year_Month
							,Bill_Day_Week
							,Bill_Time 
							,Bill_Qty
							,Bill_Gross_Val
							,Bill_Disc
							,Bill_Tax
							,Bill_Total_Val
						)  
							SELECT 
								TCBD.Bill_Header_Id AS Bill_Header_Id
								,1
								,TCBD.Customer_Id AS Customer_Id
								,TCBD.Store_Id AS Store_Id
								,TCBD.Bill_Date AS Bill_Date
								,IFNULL(YEAR(TCBD.Bill_Date),'') AS Bill_Year
								,IFNULL(EXTRACT(YEAR_MONTH FROM TCBD.Bill_Date),'') AS Bill_Year_Month
								,IFNULL(WEEKDAY(TCBD.Bill_Date),-1) AS Bill_Day_Week
								,TCBD.Bill_Time AS Bill_Time
								,SUM(TCBD.Sale_Qty)
								,SUM(TCBD.Sale_Gross_Val)
								,SUM(TCBD.Sale_Disc_Val)
								,SUM(TCBD.Sale_Tax_Val)
								,SUM(TCBD.Sale_Net_Val)
							FROM  
								Temp_CDM_Bill_Details TCBD
							WHERE  
								Customer_Id > 0 AND 
								Sale_Net_Val >= 0   
							GROUP BY  
								Bill_Header_Id, 
								Customer_Id, 
								Store_Id, 
								Bill_Date,
								Bill_Year,
								Bill_Year_Month,
								Bill_Day_Week,
								Bill_Time; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_45'), 'PROCESS_BD_HIST', 1); 

						
						INSERT IGNORE INTO CDM_NM_Bill_Header( 
							Bill_Header_Id
							,Is_New
							,Store_Id
							,Bill_Date
							,Bill_Year
							,Bill_Year_Month
							,Bill_Day_Week
							,Bill_Time 
							,Bill_Qty
							,Bill_Gross_Val
							,Bill_Disc
							,Bill_Tax
							,Bill_Total_Val
						)  
							SELECT 
								Bill_Header_Id
								,1
								,Store_Id
								,Bill_Date
								,IFNULL(YEAR(Bill_Date),'') AS Bill_Year
								,IFNULL(EXTRACT(YEAR_MONTH FROM Bill_Date),'') AS Bill_Year_Month
								,IFNULL(WEEKDAY(Bill_Date),-1) AS Bill_Day_Week
								,Bill_Time AS Bill_Time
								,SUM(Sale_Qty)
								,SUM(Sale_Gross_Val)
								,SUM(Sale_Disc_Val)
								,SUM(Sale_Tax_Val)
								,SUM(Sale_Net_Val)
							FROM  
								Temp_CDM_Bill_Details 
							WHERE  
								Customer_Id <= 0
							GROUP BY  
								Bill_Header_Id, 
								Store_Id, 
								Bill_Date,
								Bill_Year,
								Bill_Year_Month,
								Bill_Day_Week,
								Bill_Time; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_46'), 'PROCESS_BD_HIST', 1); 

						
						INSERT IGNORE INTO CDM_Non_Sales_Bill_Header( 
							Bill_Header_Id
							,Is_New
							,Customer_Id
							,Store_Id
							,Bill_Date
							,Bill_Year
							,Bill_Year_Month
							,Bill_Day_Week
							,Bill_Time 
							,Bill_Qty
							,Bill_Gross_Val
							,Bill_Disc
							,Bill_Tax
							,Bill_Total_Val
						)  
							SELECT 
								TCBD.Bill_Header_Id AS Bill_Header_Id
								,1
								,TCBD.Customer_Id AS Customer_Id
								,TCBD.Store_Id AS Store_Id
								,TCBD.Bill_Date AS Bill_Date
								,IFNULL(YEAR(TCBD.Bill_Date),'') AS Bill_Year
								,IFNULL(EXTRACT(YEAR_MONTH FROM TCBD.Bill_Date),'') AS Bill_Year_Month
								,IFNULL(WEEKDAY(TCBD.Bill_Date),-1) AS Bill_Day_Week
								,TCBD.Bill_Time AS Bill_Time
								,SUM(TCBD.Sale_Qty)
								,SUM(TCBD.Sale_Gross_Val)
								,SUM(TCBD.Sale_Disc_Val)
								,SUM(TCBD.Sale_Tax_Val)
								,SUM(TCBD.Sale_Net_Val)
							FROM  
								Temp_CDM_Bill_Details TCBD
							WHERE  
								Customer_Id > 0 AND 
								Sale_Net_Val < 0 
							GROUP BY  
								Bill_Header_Id, 
								Customer_Id, 
								Store_Id, 
								Bill_Date,
								Bill_Year,
								Bill_Year_Month,
								Bill_Day_Week,
								Bill_Time; 

						CALL CDM_Update_Process_Log(vProc_Name, CONCAT(vStart_Cnt,'_47'), 'PROCESS_BD_HIST', 1); 
						
						SET vStart_Cnt = vStart_Cnt + vBatchSizeCustomerId; 
						SET vEnd_Cnt = vEnd_Cnt + vBatchSizeCustomerId; 
				 
						IF vStart_Cnt  >= vMax_Cnt THEN 
							LEAVE PROCESS_BD_HIST;
						END IF; 
					END LOOP PROCESS_BD_HIST; 
					
                                        
					DROP TABLE Temp_CDM_Bill_Details  ; 
					
				ELSE
					CALL CDM_Update_Process_Log(vProc_Name, 6, 'No records to process.', 1);
				END IF;
			ELSE
				CALL CDM_Update_Process_Log(vProc_Name, 7, 'No records to process.', 1); 
			END IF;
		ELSE
			CALL CDM_Update_Process_Log(vProc_Name, 8, 'No records to process.', 1); 
		END IF;
	ELSE
		CALL CDM_Update_Process_Log(vProc_Name, 9, 'No records to process.', 1); 	
	END IF;
 
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', 'Done', 'CDM_ETL_Bill_Detail', 1); 

    SELECT concat(vProc_Name,' COMPLETED !! .',now()) as '';    
    SELECT "=================== SUMMARY OF DATA LOAD ===================================" as '';
    SELECT concat( 'Records in csv_Bill_Details ' , count(1)) as ''  from csv_Bill_Details;
    select concat( 'Records in CDM_Stg_Bill_Details ' , count(1)) as ''from CDM_Stg_Bill_Details;
    SELECT COUNT(1) into @vcnt FROM CDM_Bill_Details where Bill_Details_Id > @vBill_Details_Id  ;
	SELECT concat(vProc_Name,' New Records Loaded to   CDM_Bill_Details !! ',@vcnt ) as '';
	SELECT COUNT(1) into @vcnt FROM CDM_NM_Bill_Details where Bill_Details_Id > @vNM_Bill_Details_Id  ;
    SELECT concat(vProc_Name,' New Records Loaded to    CDM_NM_Bill_Details : ',@vcnt ) as '';	
	SELECT COUNT(1) into @vcnt FROM CDM_Non_Sales_Bill_Details where Bill_Details_Id > @vNon_Sales_Bill_Details_Id  ;
    SELECT concat(vProc_Name,' New Records Loaded to   CDM_Non_Sales_Bill_Details :  ',@vcnt ) as '';
	SELECT COUNT(1) into @vcnt FROM CDM_Customer_Master where Customer_Id > @vCustomer_Id  ;
    SELECT concat(vProc_Name,' New Records Loaded to  CDM_Customer_Master : ',@vcnt ) as '';
	SELECT COUNT(1) into @vcnt FROM CDM_Product_Master where Product_Id > @vProduct_Id  ;
    SELECT concat(vProc_Name,' New Records Loaded to CDM_Product_Master : ',@vcnt ) as '';
	SELECT COUNT(1) into @vcnt FROM CDM_Store_Master where Store_Id > @vStore_Id  ;
    SELECT concat(vProc_Name,' New Records Loaded to  CDM_Store_Master : ',@vcnt ) as '';
    select "=============================================================================" as '';
END$$
DELIMITER ;
