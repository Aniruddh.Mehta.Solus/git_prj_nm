DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_DQC_Bill_Details (OUT msg text)

dqc_bill:BEGIN
declare msg varchar(1024);
select count(1) into @n1 from CDM_Customer_Master ;
select count(1) into @nproducts from CDM_Product_Master_Original ;
select count(distinct Product_Id) into @nproductsbills from CDM_Bill_Details ;
select count(distinct Customer_Id ) into @n2 from CDM_Bill_Details  ;
select max(Bill_Date) into @date1 from CDM_Bill_Details;
select count(1) into @nstores from CDM_Store_Master ;
select count(distinct Store_Id) into @nstoresbills from CDM_Bill_Details ;

if @n1<=0.8*(@n2)  then
  set msg='Number of Customers in Bills is Greater than Customers in Customer Master  ';
  leave dqc_bill;
end if ;

if @date1> curdate()  then
  set msg='Max Bill date is Greater than Curdate  ';
  leave dqc_bill;
end if ;

if @nproducts < @nproductsbills  then
  set msg=' Products in Bills is greater than Products ; Please Relaod the Product Master wwith Complete Bills  ';
  leave dqc_bill;
end if ;

if @nstores < @nstoresbills  then
  set msg=' Stores in Bills is greater than Stores ; Please Relaod the Store Master with Complete Bills  ';
  leave dqc_bill;
end if ;

set msg="";

END$$
DELIMITER ;
