DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Customer_Membership (IN vBatchSize BIGINT) 
BEGIN 
    DECLARE vSuccessRet TINYINT DEFAULT 0; 
 
    DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
 
     
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET vSuccessRet = 0; 
 
    SET vEnd_Cnt = vBatchSize; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Membership', 1, 'CDM_ETL_Customer_Membership', 1); 
     
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Customer_Membership ORDER BY Rec_Id ASC LIMIT 1; 
    SET vStart_Cnt = @Rec_Cnt; 
    SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
     
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Customer_Membership ORDER BY Rec_Id DESC LIMIT 1; 
 
    IF @Rec_Cnt IS NULL THEN 
      SET @Rec_Cnt = 0; 
    END IF; 
   
  CUSTOMER_MEMBERSHIP: LOOP 
     
        CALL CDM_Update_Process_Log('CDM_ETL_Customer_Membership', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Customer_Membership', 1); 
 
     
    INSERT IGNORE INTO CDM_Membership_Key_Lookup ( 
      Membership_Rec_Key 
    ) 
      SELECT  
        Membership_Rec_Key 
      FROM  
        CDM_Stg_Customer_Membership 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
     
     
    CALL CDM_Update_Process_Log('CDM_ETL_Customer_Membership', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Customer_Membership', 1); 
 
    UPDATE IGNORE CDM_Stg_Customer_Membership 
        SET Membership_Rec_Id = ( 
      SELECT Membership_Rec_Id  
            FROM CDM_Membership_Key_Lookup 
            WHERE 
        CDM_Stg_Customer_Membership.Membership_Rec_Key = CDM_Membership_Key_Lookup.Membership_Rec_Key 
    ) 
    WHERE  
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Membership_Rec_Id <= 0 AND 
      Rec_Is_Processed = -1; 
 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Customer_Membership', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Customer_Membership', 1); 
 
     
  UPDATE IGNORE CDM_Stg_Customer_Membership AS A 
    SET Tier_Name_LOV_Id = ( 
      SELECT LOV_Id   
      FROM CDM_LOV_Master AS B 
      WHERE  
        A.Tier_Name = B.LOV_Key 
        AND B.LOV_Attribute = 'TIER_NAME'  
    ) 
    WHERE  
      Tier_Name_LOV_Id <= 0 AND 
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt ; 
   
  CALL CDM_Update_Process_Log('CDM_ETL_Customer_Membership', CONCAT(vStart_Cnt,'_5'), 'CDM_ETL_Customer_Membership', 1); 
     
  INSERT IGNORE INTO CDM_Customer_Membership ( 
    Customer_Id, 
        Membership_Rec_Id, 
    Mobile, 
    Tier_Name_LOV_Id, 
    Tier_Expiry_Date, 
    Tier_Updated_Date 
    ) 
      SELECT  
        Customer_Id, 
                Membership_Rec_Id, 
        Mobile, 
        Tier_Name_LOV_Id, 
        Tier_Expiry_Date, 
        Tier_Updated_Date 
      FROM  CDM_Stg_Customer_Membership  
      WHERE    
        CDM_Stg_Customer_Membership.Rec_Is_Processed = -1 AND 
        CDM_Stg_Customer_Membership.Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt ; 
         
      SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE CUSTOMER_MEMBERSHIP; 
        END IF; 
         
  END LOOP CUSTOMER_MEMBERSHIP; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Customer_Membership', 3, 'CDM_ETL_Customer_Membership', 1); 
        
END	
mysolus$$
