DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_DQC_Customer_Master (OUT msg text)
dqc_cust:BEGIN
declare msg varchar(1024);
select count(1) into @n1 from CDM_Customer_Master ;
select count(distinct Customer_Id ) into @n2 from CDM_Bill_Details  ;
if @n1<=0.8*(@n2)  then
  set msg='Number of Customers in Bills is Greater than Customers in Customer Master  ';
  leave dqc_cust;
end if ;


/*
INSERT INTO  CART_SOLUS_PROD . CDM_DQC_ERRORS 
(
 entity ,
 error_code ,
 error_msg ,
 reject_record_key ,
 reject_record_details )
VALUES
(
'Customer_Master',
001,
'error',
'customer_id',
'record details'
);
*/
set msg="";
END$$
DELIMITER ;
