DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Bill_Detail_Solus50 (IN vBatchSize BIGINT) 
BEGIN 
    DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
         
    set sql_safe_updates = 0;  
         
    -- SELECT B.Customer_Id INTO vStart_Cnt FROM CDM_Stg_Bill_Details A JOIN CDM_Customer_Key_Lookup B ON (A.Customer_Key = B.Customer_Key)  
    -- ORDER BY B.Customer_Id ASC LIMIT 1; 
     
    SET vStart_Cnt=0; 
    select vBatchSize; 
     
    SET @Rec_Cnt = 0; 
    SELECT B.Customer_Id INTO @Rec_Cnt FROM CDM_Stg_Bill_Details A JOIN CDM_Customer_Key_Lookup B ON (A.Customer_Key = B.Customer_Key)  
     where B.Customer_Id  ORDER BY B.Customer_Id DESC LIMIT 1;     
    select @Rec_Cnt; 
    IF @Rec_Cnt IS NULL THEN  
        SET @Rec_Cnt = 0; 
    END IF; 
     
    SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
    DROP TABLE IF EXISTS Temp_CDM_Bill_Details ; 
    CREATE TABLE IF NOT EXISTS Temp_CDM_Bill_Details   ( 
        Rec_Id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
        Rec_Is_Processed TINYINT DEFAULT - 1 NOT NULL, 
        Bill_Details_Key VARCHAR(128) NOT NULL, 
        Bill_Details_Id BIGINT DEFAULT - 1 NOT NULL, 
        Bill_Header_Key VARCHAR(128) NOT NULL, 
        Bill_Header_Id BIGINT DEFAULT - 1 NOT NULL, 
        Line_No SMALLINT DEFAULT - 1 NOT NULL, 
        Store_Key VARCHAR(128) NOT NULL, 
        Store_Id BIGINT DEFAULT -1 NOT NULL, 
        Store_LOV_Id BIGINT DEFAULT -1 NOT NULL, 
        Product_Key VARCHAR(128) NOT NULL, 
        Product_Id BIGINT DEFAULT -1 NOT NULL, 
        Product_LOV_Id BIGINT DEFAULT -1 NOT NULL, 
        Broker_Key VARCHAR(128) NOT NULL, 
        Broker_Id BIGINT(20) NOT NULL DEFAULT -1, 
        Customer_Key VARCHAR(128) DEFAULT '' NOT NULL, 
        Customer_Id BIGINT DEFAULT -1 NOT NULL, 
        Bill_Date DATE, 
        Bill_Time TIME, 
        Cust_Txn_Number SMALLINT NOT NULL DEFAULT -1, 
        Sale_Qty DECIMAL(15 , 2 ) NOT NULL DEFAULT 0, 
        Sale_Gross_Val DECIMAL(15 , 2 ) NOT NULL DEFAULT 0, 
        Sale_Disc_Val DECIMAL(15 , 2 ) NOT NULL DEFAULT 0, 
        Sale_Tax_Val DECIMAL(15 , 2 ) NOT NULL DEFAULT 0, 
        Sale_Net_Val DECIMAL(15 , 2 ) NOT NULL DEFAULT 0, 
        Mrp DECIMAL(15 , 2 ) DEFAULT - 1.0 NOT NULL, 
        Trans_Type VARCHAR(128) DEFAULT '' NOT NULL, 
        Trans_Sub_Type VARCHAR(128) DEFAULT '' NOT NULL, 
        Source_Program VARCHAR(128) DEFAULT '' NOT NULL, 
        Cat_Size VARCHAR(128) DEFAULT '' NOT NULL 
    ) ENGINE = INNODB DEFAULT CHARACTER SET=LATIN1; 
 
    ALTER TABLE Temp_CDM_Bill_Details   
        ADD INDEX (Bill_Details_Key),     
        ADD INDEX (Bill_Header_Key), 
        ADD INDEX (Bill_Details_Id),     
        ADD INDEX (Bill_Header_Id), 
        ADD INDEX (Sale_Net_Val), 
        ADD INDEX (Store_Id), 
        ADD INDEX (Store_Key), 
        ADD INDEX (Customer_Key), 
        ADD INDEX (Customer_Id), 
        ADD INDEX (Product_Key), 
        ADD INDEX (Product_Id), 
        ADD INDEX (Broker_Key), 
        ADD INDEX (Broker_Id), 
        ADD INDEX (Rec_Is_Processed), 
        ADD INDEX (Bill_Date); 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', 1, 'CDM_ETL_Bill_Detail', 1); 
        Delete from  log_solus where module='CDM_ETL_Bill_Detail'; 
 
    PROCESS_BD_HIST: LOOP 
        select  vEnd_Cnt ,vStart_Cnt , vBatchSize; 
        TRUNCATE TABLE Temp_CDM_Bill_Details  ; 
 
        CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_0'), 'CDM_ETL_Bill_Detail', 1); 
        INSERT IGNORE INTO Temp_CDM_Bill_Details   ( 
            Bill_Details_Key, 
            Bill_Details_Id, 
            Bill_Header_Key, 
            Bill_Header_Id, 
            Line_No, 
            Store_Key, 
            Store_Id, 
            Store_LOV_Id, 
            Product_Key, 
            Product_Id, 
            Product_LOV_Id, 
            Broker_Key, 
            Broker_Id, 
            Customer_Key, 
            Customer_Id, 
            Bill_Date, 
            Bill_Time, 
            Cust_Txn_Number, 
            Sale_Qty, 
            Sale_Gross_Val, 
            Sale_Disc_Val, 
            Sale_Tax_Val, 
            Sale_Net_Val, 
            Mrp, 
            Trans_Type, 
            Trans_Sub_Type, 
            Source_Program, 
            Cat_Size 
        ) 
            SELECT 
                Bill_Details_Key, 
                Bill_Details_Id, 
                Bill_Header_Key, 
                Bill_Header_Id, 
                Line_No, 
                Store_Key, 
                Store_Id, 
                Store_LOV_Id, 
                Product_Key, 
                Product_Id, 
                Product_LOV_Id, 
                Broker_Key, 
                Broker_Id, 
                A.Customer_Key, 
                B.Customer_Id, 
                Bill_Date, 
                Bill_Time, 
                Cust_Txn_Number, 
                Sale_Qty, 
                Sale_Gross_Val, 
                Sale_Disc_Val, 
                Sale_Tax_Val, 
                Sale_Net_Val, 
                Mrp, 
                Trans_Type, 
                Trans_Sub_Type, 
                Source_Program, 
                Cat_Size 
            FROM CDM_Stg_Bill_Details   A ,CDM_Customer_Key_Lookup B 
            WHERE B.Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt 
            AND A.Customer_Key = B.Customer_Key;    
            CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Bill_Detail', 1); 
             
            INSERT IGNORE INTO CDM_Store_Key_Lookup ( 
            Store_Key 
        ) 
            SELECT  
                Store_Key 
            FROM Temp_CDM_Bill_Details 
            WHERE 
                Store_Key <> '' 
            GROUP BY Store_Key; 
 
    INSERT IGNORE INTO CDM_LOV_Master 
        ( 
            LOV_Attribute, 
            LOV_Key, 
            LOV_Value 
        ) 
            SELECT  
                'STORE', 
                Store_Key, 
                Store_Key 
            FROM Temp_CDM_Bill_Details 
            WHERE 
                Store_Key <> '' 
            GROUP BY Store_Key; 
             
        
        INSERT IGNORE INTO CDM_Product_Key_Lookup ( 
            Product_Key 
        ) 
            SELECT  
                Product_Key 
            FROM Temp_CDM_Bill_Details   
            WHERE 
                Product_Key <> '' 
            GROUP BY Product_Key; 
 
        INSERT IGNORE INTO CDM_LOV_Master 
        ( 
            LOV_Attribute, 
            LOV_Key, 
            LOV_Value 
        ) 
            SELECT  
                'PRODUCT', 
                Product_Key, 
                Product_Key 
            FROM Temp_CDM_Bill_Details   
            WHERE 
                Product_Key <> '' 
            GROUP BY Product_Key;  
         
        CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Bill_Detail', 1); 
    UPDATE IGNORE Temp_CDM_Bill_Details 
        SET Store_Id = ( 
                SELECT Store_Id 
                FROM CDM_Store_Key_Lookup 
                WHERE CDM_Store_Key_Lookup.Store_Key = Temp_CDM_Bill_Details.Store_Key 
            ) 
        WHERE 
            Store_Key  <> ''; 
             
        UPDATE IGNORE Temp_CDM_Bill_Details   
        SET  
            Product_Id = ( 
                SELECT Product_Id 
                FROM CDM_Product_Key_Lookup 
                WHERE  
                    Temp_CDM_Bill_Details  .Product_Key = CDM_Product_Key_Lookup.Product_Key 
            ) 
        WHERE  
            Product_Key  <> ''; 
 
        CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_Bill_Detail', 1); 
 
        INSERT IGNORE INTO CDM_Product_Master_Original ( 
            Product_Id, 
            Product_LOV_Id 
        ) 
            SELECT  
                C.Product_Id, 
                B.LOV_Id 
            FROM CDM_LOV_Master AS B, Temp_CDM_Bill_Details   AS C 
            WHERE  
                B.LOV_Attribute = 'PRODUCT' AND 
                B.LOV_Key = C.Product_Key 
            GROUP BY C.Product_Id, B.LOV_Id; 
        CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_5'), 'CDM_ETL_Bill_Detail', 1); 
 
        INSERT IGNORE INTO CDM_Bill_Header_Key_Lookup ( 
            Bill_Header_Key 
        ) 
            SELECT Bill_Header_Key 
            FROM Temp_CDM_Bill_Details   TCBD, CDM_Product_Master_Original CDM_PM 
            WHERE 
                TCBD.Product_Id = CDM_PM.Product_Id AND 
                Bill_Header_Key <> '' AND 
                Customer_Key <> '' AND 
                Sale_Net_Val >= 0 AND 
                Is_Active <> 0 
            GROUP BY Bill_Header_Key; 
         
        CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_6'), 'CDM_ETL_Bill_Detail', 1); 
        UPDATE IGNORE Temp_CDM_Bill_Details   
        SET Bill_Header_Id = ( 
            SELECT Bill_Header_Id  
            FROM CDM_Bill_Header_Key_Lookup 
            WHERE 
                Temp_CDM_Bill_Details  .Bill_Header_Key = CDM_Bill_Header_Key_Lookup.Bill_Header_Key 
        ) 
        WHERE 
            Bill_Header_Key  <> '' AND 
            Bill_Header_Id <= 0; 
            
           CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_Bill_Detail', 1); 
 
        INSERT IGNORE INTO CDM_Bill_Header( 
                Bill_Header_Id, 
                Is_New, 
                Customer_Id, 
                Store_Id, 
                Bill_Date, 
                Bill_Year, 
                Bill_Year_Month, 
                Bill_Day_Week, 
                Bill_Time, 
                 Bill_Qty, 
                Bill_Gross_Val, 
                Bill_Disc, 
                Bill_Tax, 
                Bill_Total_Val 
            )  
                SELECT 
                    Bill_Header_Id, 
                    1, 
                    IFNULL(Customer_Id,-1), 
                    IFNULL(Store_Id,-1), 
                    Bill_Date, 
                    IFNULL(YEAR(Bill_Date),''), 
                    IFNULL(EXTRACT(YEAR_MONTH FROM Bill_Date),''), 
                    IFNULL(WEEKDAY(Bill_Date),-1), 
                    Bill_Time, 
                    SUM(Sale_Qty) AS New_Sale_Qty,  
                    SUM(Sale_Gross_Val) AS New_Sale_Gross_Val,  
                    SUM(Sale_Disc_Val) AS New_Sale_Disc_Val,  
                    SUM(Sale_Tax_Val) AS New_Sale_Tax_Val, 
                    SUM(Sale_Net_Val) AS New_Sale_Net_Val 
                FROM  
                    Temp_CDM_Bill_Details   TCBD, CDM_Product_Master_Original CDM_PM 
                WHERE  
                    TCBD.Product_Id = CDM_PM.Product_Id AND 
                    Customer_Id > 0 AND 
                    Sale_Net_Val >= 0 AND  
                    Is_Active <> 0 
                GROUP BY  
                    Bill_Header_Id, 
                    Customer_Id, 
                    Store_Id, 
                    Bill_Date, 
                    Bill_Time; 
                     
        CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_8'), 'CDM_ETL_Bill_Detail', 1); 
         
        INSERT IGNORE INTO CDM_Bill_Details_Key_Lookup ( 
            Bill_Details_Key 
        ) 
            SELECT  
                Bill_Details_Key 
            FROM  
                Temp_CDM_Bill_Details   TCBD, CDM_Product_Master_Original CDM_PM 
            WHERE  
                TCBD.Product_Id = CDM_PM.Product_Id AND 
                Bill_Details_Key <> '' AND 
                Customer_Id > 0 AND 
                Sale_Net_Val >= 0 AND  
                Is_Active <> 0; 
     
        CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_9'), 'CDM_ETL_Bill_Detail', 1); 
 
        UPDATE IGNORE Temp_CDM_Bill_Details   
        SET Bill_Details_Id = ( 
            SELECT Bill_Details_Id  
            FROM CDM_Bill_Details_Key_Lookup 
            WHERE 
                Temp_CDM_Bill_Details  .Bill_Details_Key = CDM_Bill_Details_Key_Lookup.Bill_Details_Key 
        ) 
        WHERE  
            Bill_Details_Id <= 0;  
     
         
        CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_10'), 'CDM_ETL_Bill_Detail', 1); 
 
        INSERT IGNORE INTO CDM_Bill_Details( 
            Bill_Details_Id, 
            Bill_Header_Id, 
            Customer_Id, 
            Line_No, 
            Store_Id, 
            Product_Id, 
            Broker_Id, 
            Sale_Qty, 
            Sale_Gross_Val, 
            Sale_Disc_Val, 
            Sale_Tax_Val, 
            Sale_Net_Val, 
            Bill_Date, 
            Bill_Time, 
            Bill_Year, 
            Bill_Year_Month, 
            Bill_Day_Week, 
            Mrp, 
            Trans_Type, 
            Trans_Sub_Type, 
            Source_Program, 
            Cat_Size 
        ) 
 
            SELECT  
                TCBD.Bill_Details_Id, 
                TCBD.Bill_Header_Id,         
                IFNULL(TCBD.Customer_Id,-1), 
                TCBD.Line_No, 
                IFNULL(TCBD.Store_Id,-1), 
                IFNULL(TCBD.Product_Id,-1), 
                IFNULL(TCBD.Broker_Id,-1), 
                TCBD.Sale_Qty, 
                TCBD.Sale_Gross_Val, 
                TCBD.Sale_Disc_Val, 
                TCBD.Sale_Tax_Val, 
                TCBD.Sale_Net_Val, 
                TCBD.Bill_Date, 
                TCBD.Bill_Time, 
                IFNULL(YEAR(TCBD.Bill_Date),''), 
                IFNULL(EXTRACT(YEAR_MONTH FROM TCBD.Bill_Date),''), 
                IFNULL(WEEKDAY(TCBD.Bill_Date),-1), 
                Mrp, 
                Trans_Type, 
                Trans_Sub_Type, 
                Source_Program, 
                Cat_Size 
            FROM Temp_CDM_Bill_Details   TCBD, CDM_Product_Master_Original CDM_PM 
            WHERE  
                TCBD.Product_Id = CDM_PM.Product_Id AND 
                Customer_Id > 0 AND 
                Sale_Net_Val >=0 AND 
                Is_Active <> 0; 
     
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(vStart_Cnt,'_11'), 'CDM_ETL_Bill_Detail', 1); 
    select count(1) into @cnt from Temp_CDM_Bill_Details; 
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', CONCAT(@cnt,'_11'), 'CDM_ETL_Bill_Detail', 1); 
     
    insert into log_solus(module, rec_start, rec_end) values ('CDM_ETL_Bill_Detail',vStart_Cnt, vEnd_Cnt); 
 
     
          SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_BD_HIST; 
        END IF; 
         
    END LOOP PROCESS_BD_HIST; 
 
    DROP TABLE Temp_CDM_Bill_Details  ; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_Bill_Detail', 'Done', 'CDM_ETL_Bill_Detail', 1); 
     
END	
mysolus$$
