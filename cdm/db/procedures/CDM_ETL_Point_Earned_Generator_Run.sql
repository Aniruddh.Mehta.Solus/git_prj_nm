DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_ETL_Point_Earned_Generator_Run ()
BEGIN 
  CALL CDM_ETL_Point_Earned_Generator_P1(1);
  CALL CDM_ETL_Points_Earned(1) ;
  CALL CDM_ETL_Point_Earned_Generator_P2(1); 
  CALL CDM_ETL_Points_Earned(1) ;
END$$
DELIMITER ;
