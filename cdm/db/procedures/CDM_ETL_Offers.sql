DELIMITER mysolus$$
		CREATE  OR REPLACE  PROCEDURE  CDM_ETL_Offers (IN vBatchSize BIGINT) 
BEGIN 
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
     
  SET vEnd_Cnt = vBatchSize; 
  SET SQL_SAFE_UPDATES=0;
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Offers ORDER BY Rec_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
   
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_Offers ORDER BY Rec_Id DESC LIMIT 1; 
    IF @Rec_Cnt IS NULL THEN  
    SET @Rec_Cnt = 0; 
  END IF; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Offers', 1, 'CDM_ETL_Offers', 1); 
     
  PROCESS_OFFERS: LOOP 
     
    INSERT IGNORE INTO CDM_Offer_Key_Lookup ( 
      Offer_Key 
    ) 
      SELECT  
        Offer_Key 
      FROM  
        CDM_Stg_Offers 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
     
     
    CALL CDM_Update_Process_Log('CDM_ETL_Offers', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_Offers', 1); 
 
    UPDATE IGNORE CDM_Stg_Offers 
        SET Offer_Id = ( 
      SELECT Offer_Id  
            FROM CDM_Offer_Key_Lookup 
            WHERE 
        CDM_Stg_Offers.Offer_Key = CDM_Offer_Key_Lookup.Offer_Key 
    ) 
    WHERE  
      Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
            Offer_Id <= 0 AND 
      Rec_Is_Processed = -1; 
 
         
    CALL CDM_Update_Process_Log('CDM_ETL_Offers', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_Offers', 1); 
 
    INSERT IGNORE INTO CDM_Offers ( 
      Offer_Id, 
      Offer_Code, 
      Validity_Period, 
      Offer_Score, 
      Offer_Type, 
      Offer_Desc ,
      Offer_Name,
	  Base_Item,
	  Offer_Is_Core,
	  StartDate,
	  EndDate
    ) 
      SELECT  
        Offer_Id, 
        Offer_Code, 
        Validity_Period, 
        Offer_Score, 
        Offer_Type, 
        Offer_Desc ,
        Offer_Name,
	    Base_Item,
	    Offer_Is_Core,
	    StartDate,
	    EndDate
      FROM CDM_Stg_Offers 
      WHERE  
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 
 
      SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_OFFERS; 
        END IF; 
         
  END LOOP PROCESS_OFFERS; 
 
  UPDATE IGNORE  
        CDM_Stg_Offers AS A, 
        CDM_Offers AS B 
      SET  
  B.Offer_Code=A.Offer_Code,
  B.Validity_Period=A.Validity_Period,
  B.Offer_Score=A.Offer_Score,
  B.Offer_Type=A.Offer_Type,
  B.Offer_Desc=A.Offer_Desc,
  B.Offer_Name=A.Offer_Name,
  B.Base_Item=A.Base_Item,
  B.Offer_Is_Core=A.Offer_Is_Core,
  B.StartDate=A.StartDate,
  B.EndDate=A.EndDate
  WHERE   B.Offer_Id=A.Offer_Id;
 
 
  CALL CDM_Update_Process_Log('CDM_ETL_Offers', 2, 'CDM_ETL_Offers', 1); 
   
END
mysolus$$
