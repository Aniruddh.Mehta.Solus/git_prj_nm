DELIMITER $$
CREATE OR REPLACE PROCEDURE  S_LOOPING (IN vTable_From varchar(128), IN vTable_To varchar(128), IN vBatchSize bigint)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
SELECT Customer_Id INTO vStart_Cnt FROM vTable_From ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt FROM vTable_From ORDER BY Customer_Id DESC LIMIT 1;

PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;
insert ignore into vTable_To select * from vTable_From where Customer_Id between vStart_Cnt and vEnd_Cnt; 
SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP;  
END$$
DELIMITER ;
