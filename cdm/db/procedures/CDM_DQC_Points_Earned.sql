DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_DQC_Points_Earned (OUT msg text)
dqc_points_earned:BEGIN
declare msg varchar(1024);
select count(1) into @n1 from CDM_Customer_Master  ;
select count(distinct Customer_Id) into @ncust_points from CDM_Points_Earned ;

if @n1<=@ncust_points  then
  set msg='Number of Customers in Points Earned is Greater than Customers in Customer Master ; please relaod Customer Master  ';
  leave dqc_points_earned;
end if ;

set msg=""; 
END$$
DELIMITER ;
