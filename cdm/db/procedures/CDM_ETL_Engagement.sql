

DELIMITER $$
CREATE or REPLACE PROCEDURE  CDM_ETL_ENGAGEMENT (IN vBatchSize BIGINT,IN vTreat_As_Incr TINYINT)
BEGIN 
  DECLARE vStart_Cnt BIGINT DEFAULT 0; 
    DECLARE vEnd_Cnt BIGINT; 
     
  SET vEnd_Cnt = vBatchSize; 
  SET SQL_SAFE_UPDATES=0;
  SET @Rec_Cnt = 0; 
  SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_ENGAGEMENT ORDER BY Rec_Id ASC LIMIT 1; 
  SET vStart_Cnt = @Rec_Cnt; 
  SET vEnd_Cnt = vStart_Cnt + vBatchSize; 
   
    SET @Rec_Cnt = 0; 
    SELECT Rec_Id INTO @Rec_Cnt FROM CDM_Stg_ENGAGEMENT ORDER BY Rec_Id DESC LIMIT 1; 
    IF @Rec_Cnt IS NULL THEN  
    SET @Rec_Cnt = 0; 
  END IF; 
  DROP TABLE IF EXISTS Temp_CDM_ENGAGEMENT; 
  CREATE TABLE IF NOT EXISTS Temp_CDM_ENGAGEMENT ( 
  Rec_Id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  Rec_Is_Processed TINYINT DEFAULT - 1 NOT NULL, 
  Engagement_Key varchar(512) NOT NULL,
  Engagement_Id bigint(20) NOT NULL,
  Customer_Key varchar(512) NOT NULL,
  Customer_Id bigint(20) NOT NULL,
  Store_Key varchar(512) DEFAULT NULL,
  Store_Id bigint(20) DEFAULT NULL,
  Product_Key varchar(512) DEFAULT NULL,
  Product_Id bigint(20) DEFAULT NULL,
  Engagement_Date date DEFAULT NULL,
	Engagement_Params text DEFAULT NULL,
  Engagement_Type varchar(512) DEFAULT NULL,
  Engagement_Type_LOV_Id bigint(20) DEFAULT NULL,
  Engagement_Type_Key varchar(512) DEFAULT NULL,
  Engagement_Type_Key_LOV_Id bigint(20) DEFAULT NULL,
  CUSTOM_TAG_ATTR_1 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_ATTR_1_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_ATTR_2 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_ATTR_2_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_ATTR_3 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_ATTR_3_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_ATTR_4 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_ATTR_4_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_ATTR_5 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_ATTR_5_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_ATTR_6 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_ATTR_6_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_ATTR_7 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_ATTR_7_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_ATTR_8 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_ATTR_8_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_VAL_1 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_VAL_1_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_VAL_2 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_VAL_2_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_VAL_3 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_VAL_3_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_VAL_4 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_VAL_4_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_KEY_1 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_KEY_1_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_KEY_2 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_KEY_2_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_KEY_3 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_KEY_3_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_KEY_4 varchar(512) DEFAULT NULL,
  CUSTOM_TAG_KEY_4_LOV_Id bigint(20) DEFAULT -1,
  CUSTOM_TAG_DATE_1 date DEFAULT NULL,
  CUSTOM_TAG_DATE_2 date DEFAULT NULL,
  CUSTOM_TAG_DATE_3 date DEFAULT NULL,
  CUSTOM_TAG_DATE_4 date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=Latin1;

ALTER TABLE Temp_CDM_ENGAGEMENT ADD  INDEX(Customer_Id);
ALTER TABLE Temp_CDM_ENGAGEMENT ADD INDEX(Customer_Key);
ALTER TABLE Temp_CDM_ENGAGEMENT ADD INDEX(Engagement_Id);
ALTER TABLE Temp_CDM_ENGAGEMENT ADD INDEX(Engagement_Key);

  CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', 1, 'CDM_ETL_ENGAGEMENT', 1); 
     
  PROCESS_ENG_HIST: LOOP 
     
    TRUNCATE TABLE Temp_CDM_ENGAGEMENT; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_0'), 'CDM_ETL_ENGAGEMENT', 1); 
    INSERT IGNORE INTO Temp_CDM_ENGAGEMENT ( 
    Engagement_Key,
    Engagement_Id,
    Customer_Key,
    Customer_Id,
    Store_Key,
    Store_Id,
    Product_Key,
    Product_Id,
    Engagement_Date,
    Engagement_Type,
    Engagement_Type_LOV_Id,
    Engagement_Type_Key,
    Engagement_Type_Key_LOV_Id,
    CUSTOM_TAG_ATTR_1,
    CUSTOM_TAG_ATTR_1_LOV_Id,
    CUSTOM_TAG_ATTR_2,
    CUSTOM_TAG_ATTR_2_LOV_Id,
    CUSTOM_TAG_ATTR_3,
    CUSTOM_TAG_ATTR_3_LOV_Id,
    CUSTOM_TAG_ATTR_4,
    CUSTOM_TAG_ATTR_4_LOV_Id,
    CUSTOM_TAG_ATTR_5,
    CUSTOM_TAG_ATTR_5_LOV_Id,
    CUSTOM_TAG_ATTR_6,
    CUSTOM_TAG_ATTR_6_LOV_Id,
    CUSTOM_TAG_ATTR_7,
    CUSTOM_TAG_ATTR_7_LOV_Id,
    CUSTOM_TAG_ATTR_8,
    CUSTOM_TAG_ATTR_8_LOV_Id,
    CUSTOM_TAG_VAL_1,
    CUSTOM_TAG_VAL_1_LOV_Id,
    CUSTOM_TAG_VAL_2,
    CUSTOM_TAG_VAL_2_LOV_Id,
    CUSTOM_TAG_VAL_3,
    CUSTOM_TAG_VAL_3_LOV_Id,
    CUSTOM_TAG_VAL_4,
    CUSTOM_TAG_VAL_4_LOV_Id,
    CUSTOM_TAG_KEY_1,
    CUSTOM_TAG_KEY_1_LOV_Id,
    CUSTOM_TAG_KEY_2,
    CUSTOM_TAG_KEY_2_LOV_Id,
    CUSTOM_TAG_KEY_3,
    CUSTOM_TAG_KEY_3_LOV_Id,
    CUSTOM_TAG_KEY_4,
    CUSTOM_TAG_KEY_4_LOV_Id,
    CUSTOM_TAG_DATE_1,
    CUSTOM_TAG_DATE_2,
    CUSTOM_TAG_DATE_3,
    CUSTOM_TAG_DATE_4,
	Engagement_Params)

	SELECT 
	Engagement_Key,
    Engagement_Id,
    Customer_Key,
    Customer_Id,
    Store_Key,
    Store_Id,
    Product_Key,
    Product_Id,
    Engagement_Date,
    Engagement_Type,
    Engagement_Type_LOV_Id,
    Engagement_Type_Key,
    Engagement_Type_Key_LOV_Id,
    CUSTOM_TAG_ATTR_1,
    CUSTOM_TAG_ATTR_1_LOV_Id,
    CUSTOM_TAG_ATTR_2,
    CUSTOM_TAG_ATTR_2_LOV_Id,
    CUSTOM_TAG_ATTR_3,
    CUSTOM_TAG_ATTR_3_LOV_Id,
    CUSTOM_TAG_ATTR_4,
    CUSTOM_TAG_ATTR_4_LOV_Id,
    CUSTOM_TAG_ATTR_5,
    CUSTOM_TAG_ATTR_5_LOV_Id,
    CUSTOM_TAG_ATTR_6,
    CUSTOM_TAG_ATTR_6_LOV_Id,
    CUSTOM_TAG_ATTR_7,
    CUSTOM_TAG_ATTR_7_LOV_Id,
    CUSTOM_TAG_ATTR_8,
    CUSTOM_TAG_ATTR_8_LOV_Id,
    CUSTOM_TAG_VAL_1,
    CUSTOM_TAG_VAL_1_LOV_Id,
    CUSTOM_TAG_VAL_2,
    CUSTOM_TAG_VAL_2_LOV_Id,
    CUSTOM_TAG_VAL_3,
    CUSTOM_TAG_VAL_3_LOV_Id,
    CUSTOM_TAG_VAL_4,
    CUSTOM_TAG_VAL_4_LOV_Id,
    CUSTOM_TAG_KEY_1,
    CUSTOM_TAG_KEY_1_LOV_Id,
    CUSTOM_TAG_KEY_2,
    CUSTOM_TAG_KEY_2_LOV_Id,
    CUSTOM_TAG_KEY_3,
    CUSTOM_TAG_KEY_3_LOV_Id,
    CUSTOM_TAG_KEY_4,
    CUSTOM_TAG_KEY_4_LOV_Id,
    CUSTOM_TAG_DATE_1,
    CUSTOM_TAG_DATE_2,
    CUSTOM_TAG_DATE_3,
    CUSTOM_TAG_DATE_4,
    Engagement_Params
	FROM CDM_Stg_ENGAGEMENT 
      WHERE 
        Rec_Id BETWEEN vStart_Cnt AND vEnd_Cnt AND 
        Rec_Is_Processed = -1; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_1'), 'CDM_ETL_ENGAGEMENT', 1); 
	
  INSERT IGNORE INTO CDM_Engagement_Key_Lookup ( 
  Engagement_Key
  )
       SELECT  
        Engagement_Key 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        Engagement_Key <> '' 
      GROUP BY Engagement_Key; 

 
    INSERT IGNORE INTO CDM_Store_Key_Lookup ( 
      Store_Key 
    ) 
      SELECT  
        Store_Key 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        Store_Key <> '' 
      GROUP BY Store_Key; 
 
    
	
    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'STORE', 
        Store_Key, 
        Store_Key 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        Store_Key <> '' 
      GROUP BY Store_Key; 
 
     
    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_2'), 'CDM_ETL_ENGAGEMENT', 1); 
 
    INSERT IGNORE INTO CDM_Customer_Key_Lookup ( 
      Customer_Key 
    ) 
      SELECT Customer_Key 
      FROM Temp_CDM_ENGAGEMENT
      WHERE 
        Customer_Key <> '' 
      GROUP BY Customer_Key; 
 
    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_3'), 'CDM_ETL_ENGAGEMENT', 1); 
 
    INSERT IGNORE INTO CDM_Product_Key_Lookup ( 
      Product_Key 
    ) 
      SELECT  
        Product_Key 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        Product_Key <> '' 
      GROUP BY Product_Key; 
 
    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'PRODUCT', 
        Product_Key, 
        Product_Key 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        Product_Key <> '' 
      GROUP BY Product_Key; 
       
    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_4'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_TYPE', 
        Engagement_Type, 
        Engagement_Type 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        Engagement_Type <> '' 
      GROUP BY Engagement_Type; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_5'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_TYPE_KEY', 
        Engagement_Type_Key, 
        Engagement_Type_Key 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        Engagement_Type_Key <> '' 
      GROUP BY Engagement_Type_Key; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_6'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_ATTR_1', 
        CUSTOM_TAG_ATTR_1, 
        CUSTOM_TAG_ATTR_1 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_ATTR_1 <> '' 
      GROUP BY CUSTOM_TAG_ATTR_1; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_ATTR_2', 
        CUSTOM_TAG_ATTR_2, 
        CUSTOM_TAG_ATTR_2 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_ATTR_2 <> '' 
      GROUP BY CUSTOM_TAG_ATTR_2; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_ATTR_3', 
        CUSTOM_TAG_ATTR_3, 
        CUSTOM_TAG_ATTR_3 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_ATTR_3 <> '' 
      GROUP BY CUSTOM_TAG_ATTR_3; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_ATTR_4', 
        CUSTOM_TAG_ATTR_4, 
        CUSTOM_TAG_ATTR_4 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_ATTR_4 <> '' 
      GROUP BY CUSTOM_TAG_ATTR_4; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_ATTR_5', 
        CUSTOM_TAG_ATTR_5, 
        CUSTOM_TAG_ATTR_5 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_ATTR_5 <> '' 
      GROUP BY CUSTOM_TAG_ATTR_5; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_ATTR_6', 
        CUSTOM_TAG_ATTR_6, 
        CUSTOM_TAG_ATTR_6 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_ATTR_6 <> '' 
      GROUP BY CUSTOM_TAG_ATTR_6; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_ATTR_7', 
        CUSTOM_TAG_ATTR_7, 
        CUSTOM_TAG_ATTR_7 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_ATTR_7 <> '' 
      GROUP BY CUSTOM_TAG_ATTR_7; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_ATTR_8', 
        CUSTOM_TAG_ATTR_8, 
        CUSTOM_TAG_ATTR_8 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_ATTR_8 <> '' 
      GROUP BY CUSTOM_TAG_ATTR_8; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_ENGAGEMENT', 1); 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_VAL_1', 
        CUSTOM_TAG_VAL_1, 
        CUSTOM_TAG_VAL_1 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_VAL_1 <> '' 
      GROUP BY CUSTOM_TAG_VAL_1; 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_VAL_2', 
        CUSTOM_TAG_VAL_2, 
        CUSTOM_TAG_VAL_2 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_VAL_2 <> '' 
      GROUP BY CUSTOM_TAG_VAL_2; 
	  
    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_VAL_3', 
        CUSTOM_TAG_VAL_3, 
        CUSTOM_TAG_VAL_3 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_VAL_3 <> '' 
      GROUP BY CUSTOM_TAG_VAL_3; 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_VAL_4', 
        CUSTOM_TAG_VAL_4, 
        CUSTOM_TAG_VAL_4 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_VAL_4 <> '' 
      GROUP BY CUSTOM_TAG_VAL_4; 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_KEY_1', 
        CUSTOM_TAG_KEY_1, 
        CUSTOM_TAG_KEY_1 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_KEY_1 <> '' 
      GROUP BY CUSTOM_TAG_KEY_1; 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_KEY_2', 
        CUSTOM_TAG_KEY_2, 
        CUSTOM_TAG_KEY_2 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_KEY_2 <> '' 
      GROUP BY CUSTOM_TAG_KEY_2; 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_KEY_3', 
        CUSTOM_TAG_KEY_3, 
        CUSTOM_TAG_KEY_3 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_KEY_3 <> '' 
      GROUP BY CUSTOM_TAG_KEY_3; 

    INSERT IGNORE INTO CDM_LOV_Master 
    ( 
      LOV_Attribute, 
      LOV_Key, 
      LOV_Value 
    ) 
      SELECT  
        'ENGAGEMENT_CUSTOM_TAG_KEY_4', 
        CUSTOM_TAG_KEY_4, 
        CUSTOM_TAG_KEY_4 
      FROM Temp_CDM_ENGAGEMENT 
      WHERE 
        CUSTOM_TAG_KEY_4 <> '' 
      GROUP BY CUSTOM_TAG_KEY_4; 

    CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', CONCAT(vStart_Cnt,'_7'), 'CDM_ETL_ENGAGEMENT', 1); 


    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET Customer_Id = ( 
        SELECT Customer_Id 
        FROM CDM_Customer_Key_Lookup 
        WHERE CDM_Customer_Key_Lookup.Customer_Key = Temp_CDM_ENGAGEMENT.Customer_Key 
      ) 
    WHERE 
            Customer_Key  <> ''; 


    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET Engagement_Id = ( 
        SELECT Engagement_Id 
        FROM CDM_Engagement_Key_Lookup 
        WHERE CDM_Engagement_Key_Lookup.Engagement_Key = Temp_CDM_ENGAGEMENT.Engagement_Key 
      ) 
    WHERE 
            Engagement_Key  <> ''; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET Store_Id = ( 
        SELECT Store_Id 
        FROM CDM_Store_Key_Lookup 
        WHERE CDM_Store_Key_Lookup.Store_Key = Temp_CDM_ENGAGEMENT.Store_Key 
      ) 
    WHERE 
            Store_Key  <> ''; 


    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET Product_Id = ( 
        SELECT Product_Id 
        FROM CDM_Product_Key_Lookup 
        WHERE CDM_Product_Key_Lookup.Product_Key = Temp_CDM_ENGAGEMENT.Product_Key 
      ) 
    WHERE 
            Product_Key  <> ''; 
            
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET Engagement_Type_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.Engagement_Type 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_TYPE'
      ) 
    WHERE 
            Engagement_Type  <> '' ; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET Engagement_Type_Key_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.Engagement_Type_Key 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_TYPE_KEY'
      ) 
    WHERE 
            Engagement_Type_Key  <> '' ; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_1_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_1 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_ATTR_1'
      ) 
    WHERE 
            CUSTOM_TAG_ATTR_1  <> '' ; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_2_LOV_Id = ( 
        SELECT CUSTOM_TAG_ATTR_2_LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_2 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_ATTR_2'
      ) 
    WHERE 
            CUSTOM_TAG_ATTR_2  <> '' ; 
            
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_3_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_3 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_ATTR_3'
      ) 
    WHERE 
            CUSTOM_TAG_ATTR_3  <> '' ; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_4_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_4 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_ATTR_4'
      ) 
    WHERE 
            CUSTOM_TAG_ATTR_4  <> '' ;

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_5_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_5 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_ATTR_5'
      ) 
    WHERE 
            CUSTOM_TAG_ATTR_5  <> '' ; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_6_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_6 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_ATTR_6'
      ) 
    WHERE 
            CUSTOM_TAG_ATTR_6  <> '' ; 
            
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_7_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_7 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_ATTR_7'
      ) 
    WHERE 
            CUSTOM_TAG_ATTR_7  <> '' ; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_8_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_8 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_ATTR_8'
      ) 
    WHERE 
            CUSTOM_TAG_ATTR_8  <> '' ; 
            
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_VAL_1_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_VAL_1 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_VAL_1'
      ) 
    WHERE 
            CUSTOM_TAG_VAL_1  <> '' ; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_VAL_2_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_VAL_2 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_VAL_2'
      ) 
    WHERE 
            CUSTOM_TAG_VAL_2  <> '' ; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_VAL_3_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_VAL_3 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_VAL_3'
      ) 
    WHERE 
            CUSTOM_TAG_VAL_3  <> '' ; 
            
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_VAL_4_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_VAL_4 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_VAL_4'
      ) 
    WHERE 
            CUSTOM_TAG_VAL_4  <> '' ; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_KEY_1_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_KEY_1 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_KEY_1'
      ) 
    WHERE 
            CUSTOM_TAG_KEY_1  <> '' ; 
    
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_KEY_2_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_KEY_2 
        AND  CDM_LOV_Master.LOV_Attribute='CUSTOM_TAG_KEY_2'
      ) 
    WHERE 
            CUSTOM_TAG_KEY_2  <> '' ; 

    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_KEY_3_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_KEY_3 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_KEY_3'
      ) 
    WHERE 
            CUSTOM_TAG_KEY_3  <> '' ; 


    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_KEY_4_LOV_Id = ( 
        SELECT LOV_Id 
        FROM CDM_LOV_Master 
        WHERE CDM_LOV_Master.LOV_Key = Temp_CDM_ENGAGEMENT.CUSTOM_TAG_KEY_4 
        AND  CDM_LOV_Master.LOV_Attribute='ENGAGEMENT_CUSTOM_TAG_KEY_4'
      ) 
    WHERE 
            CUSTOM_TAG_KEY_4  <> '' ; 



    INSERT IGNORE INTO CDM_Store_Master ( 
      Store_Id, 
      LOV_Id 
    ) 
      SELECT  
        C.Store_Id, 
        B.LOV_Id 
      FROM CDM_LOV_Master AS B, Temp_CDM_ENGAGEMENT AS C 
      WHERE  
        B.LOV_Attribute = 'STORE'  
        AND B.LOV_Key = C.Store_Key 
      GROUP BY C.Store_Id, B.LOV_Id; 
      
    INSERT IGNORE INTO  CDM_Customer_Master ( 
      Customer_Id, 
      Is_New_Cust_Flag 
    ) 
      SELECT  
        Customer_Id, 
        1 
      FROM Temp_CDM_ENGAGEMENT                         
      WHERE  
        Customer_Key <> '' 
      GROUP BY Customer_Id;
  
    INSERT IGNORE INTO CDM_Customer_PII_Master ( 
      Customer_Id 
    )     
      SELECT  
        Customer_Id 
      FROM Temp_CDM_ENGAGEMENT
      WHERE  
        Customer_Key <> '' 
      GROUP BY Customer_Id; 
  
  
    INSERT IGNORE INTO CDM_Product_Master_Original ( 
      Product_Id, 
      Product_LOV_Id 
    ) 
      SELECT  
        C.Product_Id, 
        B.LOV_Id 
      FROM CDM_LOV_Master AS B, Temp_CDM_ENGAGEMENT AS C 
      WHERE  
        B.LOV_Attribute = 'PRODUCT' AND 
        B.LOV_Key = C.Product_Key 
      GROUP BY C.Product_Id, B.LOV_Id; 



    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_1_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_ATTR_1'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_1=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_ATTR_1  <> '' ; 
      
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_2_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_ATTR_2'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_2=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_ATTR_2  <> '' ; 
      
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_3_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_ATTR_3'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_3=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_ATTR_3  <> '' ; 
      
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_4_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_ATTR_4'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_4=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_ATTR_4  <> ''; 
      
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_5_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_ATTR_5'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_5=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_ATTR_5  <> '' ; 
      
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_6_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_ATTR_6'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_6=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_ATTR_6  <> '' ; 
	
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_7_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_ATTR_7'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_7=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_ATTR_7  <> '' ; 
      
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_ATTR_8_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_ATTR_8'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_ATTR_8=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_ATTR_8  <> '' ; 
 
 
 
     UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_VAL_1_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_VAL_1'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_VAL_1=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_VAL_1  <> '' ; 
      
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_VAL_2_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_VAL_2'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_VAL_2=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_VAL_2  <> '' ; 
      
      
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_VAL_3_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_VAL_3'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_VAL_3=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_VAL_3  <> '' ; 
      
    UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_VAL_4_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_VAL_4'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_VAL_4=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_VAL_4  <> '' ; 
      
 
 
      UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_KEY_1_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_KEY_1'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_KEY_1=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_KEY_1  <> '' ; 

     UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_KEY_2_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_KEY_2'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_KEY_2=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_KEY_2  <> '' ; 

     UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_KEY_3_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_KEY_3'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_KEY_3=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_KEY_3  <> '' ; 

     UPDATE IGNORE Temp_CDM_ENGAGEMENT 
    SET CUSTOM_TAG_KEY_4_LOV_Id = ( 
    SELECT 
    LOV_Id
    FROM CDM_LOV_Master 
    where 
    LOV_Attribute = 'ENGAGEMENT_CUSTOM_TAG_KEY_4'
    AND Temp_CDM_ENGAGEMENT.CUSTOM_TAG_KEY_4=CDM_LOV_Master.LOV_Value
)
    WHERE  
	  CUSTOM_TAG_KEY_4  <> '' ; 

	  
    IF vTreat_As_Incr = 1 THEN 
       
       
      UPDATE CDM_ENGAGEMENT AS CE, Temp_CDM_ENGAGEMENT AS TCE 
      SET 
        CE.Customer_Id = TCE.Customer_Id,
        CE.Store_Id = TCE.Store_Id,
        CE.Product_Id = TCE.Product_Id,
        CE.Engagement_Type_LOV_Id = TCE.Engagement_Type_LOV_Id,
        CE.Engagement_Type_Key_LOV_Id = TCE.Engagement_Type_Key_LOV_Id,
		CE.Engagement_Date = TCE.Engagement_Date,
        CE.Engagement_Params=TCE.Engagement_Params,
        CE.Engagement_Type=TCE.Engagement_Type,
        CE.CUSTOM_TAG_ATTR_1_LOV_Id = TCE.CUSTOM_TAG_ATTR_1_LOV_Id,
        CE.CUSTOM_TAG_ATTR_2_LOV_Id = TCE.CUSTOM_TAG_ATTR_2_LOV_Id,
        CE.CUSTOM_TAG_ATTR_3_LOV_Id = TCE.CUSTOM_TAG_ATTR_3_LOV_Id,
        CE.CUSTOM_TAG_ATTR_4_LOV_Id = TCE.CUSTOM_TAG_ATTR_4_LOV_Id,
        CE.CUSTOM_TAG_ATTR_5_LOV_Id = TCE.CUSTOM_TAG_ATTR_5_LOV_Id,
        CE.CUSTOM_TAG_ATTR_6_LOV_Id = TCE.CUSTOM_TAG_ATTR_6_LOV_Id,
        CE.CUSTOM_TAG_ATTR_7_LOV_Id = TCE.CUSTOM_TAG_ATTR_7_LOV_Id,
        CE.CUSTOM_TAG_ATTR_8_LOV_Id = TCE.CUSTOM_TAG_ATTR_8_LOV_Id,
        CE.CUSTOM_TAG_VAL_1_LOV_Id = TCE.CUSTOM_TAG_VAL_1_LOV_Id,
        CE.CUSTOM_TAG_VAL_2_LOV_Id = TCE.CUSTOM_TAG_VAL_2_LOV_Id,
        CE.CUSTOM_TAG_VAL_3_LOV_Id = TCE.CUSTOM_TAG_VAL_3_LOV_Id,
        CE.CUSTOM_TAG_VAL_4_LOV_Id = TCE.CUSTOM_TAG_VAL_4_LOV_Id,
        CE.CUSTOM_TAG_KEY_1_LOV_Id = TCE.CUSTOM_TAG_KEY_1_LOV_Id,
        CE.CUSTOM_TAG_KEY_2_LOV_Id = TCE.CUSTOM_TAG_KEY_2_LOV_Id,
        CE.CUSTOM_TAG_KEY_3_LOV_Id = TCE.CUSTOM_TAG_KEY_3_LOV_Id,
        CE.CUSTOM_TAG_KEY_4_LOV_Id = TCE.CUSTOM_TAG_KEY_4_LOV_Id ,
		CE.CUSTOM_TAG_DATE_1 = TCE.CUSTOM_TAG_DATE_1,
        CE.CUSTOM_TAG_DATE_2 = TCE.CUSTOM_TAG_DATE_2,
        CE.CUSTOM_TAG_DATE_3 = TCE.CUSTOM_TAG_DATE_3,
        CE.CUSTOM_TAG_DATE_4 = TCE.CUSTOM_TAG_DATE_4
		WHERE  
        TCE.Engagement_Id = CE.Engagement_Id AND 
		TCE.Engagement_Id <>''; 

	END IF;

INSERT IGNORE INTO CDM_ENGAGEMENT ( 	
 Engagement_Id,
 Customer_Id,
 Store_Id,
 Product_Id,
 Engagement_Date,
  Engagement_Type,
 Engagement_Type_LOV_Id,
 Engagement_Type_Key_LOV_Id,
 CUSTOM_TAG_ATTR_1_LOV_Id,
 CUSTOM_TAG_ATTR_2_LOV_Id,
 CUSTOM_TAG_ATTR_3_LOV_Id,
 CUSTOM_TAG_ATTR_4_LOV_Id,
 CUSTOM_TAG_ATTR_5_LOV_Id,
 CUSTOM_TAG_ATTR_6_LOV_Id,
 CUSTOM_TAG_ATTR_7_LOV_Id,
 CUSTOM_TAG_ATTR_8_LOV_Id,
 CUSTOM_TAG_VAL_1_LOV_Id,
 CUSTOM_TAG_VAL_2_LOV_Id,
 CUSTOM_TAG_VAL_3_LOV_Id,
 CUSTOM_TAG_VAL_4_LOV_Id,
 CUSTOM_TAG_KEY_1_LOV_Id,
 CUSTOM_TAG_KEY_2_LOV_Id,
 CUSTOM_TAG_KEY_3_LOV_Id,
 CUSTOM_TAG_KEY_4_LOV_Id,
 CUSTOM_TAG_DATE_1,
 CUSTOM_TAG_DATE_2,
 CUSTOM_TAG_DATE_3,
 CUSTOM_TAG_DATE_4,

 Engagement_Params)
 SELECT 
  Engagement_Id,
 Customer_Id,
 Store_Id,
 Product_Id,
 Engagement_Date,
  Engagement_Type,
 Engagement_Type_LOV_Id,
 Engagement_Type_Key_LOV_Id,
 CUSTOM_TAG_ATTR_1_LOV_Id,
 CUSTOM_TAG_ATTR_2_LOV_Id,
 CUSTOM_TAG_ATTR_3_LOV_Id,
 CUSTOM_TAG_ATTR_4_LOV_Id,
 CUSTOM_TAG_ATTR_5_LOV_Id,
 CUSTOM_TAG_ATTR_6_LOV_Id,
 CUSTOM_TAG_ATTR_7_LOV_Id,
 CUSTOM_TAG_ATTR_8_LOV_Id,
 CUSTOM_TAG_VAL_1_LOV_Id,
 CUSTOM_TAG_VAL_2_LOV_Id,
 CUSTOM_TAG_VAL_3_LOV_Id,
 CUSTOM_TAG_VAL_4_LOV_Id,
 CUSTOM_TAG_KEY_1_LOV_Id,
 CUSTOM_TAG_KEY_2_LOV_Id,
 CUSTOM_TAG_KEY_3_LOV_Id,
 CUSTOM_TAG_KEY_4_LOV_Id,
 CUSTOM_TAG_DATE_1,
 CUSTOM_TAG_DATE_2,
 CUSTOM_TAG_DATE_3,
 CUSTOM_TAG_DATE_4,
 Engagement_Params
      FROM Temp_CDM_ENGAGEMENT 
      WHERE  
        Customer_Id > 0;
		
      SET vStart_Cnt = vStart_Cnt + vBatchSize; 
        SET vEnd_Cnt = vEnd_Cnt + vBatchSize; 
         
        IF vStart_Cnt  >= @Rec_Cnt THEN 
            LEAVE PROCESS_ENG_HIST; 
        END IF; 
         
  END LOOP PROCESS_ENG_HIST; 
 
  DROP TABLE Temp_CDM_ENGAGEMENT; 
 
  CALL CDM_Update_Process_Log('CDM_ETL_ENGAGEMENT', 5, 'CDM_ETL_ENGAGEMENT', 1); 
END$$
DELIMITER ;

