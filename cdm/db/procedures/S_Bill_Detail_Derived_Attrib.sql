DELIMITER $$
CREATE or replace PROCEDURE  S_Bill_Detail_Derived_Attrib (IN vBatchSize BIGINT,IN vTreat_As_Incr TINYINT)
BEGIN
DECLARE vStart_Cnt BIGINT;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
DECLARE vRec_Cnt BIGINT;
set sql_safe_updates=0;
IF vTreat_As_Incr<>1
THEN 
SELECT Customer_id INTO vStart_Cnt FROM CDM_Customer_Master ORDER BY Customer_id ASC LIMIT 1;
SELECT Customer_id INTO vRec_Cnt FROM CDM_Customer_Master ORDER BY Customer_id DESC LIMIT 1;
delete from log_solus where module='S_Bill_Detail_Derived_Attrib';
PROCESS_1: LOOP
SET vEnd_Cnt=vEnd_Cnt+vBatchSize;
insert into log_solus(module,rec_start,rec_end) values ('S_Bill_Detail_Derived_Attrib',vStart_Cnt,vEnd_Cnt);
update CDM_Bill_Details_Derived_Attr cdba,CDM_Bill_Details cdb,CDM_Product_Master CPM
set cdba.Bill_Date=cdb.Bill_Date,
cdba.Sale_Qty=cdb.Sale_Qty,
cdba.Sale_Net_Val=cdb.Sale_Net_Val,
cdba.Product_LOV_Id=CPM.Product_LOV_Id,
cdba.Brand_LOV_Id=CPM.Brand_LOV_Id,
cdba.BU_LOV_Id=CPM.BU_LOV_Id,
cdba.Cat_LOV_Id=CPM.Cat_LOV_Id,
cdba.Dep_LOV_Id=CPM.Dep_LOV_Id,
cdba.Div_LOV_Id=CPM.Div_LOV_Id,
cdba.Season_LOV_Id=CPM.Season_LOV_Id,
cdba.Section_LOV_Id=CPM.Section_LOV_Id
where
cdba.Customer_ID between vStart_Cnt and vEnd_Cnt
and cdb.Customer_ID between vStart_Cnt and vEnd_Cnt
and cdba.Customer_ID=cdb.Customer_ID
and cdba.Bill_Details_Id=cdb.Bill_Details_Id
and cdb.Product_Id=CPM.Product_Id;
SET vStart_Cnt = vEnd_Cnt+1;
IF vStart_Cnt  >= vRec_Cnt THEN
     LEAVE PROCESS_1;
END IF;
END LOOP PROCESS_1;
END IF;

IF vTreat_As_Incr=1 
THEN
SELECT Customer_id INTO vStart_Cnt FROM CDM_Recompute_Customer_Var_List ORDER BY Customer_id ASC LIMIT 1;
SELECT Customer_id INTO vRec_Cnt FROM CDM_Recompute_Customer_Var_List ORDER BY Customer_id DESC LIMIT 1;
delete from log_solus where module='S_Bill_Detail_Derived_Attrib';
PROCESS_2: LOOP
SET vEnd_Cnt=vEnd_Cnt+vBatchSize;
insert into log_solus(module,rec_start,rec_end) values ('S_Bill_Detail_Derived_Attrib',vStart_Cnt,vEnd_Cnt);
update CDM_Bill_Details_Derived_Attr cdba,CDM_Bill_Details cdb,CDM_Recompute_Customer_Var_List CRC,CDM_Product_Master CPM
set cdba.Bill_Date=cdb.Bill_Date,
cdba.Sale_Qty=cdb.Sale_Qty,
cdba.Sale_Net_Val=cdb.Sale_Net_Val,
cdba.Product_LOV_Id=CPM.Product_LOV_Id,
cdba.Brand_LOV_Id=CPM.Brand_LOV_Id,
cdba.BU_LOV_Id=CPM.BU_LOV_Id,
cdba.Cat_LOV_Id=CPM.Cat_LOV_Id,
cdba.Dep_LOV_Id=CPM.Dep_LOV_Id,
cdba.Div_LOV_Id=CPM.Div_LOV_Id,
cdba.Season_LOV_Id=CPM.Season_LOV_Id,
cdba.Section_LOV_Id=CPM.Section_LOV_Id
where cdba.Customer_ID between vStart_Cnt and vEnd_Cnt
and cdb.Customer_ID between vStart_Cnt and vEnd_Cnt
and CRC.Customer_ID between vStart_Cnt and vEnd_Cnt
and CRC.Customer_ID=cdb.Customer_ID
and cdba.Customer_ID=cdb.Customer_ID
and cdb.Product_Id=CPM.Product_Id
and cdba.Bill_Details_Id=cdb.Bill_Details_Id
and datediff(Current_Date,cdb.Bill_Date)<=3;
SET vStart_Cnt = vEnd_Cnt+1;
IF vStart_Cnt  >= vRec_Cnt THEN
     LEAVE PROCESS_2;
END IF;
END LOOP PROCESS_2;
END IF;
END$$
DELIMITER ;
