DELIMITER $$
CREATE OR REPLACE PROCEDURE  CDM_DQC_Product_Master (OUT msg text)
dqc_products:BEGIN
declare msg varchar(1024);
select count(1) into @n1 from CDM_Customer_Master ;
select count(1) into @nproducts from CDM_Product_Master_Original ;
select count(distinct Product_Id) into @nproductsbills from CDM_Bill_Details ;

if @nproducts < @nproductsbills  then
  set msg=' Products in Bills is greater than Products ; Please Relaod the Product Master wwith Complete Bills  ';
  leave dqc_products;
end if ;

END$$
DELIMITER ;
