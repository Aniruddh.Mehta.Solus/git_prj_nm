alter table RankedPickList add column Algo char(40); 

alter table Event_Execution_History
add column Recommendation_Algo_1 char(40),
add column Recommendation_Algo_2 char(40),
add column Recommendation_Algo_3 char(40),
add column Recommendation_Algo_4 char(40),
add column Recommendation_Algo_5 char(40),
add column Recommendation_Algo_6 char(40);

alter table Event_Execution
add column Recommendation_Algo_1 char(40),
add column Recommendation_Algo_2 char(40),
add column Recommendation_Algo_3 char(40),
add column Recommendation_Algo_4 char(40),
add column Recommendation_Algo_5 char(40),
add column Recommendation_Algo_6 char(40);

alter table Event_Execution_Stg
add column Recommendation_Algo_1 char(40),
add column Recommendation_Algo_2 char(40),
add column Recommendation_Algo_3 char(40),
add column Recommendation_Algo_4 char(40),
add column Recommendation_Algo_5 char(40),
add column Recommendation_Algo_6 char(40);

alter table Event_Execution_Voucher_Pending
add column Recommendation_Algo_1 char(40),
add column Recommendation_Algo_2 char(40),
add column Recommendation_Algo_3 char(40),
add column Recommendation_Algo_4 char(40),
add column Recommendation_Algo_5 char(40),
add column Recommendation_Algo_6 char(40);

alter table Event_Execution_Rejected_History
add column Recommendation_Algo_1 char(40),
add column Recommendation_Algo_2 char(40),
add column Recommendation_Algo_3 char(40),
add column Recommendation_Algo_4 char(40),
add column Recommendation_Algo_5 char(40),
add column Recommendation_Algo_6 char(40);

alter table Event_Execution_Rejected
add column Recommendation_Algo_1 char(40),
add column Recommendation_Algo_2 char(40),
add column Recommendation_Algo_3 char(40),
add column Recommendation_Algo_4 char(40),
add column Recommendation_Algo_5 char(40),
add column Recommendation_Algo_6 char(40);


alter table Event_Execution_Voucher_Pending_History
add column Recommendation_Algo_1 char(40),
add column Recommendation_Algo_2 char(40),
add column Recommendation_Algo_3 char(40),
add column Recommendation_Algo_4 char(40),
add column Recommendation_Algo_5 char(40),
add column Recommendation_Algo_6 char(40);
