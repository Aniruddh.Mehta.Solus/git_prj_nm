alter table CDM_Customer_Time_Dependent_Var
add column Num_Of_Cart_Items bigint(10) default 0;

create index if not exists Num_Of_Cart_Items on CDM_Customer_Time_Dependent_Var(Num_Of_Cart_Items);

create index if not exists Engagement_Date on CDM_ENGAGEMENT(Engagement_Date);
create index if not exists Engagement_Type_LOV_Id on CDM_ENGAGEMENT(Engagement_Type_LOV_Id);
create index if not exists Engagement_Type_Key_LOV_Id on CDM_ENGAGEMENT(Engagement_Type_Key_LOV_Id);
create index if not exists CUSTOM_TAG_ATTR_1_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_ATTR_1_LOV_Id);
create index if not exists CUSTOM_TAG_ATTR_2_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_ATTR_2_LOV_Id);
create index if not exists CUSTOM_TAG_ATTR_3_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_ATTR_3_LOV_Id);
create index if not exists CUSTOM_TAG_ATTR_4_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_ATTR_4_LOV_Id);
create index if not exists CUSTOM_TAG_ATTR_5_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_ATTR_5_LOV_Id);
create index if not exists CUSTOM_TAG_ATTR_6_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_ATTR_6_LOV_Id);
create index if not exists CUSTOM_TAG_ATTR_7_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_ATTR_7_LOV_Id);
create index if not exists CUSTOM_TAG_ATTR_8_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_ATTR_8_LOV_Id);
create index if not exists CUSTOM_TAG_VAL_1_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_VAL_1_LOV_Id);
create index if not exists CUSTOM_TAG_VAL_2_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_VAL_2_LOV_Id);
create index if not exists CUSTOM_TAG_VAL_3_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_VAL_3_LOV_Id);
create index if not exists CUSTOM_TAG_VAL_4_LOV_Id on CDM_ENGAGEMENT(CUSTOM_TAG_VAL_4_LOV_Id);
create index if not exists CUSTOM_TAG_DATE_1 on CDM_ENGAGEMENT(CUSTOM_TAG_DATE_1);
create index if not exists CUSTOM_TAG_DATE_2 on CDM_ENGAGEMENT(CUSTOM_TAG_DATE_2);
create index if not exists CUSTOM_TAG_DATE_3 on CDM_ENGAGEMENT(CUSTOM_TAG_DATE_3);
create index if not exists CUSTOM_TAG_DATE_4 on CDM_ENGAGEMENT(CUSTOM_TAG_DATE_4);
