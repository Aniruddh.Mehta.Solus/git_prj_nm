alter table CDM_Bill_Details_Derived_Attr add column Bill_Date date default '0000-00-00';
alter table CDM_Bill_Details_Derived_Attr add column Sale_Qty decimal(15,2) default 0.00;
alter table CDM_Bill_Details_Derived_Attr add column Sale_Net_Val decimal(15,2) default 0.00;
alter table CDM_Bill_Details_Derived_Attr add index Bill_Date(Bill_Date);
alter table CDM_Bill_Details_Derived_Attr add index Sale_Qty(Sale_Qty);
alter table CDM_Bill_Details_Derived_Attr add index Sale_Net_Val(Sale_Net_Val);