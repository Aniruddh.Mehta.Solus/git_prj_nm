alter table CDM_ENGAGEMENT
add column if not exists Engagement_Type varchar(512) DEFAULT NULL;

alter table CDM_ENGAGEMENT
add column if not exists Engagement_Params text DEFAULT NULL;

alter table CDM_ENGAGEMENT
add column if not exists Engagement_Time time DEFAULT NULL;

alter table CDM_Stg_ENGAGEMENT
add column if not exists Engagement_Type varchar(512) DEFAULT NULL;

alter table CDM_Stg_ENGAGEMENT
add column if not exists Engagement_Params text DEFAULT NULL;

alter table CDM_Stg_ENGAGEMENT
add column if not exists Engagement_Time time DEFAULT NULL;

alter table CDM_ENGAGEMENT add index Engagement_Type(Engagement_Type);
alter table CDM_ENGAGEMENT add index Engagement_Time(Engagement_Time);
