DELIMITER mysolus$$
		CREATE  OR REPLACE  FUNCTION  F_Remove_All_Spaces (str VARCHAR(255)) RETURNS varchar(255) CHARSET latin1 BEGIN     while instr(str, '  ') > 0 do         set str := replace(str, '  ', ' ');     end while;     return trim(str); end			
mysolus$$
