DELIMITER mysolus$$
		CREATE  OR REPLACE  FUNCTION  CDM_Get_Is_Valid_Email (vEmail VARCHAR(128)) RETURNS tinyint(4) BEGIN     DECLARE vIs_Valid_Email_Ret TINYINT DEFAULT -1;      DECLARE CONTINUE HANDLER FOR SQLEXCEPTION RETURN -1;        IF vEmail REGEXP '(.*)@(.*)\\.(.*)' THEN      SET vIs_Valid_Email_Ret  = 1;   END IF;    RETURN vIs_Valid_Email_Ret;     END			
mysolus$$
