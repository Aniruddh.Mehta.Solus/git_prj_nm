DELIMITER mysolus$$
		CREATE  OR REPLACE  FUNCTION  CDM_Get_Hash_Id (vInStr VARCHAR(256)) RETURNS char(64) CHARSET latin1 BEGIN     DECLARE vHash_Id_Ret CHAR(64);          SET vHash_Id_Ret = SHA2(vInStr,256);        RETURN vHash_Id_Ret;      END			
mysolus$$
