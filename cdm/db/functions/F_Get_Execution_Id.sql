DELIMITER mysolus$$
		CREATE  OR REPLACE  FUNCTION  F_Get_Execution_Id () RETURNS int(11) BEGIN      set @count:=(select count(*) from ETL_Execution_Details);     IF(@count=0)   then      set @Execution_ID:=1;   else      set @Execution_ID:=(select max(Execution_ID) from ETL_Execution_Details);   END IF;    RETURN @Execution_ID; END			
mysolus$$
