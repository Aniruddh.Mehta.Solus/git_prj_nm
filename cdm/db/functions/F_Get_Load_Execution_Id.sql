DELIMITER mysolus$$
		CREATE  OR REPLACE  FUNCTION  F_Get_Load_Execution_Id () RETURNS int(11) BEGIN      set @count:=(select count(*) from ETL_Execution_Master);     IF(@count=0)   then      set @Load_Execution_ID:=1;   else      set @Load_Execution_ID:=(select max(Load_Execution_ID) from ETL_Execution_Master);   END IF;    RETURN @Load_Execution_ID; END			
mysolus$$
