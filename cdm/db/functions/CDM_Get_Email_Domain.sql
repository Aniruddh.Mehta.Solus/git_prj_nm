DELIMITER mysolus$$
		CREATE  OR REPLACE  FUNCTION  CDM_Get_Email_Domain (vEmail VARCHAR(128)) RETURNS varchar(64) CHARSET latin1 BEGIN     DECLARE vEmail_Domain_Ret_Str VARCHAR(64) DEFAULT '';      DECLARE CONTINUE HANDLER FOR SQLEXCEPTION RETURN '';            IF vEmail  IS NOT NULL THEN     SET vEmail_Domain_Ret_Str = TRIM(vEmail);       SET vEmail_Domain_Ret_Str = SUBSTR(vEmail_Domain_Ret_Str FROM INSTR(vEmail_Domain_Ret_Str, '@'));     ELSE     SET vEmail_Domain_Ret_Str = '';     END IF;    RETURN vEmail_Domain_Ret_Str;      END			
mysolus$$
