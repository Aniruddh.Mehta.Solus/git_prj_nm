DELIMITER mysolus$$
		CREATE  OR REPLACE  FUNCTION  F_Is_Active_Simulator_Segment_Name (segment_name_check varchar(225)) RETURNS char(1) CHARSET utf8  begin  Declare  status  char(1);  select Is_Enabled into  status  from Simulator_Segment_Master where  Name  = segment_name_check;  return  status ;   end			
mysolus$$
