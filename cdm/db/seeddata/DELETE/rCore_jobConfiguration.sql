DELIMITER mysolus$$ 
INSERT INTO `rCore_jobConfiguration` VALUES (1,'FIRST_TIME_SETUP','Sequence Of Jobs to run for the First Time'),(2,'SCORE_CUSTOMERS','Scores Customers only.'),(3,'CALIBRATE','Reruns the whole rCore to recalibrate'),(4,'GET_METRICS','Displays the Measurement Metrics of Recommendation Systems'),(9,'DB_SCHEMA_SETUP','Set Up user credentials in keyring. First Time Set up or Reset');
mysolus$$
