. $HOME/.solusconfig
## Insert Seed Values
cd $HOME/mysolus/cdm/
mysql --host=$HOST --user=${USER_NAME} --password=${PASSWORD}  --database=$DB_NAME   --verbose << EOF
source ./Insert_Seed_Values.sql
EOF

