DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Engagement_Key_Lookup ; 
        CREATE TABLE  CDM_Engagement_Key_Lookup  (
   Engagement_Id  bigint(20) NOT NULL AUTO_INCREMENT,
   Engagement_Key  varchar(128) NOT NULL,
   Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),
   Modified_Date  datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY ( Engagement_Id ),
  UNIQUE KEY  Engagement_Key  ( Engagement_Key ),
  KEY  Modified_Date  ( Modified_Date )
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
mysolus$$
