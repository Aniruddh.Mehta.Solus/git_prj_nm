CREATE or REPLACE TABLE  CDM_Delivery_Report  (
   Customer_Id  bigint(20) NOT NULL,
   Event_Execution_ID  char(50) NOT NULL,
   Event_Execution_Date_ID  bigint(20) NOT NULL,
   Event_ID  bigint(20) NOT NULL,
   Communication_Channel  varchar(50) DEFAULT NULL,
   Delivery_Date  date DEFAULT NULL,
   Delivery_Status  char(1) DEFAULT NULL,
   Open_Date  date DEFAULT NULL,
   Open_Status  char(1) DEFAULT NULL,
   CTA_Date  date DEFAULT NULL,
   CTA_Status  char(1) DEFAULT NULL,
   Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY ( Customer_Id , Event_Execution_ID ),
  KEY  Event_Execution_Date_ID  ( Event_Execution_Date_ID ),
  KEY  Event_ID  ( Event_ID ),
  KEY  Delivery_Date  ( Delivery_Date ),
  KEY  Delivery_Status  ( Delivery_Status ),
  KEY  Open_Date  ( Open_Date ),
  KEY  Open_Status  ( Open_Status ),
  KEY  Created_Date  ( Created_Date )
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE ( Customer_Id )
(PARTITION  p1  VALUES LESS THAN (1000000) ENGINE = InnoDB,
 PARTITION  p2  VALUES LESS THAN (2000000) ENGINE = InnoDB,
 PARTITION  p3  VALUES LESS THAN (3000000) ENGINE = InnoDB,
 PARTITION  p4  VALUES LESS THAN (4000000) ENGINE = InnoDB,
 PARTITION  p5  VALUES LESS THAN (5000000) ENGINE = InnoDB,
 PARTITION  p6  VALUES LESS THAN (6000000) ENGINE = InnoDB,
 PARTITION  p7  VALUES LESS THAN (7000000) ENGINE = InnoDB,
 PARTITION  p8  VALUES LESS THAN (8000000) ENGINE = InnoDB,
 PARTITION  p9  VALUES LESS THAN (9000000) ENGINE = InnoDB,
 PARTITION  p10  VALUES LESS THAN (10000000) ENGINE = InnoDB,
 PARTITION  p11  VALUES LESS THAN (11000000) ENGINE = InnoDB,
 PARTITION  p12  VALUES LESS THAN (12000000) ENGINE = InnoDB,
 PARTITION  p13  VALUES LESS THAN (13000000) ENGINE = InnoDB,
 PARTITION  p14  VALUES LESS THAN (14000000) ENGINE = InnoDB,
 PARTITION  p15  VALUES LESS THAN (15000000) ENGINE = InnoDB,
 PARTITION  p16  VALUES LESS THAN (16000000) ENGINE = InnoDB,
 PARTITION  p17  VALUES LESS THAN (17000000) ENGINE = InnoDB,
 PARTITION  p18  VALUES LESS THAN (18000000) ENGINE = InnoDB,
 PARTITION  p19  VALUES LESS THAN (19000000) ENGINE = InnoDB,
 PARTITION  p20  VALUES LESS THAN (20000000) ENGINE = InnoDB,
 PARTITION  p21  VALUES LESS THAN (21000000) ENGINE = InnoDB,
 PARTITION  p22  VALUES LESS THAN (22000000) ENGINE = InnoDB,
 PARTITION  p23  VALUES LESS THAN (23000000) ENGINE = InnoDB,
 PARTITION  p24  VALUES LESS THAN (24000000) ENGINE = InnoDB,
 PARTITION  p25  VALUES LESS THAN (25000000) ENGINE = InnoDB,
 PARTITION  p26  VALUES LESS THAN (26000000) ENGINE = InnoDB,
 PARTITION  p27  VALUES LESS THAN (27000000) ENGINE = InnoDB,
 PARTITION  p28  VALUES LESS THAN (28000000) ENGINE = InnoDB,
 PARTITION  p29  VALUES LESS THAN (29000000) ENGINE = InnoDB,
 PARTITION  p30  VALUES LESS THAN (30000000) ENGINE = InnoDB,
 PARTITION  p501  VALUES LESS THAN MAXVALUE ENGINE = InnoDB);

