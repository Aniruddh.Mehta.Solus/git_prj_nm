DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Feedback_Key_Lookup ; CREATE TABLE    CDM_Feedback_Key_Lookup  (    Fdbk_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Fdbk_Key  varchar(128) NOT NULL,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Fdbk_Id ),   UNIQUE KEY  Fdbk_Key  ( Fdbk_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB DEFAULT CHARSET=latin1
mysolus$$
