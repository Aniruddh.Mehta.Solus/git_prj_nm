drop table if exists CDM_DQC_ERRORS;
Create table CDM_DQC_ERRORS
( id bigint(20) auto_increment,
  entity varchar(120) not null, 
  error_code varchar(60),
  error_msg text,
  reject_record_key varchar (256),
  reject_record_details text,
  created_date timestamp default current_timestamp(),
  primary key (id),
  key entity (entity),
  key reject_record_key(reject_record_key),
  key error_code (error_code)
);

