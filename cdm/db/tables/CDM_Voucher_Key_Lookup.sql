DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Voucher_Key_Lookup ; CREATE TABLE    CDM_Voucher_Key_Lookup  (    Voucher_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Voucher_Key  varchar(128) NOT NULL,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Voucher_Id ),   UNIQUE KEY  Voucher_Key  ( Voucher_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=2490863 DEFAULT CHARSET=latin1
mysolus$$
