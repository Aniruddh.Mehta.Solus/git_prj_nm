DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Points_Expiry_Key_Lookup ; CREATE TABLE    CDM_Points_Expiry_Key_Lookup  (    Points_Expiry_Rec_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Points_Expiry_Rec_Key  varchar(128) NOT NULL,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Points_Expiry_Rec_Id ),   UNIQUE KEY  Points_Expiry_Rec_Key  ( Points_Expiry_Rec_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=291704 DEFAULT CHARSET=latin1
mysolus$$
