CREATE TABLE  CDM_Customer_Portfolio_Key_Lookup  (
   Customer_Portfolio_Id  bigint(20) NOT NULL AUTO_INCREMENT,
   Customer_Portfolio_Key  varchar(128) NOT NULL,
   Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),
   Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY ( Customer_Portfolio_Id ),
  UNIQUE KEY  Customer_Portfolio_Key  ( Customer_Portfolio_Key ),
  KEY  Modified_Date  ( Modified_Date )
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
