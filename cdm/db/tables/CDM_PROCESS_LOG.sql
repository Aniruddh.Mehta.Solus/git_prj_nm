DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_PROCESS_LOG ; CREATE TABLE    CDM_PROCESS_LOG  (    CDM_PL_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Proc_Name  varchar(128) DEFAULT '',    Step_Id  varchar(128) DEFAULT '',    Step_Desc  varchar(128) DEFAULT '',    Step_Status  tinyint(4) DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),   PRIMARY KEY ( CDM_PL_Id ),   KEY  Created_Date  ( Created_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=378 DEFAULT CHARSET=latin1
mysolus$$
