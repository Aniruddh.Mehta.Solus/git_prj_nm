DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Product_Key_Lookup ; CREATE TABLE    CDM_Product_Key_Lookup  (    Product_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Product_Key  varchar(128) NOT NULL DEFAULT '',    LOV_Id  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Product_Id ),   UNIQUE KEY  Product_Key  ( Product_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=3316548 DEFAULT CHARSET=latin1
mysolus$$
