CREATE TABLE  CDM_Sip_Key_Lookup  (
   Sip_Id  bigint(20) NOT NULL AUTO_INCREMENT,
   Sip_Key  varchar(256) NOT NULL,
   Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),
   Modified_Date  datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY ( Sip_Id ),
  UNIQUE KEY  Sip_Key  ( Sip_Key ),
  KEY  Modified_Date  ( Modified_Date )
) ENGINE=InnoDB AUTO_INCREMENT=1028 DEFAULT CHARSET=latin1;

