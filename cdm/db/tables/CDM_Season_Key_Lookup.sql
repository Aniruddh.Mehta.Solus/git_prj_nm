DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Season_Key_Lookup ; CREATE TABLE    CDM_Season_Key_Lookup  (    Season_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Season_Key  varchar(128) NOT NULL DEFAULT '',    LOV_Id  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Season_Id ),   UNIQUE KEY  Season_Key  ( Season_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=406 DEFAULT CHARSET=latin1
mysolus$$
