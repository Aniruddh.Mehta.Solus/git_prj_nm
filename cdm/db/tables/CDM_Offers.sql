DELIMITER mysolus$$

DROP TABLE IF EXISTS  CDM_Offers ;
CREATE TABLE  CDM_Offers  (
   Offer_Id  bigint(20) NOT NULL,
   Is_Processed  tinyint(4) NOT NULL DEFAULT -1,
   Offer_Code  varchar(128) NOT NULL DEFAULT '',
   Validity_Period  int(11) NOT NULL DEFAULT -1,
   Offer_Score  decimal(10,3) NOT NULL DEFAULT 0.000,
   Offer_Type  varchar(128) NOT NULL DEFAULT '',
   Offer_Desc  varchar(128) NOT NULL DEFAULT '',
   Offer_Name  varchar(128) NOT NULL DEFAULT '',
   Base_Item  varchar(128) NOT NULL DEFAULT '',
   Offer_Is_Core  varchar(128) NOT NULL DEFAULT '',
   StartDate  date DEFAULT NULL,
   EndDate  date DEFAULT NULL,
   Product_Id  bigint(20) DEFAULT NULL,
   Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),
   Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY ( Offer_Id ),
  KEY  Offer_Code  ( Offer_Code ),
  KEY  Offer_Type  ( Offer_Type ),
  KEY  Is_Processed  ( Is_Processed ),
  KEY  Modified_Date  ( Modified_Date ),
  KEY  Product_Id  ( Product_Id )
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

mysolus$$
