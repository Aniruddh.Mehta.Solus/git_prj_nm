DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Div_Key_Lookup ; CREATE TABLE    CDM_Div_Key_Lookup  (    Div_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Div_Key  varchar(128) NOT NULL DEFAULT '',    LOV_Id  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Div_Id ),   UNIQUE KEY  Div_Key  ( Div_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1
mysolus$$
