DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Bill_Details_Key_Lookup ; CREATE TABLE    CDM_Bill_Details_Key_Lookup  (    Bill_Details_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Bill_Details_Key  varchar(128) NOT NULL,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Bill_Details_Id ),   UNIQUE KEY  Bill_Details_Key  ( Bill_Details_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=1  DEFAULT CHARSET=latin1
mysolus$$
