DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Section_Key_Lookup ; CREATE TABLE    CDM_Section_Key_Lookup  (    Section_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Section_Key  varchar(128) NOT NULL DEFAULT '',    LOV_Id  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Section_Id ),   UNIQUE KEY  Section_Key  ( Section_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB DEFAULT CHARSET=latin1
mysolus$$
