DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Dep_Key_Lookup ; CREATE TABLE    CDM_Dep_Key_Lookup  (    Dep_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Dep_Key  varchar(128) NOT NULL DEFAULT '',    LOV_Id  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Dep_Id ),   UNIQUE KEY  Dep_Key  ( Dep_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=latin1
mysolus$$
