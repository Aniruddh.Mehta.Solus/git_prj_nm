DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Product_Ingredients ; CREATE TABLE    CDM_Product_Ingredients  (    Product_Id  bigint(20) NOT NULL,    Parent_Product_Id  bigint(20) NOT NULL,    Is_Processed  tinyint(4) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   UNIQUE KEY  Product_Id  ( Product_Id , Parent_Product_Id ),   KEY  Is_Processed  ( Is_Processed ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB DEFAULT CHARSET=latin1
mysolus$$
