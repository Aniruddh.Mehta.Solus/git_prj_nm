DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Membership_Key_Lookup ; CREATE TABLE    CDM_Membership_Key_Lookup  (    Membership_Rec_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Membership_Rec_Key  varchar(128) NOT NULL,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Membership_Rec_Id ),   UNIQUE KEY  Membership_Rec_Key  ( Membership_Rec_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB DEFAULT CHARSET=latin1
mysolus$$
