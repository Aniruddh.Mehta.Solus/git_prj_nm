DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Cat_Key_Lookup ; CREATE TABLE    CDM_Cat_Key_Lookup  (    Cat_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Cat_Key  varchar(128) NOT NULL DEFAULT '',    LOV_Id  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Cat_Id ),   UNIQUE KEY  Cat_Key  ( Cat_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=557 DEFAULT CHARSET=latin1
mysolus$$
