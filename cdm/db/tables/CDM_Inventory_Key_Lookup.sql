DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Inventory_Key_Lookup ; CREATE TABLE    CDM_Inventory_Key_Lookup  (    Inventory_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Inventory_Key  varchar(128) NOT NULL,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Inventory_Id ),   UNIQUE KEY  Inventory_Key  ( Inventory_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=553488 DEFAULT CHARSET=latin1
mysolus$$
