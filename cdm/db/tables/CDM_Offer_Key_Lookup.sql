DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Offer_Key_Lookup ; CREATE TABLE    CDM_Offer_Key_Lookup  (    Offer_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Offer_Key  varchar(128) NOT NULL,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Offer_Id ),   UNIQUE KEY  Offer_Key  ( Offer_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB DEFAULT CHARSET=latin1
mysolus$$
