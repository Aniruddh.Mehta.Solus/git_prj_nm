CREATE TABLE  CDM_Product_Pricefeed_Key_Lookup  (
   Product_Pricefeed_Id  bigint(20) NOT NULL AUTO_INCREMENT,
   Product_Pricefeed_Key  varchar(128) NOT NULL,
   Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),
   Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY ( Product_Pricefeed_Id ),
  UNIQUE KEY  Product_Pricefeed_Key  ( Product_Pricefeed_Key ),
  KEY  Modified_Date  ( Modified_Date )
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
