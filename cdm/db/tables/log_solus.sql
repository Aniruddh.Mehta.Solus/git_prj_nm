DELIMITER mysolus$$
	DROP TABLE IF EXISTS log_solus ; CREATE TABLE    log_solus  (    Id  bigint(20) NOT NULL AUTO_INCREMENT,    module  varchar(256) DEFAULT NULL,    rec_start  bigint(20) NOT NULL DEFAULT -1,    rec_end  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    msg  varchar(128) DEFAULT NULL,   PRIMARY KEY ( Id ),   KEY  idx_Created_Date  ( Created_Date ),   KEY  idx_module  ( module ),   KEY  idx_rec_end  ( rec_end ),   KEY  idx_rec_start  ( rec_start ) ) ENGINE=InnoDB AUTO_INCREMENT=1817426 DEFAULT CHARSET=latin1
mysolus$$
