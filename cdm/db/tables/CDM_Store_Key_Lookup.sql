DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Store_Key_Lookup ; CREATE TABLE    CDM_Store_Key_Lookup  (    Store_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Store_Key  varchar(128) NOT NULL,    LOV_Id  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Store_Id ),   UNIQUE KEY  Store_Key  ( Store_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=1636 DEFAULT CHARSET=latin1
mysolus$$
