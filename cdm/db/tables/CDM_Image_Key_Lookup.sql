DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Image_Key_Lookup ; CREATE TABLE    CDM_Image_Key_Lookup  (    Image_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Image_Key  varchar(128) NOT NULL DEFAULT '',    LOV_Id  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Image_Id ),   UNIQUE KEY  Image_Key  ( Image_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB DEFAULT CHARSET=latin1
mysolus$$
