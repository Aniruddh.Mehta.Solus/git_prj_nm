CREATE or Replace TABLE  CDM_Cart_Activity_Key_Lookup  (
   Cart_Activity_Id  bigint(20) NOT NULL AUTO_INCREMENT,
   Cart_Activity_Key  varchar(128) NOT NULL,
   Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),
   Modified_Date  datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY ( Cart_Activity_Id ),
  UNIQUE KEY  Cart_Activity_Key  ( Cart_Activity_Key ),
  KEY  Modified_Date  ( Modified_Date )
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

