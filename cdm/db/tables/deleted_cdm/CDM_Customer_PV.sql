DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Customer_PV ; CREATE TABLE   `CDM_Customer_PV` (   `Customer_Id` bigint(20) NOT NULL,   `Is_Processed` tinyint(4) NOT NULL DEFAULT -1,   `Created_Date` timestamp NOT NULL DEFAULT current_timestamp(),   `Modified_Date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY (`Customer_Id`),   KEY `Is_Processed` (`Is_Processed`),   KEY `Modified_Date` (`Modified_Date`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1
mysolus$$
