DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Customer_Segment_Definition ; CREATE TABLE   `CDM_Customer_Segment_Definition` (   `Segment_Type` varchar(64) DEFAULT NULL,   `Segment_Rule` varchar(256) DEFAULT NULL,   `Created_Date` timestamp NOT NULL DEFAULT current_timestamp(),   `Modified_Date` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),   `Segment_Id` smallint(4) DEFAULT NULL,   `Segment_Rule_Sequence` smallint(4) DEFAULT NULL,   `is_active` char(1) DEFAULT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1
mysolus$$
