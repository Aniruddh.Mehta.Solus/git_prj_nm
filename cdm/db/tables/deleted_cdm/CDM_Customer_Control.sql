DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Customer_Control ; CREATE TABLE   `CDM_Customer_Control` (   `Customer_Id` bigint(20) NOT NULL AUTO_INCREMENT,   `Customer_Key` varchar(128) NOT NULL,   `Created_Date` timestamp NOT NULL DEFAULT current_timestamp(),   `Modified_Date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY (`Customer_Id`),   UNIQUE KEY `Customer_Key` (`Customer_Key`),   KEY `Modified_Date` (`Modified_Date`) ) ENGINE=InnoDB AUTO_INCREMENT=6852502 DEFAULT CHARSET=latin1
mysolus$$
