DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Non_Sales_Bill_Header_Key_Lookup ; CREATE TABLE    CDM_Non_Sales_Bill_Header_Key_Lookup  (    Bill_Header_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Bill_Header_Key  varchar(128) NOT NULL,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Bill_Header_Id ),   UNIQUE KEY  Bill_Header_Key  ( Bill_Header_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=966209 DEFAULT CHARSET=latin1
mysolus$$
