drop table if exists SDL_Create_Incremental_Data;
create  table SDL_Create_Incremental_Data(
Bill_Date date default null ,
Bill_Count bigint default null,
Created_Bills  bigint default null,
Day_Diff  bigint default null
)
Engine=Innodb Default charset =latin1;
