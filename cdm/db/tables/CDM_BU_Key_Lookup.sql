DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_BU_Key_Lookup ; CREATE TABLE    CDM_BU_Key_Lookup  (    BU_Id  bigint(20) NOT NULL AUTO_INCREMENT,    BU_Key  varchar(128) NOT NULL DEFAULT '',    LOV_Id  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( BU_Id ),   UNIQUE KEY  BU_Key  ( BU_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=latin1
mysolus$$
