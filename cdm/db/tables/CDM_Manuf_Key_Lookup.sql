DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Manuf_Key_Lookup ; CREATE TABLE    CDM_Manuf_Key_Lookup  (    Manuf_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Manuf_Key  varchar(128) NOT NULL DEFAULT '',    LOV_Id  bigint(20) NOT NULL DEFAULT -1,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Manuf_Id ),   UNIQUE KEY  Manuf_Key  ( Manuf_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB DEFAULT CHARSET=latin1
mysolus$$
