DELIMITER mysolus$$
	DROP TABLE IF EXISTS CDM_Broker_Key_Lookup ; CREATE TABLE    CDM_Broker_Key_Lookup  (    Broker_Id  bigint(20) NOT NULL AUTO_INCREMENT,    Broker_Key  varchar(128) NOT NULL,    Created_Date  timestamp NOT NULL DEFAULT current_timestamp(),    Modified_Date  datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),   PRIMARY KEY ( Broker_Id ),   UNIQUE KEY  Broker_Key  ( Broker_Key ),   KEY  Modified_Date  ( Modified_Date ) ) ENGINE=InnoDB DEFAULT CHARSET=latin1
mysolus$$
