DELIMITER mysolus$$
CREATE OR REPLACE TABLE Curated_Product_Master (
  Product_Key varchar(128) NOT NULL,
  Product_Attr_1 varchar(128)  DEFAULT NULL,
  Product_Attr_2 varchar(128)  DEFAULT NULL,
  Product_Attr_3 varchar(128)  DEFAULT NULL,
  Product_Attr_4 varchar(128)  DEFAULT NULL,
  Product_Attr_5 varchar(128)  DEFAULT NULL,
  Product_Attr_6 varchar(128)  DEFAULT NULL,
  Product_Attr_7 varchar(128)  DEFAULT NULL,
  Product_Attr_8 varchar(128)  DEFAULT NULL,
  Product_Attr_9 varchar(128)  DEFAULT NULL,
  Product_Attr_10 varchar(128)  DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE Curated_Product_Master  ADD INDEX (Product_Key);



mysolus$$
