. $HOME/.solusconfig
export BILL_DETAILS=$HOME/client/inbound/sample_data/Bill_Details.csv
echo " No of Bills Before Processing : "`wc -l $BILL_DETAILS`
## Remove Old History Data
#year=2016
#month=06
#for i in 01 02 03 04 05 06 07 08 09 10 11 12
#do
#	if [ $i == $month ]
#	then
#	 	break 
#	fi
#	date_pre=$year-$i
#	echo $date_pre 
#        grep -v ${date_pre} $BILL_DETAILS > $$.temp
#        mv $$.temp $BILL_DETAILS
#done
#year=2019
#for i in 06 07 08
#do
#	date_pre=$year-$i
#	date_post=2021-$i
#	echo $date_pre $date_post
#	sed 's/'${date_pre}'/'${date_post}'/g' < $BILL_DETAILS > $$.temp
#        mv $$.temp $BILL_DETAILS 
#done

#year=2018
#for i in 01 02 03 04 05 06 07 08
#do
#	date_pre=$year-$i
#	date_post=2019-$i
#	echo $date_pre $date_post
#	sed 's/'${date_pre}'/'${date_post}'/g' < $BILL_DETAILS > $$.temp
#        mv $$.temp $BILL_DETAILS 
#done
#year=2017
#for i in 01 02 03 04 05 06 07 08 09 10 11 12
#do
#	date_pre=$year-$i
#	date_post=2020-$i
#	echo $date_pre $date_post
#	sed 's/'${date_pre}'/'${date_post}'/g' < $BILL_DETAILS > $$.temp
#        mv $$.temp $BILL_DETAILS 
#done
## Daily Run
year=`date +%Y`
day=`date +%d`
month=`date +%m`
day7=`expr $day - 7`
for i in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 
do
	if [ $i == $day7 ]
	then
	 	break 
	fi
	date_pre=2018-$month-$i
	date_post=$year-$month-$i
	echo $date_pre $date_post
	sed 's/'${date_pre}'/'${date_post}'/g' < $BILL_DETAILS > $$.temp
        mv $$.temp $BILL_DETAILS 
done

rm -f *.temp
echo "Here are The Bill Dates"
cut -d"|" -f8 $BILL_DETAILS | sort -u
echo "No of days in 2021"
cut -d"|" -f8 $BILL_DETAILS | grep 2021 | sort -u | wc -l
echo "No of days in 2020"
cut -d"|" -f8 $BILL_DETAILS | grep 2020 | sort -u | wc -l
echo "No of days in 2019"
cut -d"|" -f8 $BILL_DETAILS | grep 2019 | sort -u | wc -l
echo "No of days in 2018"
cut -d"|" -f8 $BILL_DETAILS | grep 2018 | sort -u | wc -l
echo "No of days in 2017"
cut -d"|" -f8 $BILL_DETAILS | grep 2017 | sort -u | wc -l
echo "No of days in 2016"
cut -d"|" -f8 $BILL_DETAILS | grep 2016 | sort -u | wc -l
echo " No of Bills After Processing : "`wc -l $BILL_DETAILS`

exit 0
