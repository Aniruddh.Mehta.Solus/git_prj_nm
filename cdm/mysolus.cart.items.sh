. $HOME/.solusconfig

echo "Populating Num_Of_Cart_Items column in CDM_Customer_Time_Dependent_Var Table"

mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF


call S_Update_Cart_Items(100000);

EOF
