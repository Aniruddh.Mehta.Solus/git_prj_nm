. $HOME/.solusconfig


mysql --host=$HOST --user=${USER_NAME} --password=${PASSWORD}  --database=$DB_NAME   --verbose << EOF
 TRUNCATE CDM_Points_Earned;
 CALL CDM_ETL_Point_Earned_Generator_Run();
EOF
