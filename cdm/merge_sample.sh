CDM_ENTITY=$1
MAPPING_FILE=$2
#UPDATE_COLUMN=$3
CDM_CORE=CDM_${CDM_ENTITY}
CSV_TABLE=csv_${CDM_ENTITY}
SOURCE_DELIM="|"
MAPPING_DELIM="="
DEBUG=1

USER_NAME='root'
PASSWORD='mysql123'
HOST='localhost'
DB_NAME='test_base'


echo "CDM_CORE="$CDM_CORE"|CSV_TABLE="$CSV_TABLE"|MAPPING_FILE="$MAPPING_FILE


if [[ "$1" == "--help" || "$1" == "-h" || -z "$1" || -z "$2" ]]
then
echo "-- Gettig Started ----"
echo "Usage: $0 <CDM_ENTITY> <MAPPING_FILE> <MODE>"
echo "Valid Values CDM_ENTITY : Product_Master, Customer_Master, Store_Master, Bill_Details, Inventory_Master "
echo "Mapping_File must follow the mapping template provided at $HOME/client/config/mapping " 
echo "Data File location should be provided in Mapping File. Please check Mapping Templates" 
echo "Data File DELIMITER is |. You must preprocess the delimiter accordingly"  
    if [[ -z "$2" ]]
    then
    echo "Please Provide Mapping argumnent "
    fi
echo "---------------------"
exit 1
fi
#####################################################################################################################
########### Recieve Mode
#####################################################################################################################



echo "" 
echo "MAPPING FILE     :"$MAPPING_FILE
if [ ! -f ${MAPPING_FILE} ] 
then
exit "ERROR-mysolus-sdl001 : Mapping File ${MAPPING_FILE} doesnt exists "
fi

####### Checking source file validity
export SOURCEFILE=`grep ^SOURCEFILE ${MAPPING_FILE} | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
echo "SOURCE FILE        :"$SOURCEFILE
if [ ! -f ${SOURCEFILE} ] 
then
echo "ERROR-mysolus-sdl002 : Source File ${SOURCEFILE} doesnt exist as mentioned in mapping file :${MAPPING_FILE}"
exit 1
fi


dos2unix  ${SOURCEFILE}
sed 's/\\N//Ig' ${SOURCEFILE} | sed 's/"//g' | sed 's/|/"|"/Ig'  | sed 's/${y}/"${y}"/Ig'  | sed 's/^/"/Ig' | sed 's/$/"/Ig'|sed 's/"//g' | sed 's/\\r\\n/\\n/g' > ${SOURCEFILE}.processing 
export SOURCEFILE=${SOURCEFILE}.processing 


#####################
## Mapping Validation
#####################
declare -A columns

for i in `cat ${MAPPING_FILE} | grep -v ^#  | grep -v ^SOURCEFILE `  
do 
echo $i
sdl_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $1}' | sed 's/ *$//g' | sed 's/^ *//g'`
csv_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
if [[ -n "$csv_colname" ]]
then
echo "Both Values Present"
case "$sdl_colname" in 
    Update_Column* )
        columns["$sdl_colname"]="$csv_colname"
        ;;
    Primary_Key* )
        columns["$sdl_colname"]="$csv_colname"
        ;;
    * )
        echo "NA"
        ;;
esac

fi

if [[ ${i:0:1} == "#" ]]
then
 echo $sdl_colname
 echo $csv_colname
fi

done


####### CHeck the Column Names are from the same CDM Entity
echo "Checking the Column Names are from the same CDM Entity......."
for key in "${!columns[@]}"; do
    value="${columns["$key"]}"
    echo $key":"$value
    if [[ $CDM_ENTITY == 'Customer_Master'  ]]
    then  
    Update_Table='CDM_Customer_Master'
    Lookup_Table='CDM_Customer_Key_Lookup'
    query="select ${value} from ${Update_Table}"
    result=$(mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -N -B -e "$query" )
    if [ -z "$result" ]; then
        echo "Column $value does not exist in table_name"
        echo "ERROR-mysolus-sdl003 : Source File ${value} doesnt exist in CDM table :${Update_Table} as mentioned in the ${MAPPING_FILE}"
        exit 1
    else
        echo "Column $value exist in CDM table :${Update_Table}"
    fi
    fi
done