. $HOME/.solusconfig
CDM_ENTITY="CDM_ENGAGEMENT"
MAPPING_FILE=$1
CDM_TABLE="CDM_Stg_ENGAGEMENT"
CDM_CORE="CDM_ENGAGEMENT"
CSV_TABLE="csv_ENGAGEMENT"
SOURCE_DELIM="|"
MAPPING_DELIM="="
DEBUG=1



if [[ "$2" == "historical"  ]]
then
MODE=0
elif [[  "$2" == "incremental" ]]
then
MODE=1
else 
echo "Invalid Data Load Mode "
exit 1 
fi
echo "" 
echo "MAPPING FILE     :"$MAPPING_FILE
if [ ! -f ${MAPPING_FILE} ] 
then
exit "ERROR-mysolus-sdl001 : Mapping File ${MAPPING_FILE} doesnt exists "
fi


export SOURCEFILE=`grep ^SOURCEFILE ${MAPPING_FILE} | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
echo "SOURCE FILE        :"$SOURCEFILE
if [ ! -f ${SOURCEFILE} ] 
then
echo "ERROR-mysolus-sdl002 : Source File ${SOURCEFILE} doesnt exist as mentioned in mapping file :${MAPPING_FILE}"
exit 1
fi

echo "CDM Table         :"$CDM_ENTITY
echo ""
status=`mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -s -e "select 1 from ${CDM_TABLE} limit 1" `
if [ $status -ne 1 ]
then
exit "ERROR-mysolus-sdl003 : Source Table ${CDM_TABLE} doesnt exists "
fi

###################
# Mapping Validation
###################
echo "Mapping Validation starts ...."
for i in `cat ${MAPPING_FILE} | grep -v ^#  | grep -v ^SOURCEFILE `  
do 
echo $i
sdl_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $1}' | sed 's/ *$//g' | sed 's/^ *//g'`
csv_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
if [[ ${i:0:1} == "#" ]]
then
 continue
fi
if [[ ${i:0:1} == "*" ]]
then 
  if [[ -z "${csv_colname}" ]]
  then 
     echo "ERROR-mysolus-sdl008 : Mapping is Incorrect. Mandatory attribute $sdl_colname is not mapped to any attribute"
  fi
  sdl_colname=`echo $sdl_colname | awk -F"*" '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
fi

if [[ ! -z "${sdl_colname}" ]]
then 
  sqlstmt="select 1 from Information_Schema.columns where table_schema='$DB_NAME' and table_name='${CDM_TABLE}' and column_name='${sdl_colname}' limit 1;"

  status=`mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -s -e "${sqlstmt}" `
  if [[ $status -ne 1 ]]
  then
     exit "ERROR-mysolus-sdl0011 : Mapping is Incorrect. Column Name ${sdl_colname} doesnt exist in CDM TABLE ${CDM_TABLE} "
  fi
fi
done
echo "MAPPING VALIDATION ....OK !!!!"

##
## CREATE CSV TABLE
##
dos2unix  ${SOURCEFILE} 
sed 's/\\N//Ig' ${SOURCEFILE} | sed 's/"//g' | sed 's/|/"|"/Ig' | sed 's/^/"/Ig' | sed 's/$/"/Ig' | sed 's/"//g' > ${SOURCEFILE}.processing 
export SOURCEFILE=${SOURCEFILE}.processing 

echo "Creating Table ${CSV_TABLE} from file :  $SOURCEFILE"
echo "No of Records                         :  "`wc -l $SOURCEFILE`

rm -f list_of_columns
sqlcreate=""
head -1 ${SOURCEFILE} | sed 's/"//g' | cut -f1- -d\| --output-delimiter=$'\n' > list_of_columns
cp list_of_columns list_of_columns_all
sqlcreate=`head -1 list_of_columns`
sqlcreate="$sqlcreate varchar(512) "
sed -i '1d' list_of_columns

for columns in `cat list_of_columns`
do
sqlcreate="$sqlcreate"" , $columns varchar(512) "
done
#sqlcreate="create table $CSV_TABLE ( Id bigint(20) NOT NULL AUTO_INCREMENT KEY,   Created_Date timestamp NOT NULL DEFAULT current_timestamp(),""$sqlcreate""); "
sqlcreate="create table $CSV_TABLE ( ""$sqlcreate""); "

mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
drop table if exists ${CSV_TABLE};
${sqlcreate};
-- alter table ${CSV_TABLE} add column created_date timestamp DEFAULT current_timestamp();
EOF


##
## LOADING CSV TABLE
##
#echo "Loading data to Table ${CSV_TABLE} from file  ${SOURCEFILE} "
mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
LOAD DATA LOCAL  INFILE "${SOURCEFILE}" INTO TABLE ${CSV_TABLE}
FIELDS TERMINATED BY '|' ENCLOSED BY '"' LINES TERMINATED BY '\n'
IGNORE 1 lines``;
EOF

##
## Generating SQL to insert data into CDM Staging Table
##
insertstmt="insert into ${CDM_TABLE} ( " ;
selectstmt=" select  " ;

ifirst=0

for i in `cat ${MAPPING_FILE} | grep -v "^#"  | grep -v ^SOURCEFILE`; 
do 
sdl_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $1}' | sed 's/ *$//g' | sed 's/^ *//g'`
csv_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
if [[ ${i:0:1} == "#" ]]
then
 continue
fi

if [[ ${sdl_colname:0:1} == "*" ]]
then 
  sdl_colname=`echo $sdl_colname | awk -F"*" '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
fi

if [ ! -z "${csv_colname}" ] 
then
if [ $ifirst == 0 ]
then
insertstmt="$insertstmt""${sdl_colname}"
selectstmt="$selectstmt""${csv_colname}"
ifirst=1
else
insertstmt="$insertstmt"",${sdl_colname}"
selectstmt="$selectstmt"",${csv_colname}"
fi
fi
done
sqlstatement="${insertstmt} ) "" ${selectstmt} "" FROM ${CSV_TABLE}"

##
## LOADING TO CDM STAGING
##
mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
truncate ${CDM_TABLE};
-- call solus_loop(${sqlstatement},${CSV_TABLE}),100000;
${sqlstatement};
EOF


##
## ADDING INDEX TO ALL COLUMNS
##
##echo "Adding Index to ${CSV_TABLE}"
##for columns in `cat list_of_columns_all`
##do
##sqlindex="alter table $CSV_TABLE add index $columns ($columns)";
##mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -e "$sqlindex" 
##done


###
### ADD INDEX TO CSV TABLE
###
#for i in `cat ${MAPPING_FILE} | grep -v "^#"  | grep -v ^SOURCEFILE`; 
#do 
#   sdl_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $1}' | sed 's/ *$//g' | sed 's/^ *//g'`
#   csv_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
#   if [[ ${i:0:1} == "#" ]]
#   then
#      continue
#   fi
#   if [ ! -z "${csv_colname}" ] 
#   then
#      if [[ ${sdl_colname:0:1} == "*" ]]
#      then 
#        sdl_colname=`echo $sdl_colname | awk -F"*" '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
#      fi
#      sqlstmt="select 1 from Information_Schema.columns where table_schema='$DB_NAME' and table_name='${CDM_TABLE}' and column_name='${csv_colname}' and Column_Key <> '' ;"
#      status=`mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -s -e "${sqlstmt}" `
#      if [[ $status -eq "1" ]]
#      then
#         sqlindex="alter table $CSV_TABLE add index ${csv_colname} (${csv_colname})";
#         echo $sqlindex
#         mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -e "$sqlindex" 
#      fi
#   fi
#done

##
##  DATA QUALITY CHECK
##


###
### LOAD CDM 

ETL_PROC="CDM_ETL_ENGAGEMENT(100000,$MODE)"


mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
call ${ETL_PROC};
select count(1) from ${CDM_CORE};
EOF

rm -f list_of_columns_all 
rm -f list_of_columns 
