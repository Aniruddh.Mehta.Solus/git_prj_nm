. $HOME/.solusconfig
CDM_ENTITY=$1
MAPPING_FILE=$2
CDM_CORE=CDM_${CDM_ENTITY}
CSV_TABLE=csv_${CDM_ENTITY}
SOURCE_DELIM="|"
MAPPING_DELIM="="
DEBUG=1


echo "CDM_CORE="$CDM_CORE"|CSV_TABLE="$CSV_TABLE"|MAPPING_FILE="$MAPPING_FILE


if [[ "$1" == "--help" || "$1" == "-h" || -z "$1" || -z "$2" ]]
then
echo "-- Gettig Started ----"
echo "Usage: $0 <CDM_ENTITY> <MAPPING_FILE> <MODE>"
echo "Valid Values CDM_ENTITY : Product_Master, Customer_Master, Store_Master, Bill_Details, Inventory_Master "
echo "Mapping_File must follow the mapping template provided at $HOME/client/config/mapping " 
echo "Data File location should be provided in Mapping File. Please check Mapping Templates" 
echo "Data File DELIMITER is |. You must preprocess the delimiter accordingly"  
    if [[ -z "$2" ]]
    then
    echo "Please Provide Mapping argumnent "
    fi
echo "---------------------"
exit 1
fi
#####################################################################################################################
########### Recieve Mode
#####################################################################################################################



echo "" 
echo "MAPPING FILE     :"$MAPPING_FILE
if [ ! -f ${MAPPING_FILE} ] 
then
exit "ERROR-mysolus-sdl001 : Mapping File ${MAPPING_FILE} doesnt exists "
fi

####### Checking source file validity
export SOURCEFILE=`grep ^SOURCEFILE ${MAPPING_FILE} | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
echo "SOURCE FILE        :"$SOURCEFILE
if [ ! -f ${SOURCEFILE} ] 
then
echo "ERROR-mysolus-sdl002 : Source File ${SOURCEFILE} doesnt exist as mentioned in mapping file :${MAPPING_FILE}"
exit 1
fi


dos2unix  ${SOURCEFILE}
sed 's/\\N//Ig' ${SOURCEFILE} | sed 's/"//g' | sed 's/|/"|"/Ig'  | sed 's/${y}/"${y}"/Ig'  | sed 's/^/"/Ig' | sed 's/$/"/Ig'|sed 's/"//g' | sed 's/\\r\\n/\\n/g' > ${SOURCEFILE}.processing 
export SOURCEFILE=${SOURCEFILE}.processing 


#####################
## Mapping Validation
#####################
declare -A columns

for i in `cat ${MAPPING_FILE} | grep -v ^#  | grep -v ^SOURCEFILE `  
do 
echo $i
sdl_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $1}' | sed 's/ *$//g' | sed 's/^ *//g'`
csv_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
if [[ -n "$csv_colname" ]]
then
case "$sdl_colname" in 
    Update_Column* )
        columns["$sdl_colname"]="$csv_colname"
        ;;
    Primary_Key* )
        columns["$sdl_colname"]="$csv_colname"
        ;;
    * )
        echo "NA"
        ;;
esac

fi

if [[ ${i:0:1} == "#" ]]
then
 echo $sdl_colname
 echo $csv_colname
fi

done


####### CHeck the Column Names are from the same CDM Entity
echo "Checking the Column Names are from the same CDM Entity......."
for key in "${!columns[@]}"; do
    value="${columns["$key"]}"
    echo $key":"$value
    if [[ $CDM_ENTITY == 'Customer_Master'  ]]
    then  
    Update_Table='CDM_Customer_Master'
    Lookup_Table='CDM_Customer_Key_Lookup'
    query="select ${value} from ${Update_Table}"
    result=$(mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -N -B -e "$query" )
    if [ -z "$result" ]; then
        echo "Column $value does not exist in table_name"
        echo "ERROR-mysolus-sdl003 : Source File ${value} doesnt exist in CDM table :${Update_Table} as mentioned in the ${MAPPING_FILE}"
        exit 1
    else
        echo "Column $value exist in CDM table :${Update_Table}"
    fi
    fi
done


######## Create update Columnn for running the Update Algo
# For Each Column in ${columns} create a update column and then the final update column 
# Each Update Column should have the same data type as the CDM Table 
# If an Primary_Id/ID/LOV_Id column then definetely bigint , else check the data type 
#$(echo "${!xeta[@]}" | tr ' ' '\n' | sort)
#for key in "${!columns[@]}";

sqlcreate="create or replace table Merge_Table  ( "
for key in $( echo "${!columns[@]}" |  tr ' ' '\n' | sort  ) ; do
    value="${columns["$key"]}"
    case "${value}" in 
	*_LOV_Id )
		echo "LOV_ID Column........."${key}":"${value}
		sqlcreate="${sqlcreate}  "${key}" varchar(512) default null , "${value}"  bigint(20) default null,"
		;;
	*_Id )
		echo "CDM_Lookup_Table Column.........."${key}":"${value}
		sqlcreate="${sqlcreate}  "${key}" varchar(512) default null , "${value}"  bigint(20) default null ,"
		;;
	* )
		echo "Need Additional Lookup............"${key}":"${value}
		sqlcreate="${sqlcreate}  "${key}" varchar(512) default null , "${value}"  varchar(512) default null ,"
		;;
    esac
done


sqlcreate="${sqlcreate} )  Engine=InnoDb Default charset=Latin1  "
sqlcreate=$(echo $sqlcreate |sed 's/, ) Engine=InnoDb Default charset=Latin1/ ) Engine=InnoDb Default charset=Latin1/')

echo ${sqlcreate}


## Create Merge Table
mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF

${sqlcreate}

EOF

######################
num=1
z="|"
meta="|"
for key in $( echo "${!columns[@]}" |  tr ' ' '\n' | sort  ) ; do
	#num=1
	y="@col"
	z="${z}, ${y}${num}  "
	meta="${meta}, ${key}= ${y}${num} "
	num=$((num+1))
done

z=$(echo $z | sed 's/|,//' )
meta=$(echo $meta | sed 's/|,//' )

echo $z
echo $meta

#### Insert Values from the given csv 

wc -l ${SOURCEFILE};
mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose << EOF
set sql_safe_updates=0;
LOAD DATA LOCAL  INFILE "${SOURCEFILE}" INTO TABLE Merge_Table
FIELDS TERMINATED BY '|'  OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n'
IGNORE 1 lines
( ${z}  )
SET 
${meta}

;
select count(1) from Merge_Table ;
EOF

##################################################
#### Iterate Through Each Pair in Columns and Update them respectively in the Merge Column

for key in $( echo "${!columns[@]}" |  tr ' ' '\n' | sort  ) ; do
	value="${columns["$key"]}"
	case "$value" in
		*LOV_Id )
			echo "Contains LOV Id"
			Update_Key=${value//'_LOV_Id'/''}
			insert_stmt="
			INSERT IGNORE INTO CDM_LOV_Master
			(  LOV_Attribute, LOV_Key, LOV_Value, LOV_Comm_Value )
			SELECT  '${Update_Key^^}', ${key} ,  ${key},  ${key}  FROM  Merge_Table  WHERE 
        		${key} <> ''  GROUP BY ${key} ;
			"
			echo $insert_stmt
			mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
			${insert_stmt}
EOF
			update_stmt="
			UPDATE IGNORE  Merge_Table AS A ,CDM_LOV_Master AS B 
			SET A.${value} = B.LOV_Id
			WHERE  A.${key} = B.LOV_Key AND B.LOV_Attribute = '${Update_Key^^}';
			"
			echo $update_stmt
			mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
			${update_stmt}
EOF
			;;
		*_Id )
                        echo "Contains  Id"
			#primary key= primary key from Customer_Master|Store_Master|Bill_Details|Bill_Header|Product_Master
			#update key= primary key from Customer_Master|Store_Master|Bill_Details|Bill_Header|Product_Master
			if  [[ $value == *"Customer_Id"*|| $value == *"Store_Id"* || $value == *"Bill_Details_Id"* || $value == *"Bill_Header_Id"* || $value == *"Product_Id"*  ]]
				then
					if  [[ $value == *"Customer_Id"* ]]
						then
							insert_stmt="
							INSERT IGNORE INTO CDM_Customer_Key_Lookup (Customer_Key)
							SELECT  ${key}  FROM Merge_Table WHERE  ${key}  <> '' GROUP BY ${key} ;
							"
							echo $insert_stmt
							mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
							$insert_stmt
EOF
							update_stmt="
							UPDATE IGNORE  Merge_Table AS A ,CDM_Customer_Key_Lookup AS B
							SET A.${value} = B.Customer_Id WHERE  A.${key} = B.Customer_Key
							"
							echo $update_stmt
							mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
							$update_stmt
EOF
					elif  [[ $value == *"Store_Id"* ]]
						then
							insert_stmt="
							INSERT IGNORE INTO CDM_Store_Key_Lookup (Store_Key)
							SELECT  ${key}  FROM Merge_Table WHERE  ${key}  <> '' GROUP BY ${key} ;
							"
							echo $insert_stmt
							mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
							$insert_stmt
EOF
							update_stmt="
							UPDATE IGNORE  Merge_Table AS A ,CDM_Store_Key_Lookup AS B
							SET A.${value} = B.Store_Id WHERE  A.${key} = B.Store_Key
							"
							echo $update_stmt
							mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
							$update_stmt
EOF
					elif  [[ $value == *"Product_Id"* ]]
						then
							insert_stmt="
							INSERT IGNORE INTO CDM_Product_Key_Lookup (Product_Key)
							SELECT  ${key}  FROM Merge_Table WHERE  ${key}  <> '' GROUP BY ${key} ;
							"
							echo $insert_stmt
							mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
							$insert_stmt
EOF
							update_stmt="
							UPDATE IGNORE  Merge_Table AS A ,CDM_Product_Key_Lookup AS B
							SET A.${value} = B.Product_Id WHERE  A.${key} = B.Product_Key
							"
							echo $update_stmt
							mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
							$update_stmt
EOF
					elif  [[ $value == *"Bill_Details_Id"* ]]
						then
							insert_stmt="
							INSERT IGNORE INTO CDM_Bill_Details_Key_Lookup (CDM_Bill_Details_Key)
							SELECT  ${key}  FROM Merge_Table WHERE  ${key}  <> '' GROUP BY ${key} ;
							"
							echo $insert_stmt
							mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
							$insert_stmt
EOF
							update_stmt="
							UPDATE IGNORE  Merge_Table AS A ,CDM_Bill_Details_Key_Lookup AS B
							SET A.${value} = B.Bill_Details_Id WHERE  A.${key} = B.Bill_Details_Key
							"
							echo $update_stmt
							mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
							$update_stmt
EOF
					elif  [[ $value == *"Bill_Header_Id"* ]]
						then
							insert_stmt="
							INSERT IGNORE INTO CDM_Bill_Header_Key_Lookup (CDM_Bill_Header_Key)
							SELECT  ${key}  FROM Merge_Table WHERE  ${key}  <> '' GROUP BY ${key} ;
							"
							echo $insert_stmt
							mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
							$insert_stmt
EOF
							update_stmt="
							UPDATE IGNORE  Merge_Table AS A ,CDM_Bill_Header_Key_Lookup AS B
							SET A.${value} = B.Bill_Header_Id WHERE  A.${key} = B.Bill_Header_Key
							"
							echo $update_stmt
							mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
							$update_stmt
EOF
					else
						echo "Wrong Id Key"
						exit 1
					fi

			fi
                        ;;
		* )
                        echo "Contains Other Keys"
                        update_stmt="
                        UPDATE IGNORE  Merge_Table 
                        SET ${value} = ${key} where  ${key}<>'';
                        "
                        echo $update_stmt
                        mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
                        $update_stmt
EOF
                        ;;
	esac
done


# Update the Main CDM Table based on all Columns Primary Key 

primary_column="${columns["Primary_Key"]}"
update_stmt="
set SQL_SAFE_UPDATES=0;
update ${CDM_CORE} A, Merge_Table B
set 
"
for key in $( echo "${!columns[@]}" |  tr ' ' '\n' | sort  ) ; do
        value="${columns["$key"]}"
	if [[ "${value}" != "${primary_column}" ]]
	then
		#echo "A.${value}=B.${value} "
		update_stmt="${update_stmt}  A.${value}=B.${value} ,"
	fi
done
update_stmt="${update_stmt}|"
update_stmt=$(echo $update_stmt | sed 's/,|//' )
update_stmt="${update_stmt}  where A.${primary_column}=B.${primary_column} "
echo ${update_stmt}


mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME --verbose <<EOF
$update_stmt
EOF


