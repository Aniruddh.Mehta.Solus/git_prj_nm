. $HOME/.solusconfig

MAPPING_FILE=$1
MAPPING_DELIM='='
# Read the columns 

echo "MAPPING FILE     :"$MAPPING_FILE
if [[ ! -f ${MAPPING_FILE} ]]
then
exit "ERROR-mysolus-sdl001 : Mapping File ${MAPPING_FILE} doesnt exists "
fi
dos2unix $MAPPING_FILE


rm -f list_of_input_columns
rm -f list_of_output_columns

echo "Mapping Validation starts ...."
for i in `cat ${MAPPING_FILE} | grep -v ^#  `
do
echo $i
sdl_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $1}' | sed 's/ *$//g' | sed 's/^*//g'`
csv_colname=`echo $i | awk -F${MAPPING_DELIM} '{print $2}' | sed 's/ *$//g' | sed 's/^*//g'`
echo $csv_colname >> list_of_input_columns
echo $sdl_colname >> list_of_output_columns
if [[ ${i:0:1} == "#" ]]
then
 continue
fi
if [[ ${i:0:1} == "*" ]]
then
  if [[ -z "${csv_colname}" ]]
  then
     echo "ERROR-mysolus-sdl008 : Mapping is Incorrect. Mandatory attribute $sdl_colname is not mapped to any attribute"
  fi
  sdl_colname=`echo $sdl_colname | awk -F"*" '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
fi
done
echo "MAPPING VALIDATION ....OK !!!!"


#if [[ ${i:0:1} == "*" ]]
#then
#  if [[ -z "${csv_colname}" ]]
#  then
#     echo "ERROR-mysolus-sdl008 : Mapping is Incorrect. Mandatory attribute $sdl_colname is not mapped to any attribute"
#  fi
#  sdl_colname=`echo $sdl_colname | awk -F"*" '{print $2}' | sed 's/ *$//g' | sed 's/^ *//g'`
#fi

x=1
for i in {Primary_Table_Name,Join_Column,Revenue_Center,Secondary_Table_Name,Secondary_Join_Column}
do 
	echo $i
	eval " ${i}=`head -"$x" list_of_input_columns | tail -1`"
	#$i=`head -"$x" list_of_input_columns | tail -1 `
	x=$((x+1))
done

# Check if Join_Column is part of Primary_Table_Name
if [[ ! -z "${Join_Column}" ]]
then
  sqlstmt="select 1 from Information_Schema.columns where table_schema='$DB_NAME' and table_name='${Primary_Table_Name}' and column_name='${Join_Column}' limit 1;"
  status=`mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -s -e "${sqlstmt}" `
  if [[ $status -ne 1 ]]
  then
     exit "ERROR-mysolus-sdl0011 : Mapping is Incorrect. Column Name ${Join_Column} doesnt exist in CDM TABLE ${Primary_Table_Name} "
  fi
fi
echo "Primary Table ${Primary_Table_Name}  Validation  ....OK !!!!"

# Check if _Secondary_Join_Column is part of Secondary_Table_Name 
if [[ ! -z "${Secondary_Join_Column}" ]]
then
  sqlstmt="select 1 from Information_Schema.columns where table_schema='$DB_NAME' and table_name='${Secondary_Table_Name}' and column_name='${Secondary_Join_Column}' limit 1;"
  status=`mysql  --user=$USER_NAME --password=$PASSWORD  --host=$HOST --database=$DB_NAME -s -e "${sqlstmt}" `
  if [[ $status -ne 1 ]]
  then
     exit "ERROR-mysolus-sdl0011 : Mapping is Incorrect. Column Name ${Secondary_Join_Column} doesnt exist in CDM TABLE ${Secondary_Table_Name} "
  fi
fi
echo "Primary Table ${Secondary_Table_Name}  Validation  ....OK !!!!"





l="
CREATE OR REPLACE VIEW CDM_Revenue_Center 
    (SELECT 
        a.Customer_Id AS Customer_Id,
        a.${Join_Column} AS ${Join_Column},
        CASE
            WHEN b.$Revenue_Center = '' THEN 'NA'
            ELSE IFNULL(b.$Revenue_Center , 'NA')
        END AS Revenue_Center
    FROM
        ($Primary_Table_Name a
        LEFT JOIN ${Secondary_Table_Name} b ON (a.${Join_Column} = b.${Secondary_Join_Column} )))
"

echo $l
